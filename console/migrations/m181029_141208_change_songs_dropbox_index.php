<?php

use yii\db\Migration;

/**
 * Class m181029_141208_change_songs_dropbox_index
 */
class m181029_141208_change_songs_dropbox_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('dropbox_id', 'songs');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181029_141208_change_songs_dropbox_index cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181029_141208_change_songs_dropbox_index cannot be reverted.\n";

        return false;
    }
    */
}
