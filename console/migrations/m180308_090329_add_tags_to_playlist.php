<?php

use yii\db\Migration;

/**
 * Class m180308_090329_add_tags_to_playlist
 */
class m180308_090329_add_tags_to_playlist extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('youtube_playlists', 'tags', "ENUM('Радио-эфир - Live', 'Радио-эфир - Повтор', 'Радио-эфир - Запись', 'Интернет-вещание - Повтор', 'Интернет-вещание - Запись') NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180308_090329_add_tags_to_playlist cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180308_090329_add_tags_to_playlist cannot be reverted.\n";

      return false;
      }
     */
}
