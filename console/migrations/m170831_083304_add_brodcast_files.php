<?php

use yii\db\Migration;

/**
 * Class m170831_083304_add_brodcast_files
 */
class m170831_083304_add_brodcast_files extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('playlist_files', [
            'id' => 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'youtube_playlists_id' => 'MEDIUMINT(8) UNSIGNED NULL',
            'name' => $this->string(255)->notNull(),
            'fileName' => $this->text(500)->null(),
            'long_time' => 'SMALLINT(5) UNSIGNED NULL',
            'radio_brodcast_day' => $this->date()->null(),
            'radio_brodcast_time' => $this->time()->null(),
            'status' => $this->boolean(1)->notNull()->defaultValue(1)
        ]);

        $this->createIndex('fk_playlist_files_youtube_playlists1_idx', 'playlist_files', 'youtube_playlists_id');
        $this->addForeignKey('fk_playlist_files_youtube_playlists1', 'playlist_files', 'youtube_playlists_id', 'youtube_playlists', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m170831_083304_add_brodcast_files cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m170831_083304_add_brodcast_files cannot be reverted.\n";

      return false;
      }
     */
}
