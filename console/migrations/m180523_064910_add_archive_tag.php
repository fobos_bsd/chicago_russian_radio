<?php

use yii\db\Migration;

/**
 * Class m180523_064910_add_archive_tag
 */
class m180523_064910_add_archive_tag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('timetable_tags', 'archive_name', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180523_064910_add_archive_tag cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180523_064910_add_archive_tag cannot be reverted.\n";

        return false;
    }
    */
}
