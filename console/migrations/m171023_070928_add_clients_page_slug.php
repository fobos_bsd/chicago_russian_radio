<?php

use yii\db\Migration;

/**
 * Class m171023_070928_add_clients_page_slug
 */
class m171023_070928_add_clients_page_slug extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addColumn('clients_page', 'slug', $this->string(255)->notNull()->unique()->after('title'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m171023_070928_add_clients_page_slug cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m171023_070928_add_clients_page_slug cannot be reverted.\n";

      return false;
      }
     */
}
