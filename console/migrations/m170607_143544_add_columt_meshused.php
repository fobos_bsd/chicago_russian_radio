<?php

use yii\db\Migration;

class m170607_143544_add_columt_meshused extends Migration {

    public function safeUp() {
        $this->addColumn('adv_usedmesh', 'id', 'BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST');
    }

    public function safeDown() {
        echo "m170607_143544_add_columt_meshused cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m170607_143544_add_columt_meshused cannot be reverted.\n";

      return false;
      }
     */
}
