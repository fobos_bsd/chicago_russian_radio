<?php

use yii\db\Migration;

class m170529_132735_add_file_audio extends Migration {

    public function safeUp() {
        $this->renameColumn('adv_orders', 'file', 'fileAudio');
        $this->addColumn('online_votes', 'site_id', 'TINYINT(3) NOT NULL');

        $this->createIndex('fk_online_votes_site1_idx', 'online_votes', 'site_id');
        $this->addForeignKey('fk_online_votes_site1', 'online_votes', 'site_id', 'site', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown() {
        echo "m170529_132735_add_file_audio cannot be reverted.\n";

        return false;
    }

}
