<?php

use yii\db\Migration;

class m170418_102045_add_soundcloud extends Migration {

    public function up() {
        $this->createTable('soundcloud', [
            'id' => 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'youtube_playlists_id' => 'MEDIUMINT(8) UNSIGNED NULL',
            'name' => $this->string(255)->notNull(),
            'description' => $this->text(1500),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'sclink' => $this->string(255)->notNull()
        ]);

        $this->createIndex('fk_soundcloud_youtube_playlists1_idx', 'soundcloud', 'youtube_playlists_id');
        $this->addForeignKey('fk_soundcloud_youtube_playlists1', 'soundcloud', 'youtube_playlists_id', 'youtube_playlists', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_soundcloud_youtube_playlists1', 'soundcloud');
        $this->dropTable('soundcloud');
        echo "m170418_102045_add_soundcloud cannot be reverted.\n";
    }

}
