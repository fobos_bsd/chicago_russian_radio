<?php

use yii\db\Migration;

class m170517_083853_add_google_statistic extends Migration {

    public function up() {
        $this->createTable('google_statistics', [
            'id' => 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'realtime' => 'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0',
            'perday' => 'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0',
            'permonth' => 'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0',
            'last_update' => $this->integer(11)->notNull()
        ]);
    }

    public function down() {
        $this->dropTable('google_statistics');
        echo "m170517_083853_add_google_statistic cannot be reverted.\n";
    }

}
