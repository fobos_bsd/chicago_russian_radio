<?php

use yii\db\Migration;

class m170529_085600_add_votes_online extends Migration {

    public function safeUp() {
        $this->createTable('online_votes', [
            'id' => $this->primaryKey(9),
            'name' => $this->string(150)->notNull(),
            'slug' => $this->string(255)->notNull(),
            'description' => $this->text(1500),
            'vtype' => 'ENUM(\'phone\', \'sms\') NOT NULL  DEFAULT \'phone\'',
            'period_call' => $this->integer(6),
            'created_at' => $this->integer(11)->notNull(),
            'start_at' => $this->integer(11)->notNull(),
            'finish_at' => $this->integer(11),
            'status' => $this->boolean()->defaultValue(1)->notNull(),
            'params' => $this->text(1500),
        ]);

        $this->createTable('current_phone_vote', [
            'id' => $this->primaryKey(11),
            'online_votes_id' => $this->integer(9)->notNull(),
            'phone' => $this->string(28)->notNull(),
            'vote_phone' => $this->string(28)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_current_phone_vote_online_votes1_idx', 'current_phone_vote', 'online_votes_id');
        $this->addForeignKey('fk_current_phone_vote_online_votes1', 'current_phone_vote', 'online_votes_id', 'online_votes', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('current_sms_vote', [
            'id' => $this->primaryKey(11),
            'online_votes_id' => $this->integer(9)->notNull(),
            'phone' => $this->string(28)->notNull(),
            'vote_sms' => $this->integer(2)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_current_sms_vote_online_votes1_idx', 'current_sms_vote', 'online_votes_id');
        $this->addForeignKey('fk_current_sms_vote_online_votes1', 'current_sms_vote', 'online_votes_id', 'online_votes', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('arh_phone_vote', [
            'id' => $this->bigPrimaryKey(22),
            'online_votes_id' => $this->integer(9)->notNull(),
            'phone' => $this->string(28)->notNull(),
            'vote_phone' => $this->string(28)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_arh_phone_vote_online_votes1_idx', 'arh_phone_vote', 'online_votes_id');
        $this->addForeignKey('fk_arh_phone_vote_online_votes1', 'arh_phone_vote', 'online_votes_id', 'online_votes', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('arh_sms_vote', [
            'id' => $this->bigPrimaryKey(22),
            'online_votes_id' => $this->integer(9)->notNull(),
            'phone' => $this->string(28)->notNull(),
            'vote_sms' => $this->integer(2)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_arh_sms_vote_online_votes1_idx', 'arh_sms_vote', 'online_votes_id');
        $this->addForeignKey('fk_arh_sms_vote_online_votes1', 'arh_sms_vote', 'online_votes_id', 'online_votes', 'id', 'CASCADE', 'CASCADE');


        $this->createTable('clients', [
            'id' => $this->bigPrimaryKey(20),
            'name' => $this->string(255)->notNull()->unique(),
            'email' => $this->string(255),
            'phone' => $this->string(255)->notNull(),
            'params' => $this->text(1500),
            'description' => $this->text(1500),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)
        ]);

        $this->createTable('adv_settings', [
            'id' => $this->primaryKey(3),
            'name' => $this->string(250)->notNull()->unique(),
            'setvalue' => $this->string(255)->notNull(),
            'description' => $this->text(1500)
        ]);

        $this->createTable('adv_mesh', [
            'id' => $this->primaryKey(3),
            'start_time' => $this->time()->notNull(),
            'end_time' => $this->time()->notNull(),
            'long' => $this->smallInteger(3)->notNull(),
            'important' => $this->smallInteger(3)->notNull()->defaultValue(100)
        ]);

        $this->createTable('adv_orders', [
            'id' => $this->bigPrimaryKey(20),
            'clients_id' => $this->bigInteger(20)->notNull(),
            'count_days' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'count_onday' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'used_days' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'title' => $this->string(255)->notNull(),
            'content' => $this->text(1500),
            'how_seconds' => $this->smallInteger(3)->notNull()->defaultValue(1),
            'start_at' => $this->date()->notNull(),
            'finish_at' => $this->date(),
            'file' => $this->text(500),
            'file_link' => $this->text(500),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_adv_orders_clients1_idx', 'adv_orders', 'clients_id');
        $this->addForeignKey('fk_adv_orders_clients1', 'adv_orders', 'clients_id', 'clients', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('adv_calendar', [
            'id' => $this->bigPrimaryKey(20),
            'adv_mesh_id' => $this->integer(3)->notNull(),
            'adv_orders_id' => $this->bigInteger(20)->notNull(),
            'online_day' => $this->date()->notNull(),
            'gravity' => $this->boolean()->defaultValue(0)->notNull(),
            'count_time' => 'TINYINT(3) NOT NULL DEFAULT 1'
        ]);

        $this->createIndex('fk_adv_calendar_adv_mesh1_idx', 'adv_calendar', 'adv_mesh_id');
        $this->addForeignKey('fk_adv_calendar_adv_mesh1', 'adv_calendar', 'adv_mesh_id', 'adv_mesh', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('fk_adv_calendar_adv_orders1_idx', 'adv_calendar', 'adv_orders_id');
        $this->addForeignKey('fk_adv_calendar_adv_orders1', 'adv_calendar', 'adv_orders_id', 'adv_orders', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('adv_usedmesh', [
            'adv_mesh_id' => $this->integer(3)->notNull(),
            'used' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'less_seconds' => $this->smallInteger(3)->notNull()->defaultValue(180),
            'used_procent' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'used_date' => $this->date()->notNull()
        ]);

        $this->createIndex('fk_adv_usedmesh_adv_mesh1_idx', 'adv_usedmesh', 'adv_mesh_id');
        $this->addForeignKey('fk_adv_usedmesh_adv_mesh1', 'adv_usedmesh', 'adv_mesh_id', 'adv_mesh', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        echo "m170529_085600_add_votes_online cannot be reverted.\n";
        return false;
    }

}
