<?php

use yii\db\Migration;

class m170516_140902_add_columnt_menu extends Migration {

    public function up() {
        $this->addColumn('menu_list', 'params', 'VARCHAR(255) NULL');
    }

    public function down() {
        $this->dropColumn('menu_list', 'params');
        echo "m170516_140902_add_columnt_menu cannot be reverted.\n";
    }

}
