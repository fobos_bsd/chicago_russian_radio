<?php

use yii\db\Migration;

class m170424_114838_add_adv_timetable extends Migration {

    public function up() {
        $this->createTable('audio_adv_timetable', [
            'id' => $this->primaryKey(10)->unsigned(),
            'audio_advertising_id' => $this->integer(11)->unsigned()->notNull(),
            'day_of_week' => 'ENUM("Mo", "Tu", "We", "Th", "Fr", "Sa", "Su") NOT NULL',
            'start_time' => $this->time()->notNull(),
            'from_date' => $this->date()->notNull(),
            'to_date' => $this->date()->notNull(),
            'status' => 'TINYINT(1) NOT NULL DEFAULT 1'
        ]);

        $this->createIndex('fk_audio_adv_timetable_audio_advertising1_idx', 'audio_adv_timetable', 'audio_advertising_id');
        $this->addForeignKey('fk_audio_adv_timetable_audio_advertising1', 'audio_adv_timetable', 'audio_advertising_id', 'audio_advertising', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('audio_adv_used', [
            'id' => $this->primaryKey(10)->unsigned(),
            'audio_advertising_id' => $this->integer(11)->unsigned()->notNull(),
            'timeused' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_audio_adv_used_audio_advertising1_idx', 'audio_adv_used', 'audio_advertising_id');
        $this->addForeignKey('fk_audio_adv_used_audio_advertising1', 'audio_adv_used', 'audio_advertising_id', 'audio_advertising', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('audio_adv_archive', [
            'id' => $this->bigPrimaryKey(20)->unsigned(),
            'audio_advertising_id' => $this->integer(11)->unsigned()->notNull(),
            'timeused' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_audio_adv_archive_audio_advertising1_idx', 'audio_adv_archive', 'audio_advertising_id');
        $this->addForeignKey('fk_audio_adv_archive_audio_advertising1', 'audio_adv_archive', 'audio_advertising_id', 'audio_advertising', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_audio_adv_archive_audio_advertising1', 'audio_adv_archive');
        $this->dropTable('audio_adv_archive');
        $this->dropForeignKey('fk_audio_adv_used_audio_advertising1', 'audio_adv_used');
        $this->dropTable('audio_adv_used');
        $this->dropForeignKey('fk_audio_adv_timetable_audio_advertising1', 'audio_adv_timetable');
        $this->dropTable('audio_adv_timetable');
        echo "m170424_114838_add_adv_timetable cannot be reverted.\n";
    }

}
