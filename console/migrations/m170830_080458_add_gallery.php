<?php

use yii\db\Migration;

/**
 * Class m170830_080458_add_gallery
 */
class m170830_080458_add_gallery extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('gallery', [
            'id' => 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'site_id' => 'TINYINT(3) NOT NULL',
            'name' => $this->string(255)->notNull(),
            'slug' => $this->string(255)->unique()->notNull(),
            'parent_id' => 'MEDIUMINT(8) UNSIGNED NULL',
            'youtube_playlists_id' => 'MEDIUMINT(8) UNSIGNED NULL',
            'title' => $this->string(255)->null(),
            'keywords' => $this->text(500)->null(),
            'meta_description' => $this->text(1500)->null(),
            'description' => $this->text(50000)->null(),
            'status' => $this->boolean(1)->notNull()->defaultValue(1)
        ]);

        $this->createIndex('fk_gallery_site1_idx', 'gallery', 'site_id');
        $this->addForeignKey('fk_gallery_site1', 'gallery', 'site_id', 'site', 'id', 'RESTRICT', 'CASCADE');
        $this->createIndex('fk_gallery_youtube_playlists1_idx', 'gallery', 'youtube_playlists_id');
        $this->addForeignKey('fk_gallery_youtube_playlists1', 'gallery', 'youtube_playlists_id', 'youtube_playlists', 'id', 'SET NULL', 'CASCADE');

        $this->createTable('gallery_data', [
            'id' => 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'gallery_id' => 'MEDIUMINT(8) UNSIGNED NOT NULL',
            'name' => $this->string(255)->notNull(),
            'fileName' => $this->string(255)->notNull(),
            'status' => $this->boolean(1)->notNull()->defaultValue(1),
            'gravity' => 'TINYINT(3) NOT NULL DEFAULT 0',
            'title' => $this->string(255)->null(),
            'link' => $this->text(500)->null()
        ]);

        $this->createIndex('fk_gallery_data_gallery1_idx', 'gallery_data', 'gallery_id');
        $this->addForeignKey('fk_gallery_data_gallery1', 'gallery_data', 'gallery_id', 'gallery', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m170830_080458_add_gallery cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m170830_080458_add_gallery cannot be reverted.\n";

      return false;
      }
     */
}
