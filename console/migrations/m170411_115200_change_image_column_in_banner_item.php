<?php

use yii\db\Migration;

class m170411_115200_change_image_column_in_banner_item extends Migration
{
    public function up()
    {
        $this->alterColumn('banner_item','image', $this->string(255)->null());
    }

    public function down()
    {
        echo "m170411_115200_change_image_column_in_banner_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
