<?php

use yii\db\Migration;

class m170420_093704_add_custom_block extends Migration {

    public function up() {
        $this->createTable('custom_blocks', [
            'id' => 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'site_id' => 'TINYINT(3) NOT NULL',
            'lang_id' => 'TINYINT(3) NOT NULL',
            'name' => $this->string(250)->notNull(),
            'content' => $this->text(5000)
        ]);

        $this->createIndex('fk_custom_blocks_lang1_idx', 'custom_blocks', 'lang_id');
        $this->createIndex('fk_custom_blocks_site1_idx', 'custom_blocks', 'site_id');
        $this->addForeignKey('fk_custom_blocks_lang1', 'custom_blocks', 'lang_id', 'lang', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_custom_blocks_site1', 'custom_blocks', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_custom_blocks_lang1', 'custom_blocks');
        $this->dropForeignKey('fk_custom_blocks_site1', 'custom_blocks');
        $this->dropTable('custom_blocks');
        echo "m170420_093704_add_custom_block cannot be reverted.\n";
    }

}
