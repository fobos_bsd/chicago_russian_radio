<?php

use yii\db\Migration;

class m170425_054911_add_timeread_reklam extends Migration {

    public function safeUp() {
        $this->addColumn('audio_adv_timetable', 'lasttime_read', 'INT(11) NULL');
    }

    public function safeDown() {
        $this->dropColumn('audio_adv_timetable', 'lasttime_read');
        echo "m170425_054911_add_timeread_reklam cannot be reverted.\n";
    }

}
