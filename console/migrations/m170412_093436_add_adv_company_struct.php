<?php

use yii\db\Migration;

class m170412_093436_add_adv_company_struct extends Migration
{
    public function safeUp()
    {
        $this->createTable('adv_company', [
            'id' => $this->primaryKey(5),
            'site_id' => 'tinyint(3) not null',
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createTable('banner_item_has_adv_company', [
            'banner_item_id' => 'int(5) not null',
            'adv_company_id' => 'int(5) not null',
        ]);

        $this->createIndex('fk_adv_company_site1_idx', 'adv_company', 'site_id');
        $this->addForeignKey('fk_adv_company_site1', 'adv_company', 'site_id', 'site', 'id', 'CASCADE', 'RESTRICT');

        $this->createIndex('fk_banner_item_has_adv_company_adv_company1_idx', 'banner_item_has_adv_company', 'adv_company_id');
        $this->createIndex('fk_banner_item_has_adv_company_banner_item1_idx', 'banner_item_has_adv_company', 'banner_item_id');

        $this->addForeignKey('fk_banner_item_has_adv_company_banner_item1', 'banner_item_has_adv_company',
            'adv_company_id', 'adv_company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_banner_item_has_adv_company_adv_company1', 'banner_item_has_adv_company',
            'banner_item_id', 'banner_item', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('adv_company');
        $this->dropTable('adv_company_id');
    }
}
