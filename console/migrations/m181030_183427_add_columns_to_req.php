<?php

use yii\db\Migration;

/**
 * Class m181030_183427_add_columns_to_req
 */
class m181030_183427_add_columns_to_req extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('radioquiz', 'advertiser_name', $this->text(500)->null());
        $this->addColumn('radioquiz', 'advertiser_site', $this->text(500)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m181030_183427_add_columns_to_req cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m181030_183427_add_columns_to_req cannot be reverted.\n";

      return false;
      }
     */
}
