<?php

use yii\db\Migration;

/**
 * Class m181029_140808_change_songs_dropbox
 */
class m181029_140808_change_songs_dropbox extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('songs', 'dropbox_id', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181029_140808_change_songs_dropbox cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181029_140808_change_songs_dropbox cannot be reverted.\n";

        return false;
    }
    */
}
