<?php

use yii\db\Migration;

/**
 * Class m171113_112909_add_opinion_and_wishs
 */
class m171113_112909_add_opinion_and_wishs extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('opinions_and_wishs', [
            'id' => $this->primaryKey(10)->unsigned(),
            'site_id' => 'TINYINT(3) NOT NULL',
            'user_name' => $this->text(500)->notNull(),
            'email' => $this->string(255)->null(),
            'opinion' => $this->text(5000)->notNull(),
            'created_at' => $this->date()->notNull(),
            'raiting' => 'TINYINT(1) NOT NULL DEFAULT 0',
            'status' => $this->boolean()->notNull()->defaultValue(0)
        ]);

        $this->createIndex('fk_opinions_and_wishs_site1_idx', 'opinions_and_wishs', 'site_id');
        $this->addForeignKey('fk_opinions_and_wishs_site1', 'opinions_and_wishs', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m171113_112909_add_opinion_and_wishs cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m171113_112909_add_opinion_and_wishs cannot be reverted.\n";

      return false;
      }
     */
}
