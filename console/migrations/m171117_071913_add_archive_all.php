<?php

use yii\db\Migration;

/**
 * Class m171117_071913_add_archive_all
 */
class m171117_071913_add_archive_all extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('radio_archive', [
            'id' => 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'youtube_playlists_id' => 'MEDIUMINT(8) UNSIGNED NULL',
            'name' => $this->string(255)->notNull(),
            'fileName' => $this->text(500)->null(),
            'radio_brodcast_day' => $this->date()->null(),
            'radio_brodcast_time' => $this->time()->null(),
            'onair' => $this->boolean(1)->notNull()->defaultValue(0)
        ]);

        $this->createIndex('fk_radio_archive_youtube_playlists1_idx', 'radio_archive', 'youtube_playlists_id');
        $this->addForeignKey('fk_radio_archive_youtube_playlists1', 'radio_archive', 'youtube_playlists_id', 'youtube_playlists', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m171117_071913_add_archive_all cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m171117_071913_add_archive_all cannot be reverted.\n";

      return false;
      }
     */
}
