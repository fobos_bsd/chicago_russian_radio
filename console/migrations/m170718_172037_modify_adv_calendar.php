<?php

use yii\db\Migration;

class m170718_172037_modify_adv_calendar extends Migration {

    public function safeUp() {
        $this->alterColumn('adv_calendar', 'gravity', 'TINYINT(3) NOT NULL DEFAULT 0');
    }

    public function safeDown() {
        echo "m170718_172037_modify_adv_calendar cannot be reverted.\n";

        return false;
    }
}
