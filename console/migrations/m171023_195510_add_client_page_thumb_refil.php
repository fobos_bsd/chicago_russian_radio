<?php

use yii\db\Migration;

/**
 * Class m171023_195510_add_client_page_thumb_refil
 */
class m171023_195510_add_client_page_thumb_refil extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->dropColumn('clients_page', 'fileStyle');
        $this->dropColumn('clients_page', 'fileJS');
        $this->addColumn('clients_page', 'thumb', $this->text(500)->null()->after('status'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m171023_195510_add_client_page_thumb_refil cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m171023_195510_add_client_page_thumb_refil cannot be reverted.\n";

      return false;
      }
     */
}
