<?php

use yii\db\Migration;

/**
 * Class m181027_134559_add_status_to_songs
 */
class m181027_134559_add_status_to_songs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('songs', 'status', $this->boolean()->notNull()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181027_134559_add_status_to_songs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181027_134559_add_status_to_songs cannot be reverted.\n";

        return false;
    }
    */
}
