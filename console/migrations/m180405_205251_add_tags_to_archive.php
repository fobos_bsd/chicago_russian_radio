<?php

use yii\db\Migration;

/**
 * Class m180405_205251_add_tags_to_archive
 */
class m180405_205251_add_tags_to_archive extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('radio_archive', 'tags', "ENUM('Радиоэфир - Live', 'Радиоэфир - Повтор', 'Радиоэфир - Запись', 'Интернет-вещание - Повтор', 'Интернет-вещание - Запись')");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180405_205251_add_tags_to_archive cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180405_205251_add_tags_to_archive cannot be reverted.\n";

      return false;
      }
     */
}
