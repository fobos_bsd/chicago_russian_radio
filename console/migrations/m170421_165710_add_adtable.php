<?php

use yii\db\Migration;

class m170421_165710_add_adtable extends Migration {

    public function up() {
        $this->createTable('audio_advertising', [
            'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'site_id' => 'TINYINT(3) NOT NULL',
            'name' => $this->string(250)->notNull(),
            'description' => $this->text(1500),
            'text_content' => $this->text(5000),
            'audio_file' => $this->string(255)->notNull()->unique(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_audio_advertising_site1_idx', 'audio_advertising', 'site_id');
        $this->addForeignKey('fk_audio_advertising_site1', 'audio_advertising', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_audio_advertising_site1', 'audio_advertising');
        $this->dropTable('audio_advertising');
        echo "m170421_165710_add_adtable cannot be reverted.\n";
    }

}
