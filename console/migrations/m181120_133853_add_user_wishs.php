<?php

use yii\db\Migration;

/**
 * Class m181120_133853_add_user_wishs
 */
class m181120_133853_add_user_wishs extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('users_wishs', [
            'id' => $this->bigPrimaryKey(20),
            'name' => $this->text(500)->null(),
            'email' => $this->text(500)->null(),
            'subject' => $this->text(2500)->null(),
            'message' => $this->text(5000)->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m181120_133853_add_user_wishs cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m181120_133853_add_user_wishs cannot be reverted.\n";

      return false;
      }
     */
}
