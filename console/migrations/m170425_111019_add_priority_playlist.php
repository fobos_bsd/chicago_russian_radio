<?php

use yii\db\Migration;

class m170425_111019_add_priority_playlist extends Migration {

    public function safeUp() {
        $this->addColumn('youtube_playlists', 'priority', 'SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function safeDown() {
        $this->dropColumn('youtube_playlists', 'priority');
        echo "m170425_111019_add_priority_playlist cannot be reverted.\n";
    }

}
