<?php

use yii\db\Migration;

class m170518_203413_add_site_to_analytic extends Migration {

    public function up() {
        $this->addColumn('google_statistics', 'site_id', 'TINYINT(3) NOT NULL');
        $this->createIndex('fk_google_statistics_site1_idx', 'google_statistics', 'site_id');
        $this->addForeignKey('fk_google_statistics_site1', 'google_statistics', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_google_statistics_site1', 'google_statistics');
        $this->dropColumn('google_statistics', 'site_id');
        echo "m170518_203413_add_site_to_analytic cannot be reverted.\n";
        return false;
    }

}
