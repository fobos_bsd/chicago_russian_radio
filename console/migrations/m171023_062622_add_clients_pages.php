<?php

use yii\db\Migration;

/**
 * Class m171023_062622_add_clients_pages
 */
class m171023_062622_add_clients_pages extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('clients_page', [
            'id' => 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'clients_id' => 'BIGINT(20) UNSIGNED NULL',
            'site_id' => 'TINYINT(3) NOT NULL',
            'lang_id' => 'TINYINT(3) NOT NULL',
            'title' => $this->string(255)->notNull(),
            'keywords' => $this->string(255)->null(),
            'description' => $this->text(1500)->null(),
            'content' => 'MEDIUMTEXT NULL',
            'fileStyle' => $this->string(255)->null(),
            'fileJS' => $this->string(255)->null(),
            'status' => $this->boolean(1)->notNull()->defaultValue(1),
            'created_at' => $this->date()->notNull()
        ]);

        $this->createIndex('fk_clients_page_site1_idx', 'clients_page', 'site_id');
        $this->addForeignKey('fk_clients_page_site1', 'clients_page', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('fk_clients_page_clients1_idx', 'clients_page', 'clients_id');
        $this->addForeignKey('fk_clients_page_clients1', 'clients_page', 'clients_id', 'clients', 'id', 'SET NULL', 'CASCADE');
        $this->createIndex('fk_clients_page_lang1_idx', 'clients_page', 'lang_id');
        $this->addForeignKey('fk_clients_page_lang1', 'clients_page', 'lang_id', 'lang', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m171023_062622_add_clients_pages cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m171023_062622_add_clients_pages cannot be reverted.\n";

      return false;
      }
     */
}
