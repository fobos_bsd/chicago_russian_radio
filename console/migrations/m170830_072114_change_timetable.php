<?php

use yii\db\Migration;

/**
 * Class m170830_072114_change_timetable
 */
class m170830_072114_change_timetable extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addColumn('youtube_video', 'socialcode', "ENUM('fb', 'vk', 'tw', 'youtube') NOT NULL DEFAULT 'youtube'");
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m170830_072114_change_timetable cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m170830_072114_change_timetable cannot be reverted.\n";

      return false;
      }
     */
}
