<?php

use yii\db\Migration;

/**
 * Class m181029_124102_add_soung_file_field
 */
class m181029_124102_add_soung_file_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('songs', 'fileAudio', $this->text(500)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181029_124102_add_soung_file_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181029_124102_add_soung_file_field cannot be reverted.\n";

        return false;
    }
    */
}
