<?php

use yii\db\Migration;

/**
 * Class m180308_094008_add_tags_to_timetable
 */
class m180308_094008_add_tags_to_timetable extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->dropColumn('youtube_playlists', 'tags');
        $this->addColumn('timetable', 'tags', "ENUM('Радио-эфир - Live', 'Радио-эфир - Повтор', 'Радио-эфир - Запись', 'Интернет-вещание - Повтор', 'Интернет-вещание - Запись') NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180308_094008_add_tags_to_timetable cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180308_094008_add_tags_to_timetable cannot be reverted.\n";

      return false;
      }
     */
}
