<?php

use yii\db\Migration;

/**
 * Class m180221_152930_add_archive_slug_playlist
 */
class m180221_152930_add_archive_slug_playlist extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('youtube_playlists', 'archive_slug', $this->text(1500)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180221_152930_add_archive_slug_playlist cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180221_152930_add_archive_slug_playlist cannot be reverted.\n";

      return false;
      }
     */
}
