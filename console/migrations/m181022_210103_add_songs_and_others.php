<?php

use yii\db\Migration;

/**
 * Class m181022_210103_add_songs_and_others
 */
class m181022_210103_add_songs_and_others extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('top_videos', [
            'id' => $this->primaryKey(10)->unsigned(),
            'title' => $this->text(500)->notNull(),
            'url' => $this->text(500)->notNull(),
            'description' => $this->text(15000)->null(),
            'rank' => $this->smallInteger(5)->notNull()->defaultValue(0)
        ]);

        $this->createTable('radioquiz', [
            'id' => $this->bigPrimaryKey(20)->unsigned(),
            'title' => $this->text(500)->notNull(),
            'description' => $this->text(15000)->null(),
            'prize' => $this->text(5000)->null(),
            'rules' => $this->text(5000)->null(),
            'participant' => $this->text(1500)->null(),
            'prize_image' => $this->text(500)->null(),
            'participant_photo' => $this->text(500)->null(),
            'questions' => $this->text(5000)->null(),
            'answers' => $this->text(5000)->null(),
            'status' => $this->boolean()->notNull()->defaultValue(0),
        ]);

        $this->createTable('songs', [
            'id' => $this->primaryKey(8)->unsigned(),
            'title' => $this->text(500)->notNull(),
            'url' => $this->text(500)->null(),
            'dropbox_id' => $this->string(255)->unique()->notNull(),
            'description' => $this->text(15000)->null(),
            'rank' => $this->smallInteger(5)->notNull()->defaultValue(0)
        ]);

        $this->createTable('song_votes', [
            'id' => $this->bigPrimaryKey(21)->unsigned(),
            'songs_id' => $this->integer(8)->unsigned()->notNull(),
            'ip_address' => $this->string(64)->notNull()->defaultValue('127.0.0.1')
        ]);

        $this->createIndex('fk_song_votes_songs_idx', 'song_votes', 'songs_id');
        $this->addForeignKey('fk_song_votes_songs', 'song_votes', 'songs_id', 'songs', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m181022_210103_add_songs_and_others cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m181022_210103_add_songs_and_others cannot be reverted.\n";

      return false;
      }
     */
}
