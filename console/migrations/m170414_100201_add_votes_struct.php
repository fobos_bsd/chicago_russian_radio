<?php

use yii\db\Migration;

class m170414_100201_add_votes_struct extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('questions', [
            'id' => $this->primaryKey(11),
            'site_id' => 'tinyint(3) not null',
            'name' => $this->string(255)->notNull(),
            'active' => $this->boolean()->notNull()->defaultValue(0)
        ]);

        $this->createTable('answers', [
            'id' => $this->primaryKey(11),
            'site_id' => 'tinyint(3) not null',
            'questions_id' => $this->integer(11)->notNull(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createTable('votes', [
            'id' => $this->primaryKey(11),
            'site_id' => 'tinyint(3) not null',
            'questions_id' => $this->integer(11)->notNull(),
            'answers_id' => $this->integer(11)->notNull(),
            'ip' => $this->string(60)->notNull(),
        ]);

        $this->createIndex('fk_questions_site1_idx', 'questions', 'site_id');
        $this->addForeignKey('fk_questions_site1', 'questions', 'site_id', 'site', 'id', 'CASCADE', 'RESTRICT');

        $this->createIndex('fk_answers_questions1_idx', 'answers', 'questions_id');
        $this->createIndex('fk_answers_site1_idx', 'answers', 'site_id');
        $this->addForeignKey('fk_answers_questions1', 'answers', 'questions_id', 'questions', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_answers_site1', 'answers', 'site_id', 'site', 'id', 'CASCADE', 'RESTRICT');

        $this->createIndex('fk_votes_answers1_idx', 'votes', 'answers_id');
        $this->createIndex('fk_votes_questions1_idx', 'votes', 'questions_id');
        $this->createIndex('fk_votes_site1_idx', 'votes', 'site_id');
        $this->addForeignKey('fk_votes_answers1', 'votes', 'answers_id', 'answers', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_votes_questions1', 'votes', 'questions_id', 'questions', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_votes_site1', 'votes', 'site_id', 'site', 'id', 'CASCADE', 'RESTRICT');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_votes_site1','votes');
        $this->dropForeignKey('fk_votes_questions1','votes');
        $this->dropForeignKey('fk_votes_answers1','votes');
        $this->dropIndex('fk_votes_site1_idx','votes');
        $this->dropIndex('fk_votes_questions1_idx','votes');
        $this->dropIndex('fk_votes_answers1_idx','votes');

        $this->dropForeignKey('fk_answers_site1','answers');
        $this->dropForeignKey('fk_answers_questions1','answers');
        $this->dropIndex('fk_answers_site1_idx','answers');
        $this->dropIndex('fk_answers_questions1_idx','answers');

        $this->dropForeignKey('fk_questions_site1','questions');
        $this->dropIndex('fk_questions_site1_idx','questions');

        $this->dropTable('votes');
        $this->dropTable('answers');
        $this->dropTable('questions');
    }
}
