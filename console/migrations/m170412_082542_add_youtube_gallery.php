<?php

use yii\db\Migration;

class m170412_082542_add_youtube_gallery extends Migration {

    public function safeUp() {
        $this->createTable('youtube_channels', [
            'id' => 'SMALLINT(6) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'site_id' => 'TINYINT(3) NOT NULL',
            'channel_id' => $this->string(32)->notNull()->unique(),
            'name' => $this->string(250)->notNull(),
            'slug' => $this->string(255)->notNull()->unique(),
            'description' => $this->text(1500),
            'status' => $this->boolean()->defaultValue(1)
        ]);

        $this->createIndex('fk_youtube_channels_site1_idx', 'youtube_channels', 'site_id');
        $this->addForeignKey('fk_youtube_channels_site1', 'youtube_channels', 'site_id', 'site', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('youtube_playlists', [
            'id' => 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'youtube_channels_id' => 'SMALLINT(6) UNSIGNED NOT NULL',
            'etag' => $this->string(64)->notNull(),
            'playlist_id' => $this->string(48)->notNull()->unique(),
            'publish_at' => $this->integer(11),
            'title' => $this->string(255),
            'description' => $this->text(1500),
            'thumbnails_medium' => $this->text(500),
            'thumbnails_maxres' => $this->text(500),
            'status' => $this->boolean()->defaultValue(1)
        ]);

        $this->createIndex('fk_youtube_playlists_youtube_channels1_idx', 'youtube_playlists', 'youtube_channels_id');
        $this->addForeignKey('fk_youtube_playlists_youtube_channels1', 'youtube_playlists', 'youtube_channels_id', 'youtube_channels', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('youtube_video', [
            'id' => 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'youtube_playlists_id' => 'MEDIUMINT(8) UNSIGNED NOT NULL',
            'video_id' => $this->string(32)->notNull()->unique(),
            'publish_at' => $this->integer(11),
            'title' => $this->string(255),
            'description' => $this->text(1500),
            'thumbnails_medium' => $this->text(500),
            'thumbnails_maxres' => $this->text(500),
            'status' => $this->boolean()->defaultValue(1)
        ]);

        $this->createIndex('fk_youtube_video_youtube_playlists1_idx', 'youtube_video', 'youtube_playlists_id');
        $this->addForeignKey('fk_youtube_video_youtube_playlists1', 'youtube_video', 'youtube_playlists_id', 'youtube_playlists', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        echo "m170412_082542_add_youtube_gallery cannot be reverted.\n";
    }

}
