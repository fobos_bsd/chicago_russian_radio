<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `format`.
 */
class m170413_121734_drop_format_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('banner_format');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('banner_format', [
            'id' => 'tinyint(3) not null AUTO_INCREMENT PRIMARY KEY',
            'title' => $this->string(255)->notNull(),
            'width' => $this->smallInteger(4)->notNull(),
            'height' => $this->smallInteger(4)->notNull()
        ]);
    }
}
