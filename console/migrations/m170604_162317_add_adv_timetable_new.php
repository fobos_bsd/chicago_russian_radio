<?php

use yii\db\Migration;

class m170604_162317_add_adv_timetable_new extends Migration {

    public function safeUp() {
        $this->createTable('clients', [
            'id' => $this->bigPrimaryKey(20)->unsigned(),
            'name' => $this->string(255)->notNull()->unique(),
            'email' => $this->string(255),
            'phone' => $this->string(255)->notNull(),
            'params' => $this->text(1500),
            'description' => $this->text(1500),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)
        ]);

        $this->createTable('adv_settings', [
            'id' => 'SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'site_id' => 'TINYINT(3) NOT NULL',
            'name' => $this->string(250)->notNull()->unique(),
            'setvalue' => $this->string(255)->notNull(),
            'description' => $this->text(1500)
        ]);

        $this->createIndex('fk_adv_settings_site1_idx', 'adv_settings', 'site_id');
        $this->addForeignKey('fk_adv_settings_site1', 'adv_settings', 'site_id', 'site', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('adv_mesh', [
            'id' => 'SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'site_id' => 'TINYINT(3) NOT NULL',
            'start_time' => $this->time()->notNull(),
            'end_time' => $this->time()->notNull(),
            'long' => $this->smallInteger(5)->unsigned()->notNull(),
            'important' => $this->smallInteger(5)->unsigned()->notNull()->defaultValue(100),
            'spacialy' => 'TINYINT(1) NOT NULL DEFAULT 0'
        ]);

        $this->createIndex('fk_adv_mesh_site1_idx', 'adv_mesh', 'site_id');
        $this->addForeignKey('fk_adv_mesh_site1', 'adv_mesh', 'site_id', 'site', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('adv_orders', [
            'id' => $this->bigPrimaryKey(20)->unsigned(),
            'clients_id' => $this->bigInteger(20)->unsigned()->notNull(),
            'site_id' => 'TINYINT(3) NOT NULL',
            'count_days' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'count_onday' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'used_days' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'title' => $this->string(255)->notNull(),
            'content' => $this->text(1500),
            'how_seconds' => $this->smallInteger(3)->notNull()->defaultValue(1),
            'start_at' => $this->date()->notNull(),
            'finish_at' => $this->date(),
            'fileAudio' => $this->text(500),
            'file_link' => $this->text(500),
            'created_at' => $this->integer(11)->notNull(),
            'status' => "ENUM('On Air', 'New') NOT NULL DEFAULT 'New'"
        ]);

        $this->createIndex('fk_adv_orders_clients1_idx', 'adv_orders', 'clients_id');
        $this->addForeignKey('fk_adv_orders_clients1', 'adv_orders', 'clients_id', 'clients', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('fk_adv_orders_site1_idx', 'adv_orders', 'site_id');
        $this->addForeignKey('fk_adv_orders_site1', 'adv_orders', 'site_id', 'site', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('adv_calendar', [
            'id' => $this->bigPrimaryKey(20)->unsigned(),
            'adv_orders_id' => $this->bigInteger(20)->unsigned()->notNull(),
            'adv_mesh_id' => $this->smallInteger(5)->unsigned()->notNull(),
            'online_day' => $this->date()->notNull(),
            'gravity' => $this->boolean()->defaultValue(0)->notNull(),
            'count_time' => 'TINYINT(3) UNSIGNED NOT NULL DEFAULT 1'
        ]);

        $this->createIndex('fk_adv_calendar_adv_mesh1_idx', 'adv_calendar', 'adv_mesh_id');
        $this->addForeignKey('fk_adv_calendar_adv_mesh1', 'adv_calendar', 'adv_mesh_id', 'adv_mesh', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('fk_adv_calendar_adv_orders1_idx', 'adv_calendar', 'adv_orders_id');
        $this->addForeignKey('fk_adv_calendar_adv_orders1', 'adv_calendar', 'adv_orders_id', 'adv_orders', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('adv_usedmesh', [
            'adv_mesh_id' => $this->smallInteger(5)->unsigned()->notNull(),
            'used' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'less_seconds' => $this->smallInteger(3)->notNull()->defaultValue(180),
            'used_procent' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'used_date' => $this->date()->notNull()
        ]);

        $this->createIndex('fk_adv_usedmesh_adv_mesh1_idx', 'adv_usedmesh', 'adv_mesh_id');
        $this->addForeignKey('fk_adv_usedmesh_adv_mesh1', 'adv_usedmesh', 'adv_mesh_id', 'adv_mesh', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        echo "m170604_162317_add_adv_timetable_new cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m170604_162317_add_adv_timetable_new cannot be reverted.\n";

      return false;
      }
     */
}
