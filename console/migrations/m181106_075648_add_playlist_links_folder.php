<?php

use yii\db\Migration;

/**
 * Class m181106_075648_add_playlist_links_folder
 */
class m181106_075648_add_playlist_links_folder extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('playlists_folder', [
            'id' => $this->primaryKey(8)->unsigned(),
            'link' => $this->text(1500)->notNull(),
            'status' => $this->boolean()->defaultValue(1)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m181106_075648_add_playlist_links_folder cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m181106_075648_add_playlist_links_folder cannot be reverted.\n";

      return false;
      }
     */
}
