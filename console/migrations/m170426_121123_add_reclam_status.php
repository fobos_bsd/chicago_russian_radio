<?php

use yii\db\Migration;

class m170426_121123_add_reclam_status extends Migration {

    public function safeUp() {
        $this->addColumn('audio_advertising', 'status', 'TINYINT(1) NOT NULL DEFAULT 1');
    }

    public function safeDown() {
        $this->dropColumn('audio_advertising', 'status');
        echo "m170426_121123_add_reclam_status cannot be reverted.\n";
    }

}
