<?php

use yii\db\Migration;

/**
 * Class m171109_115924_change_banner_items
 */
class m171109_115924_change_banner_items extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('banner_position', [
            'id' => 'SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'name' => $this->string(255)->unique()->notNull(),
            'width' => $this->smallInteger(5)->notNull(),
            'height' => $this->smallInteger(5)->notNull()
        ]);

        //$this->addColumn('banner_item', 'banner_position_id', $this->smallInteger(5)->unsigned()->notNull()->after('lang_id'));

        //$this->createIndex('fk_banner_item_banner_position1_idx', 'banner_item', 'banner_position_id');
        //$this->addForeignKey('fk_banner_item_banner_position1', 'banner_item', 'banner_position_id', 'banner_position', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m171109_115924_change_banner_items cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m171109_115924_change_banner_items cannot be reverted.\n";

      return false;
      }
     */
}
