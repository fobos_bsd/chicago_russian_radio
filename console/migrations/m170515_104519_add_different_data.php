<?php

use yii\db\Migration;

class m170515_104519_add_different_data extends Migration {

    public function up() {
        $this->createTable('different_data', [
            'id' => 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'site_id' => 'TINYINT(3) NOT NULL',
            'name' => $this->string(255)->notNull(),
            'action_date' => $this->dateTime()->notNull(),
            'description' => $this->text(1500),
            'note' => $this->text(5000),
        ]);

        $this->createIndex('fk_different_data_site1_idx', 'different_data', 'site_id');
        $this->addForeignKey('fk_different_data_site1', 'different_data', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_different_data_site1', 'different_data');
        $this->dropTable('different_data');
        echo "m170515_104519_add_different_data cannot be reverted.\n";
    }

}
