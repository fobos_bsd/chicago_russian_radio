<?php

use yii\db\Migration;

/**
 * Class m181020_191228_add_seo_table
 */
class m181020_191228_add_seo_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('seo_content', [
            'id' => $this->primaryKey(10),
            'page_name' => $this->string(255)->notNull()->unique(),
            'seo_title' => $this->string(1500)->null(),
            'seo_keywords' => $this->string(2500)->null(),
            'seo_description' => $this->string(5000)->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m181020_191228_add_seo_table cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m181020_191228_add_seo_table cannot be reverted.\n";

      return false;
      }
     */
}
