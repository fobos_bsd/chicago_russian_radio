<?php

use yii\db\Migration;

class m170413_120140_drop_column_in_banner_item extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropColumn('banner_item', 'format_id');
        $this->dropColumn('banner_item', 'position');
        $this->dropColumn('banner_item', 'showon');
    }

    public function safeDown()
    {
    }

}
