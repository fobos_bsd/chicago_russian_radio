<?php

use yii\db\Migration;

class m170426_051836_add_meta_playlists extends Migration {

    public function safeUp() {
        $this->addColumn('youtube_channels', 'meta_key', 'VARCHAR(255) NULL');
        $this->addColumn('youtube_channels', 'meta_description', 'TEXT(500) NULL');
        $this->addColumn('youtube_playlists', 'meta_key', 'VARCHAR(255) NULL');
        $this->addColumn('youtube_playlists', 'meta_description', 'TEXT(500) NULL');
    }

    public function safeDown() {
        $this->dropColumn('youtube_channels', 'meta_key');
        $this->dropColumn('youtube_channels', 'meta_description');
        $this->dropColumn('youtube_playlists', 'meta_key');
        $this->dropColumn('youtube_playlists', 'meta_description');

        echo "m170426_051836_add_meta_playlists cannot be reverted.\n";
    }

}
