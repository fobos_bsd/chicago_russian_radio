<?php

use yii\db\Migration;

class m170420_081051_add_column_for_questions extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_questions','status', $this->boolean()->defaultValue(0)->notNull());
        $this->addColumn('online_questions','status', $this->boolean()->defaultValue(0)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('user_questions','status');
        $this->dropColumn('online_questions','status');
    }
}
