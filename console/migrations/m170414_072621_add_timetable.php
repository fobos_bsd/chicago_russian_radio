<?php

use yii\db\Migration;

class m170414_072621_add_timetable extends Migration {

    public function safeUp() {
        $this->createTable('timetable', [
            'id' => 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'site_id' => 'TINYINT(3) NOT NULL',
            'lang_id' => 'TINYINT(3) NOT NULL',
            'youtube_playlists_id' => 'MEDIUMINT(8) UNSIGNED NOT NULL',
            'day_of_week' => "ENUM('Mo','Tu','We','Th','Fr','Sa','Su') NOT NULL",
            'name' => $this->string(255)->notNull(),
            'description' => $this->text(1500),
            'fileImage' => $this->string(255),
            'from_time' => $this->time()->notNull(),
            'to_time' => $this->time()->notNull()
        ]);

        $this->createIndex('fk_timetable_site1_idx', 'timetable', 'site_id');
        $this->addForeignKey('fk_timetable_site1', 'timetable', 'site_id', 'site', 'id', 'RESTRICT', 'CASCADE');

        $this->createIndex('fk_timetable_lang1_idx', 'timetable', 'lang_id');
        $this->addForeignKey('fk_timetable_lang1', 'timetable', 'lang_id', 'lang', 'id', 'RESTRICT', 'CASCADE');

        $this->createIndex('fk_timetable_youtube_playlists1_idx', 'timetable', 'youtube_playlists_id');
        $this->addForeignKey('fk_timetable_youtube_playlists1', 'timetable', 'youtube_playlists_id', 'youtube_playlists', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_timetable_youtube_playlists1', 'timetable');
        $this->dropForeignKey('fk_timetable_lang1', 'timetable');
        $this->dropForeignKey('fk_timetable_site1', 'timetable');
        $this->dropTable('timetable');

        echo "m170414_072621_add_timetable cannot be reverted.\n";
    }

}
