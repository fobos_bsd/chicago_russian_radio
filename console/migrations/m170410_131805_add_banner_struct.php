<?php

use yii\db\Migration;

class m170410_131805_add_banner_struct extends Migration
{
    public function safeUp()
    {
        $this->createTable('banner_item', [
            'id' => $this->primaryKey(5),
            'user_id' => $this->integer(11)->notNull(),
            'site_id' => 'tinyint(3) not null',
            'lang_id' => 'tinyint(3)',
            'name' => $this->string(255)->notNull(),
            'url' => $this->text()->notNull(),
            'url_title' => $this->string(255),
            'image' => $this->string(255)->notNull(),
            'format_id' => 'tinyint(3) not null',
            'from_date' => $this->integer(11)->notNull(),
            'to_date' => $this->integer(11)->notNull(),
            'priority' => 'tinyint(3) not null',
            'position' => 'tinyint(2) not null',
            'showon' => 'tinyint(2) not null',
            'active' => $this->boolean()->notNull()
        ]);

        $this->createTable('banner_format', [
            'id' => 'tinyint(3) not null AUTO_INCREMENT PRIMARY KEY',
            'title' => $this->string(255)->notNull(),
            'width' => $this->smallInteger(4)->notNull(),
            'height' => $this->smallInteger(4)->notNull()
        ]);

        $this->createTable('banner_statistics', [
            'banner_id' => 'int(5) not null PRIMARY KEY',
            'click_count' => $this->integer(11)->notNull()->defaultValue(0),
            'show_count' => $this->integer(11)->notNull()->defaultValue(0),
        ]);

        $this->createIndex('fk_banner_item_user1_idx', 'banner_item', 'user_id');
        $this->createIndex('fk_banner_item_banner_format1_idx', 'banner_item', 'format_id');
        $this->createIndex('fk_banner_item_site1_idx', 'banner_item', 'site_id');
        $this->createIndex('fk_banner_item_lang1_idx', 'banner_item', 'lang_id');

        $this->addForeignKey('fk_banner_item_user1', 'banner_item', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_banner_item_banner_format1', 'banner_item', 'format_id', 'banner_format', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_banner_item_site1', 'banner_item', 'site_id', 'site', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_banner_item_lang1', 'banner_item', 'lang_id', 'lang', 'id', 'CASCADE', 'RESTRICT');

        $this->createIndex('fk_banner_statistics_banner_item1_idx', 'banner_statistics', 'banner_id');

        $this->addForeignKey('fk_banner_statistics_banner_item1', 'banner_statistics', 'banner_id', 'banner_item', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('banner_item');
        $this->dropTable('banner_format');
        $this->dropTable('banner_statistics');
    }
}
