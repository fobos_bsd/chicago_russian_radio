<?php

use yii\db\Migration;

class m170413_115608_drop_foreign_key_in_banner_item extends Migration
{

    public function safeUp()
    {
        $this->dropForeignKey('fk_banner_item_banner_format1','banner_item');
        $this->dropIndex('fk_banner_item_banner_format1_idx','banner_item');
    }

    public function safeDown()
    {
    }

}
