<?php

use yii\db\Migration;

/**
 * Class m180424_074943_change_tags_for_titmetable
 */
class m180424_074943_change_tags_for_titmetable extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->dropColumn('radio_archive', 'tags');
        $this->dropColumn('timetable', 'tags');

        $this->createTable('timetable_tags', [
            'id' => 'SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'name' => $this->string(255)->notNull()
        ]);

        $this->addColumn('radio_archive', 'timetable_tags_id', $this->smallInteger(5)->null()->unsigned());
        $this->addColumn('timetable', 'timetable_tags_id', $this->smallInteger(5)->null()->unsigned());

        $this->createIndex('fk_radio_archive_timetable_tags1_idx', 'radio_archive', 'timetable_tags_id');
        $this->createIndex('fk_timetable_timetable_tags1_idx', 'timetable', 'timetable_tags_id');

        $this->addForeignKey('fk_radio_archive_timetable_tags1', 'radio_archive', 'timetable_tags_id', 'timetable_tags', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_timetable_timetable_tags1', 'timetable', 'timetable_tags_id', 'timetable_tags', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180424_074943_change_tags_for_titmetable cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180424_074943_change_tags_for_titmetable cannot be reverted.\n";

      return false;
      }
     */
}
