<?php

use yii\db\Migration;

/**
 * Class m171114_092659_add_onair_column_to_timetable
 */
class m171114_092659_add_onair_column_to_timetable extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addColumn('timetable', 'onair', $this->boolean()->notNull()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m171114_092659_add_onair_column_to_timetable cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m171114_092659_add_onair_column_to_timetable cannot be reverted.\n";

      return false;
      }
     */
}
