<?php

use yii\db\Migration;

class m170604_162248_delete_adv_timetable_old extends Migration {

    public function safeUp() {
        $this->delete('adv_settings');

        $this->dropForeignKey('fk_adv_orders_clients1', 'adv_orders');
        $this->dropForeignKey('fk_adv_calendar_adv_mesh1', 'adv_calendar');
        $this->dropForeignKey('fk_adv_calendar_adv_orders1', 'adv_calendar');
        $this->dropForeignKey('fk_adv_usedmesh_adv_mesh1', 'adv_usedmesh');

        $this->delete('adv_orders');
        $this->delete('adv_calendar');
        $this->delete('adv_usedmesh');
        $this->delete('clients');
        $this->delete('adv_mesh');

        $this->dropTable('adv_settings');
        $this->dropTable('adv_orders');
        $this->dropTable('adv_calendar');
        $this->dropTable('adv_usedmesh');
        $this->dropTable('clients');
        $this->dropTable('adv_mesh');
    }

    public function safeDown() {
        echo "m170604_162248_delete_adv_timetable_old cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m170604_162248_delete_adv_timetable_old cannot be reverted.\n";

      return false;
      }
     */
}
