<?php

use yii\db\Migration;

class m170413_190516_add_paret_menulist extends Migration {

    public function safeUp() {
        $this->addColumn('menu_list', 'parent_id', 'MEDIUMINT(5) NULL');
    }

    public function safeDown() {
        $this->dropColumn('menu_list', 'parent_id');
        echo "m170413_190516_add_paret_menulist cannot be reverted.\n";
    }

}
