<?php

use yii\db\Migration;

/**
 * Class m171109_084759_change_banner_date
 */
class m171109_084759_change_banner_date extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('banner_item', 'from_date', $this->date()->notNull());
        $this->alterColumn('banner_item', 'to_date', $this->date()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171109_084759_change_banner_date cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171109_084759_change_banner_date cannot be reverted.\n";

        return false;
    }
    */
}
