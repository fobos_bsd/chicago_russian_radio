<?php

namespace console\controllers;

use Yii,
    yii\console\Controller,
    yii\helpers\Console,
    linslin\yii2\curl;
use common\models\PlaylistFiles,
    common\models\RadioArchive,
    common\models\Timetable;

class AddToArchivController extends Controller {

    const UPLOADPATH = 'http://archive.chicago1330.com/ru/';

    public $site;

    public function options($actionID) {
        return ['site'];
    }

    public function optionAliases() {
        return ['st' => 'site'];
    }

    public function actionRun() {
        $arrfiles = $this->parserHtml($this->getListFiles());

        if (count($arrfiles) > 0) {
            //$this->insetArchivData($arrfiles);
            $this->makeArchiv($arrfiles);
        }
    }

    private function getListFiles() {
        $curl = new curl\Curl();

        $user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0';

        $list_files = $curl->setOptions([CURLOPT_USERAGENT => $user_agent, CURLOPT_HEADER => 0, CURLOPT_FOLLOWLOCATION => 1, CURLOPT_RETURNTRANSFER => 1])->get(self::UPLOADPATH . date('Y') . '/' . date('m') . '/' . date('d'));
        if ($curl->errorCode === null) {
            return $list_files;
        } else {
            return '';
        }
    }

    private function parserHtml($html_body) {
        $arr_files = [];
        //$this->stdout($html_body . PHP_EOL, Console::FG_GREEN);
        preg_match_all('/\"live.*\.mp3\"/i', $html_body, $matches, PREG_OFFSET_CAPTURE);
        if (isset($matches[0]) && count($matches[0]) > 0) {
            foreach ($matches[0] as $match) {
                array_push($arr_files, str_replace('"', '', $match[0]));
            }
        }
        return $arr_files;
    }

    private function insetArchivData($arrfiles) {
        $day_of_week = $this->getDayOfWeek(date('w'));
        $timtable = Timetable::find()->where(['day_of_week' => $day_of_week, 'site_id' => $this->site, 'onair' => 1])->asArray()->all();

        foreach ($arrfiles as $filename) {
            $file = self::UPLOADPATH . date('Y') . '/' . date('m') . '/' . date('d') . '/' . $filename;
            $arrfile = explode('-', str_replace('.mp3', '', $filename));
            $afir_time = $arrfile[4] . ':' . $arrfile[5] . ':00';

            if (!PlaylistFiles::find()->where(['fileName' => $file])->one()) {
                foreach ($timtable as $item) {
                    if ($item['to_time'] === '00:00:00')
                        $item['to_time'] = '24:00:00';

                    if ($item['from_time'] <= $afir_time && $item['to_time'] > $afir_time) {
                        $model = new PlaylistFiles();
                        $model->scenario = 'console';
                        $model->name = $item['name'];
                        $model->youtube_playlists_id = $item['youtube_playlists_id'];
                        $model->radio_brodcast_day = date('Y-m-d');

                        $model->radio_brodcast_time = $afir_time;
                        $model->status = 1;
                        $model->long_time = 60;
                        $model->fileName = $file;
                        $model->insert();
                    }
                }
            }
        }
    }

    private function makeArchiv($arrfiles) {
        $day_of_week = $this->getDayOfWeek(date('w'));
        $timtable = Timetable::find()->where(['day_of_week' => $day_of_week, 'site_id' => $this->site])->asArray()->all();

        foreach ($arrfiles as $filename) {
            $file = self::UPLOADPATH . date('Y') . '/' . date('m') . '/' . date('d') . '/' . $filename;
            $arrfile = explode('-', str_replace('.mp3', '', $filename));
            $afir_time = $arrfile[4] . ':' . $arrfile[5] . ':00';

            if (!RadioArchive::find()->where(['fileName' => $file])->one()) {
                foreach ($timtable as $item) {
                    $onair = ($item['onair'] == 1) ? 1 : 0;
                    if ($item['to_time'] === '00:00:00')
                        $item['to_time'] = '24:00:00';

                    if ($item['from_time'] <= $afir_time && $item['to_time'] > $afir_time) {
                        $model = new RadioArchive();
                        $model->scenario = 'console';
                        $model->name = $item['name'];
                        $model->youtube_playlists_id = $item['youtube_playlists_id'];
                        $model->radio_brodcast_day = date('Y-m-d');
                        $model->radio_brodcast_time = $afir_time;
                        $model->onair = $onair;
                        $model->timetable_tags_id = (isset($item['timetable_tags_id']) && !empty($item['timetable_tags_id'])) ? $item['timetable_tags_id'] : null;
                        $model->fileName = $file;
                        $model->insert();
                    }
                }
            }
        }
    }

    protected function getDayOfWeek($number) {
        switch ($number) {
            case 7: $result = 'Su';
                break;
            case 1: $result = 'Mo';
                break;
            case 2: $result = 'Tu';
                break;
            case 3: $result = 'We';
                break;
            case 4: $result = 'Th';
                break;
            case 5: $result = 'Fr';
                break;
            case 6: $result = 'Sa';
                break;
            default: $result = 'Su';
                break;
        }

        return $result;
    }

}
