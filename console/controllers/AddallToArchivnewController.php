<?php

namespace console\controllers;

use Yii,
    yii\console\Controller,
    yii\helpers\Console,
    yii\helpers\ArrayHelper,
    linslin\yii2\curl;
use common\models\PlaylistFiles,
    common\models\RadioArchive,
    common\models\YoutubePlaylists;

class AddallToArchivnewController extends Controller {

    const UPLOADPATH = 'http://archive.chicago1330.com/archive/программы/';

    public $site;

    public function options($actionID) {
        return ['site'];
    }

    public function optionAliases() {
        return ['st' => 'site'];
    }

    public function actionRun() {

        $this->insetArchivData();


        $this->stdout('Import archive finished ' . PHP_EOL, Console::FG_GREEN);
    }

    private function getListFiles($folder_prog) {
        $curl = new curl\Curl();

        $user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0';

        $list_files = $curl->setOptions([CURLOPT_USERAGENT => $user_agent, CURLOPT_HEADER => 0, CURLOPT_FOLLOWLOCATION => 1, CURLOPT_RETURNTRANSFER => 1])->get(self::UPLOADPATH . $folder_prog);
        if ($curl->errorCode === null) {
            return $list_files;
        } else {
            return '';
        }
    }

    private function parserHtml($html_body) {
        $arr_files = [];
        //$this->stdout($html_body . PHP_EOL, Console::FG_GREEN);
        preg_match_all('/\"live.*\.mp3\"/i', $html_body, $matches, PREG_OFFSET_CAPTURE);
        if (isset($matches[0]) && count($matches[0]) > 0) {
            foreach ($matches[0] as $match) {
                array_push($arr_files, str_replace('"', '', $match[0]));
            }
        }
        return $arr_files;
    }

    private function insetArchivData() {
        $arr_titles = ArrayHelper::map(YoutubePlaylists::find()->where(['status' => 1])->asArray()->all(), 'id', 'archive_slug');
        $begin = new \DateTime("2017-06-09");
        $end = new \DateTime(date('Y-m-d'));

        if (isset($arr_titles) && is_array($arr_titles) && count($arr_titles) > 0) {
            for ($i = $begin; $i <= $end; $i->modify('+1 month')) {
                foreach ($arr_titles as $key => $title) {
                    $folder_prog = mb_strtolower(strip_tags(trim($title)));
                    $curdate = $i->format("Y.m");
                    $arrfiles = $this->parserHtml($this->getListFiles($folder_prog . '/' . $curdate));
                    if (isset($arrfiles) && is_array($arrfiles) && count($arrfiles) > 0) {
                        foreach ($arrfiles as $filename) {
                            $file = self::UPLOADPATH . $folder_prog . '/' . $curdate . '/' . $filename;
                            $arrfile = explode('-', str_replace('.mp3', '', $filename));
                            $afir_time = $arrfile[4] . ':' . $arrfile[5] . ':00';
                            $afir_date = $arrfile[1] . '-' . $arrfile[2] . '-' . $arrfile[3];

                            if (!PlaylistFiles::find()->where(['fileName' => $file])->one()) {

                                $model = new PlaylistFiles();
                                $model->scenario = 'console';
                                $model->name = 'Программа от ';
                                $model->youtube_playlists_id = $key;
                                $model->radio_brodcast_day = $afir_date;
                                $model->radio_brodcast_time = $afir_time;
                                $model->status = 1;
                                $model->long_time = null;
                                $model->fileName = $file;
                                if (!$model->insert()) {
                                    var_dump($model->getErrors());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected function getDayOfWeek($number) {
        switch ($number) {
            case 7: $result = 'Su';
                break;
            case 1: $result = 'Mo';
                break;
            case 2: $result = 'Tu';
                break;
            case 3: $result = 'We';
                break;
            case 4: $result = 'Th';
                break;
            case 5: $result = 'Fr';
                break;
            case 6: $result = 'Sa';
                break;
            default: $result = 'Su';
                break;
        }

        return $result;
    }

}
