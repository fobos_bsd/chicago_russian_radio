<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ]
    ],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/timetables',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'GET today-timetable' => 'today-timetable',
                        'OPTIONS today-timetable' => 'today-timetable',
                        'GET timetable' => 'timetable',
                        'OPTIONS timetable' => 'timetable',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/programmes',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'GET all-programmes' => 'all-programmes',
                        'GET programm' => 'programm',
                        'GET programm-archive' => 'programm-archive',
                        'GET programms-archive' => 'programms-archive',
                        'OPTIONS all-programmes' => 'all-programmes',
                        'OPTIONS programm' => 'programm',
                        'OPTIONS programm-archive' => 'programm-archive',
                        'OPTIONS programms-archive' => 'programms-archive'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/contacts',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST add-message' => 'add-message',
                        'OPTIONS add-message' => 'add-message',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/seo',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'GET page' => 'page',
                        'OPTIONS page' => 'page',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/votes',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST current-vote' => 'current-vote',
                        'POST add-answer' => 'add-answer',
                        'OPTIONS current-vote' => 'current-vote',
                        'OPTIONS add-answer' => 'add-answer',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/songs',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'GET index' => 'index',
                        'OPTIONS index' => 'index',
                        'POST vote' => 'vote',
                        'OPTIONS vote' => 'vote',
                        'POST add-soungs' => 'add-soungs',
                        'OPTIONS add-soungs' => 'add-soungs',
                        'POST update-soungs' => 'update-soungs',
                        'OPTIONS update-soungs' => 'update-soungs',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/top-videos',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'GET index' => 'index',
                        'OPTIONS index' => 'index',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/radioquiz',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST index' => 'index',
                        'OPTIONS index' => 'index',
                        'POST archive' => 'archive',
                        'OPTIONS archive' => 'archive',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/gallery',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'GET index' => 'index',
                        'OPTIONS index' => 'index',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/votesms',
                    'extraPatterns' => [
                        'POST parser-sms' => 'parser-sms',
                        'GET parser-sms' => 'parser-sms',
                        'POST current-votes' => 'current-votes',
                        'OPTIONS parser-sms' => 'parser-sms',
                        'OPTIONS parser-sms' => 'parser-sms',
                        'POPTIONS current-votes' => 'current-votes',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/questions',
                    'extraPatterns' => [
                        'POST get-question' => 'get-question',
                        'POST add-question' => 'add-question',
                        'POST send-question' => 'send-question',
                        'OPTIONS get-question' => 'get-question',
                        'OPTIONS add-question' => 'add-question',
                        'OPTIONS send-question' => 'send-question',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/airvotes',
                    'extraPatterns' => [
                        'POST get-vote' => 'get-vote',
                        'POST add-voice' => 'add-voice',
                    ],
                ],
            ],
        ],
        'Siteinfo' => 'frontend\components\Siteinfo',
    ],
    'params' => $params,
];
