<?php

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\VerbFilter,
    yii\helpers\Json;
use common\models\OnlineVotes,
    common\models\CurrentSmsVote;

class VotesmsController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'only' => ['parser-sms', 'current-vote'],
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'parser-sms' => ['post', 'get'],
                'current-vote' => ['post']
            ]
        ];

        // Authentication
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    // Get sms and parsing it
    public function actionParserSms() {
        $result = [
            'status' => 'nosms',
            'error' => 1
        ];

        // File log sms
        $fileSms = Yii::getAlias('@api/web/uploads/sms.log');

        // If is post request
        if (Yii::$app->request->post()) {
            $smsmessage = Yii::$app->request->post();
        }

        // If get request
        if (Yii::$app->request->get()) {
            $smsmessage = Yii::$app->request->get();
        }

        // Test is array data
        if (isset($smsmessage) && isset($smsmessage['text']) && isset($smsmessage['from'])) {
            $wote = (int) trim(strip_tags($smsmessage['text']));
            $phone = trim(strip_tags($smsmessage['from']));
            $objvote = $this->findModel();

            if (isset($objvote->id) && isset($wote) && isset($phone) && !empty($wote) && !empty($phone)) {
                $model = new CurrentSmsVote();
                $model->online_votes_id = $objvote->id;
                $model->phone = $phone;
                $model->vote_sms = $wote;
                $model->created_at = time();

                if (!$model->insert()) {
                    $message = 'Error vote: ' . $model->getErrors();
                }
            }
        }

        if (file_exists($fileSms) && isset($message) && !empty($message)) {
            $fl = fopen($fileSms, 'w+');
            fwrite($fl, $message);
            fclose($fl);
        }

        return $result;
    }

    /**
     * Finds the Votes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Votes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel() {
        if (($model = OnlineVotes::find()->where(['status' => 1, 'vtype' => 'sms'])->andWhere(['>=', 'finish_at', time()])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // Send current Air vote
    public function actionCurrentVotes() {
        $result = [
            'status' => 'noactive',
            'error' => 'Not data or server error',
            'type' => '',
            'data' => []
        ];

        $vote_phone = $this->findVotePhone();
        $vote_sms = $this->findVoteSms();

        if (isset($vote_sms)) {
            $params = (array) Json::decode($vote_sms->params);
            foreach ($params as $key => $val) {
                $arrparams = (array) $val;
            }
            $result = [
                'status' => 'active',
                'error' => 'No error',
                'type' => 'sms',
                'data' => [
                    'id' => $vote_sms->id,
                    'name' => $vote_sms->name,
                    'params' => $arrparams
                ]
            ];
        } elseif (!isset($vote_sms) && isset($vote_phone)) {
            $result = [
                'status' => 'active',
                'error' => 'No error',
                'type' => 'phone',
                'data' => [
                    'id' => $vote_phone->id,
                    'name' => $vote_phone->name,
                    'params' => (array) Json::decode($vote_phone->params)
                ]
            ];
        } else {
            $result = [
                'status' => 'noactive',
                'error' => 'No data',
                'type' => '',
                'data' => []
            ];
        }

        return $result;
    }

    // Finds the Votes model based on its primary key value.
    private function findVotePhone() {
        if (($model = OnlineVotes::find()->where(['status' => 1, 'vtype' => 'phone'])->andWhere(['<=', 'start_at', time() - 5 * 3600])->andWhere(['>=', 'finish_at', time() - 5 * 3600])->one()) !== null) {
            return $model;
        } else {
            return null;
        }
    }

    // Finds the Votes model based on its primary key value.
    private function findVoteSms() {
        if (($model = OnlineVotes::find()->where(['status' => 1, 'vtype' => 'sms'])->andWhere(['<=', 'start_at', time() - 5 * 3600])->andWhere(['>=', 'finish_at', time() - 5 * 3600])->one()) !== null) {
            return $model;
        } else {
            return null;
        }
    }

}
