<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper,
    yii\filters\VerbFilter;
use common\models\Gallery,
    common\models\GalleryData;

/**
 * Description of GalleryController
 *
 * @author fobos
 */
class GalleryController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'],
            'rules' => [
                [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => ['?', '*'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Allow-Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['get', 'options'],
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['add-soungs', 'update-soungs'],
            'except' => ['options']
        ];

        return $behaviors;
    }

    // Get images of gallery
    public function actionIndex() {
        if (Yii::$app->request->isGet) {

            $get = Yii::$app->request->get();
            $content = [];

            if (isset($get['gallery_name']) && !empty($get['gallery_name'])) {
                $gallery = strip_tags(trim($get['gallery_name']));

                // Get gallery ID by slug
                $gallery_id = ArrayHelper::getValue(Gallery::find()->where(['slug' => $gallery])->asArray()->one(), 'id');
                $galleryID = (isset($gallery_id)) ? (int) $gallery_id : 0;

                // Get all images of the gallery
                $content = GalleryData::find()->select(['name', 'fileName', 'title', 'link'])->where(['gallery_id' => $galleryID, 'status' => 1])->orderBy(['gravity' => SORT_DESC, 'name' => SORT_ASC])->asArray()->all();
            }

            $result['error'] = '';
            $result['content'] = $content;
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

}
