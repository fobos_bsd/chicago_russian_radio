<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\filters\VerbFilter;
use common\models\Timetable;

/**
 * Description of TimetablesController
 *
 * @author fobos
 */
class TimetablesController extends Controller {

    const DAYS_OF_WEEK = [
        0 => 'Su',
        1 => 'Mo',
        2 => 'Tu',
        3 => 'We',
        4 => 'Th',
        5 => 'Fr',
        6 => 'Sa'
    ];

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['today-timetable', 'timetable'],
            'rules' => [
                [
                    'actions' => ['today-timetable', 'timetable'],
                    'allow' => true,
                    'roles' => ['?','*'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Allow-Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'today-timetable' => ['get', 'options'],
                'timetable' => ['get', 'options'],
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];


        /*
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['options']
        ];
         * 
         */


        return $behaviors;
    }

    // Get current timetables
    public function actionTodayTimetable() {
        if (Yii::$app->request->isGet) {

            // Get curent timetable program status
            if (($model_tt = Timetable::find()->where(['<=', 'from_time', date('H:i:s')])->andWhere(['>=', 'to_time', date('H:i:s')])->andWhere(['day_of_week' => self::DAYS_OF_WEEK[date('w')]])->andWhere(['or', ['timetable_tags_id' => 1], ['onair' => 1]])->one()) !== null) {
                $live = 1;
            } else {
                $live = 0;
            }

            // Set global site params
            $siteID = 1;
            $siteLang = 2;

            // Set current week day
            $daynumber = date('w');
            switch ($daynumber) {
                case 0: $day = 'Su';
                    break;
                case 1: $day = 'Mo';
                    break;
                case 2: $day = 'Tu';
                    break;
                case 3: $day = 'We';
                    break;
                case 4: $day = 'Th';
                    break;
                case 5: $day = 'Fr';
                    break;
                case 6: $day = 'Sa';
                    break;
            }

            $timeafter = Timetable::find()->select(['timetable.*', 'youtube_playlists.thumbnails_medium'])
                            ->leftJoin('youtube_playlists', 'timetable.youtube_playlists_id = youtube_playlists.id')
                            ->where(['site_id' => $siteID, 'lang_id' => $siteLang, 'day_of_week' => $day])
                            ->andWhere(['<=', 'timetable.from_time', date('H:i:s', time())])
                            ->andWhere(['>=', 'timetable.to_time', date('H:i:s', time())])
                            ->orderBy(['timetable.to_time' => SORT_ASC])
                            ->asArray()->all();
            $timedeafore = Timetable::find()->select(['timetable.*', 'youtube_playlists.thumbnails_medium'])
                            ->leftJoin('youtube_playlists', 'timetable.youtube_playlists_id = youtube_playlists.id')
                            ->where(['timetable.site_id' => $siteID, 'timetable.lang_id' => $siteLang, 'timetable.day_of_week' => $day])
                            ->andWhere(['>', 'timetable.from_time', date('H:i:s', time())])->orderBy(['timetable.from_time' => SORT_ASC])
                            ->asArray()->all();
            $timefinish = Timetable::find()->select(['timetable.*', 'youtube_playlists.thumbnails_medium'])
                            ->leftJoin('youtube_playlists', 'timetable.youtube_playlists_id = youtube_playlists.id')
                            ->where(['site_id' => $siteID, 'lang_id' => $siteLang, 'day_of_week' => $day])
                            ->andWhere(['<', 'timetable.to_time', date('H:i:s', time())])->orderBy(['timetable.from_time' => SORT_ASC])
                            ->asArray()->all();

            $result['error'] = '';
            $result['content'] = array_merge($timeafter, $timedeafore, $timefinish);
            $result['live'] = $live;
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

    // Get full timetable
    public function actionTimetable() {
        if (Yii::$app->request->isGet) {
            $result = array();
            $timetable = Timetable::find()->with('timetableTags')->where(['site_id' => 1, 'lang_id' => 2])->orderBy(['from_time' => SORT_ASC, 'day_of_week' => SORT_ASC, 'name' => SORT_ASC])->asArray()->all();
            foreach ($timetable as $timetb) {
                switch ($timetb['day_of_week']) {
                    case 'Mo': $result['mo'][] = [
                            'from_time' => $timetb['from_time'],
                            'to_time' => $timetb['to_time'],
                            'name' => $timetb['name'],
                            'onair' => $timetb['onair'],
                            'description' => $timetb['description'],
                            'programm_id' => $timetb['youtube_playlists_id'],
                            'fileImage' => $timetb['fileImage'],
                            'tags' => $timetb['timetableTags']['name']
                        ];
                        break;
                    case 'Tu': $result['tu'][] = [
                            'from_time' => $timetb['from_time'],
                            'to_time' => $timetb['to_time'],
                            'name' => $timetb['name'],
                            'onair' => $timetb['onair'],
                            'description' => $timetb['description'],
                            'programm_id' => $timetb['youtube_playlists_id'],
                            'fileImage' => $timetb['fileImage'],
                            'tags' => $timetb['timetableTags']['name']
                        ];
                        break;
                    case 'We': $result['we'][] = [
                            'from_time' => $timetb['from_time'],
                            'to_time' => $timetb['to_time'],
                            'name' => $timetb['name'],
                            'onair' => $timetb['onair'],
                            'description' => $timetb['description'],
                            'programm_id' => $timetb['youtube_playlists_id'],
                            'fileImage' => $timetb['fileImage'],
                            'tags' => $timetb['timetableTags']['name']
                        ];
                        break;
                    case 'Th': $result['th'][] = [
                            'from_time' => $timetb['from_time'],
                            'to_time' => $timetb['to_time'],
                            'name' => $timetb['name'],
                            'onair' => $timetb['onair'],
                            'description' => $timetb['description'],
                            'programm_id' => $timetb['youtube_playlists_id'],
                            'fileImage' => $timetb['fileImage'],
                            'tags' => $timetb['timetableTags']['name']
                        ];
                        break;
                    case 'Fr': $result['fr'][] = [
                            'from_time' => $timetb['from_time'],
                            'to_time' => $timetb['to_time'],
                            'name' => $timetb['name'],
                            'onair' => $timetb['onair'],
                            'description' => $timetb['description'],
                            'programm_id' => $timetb['youtube_playlists_id'],
                            'fileImage' => $timetb['fileImage'],
                            'tags' => $timetb['timetableTags']['name']
                        ];
                        break;
                    case 'Sa': $result['sa'][] = [
                            'from_time' => $timetb['from_time'],
                            'to_time' => $timetb['to_time'],
                            'name' => $timetb['name'],
                            'onair' => $timetb['onair'],
                            'description' => $timetb['description'],
                            'programm_id' => $timetb['youtube_playlists_id'],
                            'fileImage' => $timetb['fileImage'],
                            'tags' => $timetb['timetableTags']['name']
                        ];
                        break;
                    case 'Su': $result['su'][] = [
                            'from_time' => $timetb['from_time'],
                            'to_time' => $timetb['to_time'],
                            'name' => $timetb['name'],
                            'onair' => $timetb['onair'],
                            'description' => $timetb['description'],
                            'programm_id' => $timetb['youtube_playlists_id'],
                            'fileImage' => $timetb['fileImage'],
                            'tags' => $timetb['timetableTags']['name']
                        ];
                        break;
                }
            }

            $results['error'] = '';
            $results['content'] = $result;
            Yii::$app->response->statusCode = 200;
            return $results;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

}
