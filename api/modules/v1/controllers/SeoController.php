<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\filters\VerbFilter;
use common\models\SeoContent;

/**
 * Description of TimetablesController
 *
 * @author fobos
 */
class SeoController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['page'],
            'rules' => [
                [
                    'actions' => ['page'],
                    'allow' => true,
                    'roles' => ['?', '*'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Allow-Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'page' => ['get', 'options'],
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];


        /*
          $behaviors['authenticator'] = [
          'class' => HttpBearerAuth::className(),
          'except' => ['options']
          ];
         * 
         */


        return $behaviors;
    }

    // Get page`s seo data
    public function actionPage() {
        if (Yii::$app->request->isGet) {
            
            $alias = Yii::$app->request->get('alias');

            $link = (isset($alias) && !empty($alias)) ? $link = strip_tags(trim($alias)) : '';

            $result['error'] = '';
            $result['content'] = SeoContent::findOne(['page_name' => $alias]);
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

}
