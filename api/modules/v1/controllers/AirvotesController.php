<?php

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\VerbFilter,
    yii\helpers\Json;
use common\models\OnlineVotes,
    common\models\CurrentPhoneVote,
    common\models\CurrentSmsVote;

class AirvotesController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'only' => ['get-vote', 'add-voice'],
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'get-vote' => ['post'],
                'add-voice' => ['post']
            ]
        ];

        // Authentication
        /*
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];
         * 
         */

        return $behaviors;
    }

    // Send current Air vote
    public function actionGetVote() {
        $result = [
            'status' => 'noactive',
            'error' => 'Not data or server error',
            'type' => '',
            'data' => []
        ];

        $vote_phone = $this->findVotePhone();
        $vote_sms = $this->findVoteSms();

        if (isset($vote_sms)) {
            $params = (array) Json::decode($vote_sms->params);
            foreach ($params as $key => $val) {
                $arrparams = (array) $val;
            }
            $result = [
                'status' => 'active',
                'error' => 'No error',
                'type' => 'sms',
                'data' => [
                    'id' => $vote_sms->id,
                    'name' => $vote_sms->name,
                    'params' => $arrparams
                ]
            ];
        } elseif (!isset($vote_sms) && isset($vote_phone)) {
            $result = [
                'status' => 'active',
                'error' => 'No error',
                'type' => 'phone',
                'data' => [
                    'id' => $vote_phone->id,
                    'name' => $vote_phone->name,
                    'params' => (array) Json::decode($vote_phone->params)
                ]
            ];
        } else {
            $result = [
                'status' => 'noactive',
                'error' => 'No data',
                'type' => '',
                'data' => []
            ];
        }

        return $result;
    }

    // Set voice to current vote
    public function actionAddVoice() {

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            
            $type = $post['type'];
            $data = $post['data'][0];

            $isExistVote = $this->isActiveVote((int) $data['online_votes_id']);
            $isMakeSms = $this->isMakeVoteSms((int) $data['online_votes_id'], strip_tags(trim($data['phone'])));
            $isMakePhone = $this->isMakeVotePhone((int) $data['online_votes_id'], strip_tags(trim($data['phone'])));

            if ($isMakePhone !== '0' || $isMakeSms !== '0') {
                return [
                    'status' => 'stop',
                    'error' => 'You are voted olready',
                ];
            }

            if (isset($isExistVote) && $isExistVote != 0) {
                switch ($type) {
                    case 'sms': $model = new CurrentSmsVote();
                        $model->vote_sms = (int) $data['vote_sms'];
                        break;
                    case 'phone': $model = new CurrentPhoneVote();
                        $model->vote_phone = (int) $data['vote_phone'];
                        break;
                    default: $model = new CurrentPhoneVote();
                        break;
                }

                $model->online_votes_id = (int) $data['online_votes_id'];
                $model->phone = strip_tags(trim($data['phone']));

                $model->created_at = time() - 5 * 3600;

                if ($model->save()) {
                    return [
                        'status' => 'success',
                        'error' => 'No error',
                    ];
                } else {
                    return [
                        'status' => 'wrong',
                        'error' => 'Error add vote',
                    ];
                }
            } else {
                return [
                    'status' => 'miss',
                    'error' => 'No active vote',
                ];
            }
        } else {
            return [
                'status' => 'miss',
                'error' => 'No data',
            ];
        }
    }

    // Finds the Phone votes model based on its primary key value.
    private function findVotePhone() {
        if (($model = OnlineVotes::find()->where(['status' => 1, 'vtype' => 'phone'])->andWhere(['<=', 'start_at', time() - 5 * 3600])->andWhere(['>=', 'finish_at', time() - 5 * 3600])->one()) !== null) {
            return $model;
        } else {
            return null;
        }
    }

    // Finds the Votes model based on its primary key value.
    private function findVoteSms() {
        if (($model = OnlineVotes::find()->where(['status' => 1, 'vtype' => 'sms'])->andWhere(['<=', 'start_at', time() - 5 * 3600])->andWhere(['>=', 'finish_at', time() - 5 * 3600])->one()) !== null) {
            return $model;
        } else {
            return null;
        }
    }

    // Finds the Votes model based on its primary key value.
    private function isActiveVote($id) {
        if (($model = OnlineVotes::find()->where(['status' => 1, 'id' => $id])->andWhere(['<=', 'start_at', time() - 5 * 3600])->andWhere(['>=', 'finish_at', time() - 5 * 3600])->count()) !== null) {
            return $model;
        } else {
            return null;
        }
    }

    // Finds the Votes phone model based on its primary key value.
    private function isMakeVotePhone($id, $phone) {
        if (($model = CurrentPhoneVote::find()->where(['phone' => $phone, 'online_votes_id' => $id])->count()) !== null) {
            return $model;
        } else {
            return null;
        }
    }

    // Finds the Votes SMS model based on its primary key value.
    private function isMakeVoteSms($id, $phone) {
        if (($model = CurrentSmsVote::find()->where(['phone' => $phone, 'online_votes_id' => $id])->count()) !== null) {
            return $model;
        } else {
            return null;
        }
    }

}
