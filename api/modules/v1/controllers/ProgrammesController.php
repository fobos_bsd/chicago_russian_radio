<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\filters\VerbFilter,
    yii\helpers\ArrayHelper,
    yii\data\Pagination;
use common\models\YoutubeChannels,
    common\models\YoutubePlaylists,
    common\models\PlaylistFiles,
    common\models\RadioArchive;

/**
 * Description of TimetablesController
 *
 * @author fobos
 */
class ProgrammesController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['all-programmes', 'programm', 'programm-archive', 'programms-archive'],
            'rules' => [
                [
                    'actions' => ['all-programmes', 'programm', 'programm-archive', 'programms-archive'],
                    'allow' => true,
                    'roles' => ['?','*'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Allow-Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all-programmes' => ['get', 'options'],
                'programm' => ['get', 'options'],
                'programm-archive' => ['get', 'options'],
                'programms-archive' => ['get', 'options']
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        /*
          $behaviors['authenticator'] = [
          'class' => HttpBearerAuth::className(),
          'only' => ['index', 'item']
          ];
         * 
         */

        return $behaviors;
    }

    // Get all programmes
    public function actionAllProgrammes() {
        if (Yii::$app->request->isGet) {

            $get = Yii::$app->request->get();

            $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
            $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;

            $arrchanl = YoutubeChannels::find()->where(['status' => 1, 'site_id' => 1])->asArray()->all();

            // Get all channels of this site
            $chanelsArr = ArrayHelper::getColumn($arrchanl, 'id');
            if (isset($chanelsArr) && count($chanelsArr) > 0) {
                $query = YoutubePlaylists::find()->select(['id', 'title', 'description', 'thumbnails_medium', 'thumbnails_maxres', 'meta_key', 'meta_description', 'etag'])->where(['youtube_channels_id' => $chanelsArr, 'status' => 1])->orderBy(['priority' => SORT_ASC, 'publish_at' => SORT_DESC, 'title' => SORT_ASC]);

                $countQuery = clone $query;
                $tottal_pages = (int) $countQuery->count();
                $pages = new Pagination(['totalCount' => $tottal_pages]);
                $pages->defaultPageSize = $count_per_page;
                $pages->page = $page;
                $contents = $query->offset($pages->offset)
                        ->limit($pages->limit)
                        ->asArray()
                        ->all();
            } else {
                $contents = null;
            }

            $result['error'] = '';
            $result['content'] = $contents;
            $result['count_all_parts'] = YoutubePlaylists::find()->where(['status' => 1])->count();
            $result['count_pages'] = ((int) $tottal_pages > 0) ? ceil((int) $tottal_pages / (int) $count_per_page) : 0;
            $result['per_page'] = $count_per_page;
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

    // Get the programm data
    public function actionProgramm() {
        if (Yii::$app->request->isGet) {

            $get = Yii::$app->request->get();
            $plid = (isset($get['id'])) ? (int) $get['id'] : 0;

            // Get playlist data
            $content = YoutubePlaylists::find()->select(['id', 'title', 'thumbnails_medium', 'thumbnails_maxres', 'description', 'etag'])->where(['id' => $plid])->one();

            $results['error'] = '';
            $results['content'] = $content;
            Yii::$app->response->statusCode = 200;
            return $results;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

    // Get programm`s archive
    public function actionProgrammArchive() {
        if (Yii::$app->request->isGet) {

            $get = Yii::$app->request->get();
            $plid = (isset($get['id'])) ? (int) $get['id'] : 0;

            $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
            $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;

            // Get audio files
            $query = PlaylistFiles::find()->select(['id', 'name', 'fileName', 'long_time', 'radio_brodcast_day', 'radio_brodcast_time'])->where(['youtube_playlists_id' => $plid, 'status' => 1])->orderBy(['id' => SORT_DESC, 'name' => SORT_ASC]);

            $countQuery = clone $query;
            $tottal_pages = (int) $countQuery->count();
            $pages = new Pagination(['totalCount' => $tottal_pages]);
            $pages->defaultPageSize = $count_per_page;
            $pages->page = $page;
            $content = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    ->asArray()
                    ->all();

            $results['error'] = '';
            $results['content'] = $content;
            $results['count_all_parts'] = PlaylistFiles::find()->where(['youtube_playlists_id' => $plid, 'status' => 1])->count();
            $results['count_pages'] = ((int) $tottal_pages > 0) ? ceil((int) $tottal_pages / (int) $count_per_page) : 0;
            $results['per_page'] = $count_per_page;
            Yii::$app->response->statusCode = 200;
            return $results;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

    // Get all programm`s archive
    public function actionProgrammsArchive() {
        if (Yii::$app->request->isGet) {

            $get = Yii::$app->request->get();

            $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
            $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;

            $query = RadioArchive::find()->orderBy(['radio_brodcast_day' => SORT_DESC, 'radio_brodcast_time' => SORT_DESC]);

            if (isset($get['program_id']) && !empty($get['program_id'])) {
                $filter_id = (int) $get['program_id'];
                $query->andFilterWhere(['youtube_playlists_id' => $filter_id]);
            }
            if (isset($get['start_date']) && !empty($get['start_date'])) {
                $filter_start = Yii::$app->formatter->asDate(strip_tags(trim($get['start_date'])), 'php:Y-m-d');
                $filter_end = (isset($get['end_date']) && !empty($get['end_date'])) ? Yii::$app->formatter->asDate(strip_tags(trim($get['end_date'])), 'php:Y-m-d') : '2100-01-01';
                
                $query->andFilterWhere(['between', 'radio_brodcast_day', $filter_start, $filter_end]);
            }

            $countQuery = clone $query;
            $tottal_pages = (int) $countQuery->count();
            $pages = new Pagination(['totalCount' => $tottal_pages]);
            $pages->defaultPageSize = $count_per_page;
            $pages->page = $page;
            $content = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    ->asArray()
                    ->all();

            $results['error'] = '';
            $results['content'] = $content;
            $results['count_all_parts'] = RadioArchive::find()->count();
            $results['count_pages'] = ((int) $tottal_pages > 0) ? ceil((int) $tottal_pages / (int) $count_per_page) : 0;
            $results['per_page'] = $count_per_page;
            Yii::$app->response->statusCode = 200;
            return $results;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

}
