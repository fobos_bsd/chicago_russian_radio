<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\filters\VerbFilter,
    yii\data\Pagination;
use common\models\TopVideos;

/**
 * Description of TimetablesController
 *
 * @author fobos
 */
class TopVideosController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'],
            'rules' => [
                [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => ['?', '*'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Allow-Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['get', 'options'],
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];


        /*
          $behaviors['authenticator'] = [
          'class' => HttpBearerAuth::className(),
          'except' => ['options']
          ];
         * 
         */


        return $behaviors;
    }

    // Get page`s seo data
    public function actionIndex() {
        if (Yii::$app->request->isGet) {

            $get = Yii::$app->request->get();

            $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
            $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;

            $query = TopVideos::find()->orderBy(['rank' => SORT_DESC, 'title' => SORT_ASC]);

            $countQuery = clone $query;
            $tottal_pages = (int) $countQuery->count();
            $pages = new Pagination(['totalCount' => $tottal_pages]);
            $pages->defaultPageSize = $count_per_page;
            $pages->page = $page;
            $contents = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    ->asArray()
                    ->all();

            $result['error'] = '';
            $result['content'] = $contents;
            $result['count_all_parts'] = TopVideos::find()->count();
            $result['count_pages'] = ((int) $tottal_pages > 0) ? ceil((int) $tottal_pages / (int) $count_per_page) : 0;
            $result['per_page'] = $count_per_page;
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

}
