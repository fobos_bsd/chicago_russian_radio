<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\filters\VerbFilter,
    yii\helpers\ArrayHelper;
use common\models\Questions,
    common\models\Votes;
use api\models\VoteForm;

/**
 * Description of TimetablesController
 *
 * @author fobos
 */
class VotesController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['current-vote', 'add-answer'],
            'rules' => [
                [
                    'actions' => ['current-vote', 'add-answer'],
                    'allow' => true,
                    'roles' => ['?','*'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Allow-Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'current-vote' => ['post', 'options'],
                'add-answer' => ['post', 'options'],
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        /*
          $behaviors['authenticator'] = [
          'class' => HttpBearerAuth::className(),
          'only' => ['index', 'item']
          ];
         * 
         */

        return $behaviors;
    }

    // Get current active vote
    public function actionCurrentVote() {
        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();
            $ip = (isset($post['ip'])) ? $post['ip'] : '127.0.0.1';

            $question = Questions::find()->with(['answers'])->where(['active' => 1])->groupBy(['id'])->one();

            $result = false;
            if ($question) {
                $arr_results = Votes::find()->select(['answers_id', 'COUNT(*) as count_answers'])->where(['questions_id' => $question->id])->groupBy(['answers_id'])->asArray()->all();

                //If cookie empty check ip in database
                if (!$result) {
                    $result = Votes::find()->select(['answers_id'])->where(['questions_id' => $question->id, 'ip' => $ip])->one();
                }
            }

            // Get tottal answers
            $totlal_result = Votes::find()->where(['questions_id' => $question->id])->count();

            $answers = (isset($question->answers)) ? $question->answers : array();
            $results['error'] = '';
            $results['question'] = $question;
            $results['answers'] = $answers;
            $results['user_result'] = $result;
            $results['all_results'] = $arr_results;
            $results['totlal_result'] = $totlal_result;

            Yii::$app->response->statusCode = 200;
            return $results;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

    // Add user`s answer
    public function actionAddAnswer() {
        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();
            $model = new VoteForm();
            $model->site_id = 1;
            $model->ip = (isset($post['ip']) && !empty($post['ip'])) ? strip_tags(trim($post['ip'])) : '127.0.0.1';
            $model->questionId = (isset($post['questionId']) && !empty($post['questionId'])) ? (int) $post['questionId'] : null;
            $model->answerId = (isset($post['answerId']) && !empty($post['answerId'])) ? (int) $post['answerId'] : null;

            if ($model->validate()) {
                if ($model->vote()) {
                    $results['error'] = '';
                    $results['content'] = 'Success';
                } else {
                    $results['error'] = 'Error add vote';
                    $results['content'] = 'miss';
                }
            } else {
                $results['error'] = (array) $model->getErrors();
                $results['content'] = 'miss';
            }


            Yii::$app->response->statusCode = 200;
            return $results;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

}
