<?php

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\VerbFilter,
    yii\helpers\Json;
use common\models\Timetable,
    common\models\OnlineQuestions,
    common\models\UserQuestions;

class QuestionsController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'only' => ['get-question', 'add-question', 'send-question'],
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'get-question' => ['post'],
                'add-question' => ['post'],
                'send-question' => ['post']
            ]
        ];

        // Authentication
        /*
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];
        */

        return $behaviors;
    }

    public function actionGetQuestion() {

        return $this->getProgram();
    }

    // Add question
    public function actionAddQuestion() {
        $result = [
            'status' => 0,
            'error' => 'No error',
        ];

        // Curent site ID
        $siteArr = $this->getSiteInfo();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            
            $data = $post['data'][0];

            $model = new OnlineQuestions();
            $model->username = strip_tags(trim($data['username']));
            $model->question = strip_tags(trim($data['question']));
            $model->phone = (isset($data['phone'])) ? strip_tags(trim($data['phone'])) : '';

            $model->created_at = time();
            $model->site_id = $siteArr['id'];

            if ($model->save()) {
                $model->sendEmail();

                $result = [
                    'status' => 1,
                    'erorr' => 'No Error',
                ];
            } else {
                $result = [
                    'status' => 0,
                    'erorr' => 'Data not send',
                ];
            }
        }

        return $result;
    }

    // Send question to radio
    public function actionSendQuestion() {
        $result = [
            'status' => 0,
            'error' => 'No error',
        ];

        // Curent site ID
        $siteArr = $this->getSiteInfo();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $data = $post['data'];

            $model = new UserQuestions();
            $model->username = strip_tags(trim($data['username']));
            $model->email = strip_tags(trim($data['email']));
            $model->question = strip_tags(trim($data['question']));
            $model->phone = (isset($data['phone'])) ? strip_tags(trim($data['phone'])) : '';

            $model->created_at = time();
            $model->site_id = $siteArr['id'];

            if ($model->save()) {
                $model->sendEmail();

                $result = [
                    'status' => 1,
                    'erorr' => 'No Error',
                ];
            } else {
                $result = [
                    'status' => 0,
                    'erorr' => 'Data not send',
                ];
            }
        }

        return $result;
    }

    // Get online program
    private function getProgram() {
        $day = substr(date('D', time()), 0, 2);
        $time = date('H:i:s', time());

        // Curent site ID
        $siteArr = $this->getSiteInfo();
        $name = Timetable::find()->select('name')->where(['day_of_week' => $day, 'site_id' => $siteArr['id'], 'lang_id' => $siteArr['ln']])->andWhere(['<=', 'from_time', $time])->andWhere(['>=', 'to_time', $time])->one();

        $result = [
            'status' => 1,
            'error' => 'No error',
            'name' => (isset($name)) ? $name : Yii::t('frontend', 'Музыка в эфире')
        ];

        return $result;
    }

    // Get site info
    private function getSiteInfo() {
        // Curent site ID
        $siteArr = Yii::$app->Siteinfo->getSite();
        $site['id'] = (isset($siteArr['id'])) ? $siteArr['id'] : 1;
        $site['ln'] = (isset($siteArr['lang'])) ? $siteArr['lang'] : 'ru';

        return $site;
    }

}
