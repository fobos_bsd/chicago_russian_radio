<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\filters\VerbFilter;
use api\models\ContactForm;

/**
 * Description of TimetablesController
 *
 * @author fobos
 */
class ContactsController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['add-message'],
            'rules' => [
                [
                    'actions' => ['add-message'],
                    'allow' => true,
                    'roles' => ['?','*'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Allow-Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'add-message' => ['post', 'options'],
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        /*
          $behaviors['authenticator'] = [
          'class' => HttpBearerAuth::className(),
          'only' => ['index', 'item']
          ];
         * 
         */

        return $behaviors;
    }

    // Add message from user
    public function actionAddMessage() {
        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();

            $model = new ContactForm();
            $model->subject = Yii::t('frontend', 'Feedback and wishes');
            $model->name = (isset($post['name']) && !empty($post['name'])) ? strip_tags(trim($post['name'])) : null;
            $model->email = (isset($post['email']) && !empty($post['email'])) ? strip_tags(trim($post['email'])) : null;
            $model->body = (isset($post['message']) && !empty($post['message'])) ? strip_tags(trim($post['message'])) : null;

            if ($model->validate()) {

                $email = 'radio@ethnicmedia.us';

                if ($model->sendEmail($email)) {
                    $result['error'] = '';
                    $result['content'] = 'success';
                } else {
                    $result['error'] = 'There was an error sending your message.';
                    $result['content'] = 'miss';
                }
            } else {
                $result['error'] = (array) $model->getErrors();
                $result['content'] = 'miss';
            }

            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

}
