<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\filters\VerbFilter;
use common\models\Songs,
    common\models\SongVotes;

/**
 * Description of TimetablesController
 *
 * @author fobos
 */
class SongsController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'],
            'rules' => [
                [
                    'actions' => ['index', 'vote'],
                    'allow' => true,
                    'roles' => ['?', '*'],
                ],
                [
                    'actions' => ['add-soungs', 'update-soungs'],
                    'allow' => true,
                    'roles' => ['*'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Allow-Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['get', 'options'],
                'vote' => ['post', 'options'],
                'add-soungs' => ['post', 'options'],
                'update-soungs' => ['post', 'options']
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['add-soungs', 'update-soungs'],
            'except' => ['options']
        ];

        return $behaviors;
    }

    // Get page`s seo data
    public function actionIndex() {
        if (Yii::$app->request->isGet) {

            $result['error'] = '';
            $result['content'] = Songs::find()->where(['status' => 1])->orderBy(['rank' => SORT_DESC])->asArray()->all();
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

    // Vote for song
    public function actionVote() {
        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();

            $ip = (isset($post['ip']) && !empty($post['ip'])) ? strip_tags(trim($post['ip'])) : '127.0.0.1';
            $songID = (isset($post['song_id']) && !empty($post['song_id'])) ? (int) $post['song_id'] : 0;

            // Check if the guest was voted for the song
            if ($songID !== 0 && SongVotes::findOne(['songs_id' => $songID, 'ip_address' => $ip]) === null) {
                $model_vote = new SongVotes();
                $model_vote->songs_id = $songID;
                $model_vote->ip_address = $ip;
                $model_vote->save();

                $model_song = Songs::findOne(['id' => $songID]);
                $model_song->rank += 1;
                $model_song->save();
            }

            $result['error'] = '';
            $result['content'] = 'success';
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

    // Add new songs from dropbox
    public function actionAddSoungs() {
        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();

            if (isset($post['content']) && is_array($post['content'])) {

                Songs::updateAll(['status' => 0]);

                foreach ($post['content'] as $content) {
                    $title = strip_tags(trim($content['title']));
                    $url = (isset($content['url'])) ? strip_tags(trim($content['url'])) : null;
                    $dropbox_id = trim($content['dropbox_id']);
                    $description = (isset($content['description'])) ? strip_tags(trim($content['description'])) : null;
                    $rank = 0;

                    if (($model = Songs::findOne(['dropbox_id' => $dropbox_id])) === null) {
                        $model = new Songs();
                        $model->title = $title;
                        $model->url = $url;
                        $model->dropbox_id = $dropbox_id;
                        $model->description = $description;
                        $model->rank = $rank;
                        $model->status = 1;
                        $model->insert();
                    } else {
                        $model->url = $url;
                        $model->dropbox_id = $dropbox_id;
                        $model->status = 1;
                        $model->update();
                    }
                }
            }

            $result['error'] = '';
            $result['content'] = 'success';
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

    // Update exist songs url from dropbox
    public function actionUpdateSoungs() {
        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();

            if (isset($post['content']) && is_array($post['content'])) {
                foreach ($post['content'] as $content) {
                    $url = (isset($content['url'])) ? strip_tags(trim($content['url'])) : null;
                    $dropbox_id = trim($content['dropbox_id']);

                    if (($model = Songs::findOne(['dropbox_id' => $dropbox_id])) !== null) {
                        $model->url = $url;
                        $model->dropbox_id = $dropbox_id;
                        $model->update();
                    }
                }
            }

            $result['error'] = '';
            $result['content'] = 'success';
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

}
