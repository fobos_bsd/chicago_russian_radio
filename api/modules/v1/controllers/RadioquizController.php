<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\data\Pagination,
    yii\filters\VerbFilter;
use common\models\Radioquiz;

/**
 * Description of TimetablesController
 *
 * @author fobos
 */
class RadioquizController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'archive'],
            'rules' => [
                [
                    'actions' => ['index', 'archive'],
                    'allow' => true,
                    'roles' => ['?', '*'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Allow-Origin' => ['http://radio.vp4.ru'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['post', 'options'],
                'archive' => ['post', 'options'],
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];


        /*
          $behaviors['authenticator'] = [
          'class' => HttpBearerAuth::className(),
          'except' => ['options']
          ];
         * 
         */


        return $behaviors;
    }

    // Get page`s seo data
    public function actionIndex() {
        if (Yii::$app->request->isPost) {

            $result['error'] = '';
            $result['content'] = Radioquiz::find()->where(['status' => 1])->asArray()->one();
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

    // Get page`s seo data
    public function actionArchive() {
        if (Yii::$app->request->isPost) {

            $get = Yii::$app->request->post();

            $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
            $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;

            $query = Radioquiz::find()->where(['status' => 0])->orderBy(['id' => SORT_DESC]);

            $countQuery = clone $query;
            $tottal_pages = (int) $countQuery->count();
            $pages = new Pagination(['totalCount' => $tottal_pages]);
            $pages->defaultPageSize = $count_per_page;
            $pages->page = $page;
            $contents = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    ->asArray()
                    ->all();

            $result['error'] = '';
            $result['content'] = $contents;
            $result['count_all_parts'] = Radioquiz::find()->count();
            $result['count_pages'] = ((int) $tottal_pages > 0) ? ceil((int) $tottal_pages / (int) $count_per_page) : 0;
            $result['per_page'] = $count_per_page;
            Yii::$app->response->statusCode = 200;
            return $result;
        } else {
            Yii::$app->response->statusCode = 200;
            return ['error' => 'Not Found'];
        }
    }

}
