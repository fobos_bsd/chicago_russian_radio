<?php

namespace api\models;

use common\models\Results;
use common\models\Votes;
use Yii;
use yii\base\Model;

/**
 * Class VoteForm
 * @package frontend\forms
 */
class VoteForm extends Model
{
    /**
     * @var
     */
    public $answerId;

    /**
     * @var
     */
    public $questionId;

    /**
     * @var
     */
    public $site_id;
    
    /**
     * @var
     */
    public $ip;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answerId', 'questionId', 'site_id'], 'required'],
            [['answerId', 'questionId', 'site_id'], 'integer'],
            ['ip', 'string', 'max' => 64],
            ['ip', 'filter', 'filter' => 'trim']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'answerId' => Yii::t('frontend', 'Вопрос'),
            'questionId' => Yii::t('frontend', 'Ответ'),
        ];
    }

    /**
     * @return bool
     */
    public function vote()
    {
        $result = new Votes([
            'answers_id' => $this->answerId,
            'questions_id' => $this->questionId,
            'ip' => $this->ip,
            'site_id' => 1
        ]);

        return $result->save();
    }
}
