<?php

namespace api\models;

use Yii;
use yii\base\Model;
use common\models\UserQuestions;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model {

    public $name;
    public $email;
    public $subject;
    public $body;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            ['subject', 'string'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
                //'reCaptchaOnline' => 'I am a human',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email) {
        // Save to db
        $model_question = new UserQuestions();
        $model_question->site_id = 1;
        $model_question->usename = $this->name;
        $model_question->email = $this->email;
        $model_question->phone = null;
        $model_question->question = $this->body;
        $model_question->created_at = time();
        $model_question->answer_at = null;
        $model_question->save();

        return Yii::$app->mailer->compose()
                        ->setTo($email)
                        ->setFrom([$this->email => $this->name])
                        ->setSubject($this->subject)
                        ->setTextBody($this->body)
                        ->send();
    }

}
