<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "audio_advertising".
 *
 * @property int $id
 * @property int $site_id
 * @property string $name
 * @property string $description
 * @property string $text_content
 * @property string $audio_file
 * @property int $created_at
 *
 * @property Site $site
 */
class AudioAdvertising extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'audio_advertising';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id', 'name', 'audio_file', 'created_at'], 'required'],
            [['site_id', 'created_at', 'status'], 'integer'],
            [['description', 'text_content'], 'string'],
            [['name'], 'string', 'max' => 250],
            [['audio_file'], 'string', 'max' => 255],
            [['audio_file'], 'unique'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'site_id' => Yii::t('common', 'Site'),
            'name' => Yii::t('common', 'Name'),
            'description' => Yii::t('common', 'Description'),
            'text_content' => Yii::t('common', 'Text Content'),
            'audio_file' => Yii::t('common', 'Audio File'),
            'created_at' => Yii::t('common', 'Created'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

}
