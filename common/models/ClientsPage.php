<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper,
    yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "clients_page".
 *
 * @property int $id
 * @property string $clients_id
 * @property int $site_id
 * @property int $lang_id
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property string $fileStyle
 * @property string $fileJS
 * @property int $status
 * @property string $created_at
 *
 * @property Clients $clients
 * @property Lang $lang
 * @property Site $site
 */
class ClientsPage extends \yii\db\ActiveRecord {

    public $text_css, $text_js;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'clients_page';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['clients_id', 'site_id', 'lang_id', 'status'], 'integer'],
            [['site_id', 'lang_id', 'title', 'created_at', 'slug'], 'required'],
            [['description', 'content'], 'string'],
            ['slug', 'unique'],
            [['created_at'], 'safe'],
            [['created_at'], 'default', 'value' => date('Y-m-d')],
            [['status'], 'default', 'value' => 1],
            [['title', 'keywords', 'slug'], 'string', 'max' => 255],
            [['text_css', 'text_js'], 'string', 'max' => 16777215],
            [['clients_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['clients_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['slug', 'title', 'keywords', 'description', 'content'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            ['thumb', 'file', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024 * 1024 * 10],
            ['thumb', 'default', 'value' => null]
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        return ArrayHelper::merge([
                    [
                        'class' => SluggableBehavior::className(),
                        'attribute' => 'title',
                        'slugAttribute' => 'slug',
                        'immutable' => 'true',
                    ],
                        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (isset($this->thumb) && is_object($this->thumb)) {
                $resupload = $this->upload($this->thumb);

                if ($resupload !== false) {
                    $this->thumb = $resupload;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function upload($image) {
        if ($this->validate()) {
            $filename = $image->baseName . '.' . $image->extension;
            $path = Yii::getAlias('@frontend') . '/web/uploads/clients_page/' . $filename;
            $image->saveAs($path);
            return $filename;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'clients_id' => Yii::t('common', 'Clients ID'),
            'site_id' => Yii::t('common', 'Site ID'),
            'lang_id' => Yii::t('common', 'Lang ID'),
            'title' => Yii::t('common', 'Title'),
            'slug' => Yii::t('common', 'Link'),
            'keywords' => Yii::t('common', 'Keywords'),
            'description' => Yii::t('common', 'Description'),
            'content' => Yii::t('common', 'Content'),
            'thumb' => Yii::t('common', 'Thumb'),
            'status' => Yii::t('common', 'Status'),
            'created_at' => Yii::t('common', 'Created At'),
            'text_css' => Yii::t('common', 'Style'),
            'text_js' => Yii::t('common', 'JavaScript'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients() {
        return $this->hasOne(Clients::className(), ['id' => 'clients_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang() {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

}
