<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "soundcloud".
 *
 * @property int $id
 * @property int $youtube_playlists_id
 * @property string $name
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property string $sclink
 *
 * @property YoutubePlaylists $youtubePlaylists
 */
class Soundcloud extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'soundcloud';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['youtube_playlists_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'created_at', 'updated_at', 'sclink'], 'required'],
            [['description'], 'string'],
            [['name', 'sclink'], 'string', 'max' => 255],
            [['youtube_playlists_id'], 'exist', 'skipOnError' => true, 'targetClass' => YoutubePlaylists::className(), 'targetAttribute' => ['youtube_playlists_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'youtube_playlists_id' => Yii::t('common', 'Youtube Playlists'),
            'name' => Yii::t('common', 'Name'),
            'description' => Yii::t('common', 'Description'),
            'created_at' => Yii::t('common', 'Created'),
            'updated_at' => Yii::t('common', 'Updated'),
            'sclink' => Yii::t('common', 'Link'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYoutubePlaylists()
    {
        return $this->hasOne(YoutubePlaylists::className(), ['id' => 'youtube_playlists_id']);
    }
}
