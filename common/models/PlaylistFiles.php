<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "playlist_files".
 *
 * @property int $id
 * @property int $youtube_playlists_id
 * @property string $name
 * @property string $fileName
 * @property int $long_time
 * @property string $radio_brodcast_day
 * @property string $radio_brodcast_time
 * @property int $status
 *
 * @property YoutubePlaylists $youtubePlaylists
 */
class PlaylistFiles extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'playlist_files';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['youtube_playlists_id', 'long_time', 'status'], 'integer'],
            [['name'], 'required'],
            ['fileName', 'file', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false, 'extensions' => 'mp3, mp4, wav', 'maxSize' => 157286400, 'on' => ['insert']],
            ['fileName', 'string', 'max' => 500, 'on' => ['console']],
            ['fileName', 'safe', 'on' => ['update']],
            [['radio_brodcast_day', 'radio_brodcast_time'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['youtube_playlists_id'], 'exist', 'skipOnError' => true, 'targetClass' => YoutubePlaylists::className(), 'targetAttribute' => ['youtube_playlists_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'youtube_playlists_id' => Yii::t('common', 'Youtube Playlists ID'),
            'name' => Yii::t('common', 'Name'),
            'fileName' => Yii::t('common', 'Audio file'),
            'long_time' => Yii::t('common', 'Long Time'),
            'radio_brodcast_day' => Yii::t('common', 'Radio Brodcast Day'),
            'radio_brodcast_time' => Yii::t('common', 'Radio Brodcast Time'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYoutubePlaylists() {
        return $this->hasOne(YoutubePlaylists::className(), ['id' => 'youtube_playlists_id']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (isset($this->fileName) && is_object($this->fileName)) {
                $resupload = $this->upload();
                if ($resupload !== false) {
                    $this->fileName = $resupload;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function upload() {
        if ($this->validate()) {
            $dir = Yii::getAlias('@frontend') . '/web/uploads/sound_playlist/';
            $filename = $this->fileName->baseName . '.' . $this->fileName->extension;
            $this->fileName->saveAs($dir . $filename);
            return $filename;
        } else {
            return false;
        }
    }

}
