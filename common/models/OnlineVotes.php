<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;
use common\components\SluggableBehavior;

/**
 * This is the model class for table "votes".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property integer $created_at
 * @property integer $start_at
 * @property integer $finish_at
 * @property integer $status
 * @property string $params
 *
 * @property ArhPhoneVote[] $arhPhoneVotes
 * @property ArhSmsVote[] $arhSmsVotes
 * @property CurrentPhoneVote[] $currentPhoneVotes
 * @property CurrentSmsVote[] $currentSmsVotes
 */
class OnlineVotes extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'online_votes';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'created_at', 'start_at', 'params', 'vtype', 'finish_at', 'site_id'], 'required'],
            [['description', 'params'], 'string'],
            [['site_id'], 'integer'],
            //[['start_at', 'finish_at'], 'integer', 'min' => time(), 'tooSmall' => 'Min value mast be more then ' . date('d.m.Y H:i:s'), 'on' => 'createVote'],
            [['created_at', 'status', 'period_call'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['slug'], 'string', 'max' => 255],
            [['vtype'], 'in', 'range' => ['phone', 'sms']],
            [['vtype'], 'default', 'value' => 'phone'],
            [['params'], 'default', 'value' => '{"status":"error"}'],
            [['created_at'], 'default', 'value' => time() - 5*3600],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        return ArrayHelper::merge([
                    [
                        'class' => SluggableBehavior::className(),
                        'attribute' => 'name',
                        'slugAttribute' => 'slug',
                        'immutable' => 'true',
                        'transliterator' => 'Russian-Latin/BGN; NFKD',
                        'forceUpdate' => false,
                        'ensureUnique' => true
                    ],
                        ], parent::behaviors());
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            if ((isset($this->start_at)) && !is_int($this->start_at))
                $this->start_at = (int) strtotime($this->start_at) - 5*3600;
            if (isset($this->finish_at) && !is_int($this->finish_at))
                $this->finish_at = (int) strtotime($this->finish_at) - 5*3600;
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'description' => 'Description',
            'created_at' => 'Created At',
            'start_at' => 'Start At',
            'finish_at' => 'Finish At',
            'status' => 'Status',
            'params' => 'Params',
            'vtype' => 'Type vote',
            'period_call' => 'Period betwin calls',
            'site_id' => 'Site',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArhPhoneVotes() {
        return $this->hasMany(ArhPhoneVote::className(), ['online_votes_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArhSmsVotes() {
        return $this->hasMany(ArhSmsVote::className(), ['online_votes_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentPhoneVotes() {
        return $this->hasMany(CurrentPhoneVote::className(), ['online_votes_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentSmsVotes() {
        return $this->hasMany(CurrentSmsVote::className(), ['online_votes_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    public function getStartAt() {
        return date('m/d/Y H:i:s', $this->start_at);
    }

    public function setStartAt($value) {
        $this->start_at = strtotime(str_replace('/', '-', $value));
    }

}
