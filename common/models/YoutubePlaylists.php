<?php

namespace common\models;

use Yii,
    pendalf89\filemanager\behaviors\MediafileBehavior;

/**
 * This is the model class for table "youtube_playlists".
 *
 * @property integer $id
 * @property integer $youtube_channels_id
 * @property string $etag
 * @property string $playlist_id
 * @property integer $publish_at
 * @property string $title
 * @property string $description
 * @property string $thumbnails_medium
 * @property string $thumbnails_maxres
 * @property integer $status
 * @property string $archive_slug
 *
 * @property YoutubeChannels $youtubeChannels
 * @property YoutubeVideo[] $youtubeVideos
 */
class YoutubePlaylists extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'youtube_playlists';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['youtube_channels_id', 'playlist_id'], 'required'],
            [['youtube_channels_id', 'publish_at', 'status', 'priority'], 'integer'],
            [['description', 'thumbnails_medium', 'thumbnails_maxres'], 'string'],
            [['etag'], 'string', 'max' => 64],
            [['playlist_id'], 'string', 'max' => 48],
            [['title', 'meta_key'], 'string', 'max' => 255],
            [['meta_description'], 'string', 'max' => 500],
            [['archive_slug'], 'string', 'max' => 1500],
            [['playlist_id'], 'unique'],
            [['youtube_channels_id'], 'exist', 'skipOnError' => true, 'targetClass' => YoutubeChannels::className(), 'targetAttribute' => ['youtube_channels_id' => 'id']],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['autoinsert'] = ['youtube_channels_id', 'etag', 'playlist_id', 'publish_at', 'status'];
        return $scenarios;
    }

    public function behaviors() {
        return [
            'mediafile' => [
                'class' => MediafileBehavior::className(),
                'name' => 'post',
                'attributes' => [
                    'thumbnails_medium', 'thumbnails_maxres'
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'youtube_channels_id' => Yii::t('common', 'Youtube Channels'),
            'etag' => Yii::t('common', 'Etag'),
            'playlist_id' => Yii::t('common', 'Playlist'),
            'publish_at' => Yii::t('common', 'Publish'),
            'title' => Yii::t('common', 'Title'),
            'description' => Yii::t('common', 'Description'),
            'thumbnails_medium' => Yii::t('common', 'Thumbnails Medium'),
            'thumbnails_maxres' => Yii::t('common', 'Thumbnails Maxres'),
            'status' => Yii::t('common', 'Status'),
            'priority' => Yii::t('common', 'Priority'),
            'meta_key' => Yii::t('common', 'Meta Keys'),
            'meta_description' => Yii::t('common', 'Meta description'),
            'archive_slug' => Yii::t('common', 'Slug for archive folder')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYoutubeChannels() {
        return $this->hasOne(YoutubeChannels::className(), ['id' => 'youtube_channels_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYoutubeVideos() {
        return $this->hasMany(YoutubeVideo::className(), ['youtube_playlists_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery() {
        return $this->hasMany(Gallery::className(), ['youtube_playlists_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimetable() {
        return $this->hasMany(Timetable::className(), ['youtube_playlists_id' => 'id']);
    }

}
