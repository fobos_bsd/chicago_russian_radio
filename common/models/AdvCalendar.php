<?php

namespace common\models;

use Yii;
use common\models\AdvOrders,
    common\models\AdvMesh,
    common\models\AdvUsedmesh,
    backend\modules\advertising\models\Calendar;

/**
 * This is the model class for table "adv_calendar".
 *
 * @property integer $id
 * @property integer $adv_mesh_id
 * @property integer $adv_orders_id
 * @property string $online_day
 * @property integer $full_time
 * @property integer $less_seconds
 *
 * @property AdvOrders $advOrders
 */
class AdvCalendar extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adv_calendar';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['adv_mesh_id', 'adv_orders_id', 'online_day', 'gravity', 'count_time'], 'required'],
            [['adv_mesh_id', 'adv_orders_id'], 'integer'],
            [['count_time'], 'integer', 'min' => 1, 'max' => 10000],
            [['online_day'], 'safe'],
            [['adv_orders_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvOrders::className(), 'targetAttribute' => ['adv_orders_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate() {
        if ($this->isNewRecord) {
            if (!isset($this->gravity) || empty($this->gravity))
                $this->gravity = 0;
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {

            // Test is place exist
            // Get events data
            $search = new Calendar();
            $oldid = (isset($this->id)) ? (int) $this->id : 0;
            $sum_theday = $search->getSummViews($this->adv_orders_id, $this->online_day, $oldid) + (int) $this->count_time;
            $orderdata = AdvOrders::find()->where(['id' => $this->adv_orders_id])->one();
            $meshdata = AdvUsedmesh::find()->where(['adv_mesh_id' => $this->adv_mesh_id])->andWhere(['used_date' => $this->online_day])->one();
            /*
              if (isset($orderdata->how_seconds) && isset($meshdata->less_seconds)) {
              $totlses = (int) $orderdata->how_seconds * (int) $this->count_time;
              if ($totlses > (int) $meshdata->less_seconds)
              return false;
              }
              if (isset($orderdata->used_days)) {
              $countdayuse = (int) $orderdata->used_days + 1;
              if (isset($orderdata->count_days) && ((int) $orderdata->count_days < $countdayuse))
              return false;
              }
              if (isset($orderdata->count_onday) && ((int) $orderdata->count_onday < $sum_theday)) {
              return false;
              }
             * 
             */

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        // Test is place exist
        $orderdata = AdvOrders::find()->where(['id' => $this->adv_orders_id])->one();
        $meshdata = AdvUsedmesh::find()->where(['adv_mesh_id' => $this->adv_mesh_id])->andWhere(['used_date' => $this->online_day])->one();
        $mesh = AdvMesh::find()->where(['id' => $this->adv_mesh_id])->one();
        if (isset($orderdata->how_seconds) && isset($meshdata->less_seconds) && isset($mesh->long)) {
            $meshdata->less_seconds -= (int) $orderdata->how_seconds * (int) $this->count_time;
            $meshdata->used += (int) $orderdata->how_seconds * (int) $this->count_time;
            $proc = 100 * $meshdata->used / (int) $mesh->long;
            $meshdata->used_procent = round($proc, 0);
            $meshdata->update();
        } else {
            $meshdata = new AdvUsedmesh();
            $meshdata->adv_mesh_id = (int) $this->adv_mesh_id;
            $meshdata->less_seconds = (int) $mesh->long - ((int) $orderdata->how_seconds * (int) $this->count_time);
            $meshdata->used = (int) $orderdata->how_seconds * (int) $this->count_time;
            $proc = 100 * $meshdata->used / (int) $mesh->long;
            $meshdata->used_procent = round($proc, 0);
            $meshdata->used_date = $this->online_day;
            $meshdata->insert();
        }
        $orderdata->used_days = (int) $orderdata->used_days + 1;
        $orderdata->save();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'adv_mesh_id' => 'Call ID',
            'adv_orders_id' => 'Order ID',
            'online_day' => 'Calendar day',
            'gravity' => 'Gravity',
            'count_time' => 'Count times'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvOrders() {
        return $this->hasOne(AdvOrders::className(), ['id' => 'adv_orders_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvMesh() {
        return $this->hasOne(AdvMesh::className(), ['id' => 'adv_mesh_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersOfTime($meshID) {
        $arrorders = array();
        $orders = $this->find()->where(['adv_mesh_id' => $meshID])->all();

        if (isset($orders) && count($orders) > 0) {
            foreach ($orders as $order) {
                array_push($arrorders, ['content' => '<span class="elementorder" id="' . $order->id . '" data-gravity="' . $order->gravity . '">' . $order->advOrders->title . ' (' . Yii::$app->formatter->asTime($order->advOrders->start_at) . ')</span>']);
            }
        }

        return $arrorders;
    }

}
