<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "adv_company".
 *
 * @property integer $id
 * @property integer $site_id
 * @property string $name
 * @property BannerItem[] $banners
 *
 */
class AdvCompany extends ActiveRecord {

    /**
     * @var
     */
    public $bIds;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adv_company';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name','site_id'], 'required'],
            [['site_id'], 'integer'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['bIds'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Adv company name'),
            'bIds' => Yii::t('backend', 'Banners')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners() {
        return $this->hasMany(BannerItem::className(), ['id' => 'banner_item_id'])
            ->viaTable(AdvCompanyBanners::tableName(), ['adv_company_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->updateBanners();
    }

    /**
     * @return mixed
     */
    private function updateBanners()
    {
        $currentBannersName = $this->getBanners()->select('id')->column();

        if(is_null($this->bIds)){
            $this->bIds = [""];
        }

        foreach (array_filter(array_diff($this->bIds, $currentBannersName)) as $bannerName) {
            $banner = BannerItem::find()->where(['id' => $bannerName])->one();
            $this->link('banners', $banner);
        }

        foreach (array_filter(array_diff($currentBannersName, $this->bIds)) as $bannerName) {
            if ($banner = BannerItem::find()->where(['id' => $bannerName])->one()) {
                $this->unlink('banners', $banner, true);
            }
        }
    }
}
