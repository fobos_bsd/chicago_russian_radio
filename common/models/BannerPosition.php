<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner_position".
 *
 * @property int $id
 * @property string $name
 * @property int $width
 * @property int $height
 *
 * @property BannerItem[] $bannerItems
 */
class BannerPosition extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'banner_position';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'width', 'height'], 'required'],
            [['width', 'height'], 'integer', 'min' => 10, 'max' => 65535],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            ['name', 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Название'),
            'width' => Yii::t('common', 'Ширина'),
            'height' => Yii::t('common', 'Длина'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerItems() {
        return $this->hasMany(BannerItem::className(), ['banner_position_id' => 'id']);
    }

}
