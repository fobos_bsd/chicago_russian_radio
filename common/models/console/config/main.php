<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'user' => [
            'class' => 'common\models\User',
            'identityClass' => 'dektrium\user\models\User',
            'enableAutoLogin' => true,
            'on ' . dektrium\user\models\User::AFTER_CREATE => ['\common\models\User', 'setUserRole']
        ],
    ],
    'modules' => [
        'rbac' => 'dektrium\rbac\RbacConsoleModule',
        'user' => [
            'mailer' => [
                'sender' => 'no-reply@moscow-services', // or ['no-reply@myhost.com' => 'Sender name']
                'welcomeSubject' => 'Welcome ',
                'confirmationSubject' => 'Confirmation ',
                'reconfirmationSubject' => 'Email change ',
                'recoverySubject' => 'Recovery ',
            ],
            'modelMap' => [
                'User' => [
                    'class' => 'common\models\User',
                    'on ' . dektrium\user\models\User::AFTER_REGISTER => ['\common\models\User', 'setUserRole'],
                ],
                'LoginForm' => [
                    'class' => 'common\models\LoginForm',
                ],
                'RegistrationForm' => [
                    'class' => 'common\models\RegistrationForm',
                ]
            ],
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'cost' => 12,
            'admins' => ['adminfm']
        ],
    ],
    'params' => $params,
];
