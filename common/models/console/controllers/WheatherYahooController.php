<?php

namespace console\controllers;

use Yii,
    yii\console\Controller,
    yii\helpers\Json,
    yii\helpers\Console;

class WheatherYahooController extends Controller {

    const BASE_URL = "http://query.yahooapis.com/v1/public/yql";

    public $yql_query;
    private $arrdats;

    /*
     * @return init data
     */

    public function init() {
        $this->yql_query = 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="chicago, il")';
        $this->arrdats = [
            'Mon' => 'Понедельник',
            'Tue' => 'Вторник',
            'Web' => 'Среда',
            'Thu' => 'Четверг',
            'Fri' => 'Пятница',
            'Sat' => 'Суббота',
            'Sun' => 'Воскресенье'
        ];

        parent::init();
    }

    // Start script
    public function actionRun() {
        $wheatherObj = $this->wheather();
        $this->todayWheatherText($wheatherObj);
    }

    // Get wheather data
    public function wheather() {
        $yql_query_url = self::BASE_URL . "?q=" . urlencode($this->yql_query) . "&format=json";
        // Make call with cURL
        $session = curl_init($yql_query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($session);

        // Convert JSON to PHP object
        return Json::decode($json);
    }

    // Produce wheather text on today
    public function todayWheatherText($wheatherObj) {
        var_dump($wheatherObj);
        // If wheather`s information exist
        if (isset($wheatherObj['query'])) {
            $wheather = $wheatherObj['query']; // Global wheather var

            $date_last_update = $wheather['created'];
            $time_of_wheathe_data = $wheather['results']['channel']['lastBuildDate'];

            // Text for russian language
            $text_ru = 'Этой ночью ожидается переменная облачность, температура -1..+1°. Атмосферное давление в пределах нормы. Возможны небольшие геомагнитные возмущения. Завтра днем переменная облачность, +3..+5°, ветер юго-восточный, умеренный. Атмосферное давление в пределах нормы. Геомагнитное поле спокойное.
17-го февраля, в течение суток ожидается малооблачная погода; ночью +1..+3°, днем +10..12°, ветер юго-западный, умеренный. Возможны небольшие геомагнитные возмущения.';
        }
    }

    // Direction wind
    public function get_wind_direction($wind_direction, $lan = 'en-US') {
        $wind_directions = array('северный', 'северо-восточный', 'восточный', 'юго-восточный',
            'южный', 'юго-западный', 'западный', 'северо-западный');
        $a = $wind_direction % 360;
        if ($a < 23 or $a > 337)
            $index = 0;
        else if ($a < 68)
            $index = 1;
        else if ($a < 113)
            $index = 2;
        else if ($a < 158)
            $index = 3;
        else if ($a < 203)
            $index = 4;
        else if ($a < 248)
            $index = 5;
        else if ($a < 293)
            $index = 6;
        else if ($a < 338)
            $index = 7;
        return $wind_directions[$index];
    }

    // Convert condition code
    public function convert_condition($code, $lan = 'en-US') {
        switch ($lan) {
            case 'ru-RU':
                $cond = [
                    0 => 'торнадо',
                    1 => 'шторм',
                    2 => 'ураган',
                    3 => 'сильная гроза',
                    4 => 'гроза',
                    5 => 'дождь со снегом',
                    6 => 'дождь с мокрым снегом',
                    7 => 'мокрый снег',
                    8 => 'ледяная пыль',
                    9 => 'морось',
                    10 => 'дождь, переходящий в снег',
                    11 => 'дождь',
                    12 => 'дождь',
                    13 => 'слабый снег',
                    14 => 'небольшой снег',
                    15 => 'метель',
                    16 => 'снег',
                    17 => 'град',
                    18 => 'дождь со снегом',
                    19 => 'пыль',
                    20 => 'туман',
                    21 => 'дымка',
                    22 => 'смог',
                    23 => 'порывистый ветер',
                    24 => 'ветрено',
                    25 => 'холодно',
                    26 => 'облачно',
                    27 => 'сильная облачность',
                    28 => 'сильная облачность',
                    29 => 'переменная облачность',
                    30 => 'переменная облачность',
                    31 => 'ясно',
                    32 => 'солнечно',
                    33 => 'ясно',
                    34 => 'ясно',
                    35 => 'дождь с градом',
                    36 => 'жарко',
                    37 => 'местами грозы',
                    38 => 'временами грозы',
                    39 => 'временами грозы', //ливни?
                    40 => 'местами дожди',
                    41 => 'сильный снег',
                    42 => 'местами снегопады',
                    43 => 'сильный снегопад',
                    44 => 'переменная облачность',
                    45 => 'гроза',
                    46 => 'снегопад',
                    47 => 'местами грозы',
                    3200 => 'не известно'
                ];
                break;
            case 'en-US':
                $cond = [
                    0 => 'tornado',
                    1 => 'tropical storm',
                    2 => 'hurricane',
                    3 => 'severe thunderstorms',
                    4 => 'thunderstorms',
                    5 => 'mixed rain and snow',
                    6 => 'mixed rain and sleet',
                    7 => 'mixed snow and sleet',
                    8 => 'freezing drizzle',
                    9 => 'drizzle',
                    10 => 'freezing rain',
                    11 => 'showers',
                    12 => 'showers',
                    13 => 'snow flurries',
                    14 => 'light snow showers',
                    15 => 'blowing snow',
                    16 => 'snow',
                    17 => 'hail',
                    18 => 'sleet',
                    19 => 'dust',
                    20 => 'foggy',
                    21 => 'haze',
                    22 => 'smoky',
                    23 => 'blustery',
                    24 => 'windy',
                    25 => 'cold',
                    26 => 'cloudy',
                    27 => 'mostly cloudy (night)',
                    28 => 'mostly cloudy (day)',
                    29 => 'partly cloudy (night)',
                    30 => 'partly cloudy (day)',
                    31 => 'clear (night)',
                    32 => 'sunny',
                    33 => 'fair (night)',
                    34 => 'fair (day)',
                    35 => 'mixed rain and hail',
                    36 => 'hot',
                    37 => 'isolated thunderstorms',
                    38 => 'scattered thunderstorms',
                    39 => 'scattered thunderstorms',
                    40 => 'scattered showers',
                    41 => 'heavy snow',
                    42 => 'scattered snow showers',
                    43 => 'heavy snow',
                    44 => 'partly cloudy',
                    45 => 'thundershowers',
                    46 => 'snow showers',
                    47 => 'isolated thundershowers',
                    3200 => 'not available'
                ];
                break;
        }


        if (array_key_exists($code, $cond))
            return $cond[$code];
        else
            return '? (код ' . $code . ')';
    }

}
