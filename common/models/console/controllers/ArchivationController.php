<?php

namespace console\controllers;

use Yii,
    yii\console\Controller,
    yii\helpers\ArrayHelper,
    yii\helpers\Console;
use common\models\Votes,
    common\models\Settings,
    common\models\CurrentPhoneVote,
    common\models\CurrentSmsVote,
    common\models\ArhPhoneVote,
    common\models\ArhSmsVote;

class ArchivationController extends Controller {

    protected $settings;

    public function init() {
        $this->settings = ArrayHelper::map(Settings::find()->where(['status' => 1])->all(), 'name', 'setvalue');
        parent::init();
    }

    // Arhivation data
    public function actionRun() {
        // Get array old votes ID
        $arrvotes = $this->getNotactivevotes();
        if (isset($arrvotes)) {
            $arrvID = ArrayHelper::getValue($arrvotes, 'id');
        }

        // Arhivation old votes
        if (isset($arrvID)) {
            $this->archivePhonevotes($arrvID);
            $this->archiveSmsvotes($arrvID);
        }

        $this->stdout('Arhivation finished' . PHP_EOL, Console::FG_GREEN);
    }

    // Archive phone votes
    public function archivePhonevotes($arrvID) {
        foreach (CurrentPhoneVote::find()->where(['in', 'votes_id', $arrvID])->each(10) as $vote) {
            $model = new ArhPhoneVote();
            $model->votes_id = $vote->votes_id;
            $model->phone = $vote->phone;
            $model->vote_phone = $vote->vote_phone;
            $model->created_at = $vote->created_at;

            if (!$model->insert()) {
                $this->stdout($model->getErrors() . PHP_EOL, Console::FG_GREEN);
            } else {
                $this->findPhoneModel($vote->id)->delete();
            }
        }

        $this->stdout('Arhivation phone`s votes finished' . PHP_EOL, Console::FG_GREEN);
    }

    // Archive phone votes
    public function archiveSmsvotes($arrvID) {
        foreach (CurrentSmsVote::find()->where(['in', 'votes_id', $arrvID])->each(10) as $vote) {
            $model = new ArhSmsVote();
            $model->votes_id = $vote->votes_id;
            $model->phone = $vote->phone;
            $model->vote_sms = $vote->vote_sms;
            $model->created_at = $vote->created_at;

            if (!$model->insert()) {
                $this->stdout($model->getErrors() . PHP_EOL, Console::FG_GREEN);
            } else {
                $this->findSmsModel($vote->id)->delete();
            }
        }

        $this->stdout('Arhivation sms`s votes finished' . PHP_EOL, Console::FG_GREEN);
    }

    // Get list notactive votes
    public function getNotactivevotes() {
        $maxtime = time() - (int) $this->settings['vote_timeout'];
        if (($objvotes = Votes::find()->select(['id'])->where(['status' => 0])->andWhere(['<', 'start_at', $maxtime])->asArray()->all()) !== null) {
            return $objvotes;
        } else {
            return null;
        }
    }

    // find one phone vote
    protected function findPhoneModel($id) {
        if (($model = CurrentPhoneVote::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // find one sms vote
    protected function findSmsModel($id) {
        if (($model = CurrentSmsVote::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
