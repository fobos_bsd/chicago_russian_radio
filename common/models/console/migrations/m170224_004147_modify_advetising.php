<?php

use yii\db\Migration;

class m170224_004147_modify_advetising extends Migration {

    public function safeUp() {
        $this->dropColumn('adv_calendar', 'full_time');
        $this->dropColumn('adv_calendar', 'less_seconds');
        $this->addColumn('adv_calendar', 'gravity', 'BOOL NOT NULL DEFAULT 0 AFTER online_day');
    }

    public function safeDown() {
        echo "m170224_004147_modify_advetising cannot be reverted.\n";
    }

}
