<?php

use yii\db\Migration;

class m170224_172148_modify_advetising_columns extends Migration {

    public function safeUp() {
        $this->addColumn('adv_orders', 'title', 'VARCHAR(255) NOT NULL AFTER used_days');
        $this->addColumn('adv_orders', 'file', 'TEXT(500) NULL AFTER finish_at');
        $this->addColumn('adv_orders', 'file_link', 'TEXT(500) NULL AFTER file');
        $this->addColumn('adv_calendar', 'count_time', 'TINYINT(3) NOT NULL DEFAULT 1 AFTER gravity');
    }

    public function safeDown() {
        $this->dropColumn('adv_orders', 'title');
        $this->dropColumn('adv_orders', 'file');
        $this->dropColumn('adv_orders', 'file_link');
        $this->dropColumn('adv_calendar', 'count_time');
        echo "m170224_172148_modify_advetising_columns cannot be reverted.\n";
    }

}
