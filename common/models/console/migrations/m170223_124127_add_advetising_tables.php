<?php

use yii\db\Migration;

class m170223_124127_add_advetising_tables extends Migration {

    public function safeUp() {
        $this->createTable('clients', [
            'id' => $this->bigPrimaryKey(20),
            'name' => $this->string(255)->notNull()->unique(),
            'email' => $this->string(255),
            'phone' => $this->string(255)->notNull(),
            'params' => $this->text(1500),
            'description' => $this->text(1500),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)
        ]);

        $this->createTable('adv_settings', [
            'id' => $this->primaryKey(3),
            'name' => $this->string(250)->notNull()->unique(),
            'setvalue' => $this->string(255)->notNull(),
            'description' => $this->text(1500)
        ]);

        $this->createTable('adv_mesh', [
            'id' => $this->primaryKey(3),
            'start_time' => $this->time()->notNull(),
            'end_time' => $this->time()->notNull(),
            'long' => $this->smallInteger(3)->notNull(),
            'important' => $this->smallInteger(3)->notNull()->defaultValue(100)
        ]);

        $this->createTable('adv_orders', [
            'id' => $this->bigPrimaryKey(20),
            'clients_id' => $this->bigInteger(20)->notNull(),
            'count_days' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'count_onday' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'used_days' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'content' => $this->text(1500),
            'how_seconds' => $this->smallInteger(3)->notNull()->defaultValue(1),
            'start_at' => $this->date()->notNull(),
            'finish_at' => $this->date(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_adv_orders_clients1_idx', 'adv_orders', 'clients_id');
        $this->addForeignKey('fk_adv_orders_clients1', 'adv_orders', 'clients_id', 'clients', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('adv_calendar', [
            'id' => $this->bigPrimaryKey(20),
            'adv_mesh_id' => $this->smallInteger(3)->notNull(),
            'adv_orders_id' => $this->bigInteger(20)->notNull(),
            'online_day' => $this->date()->notNull(),
            'full_time' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'less_seconds' => $this->smallInteger(3)->notNull()->defaultValue(180)
        ]);

        $this->createIndex('fk_adv_calendar_adv_mesh1_idx', 'adv_calendar', 'adv_mesh_id');
        $this->addForeignKey('fk_adv_calendar_adv_mesh1', 'adv_calendar', 'adv_mesh_id', 'adv_mesh', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('fk_adv_calendar_adv_orders1_idx', 'adv_calendar', 'adv_orders_id');
        $this->addForeignKey('fk_adv_calendar_adv_orders1', 'adv_calendar', 'adv_orders_id', 'adv_orders', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropTable('adv_calendar');
        $this->dropTable('adv_orders');
        $this->dropTable('adv_mesh');
        $this->dropTable('adv_settings');
        $this->dropTable('clients');

        echo "m170223_124127_add_advetising_tables cannot be reverted.\n";
    }

}
