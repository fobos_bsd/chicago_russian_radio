<?php

use yii\db\Migration;

class m170224_124235_add_table_advert extends Migration {

    public function safeUp() {
        $this->createTable('adv_usedmesh', [
            'adv_mesh_id' => $this->integer(3)->notNull(),
            'used' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'less_seconds' => $this->smallInteger(3)->notNull()->defaultValue(180),
            'used_procent' => $this->smallInteger(3)->notNull()->defaultValue(0),
            'used_date' => $this->date()->notNull()
        ]);

        $this->createIndex('fk_adv_usedmesh_adv_mesh1_idx', 'adv_usedmesh', 'adv_mesh_id');
        $this->addForeignKey('fk_adv_usedmesh_adv_mesh1', 'adv_usedmesh', 'adv_mesh_id', 'adv_mesh', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropTable('adv_usedmesh');
        echo "m170224_124235_add_table_advert cannot be reverted.\n";
    }

}
