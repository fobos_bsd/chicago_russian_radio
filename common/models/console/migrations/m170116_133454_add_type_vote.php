<?php

use yii\db\Migration;

class m170116_133454_add_type_vote extends Migration {

    public function safeUp() {
        $this->addColumn('votes', 'vtype', 'ENUM(\'phone\', \'sms\') NOT NULL  DEFAULT \'phone\' AFTER description');
    }

    public function safeDown() {
        echo "m170116_133454_add_type_vote cannot be reverted.\n";
        return false;
    }

}
