<?php

use yii\db\Migration;

class m170116_094116_add_setting_description extends Migration {

    public function safeUp() {
        $this->addColumn('settings', 'description', 'TEXT(1500) NULL AFTER status');
    }

    public function safeDown() {
        echo "m170116_094116_add_setting_description cannot be reverted.\n";
        return false;
    }

}
