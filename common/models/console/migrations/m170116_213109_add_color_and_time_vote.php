<?php

use yii\db\Migration;

class m170116_213109_add_color_and_time_vote extends Migration {

    public function safeUp() {
        $this->addColumn('votes', 'period_call', 'INT(6) NULL AFTER vtype');
    }

    public function safeDown() {
        echo "m170116_213109_add_color_and_time_vote cannot be reverted.\n";
        return false;
    }

}
