<?php

use yii\db\Migration;

class m170113_221750_add_votes_tables extends Migration {

    public function safeUp() {
        $this->createTable('votes', [
            'id' => $this->primaryKey(9),
            'name' => $this->string(150)->notNull(),
            'slug' => $this->string(255)->notNull()->unique(),
            'description' => $this->text(1500),
            'created_at' => $this->integer(11)->notNull(),
            'start_at' => $this->integer(11)->notNull(),
            'finish_at' => $this->integer(11),
            'status' => $this->boolean()->defaultValue(1)->notNull()
        ]);

        $this->createTable('settings', [
            'id' => $this->primaryKey(6),
            'name' => $this->string(255)->notNull()->unique(),
            'setvalue' => $this->text(1500),
            'status' => $this->boolean()->defaultValue(1)->notNull()
        ]);

        $this->createTable('current_phone_vote', [
            'id' => $this->primaryKey(11),
            'votes_id' => $this->integer(9)->notNull(),
            'phone' => $this->string(28)->notNull(),
            'vote_phone' => $this->string(28)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_current_phone_vote_votes_idx', 'current_phone_vote', 'votes_id');
        $this->addForeignKey('fk_current_phone_vote_votes', 'current_phone_vote', 'votes_id', 'votes', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('current_sms_vote', [
            'id' => $this->primaryKey(11),
            'votes_id' => $this->integer(9)->notNull(),
            'phone' => $this->string(28)->notNull(),
            'vote_sms' => $this->integer(2)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_current_sms_vote_votes1_idx', 'current_sms_vote', 'votes_id');
        $this->addForeignKey('fk_current_sms_vote_votes1', 'current_sms_vote', 'votes_id', 'votes', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('arh_phone_vote', [
            'id' => $this->bigPrimaryKey(22),
            'votes_id' => $this->integer(9)->notNull(),
            'phone' => $this->string(28)->notNull(),
            'vote_phone' => $this->string(28)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_arh_phone_vote_votes1_idx', 'arh_phone_vote', 'votes_id');
        $this->addForeignKey('fk_arh_phone_vote_votes1', 'arh_phone_vote', 'votes_id', 'votes', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('arh_sms_vote', [
            'id' => $this->bigPrimaryKey(22),
            'votes_id' => $this->integer(9)->notNull(),
            'phone' => $this->string(28)->notNull(),
            'vote_sms' => $this->integer(2)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_arh_sms_vote_votes1_idx', 'arh_sms_vote', 'votes_id');
        $this->addForeignKey('fk_arh_sms_vote_votes1', 'arh_sms_vote', 'votes_id', 'votes', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        echo "m170113_221750_add_votes_tables cannot be reverted.\n";
        return false;
    }

}
