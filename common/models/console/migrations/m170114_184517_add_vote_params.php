<?php

use yii\db\Migration;

class m170114_184517_add_vote_params extends Migration {

    public function safeUp() {
        $this->addColumn('votes', 'params', 'TEXT(1500) NOT NULL AFTER status');
    }

    public function safeDown() {
        echo "m170114_184517_add_vote_params cannot be reverted.\n";
        return false;
    }

}
