<?php

use yii\db\Migration;

class m170308_205807_change_name_vote extends Migration {

    public function safeUp() {
        $this->dropIndex('name', 'votes');
    }

    public function safeDown() {
        $this->createIndex('name', 'votes', 'name', $unique = true);
        echo "m170308_205807_change_name_vote cannot be reverted.\n";
    }

}
