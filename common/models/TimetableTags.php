<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "timetable_tags".
 *
 * @property int $id
 * @property string $name
 * @property string $archive_name
 *
 * @property RadioArchive[] $radioArchives
 * @property Timetable[] $timetables
 */
class TimetableTags extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'timetable_tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['name', 'archive_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'archive_name' => Yii::t('common', 'Archive Name')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRadioArchives() {
        return $this->hasMany(RadioArchive::className(), ['timetable_tags_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimetables() {
        return $this->hasMany(Timetable::className(), ['timetable_tags_id' => 'id']);
    }

}
