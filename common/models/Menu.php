<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property integer $site_id
 * @property string $name
 * @property string $position
 * @property integer $priority
 * @property integer $parent_id
 * @property integer $status
 * @property string $description
 *
 * @property Site $site
 * @property MenuList[] $menuLists
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'name', 'position'], 'required'],
            [['site_id', 'priority', 'parent_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 250],
            [['position'], 'string', 'max' => 45],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'site_id' => Yii::t('common', 'Site ID'),
            'name' => Yii::t('common', 'Name'),
            'position' => Yii::t('common', 'Position'),
            'priority' => Yii::t('common', 'Priority'),
            'parent_id' => Yii::t('common', 'Parent ID'),
            'status' => Yii::t('common', 'Status'),
            'description' => Yii::t('common', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuLists()
    {
        return $this->hasMany(MenuList::className(), ['menu_id' => 'id']);
    }
}
