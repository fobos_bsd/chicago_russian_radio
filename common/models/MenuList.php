<?php

namespace common\models;

use Yii;

class MenuList extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'menu_list';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['menu_id', 'lang_id', 'name', 'alias'], 'required'],
            [['menu_id', 'lang_id', 'parent_id', 'status', 'gravity'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 250],
            [['alias', 'params'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'menu_id' => Yii::t('common', 'Menu ID'),
            'lang_id' => Yii::t('common', 'Lang ID'),
            'name' => Yii::t('common', 'Name'),
            'alias' => Yii::t('common', 'Alias'),
            'description' => Yii::t('common', 'Description'),
            'status' => Yii::t('common', 'Status'),
            'gravity' => Yii::t('common', 'Gravity'),
            'parent_id' => Yii::t('common', 'Parent'),
            'params' => Yii::t('common', 'Parameters'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang() {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu() {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

}
