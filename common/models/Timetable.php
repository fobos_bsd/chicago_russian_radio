<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper,
    yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "timetable".
 *
 * @property integer $id
 * @property integer $site_id
 * @property integer $lang_id
 * @property integer $youtube_playlists_id
 * @property string $day_of_week
 * @property string $name
 * @property string $description
 * @property string $fileImage
 * @property string $from_time
 * @property string $to_time
 * @property enum $timetable_tags_id
 *
 * @property Lang $lang
 * @property Site $site
 * @property YoutubePlaylists $youtubePlaylists
 */
class Timetable extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'timetable';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id', 'lang_id', 'day_of_week', 'name', 'from_time', 'to_time', 'onair', 'youtube_playlists_id'], 'required'],
            [['site_id', 'lang_id', 'youtube_playlists_id'], 'integer'],
            [['day_of_week', 'description'], 'string'],
            [['from_time', 'to_time'], 'safe'],
            [['name'], 'string', 'max' => 255],
            ['onair', 'in', 'range' => [0, 1]],
            //[['fileImage'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['youtube_playlists_id'], 'exist', 'skipOnError' => true, 'targetClass' => YoutubePlaylists::className(), 'targetAttribute' => ['youtube_playlists_id' => 'id']],
            [['timetable_tags_id'], 'exist', 'skipOnError' => true, 'targetClass' => TimetableTags::className(), 'targetAttribute' => ['timetable_tags_id' => 'id']],
            ['name', 'filter', 'filter' => 'trim'],
            ['onair', 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    /*
      public function beforeSave($insert) {
      if (parent::beforeSave($insert)) {
      if (isset($this->fileImage) && is_object($this->fileImage)) {
      $resupload = $this->upload($this->fileImage);

      if ($resupload !== false) {
      $this->fileImage = $resupload;
      }
      }
      return true;
      }
      return false;
      }
     * 
     */

    /**
     * @inheritdoc
     */
    /*
      public function upload($image) {
      if ($this->validate()) {
      $filename = $image->baseName . '.' . $image->extension;
      $path = Yii::getAlias('@frontend') . '/web/uploads/timetable/' . $filename;
      $image->saveAs($path);
      return $filename;
      } else {
      return false;
      }
      }
     * 
     */

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'site_id' => Yii::t('common', 'Site'),
            'lang_id' => Yii::t('common', 'Lang'),
            'youtube_playlists_id' => Yii::t('common', 'Youtube Playlists'),
            'day_of_week' => Yii::t('common', 'Day Of Week'),
            'name' => Yii::t('common', 'Name'),
            'description' => Yii::t('common', 'Description'),
            'fileImage' => Yii::t('common', 'File Image'),
            'from_time' => Yii::t('common', 'From Time'),
            'to_time' => Yii::t('common', 'To Time'),
            'timetable_tags_id' => Yii::t('common', 'Tags'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang() {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimetableTags() {
        return $this->hasOne(TimetableTags::className(), ['id' => 'timetable_tags_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYoutubePlaylists() {
        return $this->hasOne(YoutubePlaylists::className(), ['id' => 'youtube_playlists_id']);
    }

}
