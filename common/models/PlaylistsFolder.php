<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "playlists_folder".
 *
 * @property int $id
 * @property string $link
 * @property int $status
 */
class PlaylistsFolder extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'playlists_folder';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['link'], 'required'],
            [['link'], 'string'],
            [['status'], 'boolean'],
            ['link', 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'link' => Yii::t('common', 'Link'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

}
