<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "audio_adv_archive".
 *
 * @property string $id
 * @property int $audio_advertising_id
 * @property int $timeused
 *
 * @property AudioAdvertising $audioAdvertising
 */
class AudioAdvArchive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audio_adv_archive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['audio_advertising_id', 'timeused'], 'required'],
            [['audio_advertising_id', 'timeused'], 'integer'],
            [['audio_advertising_id'], 'exist', 'skipOnError' => true, 'targetClass' => AudioAdvertising::className(), 'targetAttribute' => ['audio_advertising_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'audio_advertising_id' => Yii::t('common', 'Audio Advertising ID'),
            'timeused' => Yii::t('common', 'Timeused'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAudioAdvertising()
    {
        return $this->hasOne(AudioAdvertising::className(), ['id' => 'audio_advertising_id']);
    }
}
