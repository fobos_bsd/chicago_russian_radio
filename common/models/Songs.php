<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "songs".
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string $dropbox_id
 * @property string $description
 * @property int $rank
 * @property int $status
 * @property text $fileAudio
 *
 * @property SongVotes[] $songVotes
 */
class Songs extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'songs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title', 'url'], 'string', 'max' => 500],
            [['description'], 'string', 'max' => 15000],
            [['rank'], 'integer'],
            [['dropbox_id'], 'string', 'max' => 255],
            [['status'], 'boolean'],
            [['fileAudio'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 15],
            [['title', 'url', 'description', 'dropbox_id'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }],
            ['rank', 'default', 'value' => 0],
            ['status', 'default', 'value' => 1]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'url' => Yii::t('common', 'Url'),
            'dropbox_id' => Yii::t('common', 'Dropbox ID'),
            'description' => Yii::t('common', 'Description'),
            'rank' => Yii::t('common', 'Rank'),
            'status' => Yii::t('common', 'Status'),
            'fileAudio' => Yii::t('common', 'Audio file'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSongVotes() {
        return $this->hasMany(SongVotes::className(), ['songs_id' => 'id']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {

            if (isset($this->fileAudio) && isset($this->fileAudio->baseName)) {
                $filename = $this->fileAudio->baseName . '.' . $this->fileAudio->extension;
                $path = Yii::getAlias('@backend') . '/web/uploads/songs/' . $filename;
                $this->fileAudio->saveAs($path);
            }

            return true;
        }
        return false;
    }

}
