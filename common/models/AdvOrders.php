<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "adv_orders".
 *
 * @property integer $id
 * @property integer $clients_id
 * @property integer $count_days
 * @property integer $count_onday
 * @property integer $used_days
 * @property string $content
 * @property integer $how_seconds
 * @property string $start_at
 * @property string $finish_at
 * @property integer $created_at
 *
 * @property AdvCalendar[] $advCalendars
 * @property Clients $clients
 */
class AdvOrders extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adv_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['clients_id', 'start_at', 'finish_at', 'created_at', 'title', 'site_id'], 'required'],
            [['clients_id', 'created_at', 'site_id'], 'integer'],
            [['count_days', 'count_onday', 'how_seconds'], 'integer', 'min' => 1, 'max' => 1000],
            [['used_days'], 'integer', 'min' => 0, 'max' => 1000],
            [['content', 'file_link'], 'string'],
            ['fileAudio', 'file', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false, 'extensions' => 'mp3, mp4, wav', 'maxSize' => 10485760, 'on' => ['insert']],
            ['fileAudio', 'safe', 'on' => ['update']],
            [['start_at', 'finish_at'], 'safe'],
            [['clients_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['clients_id' => 'id']],
            [['status'], 'in', 'range' => ['On Air', 'New']],
            [['status'], 'default', 'value' => 'New'],
            [['how_seconds'], 'default', 'value' => 1],
            [['count_days', 'count_onday', 'used_days'], 'default', 'value' => 0],
            ['start_at', 'default', 'value' => time()]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate() {
        $unixstart = strtotime($this->start_at);
        if (isset($this->count_days) && !empty($this->count_days) && (!isset($this->finish_at) || empty($this->finish_at))) {
            $seconds = (int) $this->count_days * 86400;
            $unixfinish = $unixstart + $seconds;
            $this->finish_at = date('Y-m-d', $unixfinish);
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'clients_id' => 'Clients ID',
            'count_days' => 'Amount of days',
            'count_onday' => 'The number of times per day',
            'used_days' => 'Used days',
            'title' => 'Title',
            'fileAudio' => 'File',
            'file_link' => 'Link to file',
            'content' => 'Content',
            'how_seconds' => 'Duration in seconds',
            'start_at' => 'Start',
            'finish_at' => 'Finish',
            'created_at' => 'Created',
            'site_id' => 'Site',
            'status' => 'Status'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvCalendars() {
        return $this->hasMany(AdvCalendar::className(), ['adv_orders_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients() {
        return $this->hasOne(Clients::className(), ['id' => 'clients_id']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (isset($this->fileAudio) && is_object($this->fileAudio)) {
                $resupload = $this->upload();
                if ($resupload !== false) {
                    $this->fileAudio = $resupload;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function upload() {
        if ($this->validate()) {
            $dir = Yii::getAlias('@backend') . '/web/uploads/sound_reklam/';
            $filename = $this->fileAudio->baseName . '.' . $this->fileAudio->extension;
            $this->fileAudio->saveAs($dir . $filename);
            return $filename;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

}
