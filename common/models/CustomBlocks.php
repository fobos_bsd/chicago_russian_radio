<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "custom_blocks".
 *
 * @property int $id
 * @property int $site_id
 * @property int $lang_id
 * @property string $name
 * @property string $content
 *
 * @property Lang $lang
 * @property Site $site
 */
class CustomBlocks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'lang_id', 'name'], 'required'],
            [['site_id', 'lang_id'], 'integer'],
            [['content'], 'string'],
            [['name'], 'string', 'max' => 250],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'site_id' => Yii::t('common', 'Site ID'),
            'lang_id' => Yii::t('common', 'Lang ID'),
            'name' => Yii::t('common', 'Name'),
            'content' => Yii::t('common', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
