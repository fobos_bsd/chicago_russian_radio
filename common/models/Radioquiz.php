<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "radioquiz".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $prize
 * @property string $rules
 * @property string $participant
 * @property string $prize_image
 * @property string $participant_photo
 * @property string $questions
 * @property string $answers
 * @property int $status
 * @property string $advertiser_name
 * @property string $advertiser_site
 */
class Radioquiz extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'radioquiz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title', 'advertiser_name', 'advertiser_site'], 'string', 'max' => 500],
            [['description'], 'string', 'max' => 15000],
            [['prize', 'rules', 'questions', 'answers'], 'string', 'max' => 5000],
            [['participant'], 'string', 'max' => 1500],
            [['prize_image', 'participant_photo'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'gif'], 'mimeTypes' => ['image/jpeg', 'image/png', 'image/gif'], 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 8],
            [['status'], 'boolean'],
            [['title', 'advertiser_name', 'description', 'prize', 'rules', 'questions', 'answers', 'participant', 'advertiser_site'], 'filter', 'filter' => 'trim']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'description' => Yii::t('common', 'Description'),
            'prize' => Yii::t('common', 'Prize'),
            'rules' => Yii::t('common', 'Rules'),
            'participant' => Yii::t('common', 'Participant'),
            'prize_image' => Yii::t('common', 'Prize Image'),
            'participant_photo' => Yii::t('common', 'Participant Photo'),
            'questions' => Yii::t('common', 'Questions'),
            'answers' => Yii::t('common', 'Answers'),
            'status' => Yii::t('common', 'Status'),
            'advertiser_name' => Yii::t('common', 'Name of advertiser'),
            'advertiser_site' => Yii::t('common', 'Site of advertiser'),
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {

            $this->updateAll(['status' => 0]);

            if (isset($this->prize_image) && isset($this->prize_image->baseName)) {
                $filename = $this->prize_image->baseName . '.' . $this->prize_image->extension;
                $path = Yii::getAlias('@backend') . '/web/uploads/radioquiz/' . $filename;
                $this->prize_image->saveAs($path);
            }

            if (isset($this->participant_photo) && isset($this->participant_photo->baseName)) {
                $filename = $this->participant_photo->baseName . '.' . $this->participant_photo->extension;
                $path = Yii::getAlias('@backend') . '/web/uploads/radioquiz/' . $filename;
                $this->participant_photo->saveAs($path);
            }

            return true;
        }
        return false;
    }

}
