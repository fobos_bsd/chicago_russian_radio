<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "site".
 *
 * @property integer $id
 * @property integer $lang_id
 * @property string $name
 * @property string $domain
 * @property string $description
 * @property integer $status
 *
 * @property Menu[] $menus
 * @property OnlineQuestions[] $onlineQuestions
 * @property Pages[] $pages
 * @property Settings[] $settings
 * @property Lang $lang
 * @property UserQuestions[] $userQuestions
 */
class Site extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'site';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['lang_id', 'name', 'domain'], 'required'],
            [['lang_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 250],
            [['domain'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'lang_id' => Yii::t('common', 'Lang ID'),
            'name' => Yii::t('common', 'Name'),
            'domain' => Yii::t('common', 'Domain'),
            'description' => Yii::t('common', 'Description'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus() {
        return $this->hasMany(Menu::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnlineQuestions() {
        return $this->hasMany(OnlineQuestions::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages() {
        return $this->hasMany(Pages::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettings() {
        return $this->hasMany(Settings::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang() {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserQuestions() {
        return $this->hasMany(UserQuestions::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoogleStatistic() {
        return $this->hasMany(GoogleStatistic::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnlineVotes() {
        return $this->hasMany(OnlineVotes::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvSettings() {
        return $this->hasMany(AdvSettings::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvOrders() {
        return $this->hasMany(AdvSettings::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvMesh() {
        return $this->hasMany(AdvSettings::className(), ['site_id' => 'id']);
    }

}
