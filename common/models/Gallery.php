<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper,
    yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "gallery".
 *
 * @property int $id
 * @property int $site_id
 * @property string $name
 * @property string $slug
 * @property int $parent_id
 * @property string $title
 * @property string $keywords
 * @property string $meta_description
 * @property string $description
 * @property int $status
 *
 * @property Site $site
 * @property GalleryData[] $galleryDatas
 */
class Gallery extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id', 'name', 'slug'], 'required'],
            [['site_id', 'parent_id', 'youtube_playlists_id', 'status'], 'integer'],
            [['keywords', 'meta_description', 'description'], 'string'],
            [['name', 'slug', 'title'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['youtube_playlists_id'], 'exist', 'skipOnError' => true, 'targetClass' => YoutubePlaylists::className(), 'targetAttribute' => ['youtube_playlists_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        return ArrayHelper::merge([
                    [
                        'class' => SluggableBehavior::className(),
                        'attribute' => 'name',
                        'slugAttribute' => 'slug',
                        'immutable' => 'true',
                    ],
                        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'site_id' => Yii::t('common', 'Site ID'),
            'name' => Yii::t('common', 'Name'),
            'slug' => Yii::t('common', 'Slug'),
            'parent_id' => Yii::t('common', 'Parent ID'),
            'youtube_playlists_id' => Yii::t('common', 'Playlist ID'),
            'title' => Yii::t('common', 'Title'),
            'keywords' => Yii::t('common', 'Keywords'),
            'meta_description' => Yii::t('common', 'Meta Description'),
            'description' => Yii::t('common', 'Description'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryDatas() {
        return $this->hasMany(GalleryData::className(), ['gallery_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYoutubePlaylists() {
        return $this->hasOne(YoutubePlaylists::className(), ['id' => 'youtube_playlists_id']);
    }

}
