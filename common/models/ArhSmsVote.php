<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "arh_sms_vote".
 *
 * @property integer $id
 * @property integer $online_votes_id
 * @property string $phone
 * @property integer $vote_sms
 * @property integer $created_at
 *
 * @property Votes $votes
 */
class ArhSmsVote extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'arh_sms_vote';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['online_votes_id', 'phone', 'vote_sms', 'created_at'], 'required'],
            [['online_votes_id', 'vote_sms', 'created_at'], 'integer'],
            [['phone'], 'string', 'max' => 28],
            [['online_votes_id'], 'exist', 'skipOnError' => true, 'targetClass' => OnlineVotes::className(), 'targetAttribute' => ['online_votes_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'online_votes_id' => 'Online votes ID',
            'phone' => 'Phone',
            'vote_sms' => 'Vote Sms',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotes() {
        return $this->hasOne(OnlineVotes::className(), ['id' => 'online_votes_id']);
    }

}
