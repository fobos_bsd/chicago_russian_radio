<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "current_phone_vote".
 *
 * @property integer $id
 * @property integer $online_votes_id
 * @property string $phone
 * @property string $vote_phone
 * @property integer $created_at
 *
 * @property Votes $votes
 */
class CurrentPhoneVote extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'current_phone_vote';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['online_votes_id', 'phone', 'vote_phone', 'created_at'], 'required'],
            [['online_votes_id', 'created_at'], 'integer'],
            [['phone', 'vote_phone'], 'string', 'max' => 28],
            [['online_votes_id'], 'exist', 'skipOnError' => true, 'targetClass' => OnlineVotes::className(), 'targetAttribute' => ['online_votes_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'online_votes_id' => 'Votes ID',
            'phone' => 'Phone',
            'vote_phone' => 'Vote Phone',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotes() {
        return $this->hasOne(OnlineVotes::className(), ['id' => 'online_votes_id']);
    }

}
