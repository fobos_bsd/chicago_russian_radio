<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "song_votes".
 *
 * @property string $id
 * @property int $songs_id
 * @property string $ip_address
 *
 * @property Songs $songs
 */
class SongVotes extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'song_votes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['songs_id'], 'required'],
            [['songs_id'], 'integer'],
            [['ip_address'], 'ip'],
            [['songs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Songs::className(), 'targetAttribute' => ['songs_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'songs_id' => Yii::t('common', 'Songs ID'),
            'ip_address' => Yii::t('common', 'Ip Address'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSongs() {
        return $this->hasOne(Songs::className(), ['id' => 'songs_id']);
    }

}
