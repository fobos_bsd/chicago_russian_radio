<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "answers".
 *
 * @property integer $id
 * @property integer $site_id
 * @property integer $questions_id
 * @property string $name
 * @property Questions $question
 * @property Votes $votes
 *
 */
class Answers extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'answers';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id','questions_id', 'name'], 'required'],
            [['site_id','questions_id'], 'integer'],
            [['questions_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['questions_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'questions_id' => Yii::t('common', 'Question'),
        ];
    }

    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['id' => 'questions_id']);
    }

    public function getVotes()
    {
        return $this->hasMany(Votes::className(), ['answers_id' => 'id']);
    }
}
