<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper,
    yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "youtube_channels".
 *
 * @property integer $id
 * @property integer $site_id
 * @property string $channel_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property integer $status
 *
 * @property Site $site
 * @property YoutubePlaylists[] $youtubePlaylists
 */
class YoutubeChannels extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'youtube_channels';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id', 'channel_id', 'name', 'slug'], 'required'],
            [['site_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['channel_id'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 250],
            [['slug', 'meta_key'], 'string', 'max' => 255],
            [['meta_description'], 'string', 'max' => 500],
            [['channel_id'], 'unique'],
            [['slug'], 'unique'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        return ArrayHelper::merge([
                    [
                        'class' => SluggableBehavior::className(),
                        'attribute' => 'name',
                        'slugAttribute' => 'slug',
                        'immutable' => 'true',
                    ],
                        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'site_id' => Yii::t('common', 'Site ID'),
            'channel_id' => Yii::t('common', 'Channel ID'),
            'name' => Yii::t('common', 'Name'),
            'slug' => Yii::t('common', 'Slug'),
            'description' => Yii::t('common', 'Description'),
            'status' => Yii::t('common', 'Status'),
            'meta_key' => Yii::t('common', 'Meta Keys'),
            'meta_description' => Yii::t('common', 'Meta description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYoutubePlaylists() {
        return $this->hasMany(YoutubePlaylists::className(), ['youtube_channels_id' => 'id']);
    }

}
