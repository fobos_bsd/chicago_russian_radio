<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner_statistics".
 *
 * @property integer $banner_id
 * @property integer $click_count
 * @property integer $show_count
 *
 */
class BannerStatistic extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'banner_statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['click_count', 'show_count','banner_id'], 'integer'],
            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => BannerItem::className(), 'targetAttribute' => ['banner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'banner_id' => Yii::t('common', 'ID'),
            'click_count' => Yii::t('common', 'Number of clicks'),
            'show_count' => Yii::t('common', 'Number of shows'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner() {
        return $this->hasOne(BannerItem::className(), ['banner_id' => 'id']);
    }

}
