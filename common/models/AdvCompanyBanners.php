<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner_item_has_adv_company".
 *
 * @property integer $banner_item_id
 * @property integer $adv_company_id
 *
 */
class AdvCompanyBanners extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'banner_item_has_adv_company';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['banner_item_id','adv_company_id'], 'required'],
            [['banner_item_id','adv_company_id'], 'integer'],
            [['banner_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BannerItem::className(),
                'targetAttribute' => ['banner_item_id' => 'id']],
            [['adv_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvCompany::className(),
                'targetAttribute' => ['adv_company_id' => 'id']],
        ];
    }
}
