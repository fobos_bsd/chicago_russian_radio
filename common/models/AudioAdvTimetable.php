<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "audio_adv_timetable".
 *
 * @property int $id
 * @property int $audio_advertising_id
 * @property string $day_of_week
 * @property string $start_time
 * @property string $from_date
 * @property string $to_date
 * @property int $status
 *
 * @property AudioAdvertising $audioAdvertising
 */
class AudioAdvTimetable extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'audio_adv_timetable';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['audio_advertising_id', 'day_of_week', 'start_time', 'from_date', 'to_date'], 'required'],
            [['audio_advertising_id', 'lasttime_read', 'status'], 'integer'],
            [['day_of_week'], 'string'],
            [['start_time', 'from_date', 'to_date'], 'safe'],
            [['audio_advertising_id'], 'exist', 'skipOnError' => true, 'targetClass' => AudioAdvertising::className(), 'targetAttribute' => ['audio_advertising_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'audio_advertising_id' => Yii::t('common', 'Reclam'),
            'day_of_week' => Yii::t('common', 'Day Of Week'),
            'start_time' => Yii::t('common', 'Start Time'),
            'from_date' => Yii::t('common', 'From Date'),
            'to_date' => Yii::t('common', 'To Date'),
            'status' => Yii::t('common', 'Status'),
            'lasttime_read' => Yii::t('common', 'Last read'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAudioAdvertising() {
        return $this->hasOne(AudioAdvertising::className(), ['id' => 'audio_advertising_id']);
    }

}
