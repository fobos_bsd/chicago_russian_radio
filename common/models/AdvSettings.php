<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "adv_settings".
 *
 * @property integer $id
 * @property string $name
 * @property string $setvalue
 * @property string $description
 */
class AdvSettings extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adv_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'setvalue', 'site_id'], 'required'],
            ['site_id', 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 250],
            [['setvalue'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'setvalue' => 'Setvalue',
            'description' => 'Description',
            'site_id' => 'Site'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

}
