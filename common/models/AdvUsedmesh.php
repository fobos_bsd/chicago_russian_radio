<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "adv_usedmesh".
 *
 * @property integer $adv_mesh_id
 * @property integer $used
 * @property integer $less_seconds
 * @property integer $used_procent
 * @property string $used_date
 *
 * @property AdvMesh $advMesh
 */
class AdvUsedmesh extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adv_usedmesh';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adv_mesh_id', 'used_date'], 'required'],
            [['adv_mesh_id', 'used', 'less_seconds', 'used_procent'], 'integer'],
            [['used_date'], 'safe'],
            [['adv_mesh_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvMesh::className(), 'targetAttribute' => ['adv_mesh_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'adv_mesh_id' => 'Adv Mesh ID',
            'used' => 'Used',
            'less_seconds' => 'Less Seconds',
            'used_procent' => 'Used Procent',
            'used_date' => 'Used Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvMesh()
    {
        return $this->hasOne(AdvMesh::className(), ['id' => 'adv_mesh_id']);
    }
}
