<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property integer $id
 * @property integer $site_id
 * @property string $name
 * @property boolean $active
 * @property Votes $votes
 * @property Votes $answers
 *
 */
class Questions extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'questions';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id', 'name','active'], 'required'],
            [['site_id'], 'integer'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'active' => Yii::t('common', 'Active'),
        ];
    }

    public function getVotes()
    {
        return $this->hasMany(Votes::className(), ['questions_id' => 'id']);
    }

    public function getAnswers()
    {
        return $this->hasMany(Answers::className(), ['questions_id' => 'id']);
    }
}
