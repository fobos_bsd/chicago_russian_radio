<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_reviews".
 *
 * @property integer $id
 * @property integer $pages_id
 * @property integer $raiting
 * @property string $username
 * @property integer $created_at
 * @property integer $status
 * @property string $comment
 * @property string $user_ip
 *
 * @property Pages $pages
 */
class PageReviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pages_id', 'username', 'created_at', 'comment'], 'required'],
            [['pages_id', 'raiting', 'created_at', 'status'], 'integer'],
            [['comment'], 'string'],
            [['username', 'user_ip'], 'string', 'max' => 255],
            [['pages_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pages::className(), 'targetAttribute' => ['pages_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'pages_id' => Yii::t('common', 'Pages ID'),
            'raiting' => Yii::t('common', 'Raiting'),
            'username' => Yii::t('common', 'Username'),
            'created_at' => Yii::t('common', 'Created At'),
            'status' => Yii::t('common', 'Status'),
            'comment' => Yii::t('common', 'Comment'),
            'user_ip' => Yii::t('common', 'User Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasOne(Pages::className(), ['id' => 'pages_id']);
    }
}
