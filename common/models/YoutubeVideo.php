<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "youtube_video".
 *
 * @property integer $id
 * @property integer $youtube_playlists_id
 * @property string $video_id
 * @property integer $publish_at
 * @property string $title
 * @property string $description
 * @property string $thumbnails_medium
 * @property string $thumbnails_maxres
 * @property integer $status
 *
 * @property YoutubePlaylists $youtubePlaylists
 */
class YoutubeVideo extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'youtube_video';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['youtube_playlists_id', 'video_id', 'socialcode'], 'required'],
            [['youtube_playlists_id', 'publish_at', 'status'], 'integer'],
            [['description', 'thumbnails_medium', 'thumbnails_maxres'], 'string'],
            [['video_id'], 'string', 'max' => 32],
            [['title'], 'string', 'max' => 255],
            [['video_id'], 'unique'],
            ['socialcode', 'in', 'range' => ['fb', 'vk', 'tw', 'youtube']],
            ['socialcode', 'default', 'value' => 'youtube'],
            [['youtube_playlists_id'], 'exist', 'skipOnError' => true, 'targetClass' => YoutubePlaylists::className(), 'targetAttribute' => ['youtube_playlists_id' => 'id']],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['autoinsert'] = ['youtube_playlists_id', 'video_id', 'publish_at', 'status'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'youtube_playlists_id' => Yii::t('common', 'Youtube Playlists ID'),
            'video_id' => Yii::t('common', 'Video ID'),
            'publish_at' => Yii::t('common', 'Publish At'),
            'title' => Yii::t('common', 'Title'),
            'description' => Yii::t('common', 'Description'),
            'thumbnails_medium' => Yii::t('common', 'Thumbnails Medium'),
            'thumbnails_maxres' => Yii::t('common', 'Thumbnails Maxres'),
            'socialcode' => Yii::t('common', 'Type social link'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYoutubePlaylists() {
        return $this->hasOne(YoutubePlaylists::className(), ['id' => 'youtube_playlists_id']);
    }

}
