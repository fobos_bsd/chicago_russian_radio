<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "adv_mesh".
 *
 * @property integer $id
 * @property string $start_time
 * @property string $end_time
 * @property integer $long
 * @property integer $important
 */
class AdvMesh extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adv_mesh';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['start_time', 'end_time', 'long', 'site_id'], 'required'],
            [['start_time', 'end_time'], 'safe'],
            [['long'], 'integer', 'min' => 1, 'max' => 3600],
            [['important'], 'integer', 'min' => 1, 'max' => 15],
            [['site_id', 'spacialy'], 'integer'],
            [['spacialy'], 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'long' => 'Duration in seconds',
            'important' => 'Priority',
            'site_id' => 'Site',
            'spacialy' => 'Special'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

}
