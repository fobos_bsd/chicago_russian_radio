<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "top_videos".
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string $description
 * @property int $rank
 */
class TopVideos extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'top_videos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['title', 'url'], 'required'],
            [['title', 'url'], 'string', 'max' => 500],
            [['description'], 'string', 'max' => 15000],
            [['rank'], 'integer'],
            [['title', 'url', 'description'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'url' => Yii::t('common', 'Url'),
            'description' => Yii::t('common', 'Description'),
            'rank' => Yii::t('common', 'Rank'),
        ];
    }

}
