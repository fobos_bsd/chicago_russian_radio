<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper,
    yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $site_id
 * @property integer $lang_id
 * @property string $name
 * @property string $slug
 * @property integer $status
 * @property string $content
 * @property string $meta_title
 * @property string $meta_key
 * @property string $meta_description
 * @property string $params
 * @property string $thumb
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PageReviews[] $pageReviews
 * @property Lang $lang
 * @property Site $site
 * @property User $user
 */
class Pages extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id', 'site_id', 'lang_id', 'name', 'slug', 'meta_title', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'site_id', 'lang_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['content', 'meta_description', 'params'], 'string'],
            [['name', 'meta_title', 'meta_key'], 'string', 'max' => 250],
            [['slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['thumb'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        return ArrayHelper::merge([
                    [
                        'class' => SluggableBehavior::className(),
                        'attribute' => 'name',
                        'slugAttribute' => 'slug',
                        'immutable' => 'true',
                    ],
                        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (isset($this->thumb) && is_object($this->thumb)) {
                $resupload = $this->upload($this->thumb);

                if ($resupload !== false) {
                    $this->thumb = $resupload;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function upload($image) {
        if ($this->validate()) {
            $filename = $image->baseName . '.' . $image->extension;
            $path = Yii::getAlias('@frontend') . '/web/uploads/pages/' . $filename;
            $image->saveAs($path);
            return $filename;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'user_id' => Yii::t('common', 'User ID'),
            'site_id' => Yii::t('common', 'Site ID'),
            'lang_id' => Yii::t('common', 'Lang ID'),
            'name' => Yii::t('common', 'Name'),
            'slug' => Yii::t('common', 'Slug'),
            'status' => Yii::t('common', 'Status'),
            'content' => Yii::t('common', 'Content'),
            'meta_title' => Yii::t('common', 'Meta Title'),
            'meta_key' => Yii::t('common', 'Meta Key'),
            'meta_description' => Yii::t('common', 'Meta Description'),
            'params' => Yii::t('common', 'Params'),
            'thumb' => Yii::t('common', 'Thumb'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageReviews() {
        return $this->hasMany(PageReviews::className(), ['pages_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang() {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
