<?php

namespace common\models;

/**
 * Description of GoogleYoutube
 *
 * @author fobos
 */
use Yii,
    yii\base\BaseObject;
use Google_Client,
    Google_Service_YouTube,
    linslin\yii2\curl;
use common\models\Settings,
    common\models\Site,
    common\models\YoutubePlaylists,
    common\models\YoutubeVideo;

class GoogleYoutube extends BaseObject {

    private $client, $youtube;

    // Iniciale global params
    public function init() {
        parent::init();

        $this->client = $this->initializeClient();
        $this->youtube = (isset($this->client)) ? new Google_Service_YouTube($this->client) : null;
    }

    // Get chanel by ID
    public function getChanelPlaylists($chid, $count = 5) {
        if (isset($this->youtube)) {
            try {
                $searchResponse = $this->youtube->playlists->listPlaylists('id,snippet', array('channelId' => $chid, 'maxResults' => $count));
                return $searchResponse;
            } catch (Exception $ex) {
                return $ex;
            }
        } else {
            return false;
        }

        return false;
    }

    // Get playlist video by ID
    public function getPlaylistsVideo($plid, $count = 5) {
        if (isset($this->youtube)) {
            try {
                $searchResponse = $this->youtube->playlistItems->listPlaylistItems('id,snippet', array('playlistId' => $plid, 'maxResults' => $count));
                return $searchResponse;
            } catch (Exception $ex) {
                return $ex;
            }
        } else {
            return false;
        }

        return false;
    }

    // Get chanel by ID
    public function getChanelPlaylistsVideo($chid, $count = 5) {
        $arrplay = $this->getPlaylistsID($chid, $count);

        if (isset($arrplay) && count($arrplay) > 0 && isset($this->youtube)) {
            try {

                $searchResponse = $youtube->playlists->listPlaylists('id,snippet', array('channelId' => $chid, 'maxResults' => $count));
                return $searchResponse;
            } catch (Exception $ex) {
                return $ex;
            }
        }

        return false;
    }

    // Get video from channel
    public function getChanelVideos($chid, $count = 5) {
        if (isset($this->youtube)) {
            try {
                $searchResponse = $this->youtube->search->listSearch('id,snippet', array('channelId' => $chid, 'maxResults' => $count, 'order' => 'date'));
                return $searchResponse;
            } catch (Exception $ex) {
                return $ex;
            }
        } else {
            return false;
        }
    }

    // Inicialization Google Youtube Data API v3 by develop key
    private function initializeClient() {

        // Create and configure a new client object.
        $client = new Google_Client();
        $keyDevelop = $this->getKey();

        if (isset($keyDevelop) && $keyDevelop !== false) {
            $client->setDeveloperKey($keyDevelop);
            return $client;
        } else {
            return null;
        }
    }

    // Get development key from site setting
    protected function getKey() {
        $getcurrSite = Site::find()->where(['domain' => 'russian1330.com'])->asArray()->one();
        $onekeys = Settings::find()->where(['site_id' => $getcurrSite['id'], 'name' => 'developer_key'])->asArray()->one();

        if (isset($onekeys) && count($onekeys) > 0) {
            return $onekeys['value'];
        } else {
            return false;
        }
    }

    // Set ID of play lists to channel
    public function setPlaylistsID($arrpl, $count, $chid) {
        // Create array of playlists ID
        foreach ($arrpl as $plst) {

            if (($playlists = YoutubePlaylists::find()->where(['playlist_id' => $plst['id']])->one()) === null) {
                $model = new YoutubePlaylists(['scenario' => 'autoinsert']);
                $model->youtube_channels_id = $chid;
                $model->playlist_id = $plst['id'];
                $model->etag = $plst['etag'];
                $model->status = 1;
                $model->publish_at = (isset($plst['snippet']['publishedAt']) && !empty($plst['snippet']['publishedAt'])) ? strtotime($plst['snippet']['publishedAt']) : time();
                $model->title = (isset($plst['snippet']['title']) && !empty($plst['snippet']['title'])) ? $plst['snippet']['title'] : null;
                $model->description = (isset($plst['snippet']['description']) && !empty($plst['snippet']['description'])) ? $plst['snippet']['description'] : null;
                $model->thumbnails_medium = (isset($plst['snippet']['thumbnails']['medium']['url']) && !empty($plst['snippet']['thumbnails']['medium']['url'])) ? $plst['snippet']['thumbnails']['medium']['url'] : null;
                $model->thumbnails_maxres = (isset($plst['snippet']['thumbnails']['maxres']['url']) && !empty($plst['snippet']['thumbnails']['maxres']['url'])) ? $plst['snippet']['thumbnails']['maxres']['url'] : null;
                if (!$model->insert()) {
                    var_dump($model->getErrors());
                }
            }
        }
        return true;
    }

    // Set ID of videi to play lists
    public function setVideosID($arrvd, $count, $plid) {
        // Create array of playlists ID
        foreach ($arrvd as $vd) {

            if (($playlists = YoutubeVideo::find()->where(['video_id' => $vd['snippet']['resourceId']['videoId']])->one()) === null) {
                $model = new YoutubeVideo(['scenario' => 'autoinsert']);
                $model->youtube_playlists_id = $plid;
                $model->video_id = $vd['snippet']['resourceId']['videoId'];
                $model->status = 1;
                $model->publish_at = (isset($vd['snippet']['publishedAt']) && !empty($vd['snippet']['publishedAt'])) ? strtotime($vd['snippet']['publishedAt']) : time();
                $model->title = (isset($vd['snippet']['title']) && !empty($vd['snippet']['title'])) ? $vd['snippet']['title'] : null;
                $model->description = (isset($vd['snippet']['description']) && !empty($vd['snippet']['description'])) ? $vd['snippet']['description'] : null;
                $model->thumbnails_medium = (isset($vd['snippet']['thumbnails']['medium']['url']) && !empty($vd['snippet']['thumbnails']['medium']['url'])) ? $vd['snippet']['thumbnails']['medium']['url'] : null;
                $model->thumbnails_maxres = (isset($vd['snippet']['thumbnails']['maxres']['url']) && !empty($vd['snippet']['thumbnails']['maxres']['url'])) ? $vd['snippet']['thumbnails']['maxres']['url'] : null;

                if (!$model->insert()) {
                    var_dump($model->getErrors());
                }
            }
        }
        return true;
    }

}
