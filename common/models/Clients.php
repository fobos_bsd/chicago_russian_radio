<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $params
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AdvOrders[] $advOrders
 */
class Clients extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'phone', 'created_at', 'updated_at'], 'required'],
            [['params', 'description'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate() {
        if ($this->isNewRecord) {
            $this->created_at = time();
            $this->updated_at = time();
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'params' => 'Params',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvOrders() {
        return $this->hasMany(AdvOrders::className(), ['clients_id' => 'id']);
    }

}
