<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GalleryData;

/**
 * GallerySearch represents the model behind the search form about `common\models\Gallery`.
 */
class GalleryDataSearch extends GalleryData {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'gallery_id', 'status', 'gravity'], 'integer'],
            [['name', 'title', 'link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = GalleryData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gallery_id' => $this->gallery_id,
            'status' => $this->status,
            'gravity' => $this->gravity,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }

}
