<?php

namespace common\models\search;

use common\models\OnlineQuestions;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OnlineQuestionsSearch represents the model behind the search form about `common\models\OnlineQuestions`.
 */
class OnlineQuestionsSearch extends OnlineQuestions {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id','status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OnlineQuestions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $this->site_id = Yii::$app->Siteselector->getSitesCookie();

        // grid filtering conditions
        $query->andFilterWhere([
            'site_id' => $this->site_id,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }

}
