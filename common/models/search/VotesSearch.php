<?php

namespace common\models\search;

use common\models\Votes;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VotesSearch represents the model behind the search form about `common\models\Votes`.
 */
class VotesSearch extends Votes {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id','answers_id','questions_id'], 'integer'],
            [['ip'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Votes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $this->site_id = Yii::$app->Siteselector->getSitesCookie();

        // grid filtering conditions
        $query->andFilterWhere([
            'site_id' => $this->site_id,
            'answers_id' => $this->answers_id,
            'questions_id' => $this->questions_id,
        ]);

        $query->andFilterWhere(['like', 'ip', $this->ip]);

        return $dataProvider;
    }

}
