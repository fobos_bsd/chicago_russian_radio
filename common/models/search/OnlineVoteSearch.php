<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OnlineVotes;

/**
 * VoteSearch represents the model behind the search form about `common\models\Votes`.
 */
class OnlineVoteSearch extends OnlineVotes {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'site_id', 'created_at', 'start_at', 'finish_at', 'status', 'period_call'], 'integer'],
            [['name', 'slug', 'description', 'params'], 'safe'],
            [['vtype'], 'in', 'range' => ['phone', 'sms']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OnlineVotes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->site_id = Yii::$app->Siteselector->getSitesCookie();

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'site_id' => $this->site_id,
            'created_at' => $this->created_at,
            'start_at' => $this->start_at,
            'finish_at' => $this->finish_at,
            'status' => $this->status,
            'vtype' => $this->vtype
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'params', $this->params]);

        return $dataProvider;
    }

}
