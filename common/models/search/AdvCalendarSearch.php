<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AdvCalendar;

/**
 * AdvCalendarSearch represents the model behind the search form about `common\models\AdvCalendar`.
 */
class AdvCalendarSearch extends AdvCalendar {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'adv_mesh_id', 'adv_orders_id', 'gravity', 'count_time'], 'integer'],
            [['online_day'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AdvCalendar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'online_day' => SORT_DESC,
                    'gravity' => SORT_ASC,
                    'adv_mesh_id' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'adv_mesh_id' => $this->adv_mesh_id,
            'adv_orders_id' => $this->adv_orders_id,
            'online_day' => $this->online_day,
            'gravity' => $this->gravity,
            'count_time' => $this->count_time,
        ]);

        return $dataProvider;
    }

}
