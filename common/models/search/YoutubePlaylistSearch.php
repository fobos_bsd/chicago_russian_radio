<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\YoutubePlaylists;

/**
 * YoutubePlaylistSearch represents the model behind the search form about `common\models\YoutubePlaylists`.
 */
class YoutubePlaylistSearch extends YoutubePlaylists {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'youtube_channels_id', 'publish_at', 'status'], 'integer'],
            [['etag', 'playlist_id', 'title', 'description', 'thumbnails_medium', 'thumbnails_maxres'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = YoutubePlaylists::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'priority' => SORT_ASC,
                    'publish_at' => SORT_DESC,
                    'status' => SORT_ASC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'youtube_channels_id' => $this->youtube_channels_id,
            'publish_at' => $this->publish_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'etag', $this->etag])
                ->andFilterWhere(['like', 'playlist_id', $this->playlist_id])
                ->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'thumbnails_medium', $this->thumbnails_medium])
                ->andFilterWhere(['like', 'thumbnails_maxres', $this->thumbnails_maxres]);

        return $dataProvider;
    }

}
