<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Songs;

/**
 * SongSearch represents the model behind the search form of `common\models\Songs`.
 */
class SongSearch extends Songs {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'rank', 'status'], 'integer'],
            [['title', 'url', 'dropbox_id', 'description', 'fileAudio'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Songs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rank' => $this->rank,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'url', $this->url])
                ->andFilterWhere(['like', 'dropbox_id', $this->dropbox_id])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'fileAudio', $this->fileAudio]);

        return $dataProvider;
    }

}
