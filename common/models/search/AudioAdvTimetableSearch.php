<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AudioAdvTimetable;

/**
 * AudioAdvTimetableSearch represents the model behind the search form of `common\models\AudioAdvTimetable`.
 */
class AudioAdvTimetableSearch extends AudioAdvTimetable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'audio_advertising_id', 'status'], 'integer'],
            [['day_of_week', 'start_time', 'from_date', 'to_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AudioAdvTimetable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'audio_advertising_id' => $this->audio_advertising_id,
            'start_time' => $this->start_time,
            'from_date' => $this->from_date,
            'to_date' => $this->to_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'day_of_week', $this->day_of_week]);

        return $dataProvider;
    }
}
