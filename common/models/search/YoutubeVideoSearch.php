<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\YoutubeVideo;

/**
 * YoutubeVideoSearch represents the model behind the search form about `common\models\YoutubeVideo`.
 */
class YoutubeVideoSearch extends YoutubeVideo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'youtube_playlists_id', 'publish_at', 'status'], 'integer'],
            [['video_id', 'title', 'description', 'thumbnails_medium', 'thumbnails_maxres'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = YoutubeVideo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'youtube_playlists_id' => $this->youtube_playlists_id,
            'publish_at' => $this->publish_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'video_id', $this->video_id])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'thumbnails_medium', $this->thumbnails_medium])
            ->andFilterWhere(['like', 'thumbnails_maxres', $this->thumbnails_maxres]);

        return $dataProvider;
    }
}
