<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Radioquiz;

/**
 * RadioquizSearch represents the model behind the search form of `common\models\Radioquiz`.
 */
class RadioquizSearch extends Radioquiz {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'status'], 'integer'],
            [['title', 'description', 'prize', 'rules', 'participant', 'prize_image', 'participant_photo', 'questions', 'answers', 'advertiser_name', 'advertiser_site'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Radioquiz::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'prize', $this->prize])
                ->andFilterWhere(['like', 'rules', $this->rules])
                ->andFilterWhere(['like', 'participant', $this->participant])
                ->andFilterWhere(['like', 'prize_image', $this->prize_image])
                ->andFilterWhere(['like', 'participant_photo', $this->participant_photo])
                ->andFilterWhere(['like', 'questions', $this->questions])
                ->andFilterWhere(['like', 'answers', $this->answers])
                ->andFilterWhere(['like', 'advertiser_site', $this->questions])
                ->andFilterWhere(['like', 'advertiser_name', $this->questions]);

        return $dataProvider;
    }

}
