<?php

namespace common\models\search;

use common\models\BannerItem;
use common\models\BannerStatistic;
use common\models\Questions;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MenuSearch represents the model behind the search form about `common\models\BannerItem`.
 */
class QuestionsSearch extends Questions {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id'], 'integer'],
            [['name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Questions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $this->site_id = Yii::$app->Siteselector->getSitesCookie();

        // grid filtering conditions
        $query->andFilterWhere([
            'site_id' => $this->site_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

}
