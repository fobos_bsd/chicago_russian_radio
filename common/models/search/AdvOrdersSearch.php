<?php

namespace common\models\search;

use Yii,
    yii\base\Model,
    yii\data\ActiveDataProvider;
use common\models\AdvOrders;

/**
 * AdvOrdersSearch represents the model behind the search form about `common\models\AdvOrders`.
 */
class AdvOrdersSearch extends AdvOrders {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'clients_id', 'count_days', 'count_onday', 'used_days', 'how_seconds', 'created_at', 'site_id'], 'integer'],
            [['content', 'start_at', 'finish_at', 'title'], 'safe'],
            ['status', 'in', 'range' => ['On Air', 'New']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AdvOrders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'start_at' => SORT_DESC,
                    'finish_at' => SORT_DESC,
                    'id' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!isset($this->site_id) && Yii::$app->Siteselector->getSitesCookie())
            $this->site_id = Yii::$app->Siteselector->getSitesCookie();

        // Upload to DB
        if (isset($this->start_at) && !empty($this->start_at)) {
            $sdate = date_create_from_format('Y/d/m', $this->start_at);
            $this->start_at = date_format($sdate, 'Y-m-d');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'clients_id' => $this->clients_id,
            'site_id' => $this->site_id,
            'count_days' => $this->count_days,
            'count_onday' => $this->count_onday,
            'used_days' => $this->used_days,
            'how_seconds' => $this->how_seconds,
            'start_at' => $this->start_at,
            'finish_at' => $this->finish_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content]);
        $query->andFilterWhere(['like', 'status', $this->status]);

        $query->andWhere(['>=', 'finish_at', date('Y-m-d')]);

        return $dataProvider;
    }

}
