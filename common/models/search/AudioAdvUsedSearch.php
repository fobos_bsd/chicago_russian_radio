<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AudioAdvUsed,
    common\models\AudioAdvertising;

/**
 * AudioAdvUsedSearch represents the model behind the search form of `common\models\AudioAdvUsed`.
 */
class AudioAdvUsedSearch extends AudioAdvUsed {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'audio_advertising_id'], 'integer'],
            [['timeused'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AudioAdvUsed::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'timeused' => SORT_DESC
                ]
            ],
        ]);

        if (isset($params['AudioAdvUsedSearch']['timeused']) && !empty($params['AudioAdvUsedSearch']['timeused'])) {
            $formatdate = str_replace('/', '-', $params['AudioAdvUsedSearch']['timeused']);
            $date = new \DateTime($formatdate, new \DateTimeZone('Iceland'));
            $time = $date->format('U');
            $from_time = $time - 1800;
            $to_time = $time + 1800;
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // select only active
        $query->leftJoin('audio_advertising', 'audio_adv_used.audio_advertising_id = audio_advertising.id');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'audio_advertising_id' => $this->audio_advertising_id,
            'audio_advertising.status' => 1
        ]);

        if (isset($from_time) && isset($to_time)) {
            $query->andFilterWhere(['>=', 'timeused', $from_time])
                    ->andFilterWhere(['<=', 'timeused', $to_time]);
        }

        return $dataProvider;
    }

}
