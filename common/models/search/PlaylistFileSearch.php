<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PlaylistFiles;

/**
 * PlaylistFileSearch represents the model behind the search form of `common\models\PlaylistFiles`.
 */
class PlaylistFileSearch extends PlaylistFiles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'youtube_playlists_id', 'long_time', 'status'], 'integer'],
            [['name', 'fileName', 'radio_brodcast_day', 'radio_brodcast_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlaylistFiles::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'youtube_playlists_id' => $this->youtube_playlists_id,
            'long_time' => $this->long_time,
            'radio_brodcast_day' => $this->radio_brodcast_day,
            'radio_brodcast_time' => $this->radio_brodcast_time,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'fileName', $this->fileName]);

        return $dataProvider;
    }
}
