<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Timetable;

/**
 * TimetableSearch represents the model behind the search form about `common\models\Timetable`.
 */
class TimetableSearch extends Timetable {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'site_id', 'lang_id', 'youtube_playlists_id', 'onair', 'timetable_tags_id'], 'integer'],
            [['day_of_week', 'name', 'description', 'fileImage', 'from_time', 'to_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Timetable::find();

        $this->site_id = Yii::$app->Siteselector->getSitesCookie();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'site_id' => $this->site_id,
            'lang_id' => $this->lang_id,
            'youtube_playlists_id' => $this->youtube_playlists_id,
            'timetable_tags_id' => $this->timetable_tags_id,
            'from_time' => $this->from_time,
            'to_time' => $this->to_time,
            'onair' => $this->onair
        ]);

        $query->andFilterWhere(['like', 'day_of_week', $this->day_of_week])
                ->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'fileImage', $this->fileImage]);

        return $dataProvider;
    }

}
