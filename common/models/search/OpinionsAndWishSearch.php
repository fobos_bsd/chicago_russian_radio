<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OpinionsAndWishs;

/**
 * OpinionsAndWishSearch represents the model behind the search form of `common\models\OpinionsAndWishs`.
 */
class OpinionsAndWishSearch extends OpinionsAndWishs {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'site_id', 'raiting', 'status'], 'integer'],
            [['user_name', 'email', 'opinion', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OpinionsAndWishs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->site_id = Yii::$app->Siteselector->getSitesCookie();

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'site_id' => $this->site_id,
            'created_at' => $this->created_at,
            'raiting' => $this->raiting,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'user_name', $this->user_name])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'opinion', $this->opinion]);

        return $dataProvider;
    }

}
