<?php

namespace common\models\search;

use common\models\BannerItem;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MenuSearch represents the model behind the search form about `common\models\BannerItem`.
 */
class BannerItemSearch extends BannerItem {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'site_id', 'user_id', 'banner_position_id', 'to_date', 'from_date'], 'integer'],
            [['name', 'active'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = BannerItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->site_id = Yii::$app->Siteselector->getSitesCookie();

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'site_id' => $this->site_id,
            'user_id' => $this->user_id,
            'banner_position_id' => $this->banner_position_id,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

}
