<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pages;

/**
 * PageSearch represents the model behind the search form about `common\models\Pages`.
 */
class PageSearch extends Pages {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'user_id', 'site_id', 'lang_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'slug', 'content', 'meta_title', 'meta_key', 'meta_description', 'params', 'thumb'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Pages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!isset($this->site_id) && Yii::$app->Siteselector->getSitesCookie())
            $this->site_id = Yii::$app->Siteselector->getSitesCookie();

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'site_id' => $this->site_id,
            'lang_id' => $this->lang_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'slug', $this->slug])
                ->andFilterWhere(['like', 'content', $this->content])
                ->andFilterWhere(['like', 'meta_title', $this->meta_title])
                ->andFilterWhere(['like', 'meta_key', $this->meta_key])
                ->andFilterWhere(['like', 'meta_description', $this->meta_description])
                ->andFilterWhere(['like', 'params', $this->params])
                ->andFilterWhere(['like', 'thumb', $this->thumb]);

        return $dataProvider;
    }

}
