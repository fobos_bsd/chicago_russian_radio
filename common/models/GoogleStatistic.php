<?php

namespace common\models;

use Yii;
use backend\modules\google\models\Analytics;

/**
 * This is the model class for table "google_statistics".
 *
 * @property int $id
 * @property int $realtime
 * @property int $perday
 * @property int $permonth
 * @property int $last_update
 */
class GoogleStatistic extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'google_statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['realtime', 'perday', 'permonth', 'last_update', 'site_id'], 'integer'],
            [['last_update', 'site_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'realtime' => Yii::t('common', 'Realtime'),
            'perday' => Yii::t('common', 'Perday'),
            'permonth' => Yii::t('common', 'Permonth'),
            'last_update' => Yii::t('common', 'Last Update'),
            'site_id' => Yii::t('common', 'Site'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getStatistic($siteID) {
        // Default analitic data
        $arrstat = [
            'total_sessions' => 0,
            'today_users' => 0,
            'active_users' => 0,
        ];

        // Google analytics total users
        $analytics = new Analytics();

        // Get analitic data from this database
        $statsdata = $this::find()->where(['site_id' => $siteID])->orderBy(['id' => SORT_DESC])->one();

        // Test is analitic is exist in db
        if (isset($statsdata->last_update)) {
            $time_period = time() - (int) $statsdata->last_update;

            // If time period to small
            if ($time_period <= 7200) {
                $arrstat = [
                    'total_sessions' => (isset($statsdata->permonth)) ? (int) $statsdata->permonth : 0,
                    'perday' => (isset($statsdata->perday)) ? (int) $statsdata->perday : 0,
                    'realtime' => (isset($statsdata->realtime)) ? (int) $statsdata->realtime : 0,
                ];
            } else {
                $onmonth = $this->getCountSessionMonth($analytics->getReportMonthSessions());
                $onday = $this->getCountPerDay($analytics->getReportOneDay());
                $realtime = $this->getCountRealtime($analytics->realTimeReporting());

                $statsdata->realtime = (isset($realtime)) ? $realtime : 0;
                $statsdata->perday = (isset($onday)) ? $onday : 0;
                $statsdata->permonth = (isset($onmonth)) ? $onmonth : 0;
                $statsdata->last_update = time();
                $statsdata->update();

                $arrstat = [
                    'total_sessions' => $onmonth,
                    'perday' => $onday,
                    'realtime' => $realtime,
                ];
            }
        } else {
            $onmonth = $this->getCountSessionMonth($analytics->getReportMonthSessions());
            $onday = $this->getCountPerDay($analytics->getReportOneDay());
            //$realtime = $this->getCountRealtime($analytics->realTimeReporting());

            $this->realtime = (isset($realtime)) ? $realtime : 0;
            $this->perday = (isset($onday)) ? $onday : 0;
            $this->permonth = (isset($onmonth)) ? $onmonth : 0;
            $this->last_update = time();
            $this->insert();

            $arrstat = [
                'total_sessions' => $onmonth,
                'perday' => $onday,
                'realtime' => 7,
            ];
        }

        return $arrstat;
    }

    // Get realtime analitic
    protected function getCountRealtime($active_users) {
        $totals = $active_users->getTotalsForAllResults();
        foreach ($totals as $metricName => $metricTotal) {
            $result = (int) $metricTotal;
        }

        return $result;
    }

    // Get count user one day
    protected function getCountPerDay($today_users) {
        $result = 0;
        for ($reportIndex = 0; $reportIndex < count($today_users); $reportIndex++) {
            $report = $today_users[$reportIndex];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();

            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[$rowIndex];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();

                for ($j = 0; $j < count($metricHeaders) && $j < count($metrics); $j++) {
                    $entry = $metricHeaders[$j];
                    $values = $metrics[$j];
                    for ($valueIndex = 0; $valueIndex < count($values->getValues()); $valueIndex++) {
                        $value = $values->getValues()[$valueIndex];
                        $result = (int) $value;
                    }
                }
            }
        }

        return $result;
    }

    // Get count sessions on month
    protected function getCountSessionMonth($total_sessions) {
        for ($reportIndex = 0; $reportIndex < count($total_sessions); $reportIndex++) {
            $report = $total_sessions[$reportIndex];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();

            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[$rowIndex];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();

                for ($j = 0; $j < count($metricHeaders) && $j < count($metrics); $j++) {
                    $entry = $metricHeaders[$j];
                    $values = $metrics[$j];
                    for ($valueIndex = 0; $valueIndex < count($values->getValues()); $valueIndex++) {
                        $value = $values->getValues()[$valueIndex];
                        $result = (int) $value;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

}
