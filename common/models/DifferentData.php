<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "different_data".
 *
 * @property int $id
 * @property int $site_id
 * @property string $name
 * @property string $action_date
 * @property string $description
 * @property string $note
 *
 * @property Site $site
 */
class DifferentData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'different_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'name', 'action_date'], 'required'],
            [['site_id'], 'integer'],
            [['action_date'], 'safe'],
            [['description', 'note'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'site_id' => Yii::t('common', 'Site ID'),
            'name' => Yii::t('common', 'Name'),
            'action_date' => Yii::t('common', 'Action Date'),
            'description' => Yii::t('common', 'Description'),
            'note' => Yii::t('common', 'Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
