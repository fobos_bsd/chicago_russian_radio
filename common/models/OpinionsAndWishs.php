<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "opinions_and_wishs".
 *
 * @property int $id
 * @property int $site_id
 * @property string $user_name
 * @property string $email
 * @property string $opinion
 * @property string $created_at
 * @property int $raiting
 * @property int $status
 *
 * @property Site $site
 */
class OpinionsAndWishs extends \yii\db\ActiveRecord {

    public $reCaptchaOpinion;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'opinions_and_wishs';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id', 'user_name', 'opinion', 'created_at'], 'required'],
            ['site_id', 'integer'],
            ['status', 'in', 'range' => [0, 1]],
            ['raiting', 'in', 'range' => [0, 1, 2, 3, 4, 5]],
            ['user_name', 'string', 'max' => 500],
            ['opinion', 'string', 'max' => 5000],
            [['created_at'], 'date', 'format' => 'yyyy-mm-dd'],
            [['email'], 'email'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['reCaptchaOpinion'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => Yii::$app->params['secretcaptcha'], 'on' => 'site'],
            [['user_name', 'opinion'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }],
            ['created_at', 'default', 'value' => date('Y-m-d')],
            ['status', 'default', 'value' => 1],
            ['raiting', 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'site_id' => Yii::t('common', 'Site'),
            'user_name' => Yii::t('common', 'User Name'),
            'email' => Yii::t('common', 'Email'),
            'opinion' => Yii::t('common', 'Opinion'),
            'created_at' => Yii::t('common', 'Created At'),
            'raiting' => Yii::t('common', 'Raiting'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    public function sendEmail() {
        $result = Yii::$app->mailer->compose('opinions-html', ['model' => $this])
                ->setTo([Yii::$app->params['adminEmail'], Yii::$app->params['gmailEmail']])
                ->setFrom('online_question@ethnicmedia.us')
                ->setSubject('Resonanse Radio: ' . $this->user_name . ' opinions and wishs')
                ->send();

        return $result;
    }

}
