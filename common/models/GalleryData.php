<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gallery_data".
 *
 * @property int $id
 * @property int $gallery_id
 * @property string $name
 * @property string $fileName
 * @property int $status
 * @property int $gravity
 * @property string $title
 * @property string $link
 *
 * @property Gallery $gallery
 */
class GalleryData extends \yii\db\ActiveRecord {

    public $fileImage;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'gallery_data';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['gallery_id', 'name'], 'required'],
            [['gallery_id', 'status', 'gravity'], 'integer'],
            [['link'], 'string'],
            [['name', 'fileName', 'title'], 'string', 'max' => 255],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
            [['fileImage'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'gallery_id' => Yii::t('common', 'Gallery ID'),
            'name' => Yii::t('common', 'Name'),
            'fileName' => Yii::t('common', 'File Name'),
            'status' => Yii::t('common', 'Status'),
            'gravity' => Yii::t('common', 'Gravity'),
            'title' => Yii::t('common', 'Title'),
            'link' => Yii::t('common', 'Link'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery() {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {

            if (isset($this->fileImage) && is_object($this->fileImage)) {
                $resupload = $this->upload($this->fileImage);
                if ($resupload !== false) {
                    $this->fileName = $resupload;
                }
            }
            return true;
        }
        return false;
    }

    public function upload($image) {
        if ($this->validate()) {
            $filename = $image->baseName . '.' . $image->extension;
            $folder = Yii::getAlias('@backend') . '/web/uploads/gallery';

            // Is folder not exist, than create folder
            if (!file_exists($folder))
                mkdir($folder, 0777, true);
            $path = $folder . '/' . $filename;
            $image->saveAs($path);
            return $filename;
        } else {
            return false;
        }
    }

}
