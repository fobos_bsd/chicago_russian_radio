<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "seo_content".
 *
 * @property int $id
 * @property string $page_name
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 */
class SeoContent extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'seo_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['page_name'], 'required'],
            [['page_name'], 'string', 'max' => 255],
            [['seo_title'], 'string', 'max' => 1500],
            [['seo_keywords'], 'string', 'max' => 2500],
            [['seo_description'], 'string', 'max' => 5000],
            [['page_name'], 'unique'],
            [['page_name', 'seo_title', 'seo_keywords', 'seo_description'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }],
            [['seo_title', 'seo_keywords', 'seo_description'], 'default', 'value' => null]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'page_name' => Yii::t('common', 'Page Name'),
            'seo_title' => Yii::t('common', 'Seo Title'),
            'seo_keywords' => Yii::t('common', 'Seo Keywords'),
            'seo_description' => Yii::t('common', 'Seo Description'),
        ];
    }

}
