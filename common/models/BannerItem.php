<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner_item".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $site_id
 * @property integer $lang_id
 * @property integer $format_id
 * @property string $name
 * @property string $url
 * @property string $url_title
 * @property string $image
 * @property integer $from_date
 * @property integer $to_date
 * @property integer $priority
 * @property integer $position
 * @property integer $showon
 * @property boolean $active
 *
 */
class BannerItem extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'banner_item';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id', 'site_id', 'name', 'url', 'from_date', 'to_date', 'priority', 'banner_position_id'], 'required'],
            [['user_id', 'site_id', 'lang_id', 'banner_position_id'], 'integer'],
            ['priority', 'integer', 'min' => 0, 'max' => 1000],
            [['active'], 'in', 'range' => [0, 1]],
            [['from_date', 'to_date'], 'date', 'format' => 'Y-m-d'],
            [['url'], 'string'],
            [['name', 'url_title'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['banner_position_id'], 'exist', 'skipOnError' => true, 'targetClass' => BannerPosition::className(), 'targetAttribute' => ['banner_position_id' => 'id']],
            ['from_date', 'default', 'value' => date('Y-m-d')],
            ['to_date', 'default', 'value' => date('Y-m-d', strtotime("+30 days"))],
            ['active', 'default', 'value' => 1],
            ['priority', 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (isset($this->image) && is_object($this->image)) {
                $resUpload = $this->upload($this->image);

                if ($resUpload !== false) {
                    $this->image = $resUpload;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function upload($image) {
        if ($this->validate()) {
            $filename = $image->baseName . '.' . $image->extension;
            $path = Yii::getAlias('@frontend') . '/web/uploads/banners/' . $filename;
            $image->saveAs($path);
            return $filename;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'user_id' => Yii::t('common', 'User name'),
            'site_id' => Yii::t('common', 'Site ID'),
            'lang_id' => Yii::t('common', 'Lang'),
            'banner_position_id' => Yii::t('common', 'Position'),
            'name' => Yii::t('common', 'Name'),
            'url' => Yii::t('common', 'Url'),
            'url_title' => Yii::t('common', 'Url title'),
            'image' => Yii::t('common', 'Image'),
            'format_id' => Yii::t('common', 'Format'),
            'from_date' => Yii::t('common', 'Start date'),
            'to_date' => Yii::t('common', 'End date'),
            'priority' => Yii::t('common', 'Priority'),
            'position' => Yii::t('common', 'Position'),
            'showon' => Yii::t('common', 'Show on'),
            'active' => Yii::t('common', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang() {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerPosition() {
        return $this->hasOne(BannerPosition::className(), ['id' => 'banner_position_id']);
    }

}
