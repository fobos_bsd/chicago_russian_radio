<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "votes".
 *
 * @property integer $id
 * @property integer $site_id
 * @property integer $questions_id
 * @property integer $answers_id
 * @property string $ip
 * @property Questions $question
 * @property Answers $answer
 *
 */
class Votes extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'votes';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id','questions_id','answers_id','ip'], 'required'],
            [['site_id','questions_id','answers_id'], 'integer'],
            [['questions_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['questions_id' => 'id']],
            [['answers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answers::className(), 'targetAttribute' => ['answers_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'questions_id' => Yii::t('common', 'Question'),
            'answers_id' => Yii::t('common', 'Answer'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(Answers::className(), ['id' => 'answers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['id' => 'questions_id']);
    }
}
