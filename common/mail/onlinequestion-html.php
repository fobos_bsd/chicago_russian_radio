<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserQuestions */

?>

<table  style='width: 100%;'>
    <tr style="background-color: #f8f8f8;">
        <td style='padding: 10px; border: #e9e9e9 1px solid;'><b>Name:</b></td>
        <td style='padding: 10px; border: #e9e9e9 1px solid;'><?= $model->username ?></td>
    </tr>
    <tr>
        <td style='padding: 10px; border: #e9e9e9 1px solid;'><b>Phone:</b></td>
        <td style='padding: 10px; border: #e9e9e9 1px solid;'>
            <?php if($model->phone) : ?>
                <?= $model->phone ?>
            <?php else: ?>
                none
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td style='padding: 10px; border: #e9e9e9 1px solid;'><b>Question:</b></td>
        <td style='padding: 10px; border: #e9e9e9 1px solid;'><?= $model->question ?></td>
    </tr>
</table>
