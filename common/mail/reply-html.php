<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data backend\modules\contacts\models\ReplyForm */

?>

<div class="question" style="background-color: lightgray;padding: 20px;margin-bottom: 25px;">
    <?= $data->question ?>
</div>

<h3>Reply: </h3>

<div class="reply">
    <?= $data->reply ?>
</div>
