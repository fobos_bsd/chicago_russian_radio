<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@backend/modules/auth/views',
                    '@dektrium/rbac/views' => '@backend/views/rbac'
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'common*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'common' => 'content.php',
                        'common/error' => 'error.php',
                    ],
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'backend' => 'content.php',
                        'backend/error' => 'error.php',
                    ],
                ],
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'frontend' => 'content.php',
                        'frontend/error' => 'error.php',
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'controllerMap' => [
                'security' => 'frontend\modules\auth\controllers\SecurityController',
                'registration' => 'frontend\modules\auth\controllers\RegistrationController',
                'recovery' => 'frontend\modules\auth\controllers\RecoveryController',
                'profile' => 'frontend\modules\auth\controllers\ProfileController'
            ],
            'enableUnconfirmedLogin' => false,
            'cost' => 12,
            'admins' => ['adminfm', 'fobos']
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
        'filemanager' => [
            'class' => 'pendalf89\filemanager\Module',
            'routes' => [
                'baseUrl' => '',
                'basePath' => '@frontend/web',
                'uploadPath' => 'uploads/mediagallery',
            ],
            'thumbs' => [
                'small' => [
                    'name' => 'Small',
                    'size' => [100, 100],
                ],
                'medium' => [
                    'name' => 'Middle',
                    'size' => [300, 200],
                ],
                'large' => [
                    'name' => 'Big',
                    'size' => [500, 400],
                ],
            ],
        ],
        'gallery' => [
            'class' => 'onmotion\gallery\Module',
        ],
    ],
];
