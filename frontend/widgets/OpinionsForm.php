<?php

namespace frontend\widgets;

use Yii,
    yii\base\Widget;
use common\models\OpinionsAndWishs;

/**
 * Class OnlineForm
 * @package frontend\widgets
 */
class OpinionsForm extends Widget {

    public function run() {
        $model = new OpinionsAndWishs();
        $model->scenario = 'site';

        $model->status = 1;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->created_at = date('Y-m-d');
            $model->site_id = Yii::$app->Siteinfo->getSite()['id'];

            if ($model->save()) {
                $model->sendEmail();
                return $this->render('opinion-form.twig', ['model' => $model, 'success' => true]);
            } else {
                return $this->render('opinion-form.twig', ['model' => $model, 'success' => false]);
            }
        }

        return $this->render('opinion-form.twig', [
                    'model' => $model
        ]);
    }

}
