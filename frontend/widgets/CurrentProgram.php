<?php

namespace frontend\widgets;

date_default_timezone_set('America/Chicago');

use Yii,
    yii\base\Widget,
    yii\helpers\ArrayHelper;
use common\models\YoutubePlaylists,
    common\models\Settings;

class CurrentProgram extends Widget {

    public $siteID, $siteLang;
    private $settings;

    /**
     * @return string
     */
    public function init() {
        parent::init();

        // Set global site params
        $siteArr = Yii::$app->Siteinfo->getSite();
        $this->siteID = $siteArr['id'];
        $this->siteLang = $siteArr['lang'];

        // Get settings
        $this->settings = ArrayHelper::map(Settings::find()->where(['site_id' => $this->siteID])->asArray()->all(), 'name', 'value');
    }

    /**
     * @return string
     */
    public function run() {

        $curentWeek = date('w');
        $curentTime = date('H:i:s', time());

        $current_program = $this->getPlaylist((int) $curentWeek, $curentTime);

        return $this->render('current-program.twig', [
                    'current_program' => $current_program,
                    'youtube_code' => (isset($this->settings['youtobe_home_channel'])) ? $this->settings['youtobe_home_channel'] : null
        ]);
    }

    private function getPlaylist($week, $time) {
        
        switch ($week) {
            case 0: $day = 'Su';
                break;
            case 1: $day = 'Mo';
                break;
            case 2: $day = 'Tu';
                break;
            case 3: $day = 'We';
                break;
            case 4: $day = 'Th';
                break;
            case 5: $day = 'Fr';
                break;
            case 6: $day = 'Sa';
                break;
        }

        return YoutubePlaylists::find()->joinWith('timetable')->where(['timetable.day_of_week' => $day, 'timetable.site_id' => $this->siteID, 'timetable.lang_id' => $this->siteLang, 'status' => 1])->andWhere(['<=', 'timetable.from_time', $time])->andWhere(['>=', 'timetable.to_time', $time])->one();
    }

}
