<?php

/**
 * Description of ChanelYoutube
 *
 * @author fobos
 */

namespace frontend\widgets;

use yii\base\Widget;
use common\models\GoogleYoutube;

class LastvideoYoutube extends Widget {
    /*
     * @var chanel ID меню
     */

    public $id_chanel = 'UCEOPwtJG3ZfFHVqmIUA3FJw';

    public function init() {
        parent::init();
    }

    /**
     * @return string
     */
    public function run() {
        $youtube = new GoogleYoutube();
        $chanels_data = $youtube->getChanelVideos($this->id_chanel, 1);

        return $this->render('lastvideo-youtube.twig', [
                    'chanel_data' => (isset($chanels_data) && $chanels_data !== false) ? $chanels_data : false,
        ]);
    }

}
