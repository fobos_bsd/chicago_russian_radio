<?php

namespace frontend\widgets;

use Yii,
    yii\base\Widget;
use common\models\Timetable;

/**
 * Class FooterMenu
 * @package frontend\widgets
 */
class TimetableSlider extends Widget {

    /**
     * @return string
     */
    public function run() {
        // Set global site params
        $siteArr = Yii::$app->Siteinfo->getSite();
        $siteID = $siteArr['id'];
        $siteLang = $siteArr['lang'];

        // Set current week day
        $daynumber = date('w');
        switch ($daynumber) {
            case 0: $day = 'Su';
                break;
            case 1: $day = 'Mo';
                break;
            case 2: $day = 'Tu';
                break;
            case 3: $day = 'We';
                break;
            case 4: $day = 'Th';
                break;
            case 5: $day = 'Fr';
                break;
            case 6: $day = 'Sa';
                break;
        }

        $timeafter = Timetable::find()->select(['timetable.*', 'youtube_playlists.thumbnails_medium'])
                        ->leftJoin('youtube_playlists', 'timetable.youtube_playlists_id = youtube_playlists.id')
                        ->where(['site_id' => $siteID, 'lang_id' => $siteLang, 'day_of_week' => $day])
                        ->andWhere(['<=', 'timetable.from_time', date('H:i:s', time())])
                        ->andWhere(['>=', 'timetable.to_time', date('H:i:s', time())])
                        ->orderBy(['timetable.to_time' => SORT_ASC])
                        ->asArray()->all();
        $timedeafore = Timetable::find()->select(['timetable.*', 'youtube_playlists.thumbnails_medium'])
                        ->leftJoin('youtube_playlists', 'timetable.youtube_playlists_id = youtube_playlists.id')
                        ->where(['timetable.site_id' => $siteID, 'timetable.lang_id' => $siteLang, 'timetable.day_of_week' => $day])
                        ->andWhere(['>', 'timetable.from_time', date('H:i:s', time())])->orderBy(['timetable.from_time' => SORT_ASC])
                        ->asArray()->all();
        $timefinish = Timetable::find()->select(['timetable.*', 'youtube_playlists.thumbnails_medium'])
                        ->leftJoin('youtube_playlists', 'timetable.youtube_playlists_id = youtube_playlists.id')
                        ->where(['site_id' => $siteID, 'lang_id' => $siteLang, 'day_of_week' => $day])
                        ->andWhere(['<', 'timetable.to_time', date('H:i:s', time())])->orderBy(['timetable.from_time' => SORT_ASC])
                        ->asArray()->all();
        $result = array_merge($timeafter, $timedeafore, $timefinish);

        return $this->render('timetable-slider.twig', [
                    'models' => $result,
        ]);
    }

}
