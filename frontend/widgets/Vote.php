<?php

namespace frontend\widgets;

use common\models\Questions;
use common\models\Votes;
use frontend\models\VoteForm;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;

/**
 * Class Vote
 * @package frontend\widgets
 */
class Vote extends Widget {

    /**
     * @return string
     */
    public function run() {
        $cookies = Yii::$app->request->cookies;

        $model = new VoteForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->vote()) {
                Yii::$app->response->cookies->add(new Cookie(['name' => 'question', 'value' => $model->questionId, 'path' => '/',
                    'expire' => time() + 60 * 60 * 24 * 365 * 10]));
                Yii::$app->getResponse()->refresh();
            }
        }

        $question = Questions::find()->with(['answers'])->where(['active' => 1])->groupBy(['id'])->one();



        //Remove old cookie if question change
        if ($cookies->has('question') && isset($question->id)) {
            if ($cookies->getValue('question') != $question->id) {
                Yii::$app->response->cookies->remove(new Cookie(['name' => 'question', 'value' => $question->id, 'path' => '/']));
            }
        }

        $result = false;
        if ($question) {
            //Check vote in cookie
            if ($cookies->getValue('question') == $question->id) {
                $result = true;
            }

            //If cookie empty check ip in database
            if (!$result) {
                $result = Votes::find()->where(['questions_id' => $question->id, 'ip' => Yii::$app->serverHelper->getRemoteIPAddress()])->one();
            }
        }
        
        // Get tottal answers
        $totlal_result = Votes::find()->where(['questions_id' => $question->id])->count();

        $answers = (isset($question->answers)) ? ArrayHelper::map($question->answers, 'id', 'name') : array();

        return $this->render('vote.twig', [
                    'model' => $model,
                    'question' => $question,
                    'answers' => $answers,
                    'result' => $result,
                    'totlal_result' => $totlal_result
        ]);
    }

}
