<?php

namespace frontend\widgets;

use Yii,
    yii\base\Widget,
    yii\helpers\ArrayHelper,
    yii\db\Expression;
use common\models\YoutubePlaylists,
    common\models\Settings,
    common\models\Gallery,
    common\models\GalleryData;

class ProgramsGallery extends Widget {

    public $siteID, $siteLang;
    private $settings;

    /**
     * @return string
     */
    public function init() {
        parent::init();

        // Set global site params
        $siteArr = Yii::$app->Siteinfo->getSite();
        $this->siteID = $siteArr['id'];
        $this->siteLang = $siteArr['lang'];

        // Get settings
        $this->settings = ArrayHelper::map(Settings::find()->where(['site_id' => $this->siteID])->asArray()->all(), 'name', 'value');
    }

    /**
     * @return string
     */
    public function run() {

        $arrimages = $this->getGallery();

        return $this->render('programs-gallery.twig', [
                    'arrimages' => $arrimages,
                    'url' => Yii::$app->getRequest()->url
        ]);
    }

    private function getGallery() {
        $url = Yii::$app->getRequest()->url;

        if ($url === '/') {
            return $this->getAllSlideImages();
        } elseif (strpos($url, '/programmy') !== false) {
            $exp = explode('/', $url);
            $galleries = ArrayHelper::getColumn(Gallery::find()->select(['id'])->where(['youtube_playlists_id' => $exp[2]])->asArray()->all(), 'id');
            $galleryID = (isset($galleries) && count($galleries) > 0) ? implode(',', $galleries) : null;

            return $this->getSlideImages($galleryID);
        } else {
            return null;
        }
    }

    private function getSlideImages($listID = null) {
        if (isset($listID)) {
            $arrimage_buisy = GalleryData::find()->where(['in', 'gallery_id', $listID])->andWhere(['status' => 1])->orderBy(new Expression('rand()'))->limit(15)->asArray()->all();
            $arrimage_free = GalleryData::find()->where(['in', 'gallery_id', $listID])->andWhere(['status' => 0])->orderBy(new Expression('rand()'))->limit(15)->asArray()->all();
            $arrimages = array_merge($arrimage_buisy, $arrimage_free);
        } else {
            $arrimages = null;
        }

        return $arrimages;
    }

    private function getAllSlideImages() {
        $arrimage_buisy = GalleryData::find()->where(['status' => 1])->orderBy(new Expression('rand()'))->limit(15)->asArray()->all();
        $arrimage_free = GalleryData::find()->where(['status' => 0])->orderBy(new Expression('rand()'))->limit(15)->asArray()->all();
        $arrimages = array_merge($arrimage_buisy, $arrimage_free);

        return $arrimages;
    }

}
