<?php

namespace frontend\widgets;

use Yii,
    yii\base\Widget,
    yii\helpers\ArrayHelper,
    yii\db\Expression;
use common\models\YoutubePlaylists,
    common\models\YoutubeChannels,
    common\models\Settings;

class ProgramSlider extends Widget {

    public $siteID, $siteLang;
    private $settings;

    /**
     * @return string
     */
    public function init() {
        parent::init();

        // Set global site params
        $siteArr = Yii::$app->Siteinfo->getSite();
        $this->siteID = $siteArr['id'];
        $this->siteLang = $siteArr['lang'];

        // Get settings
        $this->settings = ArrayHelper::map(Settings::find()->where(['site_id' => $this->siteID])->asArray()->all(), 'name', 'value');
    }

    /**
     * @return string
     */
    public function run() {

        $arrimages = $this->getAllSlideImages();

        return $this->render('program-slider.twig', [
                    'arrimages' => $arrimages,
        ]);
    }

    private function getAllSlideImages() {
        if (($arrimages = YoutubePlaylists::find()->where(['status' => 1, 'youtube_channels_id' => $this->getChannel()])->asArray()->all()) !== null) {
            return $arrimages;
        } else {
            return [];
        }
    }

    private function getChannel() {
        if (($model = YoutubeChannels::find()->where(['site_id' => $this->siteID, 'status' => 1])->one()) !== null) {
            return $model->id;
        } else {
            return 0;
        }
    }

}
