<?php
namespace frontend\widgets;

use common\models\UserQuestions;
use Yii;
use yii\base\Widget;

/**
 * Class ContactForm
 * @package frontend\widgets
 */
class ContactForm extends Widget
{
    public function run()
    {
        $model = new UserQuestions();
        $model->scenario = 'site';

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
        {
            $model->created_at = time();
            $model->site_id = Yii::$app->Siteinfo->getSite()['id'];

            if($model->save()){
                $model->sendEmail();
                $model = new UserQuestions();
                return $this->render('contact-form.twig',['model' => $model,'success' => true]);
            }
            else
            {
                return $this->render('contact-form.twig',['model' => $model,'success' => false]);
            }
        }

        return $this->render('contact-form.twig', [
            'model' => $model
        ]);
    }
}
