<?php

namespace frontend\widgets;

use Yii,
    yii\base\Widget;
use common\models\Settings;

/**
 * Class FooterMenu
 * @package frontend\widgets
 */
class RealtimeRadio extends Widget {

    /**
     * @return string
     */
    public function run() {
        $sitedata = Yii::$app->Siteinfo->getSite();
        $siteID = $sitedata['id'];

        if (($settings = Settings::find()->select(['value'])->where(['site_id' => $siteID, 'name' => 'audio_stream'])->one()) !== null) {
            $result = $settings['value'];
        } else {
            $result = null;
        }

        return $this->render('realtime-radio.twig', [
                    'link' => $result,
        ]);
    }

}
