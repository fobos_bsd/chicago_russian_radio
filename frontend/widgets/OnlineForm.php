<?php

namespace frontend\widgets;

use common\models\OnlineQuestions;
use common\models\Timetable;
use Yii;
use yii\base\Widget;

/**
 * Class OnlineForm
 * @package frontend\widgets
 */
class OnlineForm extends Widget {

    public function run() {
        $model = new OnlineQuestions();
        $model->scenario = 'site';

        $day = substr(date('D', time()), 0, 2);
        $time = date('H:i:s', time());

        // Curent site ID
        $siteArr = Yii::$app->Siteinfo->getSite();

        $name = Timetable::find()->select('name')->where(['day_of_week' => $day, 'site_id' => $siteArr['id'], 'lang_id' => $siteArr['lang']])->andWhere(['<=', 'from_time', $time])->andWhere(['>=', 'to_time', $time])->one();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {

            $model->created_at = time();
            $model->site_id = Yii::$app->Siteinfo->getSite()['id'];

            if ($model->save()) {
                $model->sendEmail();
                $model = new OnlineQuestions();
                return $this->render('online-form.twig', ['model' => $model, 'success' => true]);
            } else {
                return $this->render('online-form.twig', ['model' => $model, 'success' => false]);
            }
        }

        return $this->render('online-form.twig', [
                    'model' => $model,
                    'name' => $name
        ]);
    }

}
