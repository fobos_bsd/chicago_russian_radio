<?php

namespace frontend\widgets;

use common\models\AdvCompany;
use common\models\BannerItem;
use common\models\BannerStatistic;
use Yii;
use yii\base\Widget;

/**
 * Class AdvertisingCampaign
 * @package frontend\widgets
 */
class AdvertisingCampaign extends Widget {
    /*
     * @var position позиция на сайте
     */

    /*
     * @var id рекламной компании
     */
    public $id_company = 0;


    public function init() {
        parent::init();
    }

    /**
     * @return string
     */
    public function run() {
        $advs = AdvCompany::find()->where(['id' => $this->id_company])->with('banners')->one();

        if (isset($advs->banners)) {
            foreach ($advs->banners as $adv) {
                $this->setStatisticShow($adv->id);
            }
        }

        return $this->render('advertising-campaign.twig', [
                    'advs' => $advs,
        ]);
    }

    protected function setStatisticShow($bannerID) {
        // Chack is session open
        $session = Yii::$app->session;
        if (!$session->isActive)
            $session->open();

        if (!$session->has('banner_' . $bannerID)) {
            $session->set('banner_' . $bannerID, 'yes');

            $model = $this->findStatistic($bannerID);
            if ($model !== false) {
                $model->show_count += 1;
                $model->update();
            }
        }
    }

    protected function findStatistic($id) {
        if (($model = BannerStatistic::find()->where(['banner_id' => $id])->one()) !== null) {
            return $model;
        } else {
            return false;
        }
    }
}
