<?php

namespace frontend\widgets;

use yii,
    yii\base\Widget;
use common\models\BannerItem,
    common\models\BannerStatistic;

/**
 * Class Banner
 * @package frontend\widgets
 */
class BannerCarousel extends Widget {

    /**
     * @var int позиции баннера
     */
    public $position;

    /**
     * @return string
     */
    public function run() {
        $position = (isset($this->position)) ? $this->position : '1';
        $bannersID = $this->getBanersID($position);

        // Set banner statistic
        if (isset($bannersID)) {
            foreach ($bannersID as $bannerID) {
                $this->setStatisticShow((int) $bannerID->id);
            }
        }

        return $this->render('banner-carousel.twig', [
                    'banners' => $bannersID,
        ]);
    }

    protected function getBanersID($position = null) {
        // Get all id of horisontal banners adn gat random id
        if ($position === null) {
            $allbanners = BannerItem::find()->where(['active' => 1])->andWhere(['<=', 'from_date', date('Y-m-d')])->andWhere(['>=', 'to_date', date('Y-m-d')])->all();
        } else {
            $allbanners = BannerItem::find()->where(['active' => 1, 'banner_position_id' => $position])->andWhere(['<=', 'from_date', date('Y-m-d')])->andWhere(['>=', 'to_date', date('Y-m-d')])->all();
        }

        if (isset($allbanners) && count($allbanners) > 0) {
            $arrid = $allbanners;
        } else {
            $arrid = null;
        }

        return $arrid;
    }

    protected function setStatisticShow($bannerID) {
        $model = $this->findStatistic($bannerID);
        if ($model !== false) {
            $model->show_count += 1;
            //var_dump($model->show_count);
            $model->update();
        } else {
            $new_model = new BannerStatistic();
            $new_model->banner_id = $bannerID;
            $new_model->click_count = 0;
            $new_model->show_count = 1;

            $new_model->insert();
        }
    }

    protected function findStatistic($bannerID) {
        if (($model = BannerStatistic::find()->where(['banner_id' => $bannerID])->one()) !== null) {
            return $model;
        } else {
            return false;
        }
    }

}
