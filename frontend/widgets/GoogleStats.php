<?php

namespace frontend\widgets;

use Yii,
    yii\base\Widget;
use common\models\GoogleStatistic;

/**
 * Class Stats
 * @package frontend\widgets
 */
class GoogleStats extends Widget {

    public $session_user;

    /**
     * @return string
     */
    public function run() {
        // Set global site params
        $siteArr = Yii::$app->Siteinfo->getSite();

        // Google analytics total users
        $analytics = new GoogleStatistic();
        $statistic = $analytics->getStatistic($siteArr['id']);

        // Google analytics month users
        $total_sessions = $statistic['total_sessions'];

        // Google analytics today users
        $today_user = $statistic['perday'];

        // Google realtime analitic
        $active_users = $statistic['realtime'];

        return $this->render('google-stats.twig', [
                    'total_sessions' => $total_sessions,
                    'today_users' => $today_user,
                    'active_users' => $active_users
        ]);
    }

}
