<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\widgets;

use Yii,
    yii\base\Widget,
    yii\helpers\ArrayHelper;
use common\models\BannerPosition,
    common\models\BannerItem;

class Banners extends Widget {

    public $position;
    private $siteID, $siteLang;

    /**
     * @return string
     */
    public function init() {
        parent::init();

        // Set global site params
        $siteArr = Yii::$app->Siteinfo->getSite();
        $this->siteID = $siteArr['id'];
        $this->siteLang = $siteArr['lang'];
    }

    /**
     * @return string
     */
    public function run() {
        $model = BannerItem::find()
                ->select(['banner_item.*', 'banner_position.width', 'banner_position.height'])
                ->rightJoin('banner_position', 'banner_position.id = banner_item.banner_position_id')
                ->where(['banner_item.active' => 1, 'banner_item.site_id' => $this->siteID, 'banner_item.lang_id' => $this->siteLang])
                ->andWhere(['<=', 'banner_item.from_date', date('Y-m-d')])
                ->andWhere(['>=', 'banner_item.to_date', date('Y-m-d')])
                ->andWhere(['banner_position.name' => $this->position])
                ->orderBy(['banner_item.priority' => SORT_ASC])
                ->asArray()
                ->all();

        return $this->render('banners.twig', ['model' => $model]);
    }

}
