<?php

namespace frontend\widgets;

use Yii,
    yii\base\Widget;
use common\models\CustomBlocks;

/**
 * Class FooterMenu
 * @package frontend\widgets
 */
class CustomBlock extends Widget {

    public $block_name, $block_id;

    /**
     * @return init
     */
    public function init() {
        parent::init();
    }

    /**
     * @return string
     */
    public function run() {
        // Set global site params
        $siteArr = Yii::$app->Siteinfo->getSite();
        $siteID = $siteArr['id'];
        $siteLang = $siteArr['lang'];

        // Get block data
        if (isset($this->block_id)) {
            $objdata = CustomBlocks::findOne($this->block_id);
            $result = (isset($objdata->content)) ? $objdata->content : null;
        } elseif (isset($this->block_name)) {
            $objdata = CustomBlocks::findOne(['name' => $this->block_name]);
            $result = (isset($objdata->content)) ? $objdata->content : null;
        } else {
            $result = null;
        }

        return $this->render('custom-block.twig', [
                    'data' => $result,
        ]);
    }

}
