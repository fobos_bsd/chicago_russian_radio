<?php

namespace frontend\widgets;

use Yii,
    yii\base\Widget,
    yii\helpers\Json;
use linslin\yii2\curl;
use common\models\Settings;

/**
 * Class News
 * @package frontend\widgets
 */
class News extends Widget {

    public $curl;

    public function run() {

        if (!$curl = new curl\Curl()) {
            $result = array();
        } else {

            $params = [
                'pageSize' => 4,
            ];

            $token = Settings::find()->select('value')->where([
                        'site_id' => Yii::$app->Siteinfo->getSite()['id'],
                        'name' => '7days_access_token',
                    ])->asArray()->one();

            $response = $curl->setRequestBody(json_encode($params))
                    ->setHeaders([
                        'Content-Type' => 'application/json',
                        'Content-Length' => strlen(json_encode($params)),
                        'Authorization' => 'Bearer ' . $token['value']
                    ])
                    ->post('http://api.7days.us/v1/news/get-last');


            $result = (isset($response) && $response !== false) ? Json::decode($response) : array();
        }

        return $this->render('news.twig', ['news' => $result]);
    }

}
