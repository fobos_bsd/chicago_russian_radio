<?php

namespace frontend\widgets;

use Yii,
    yii\base\Widget;

/**
 * Class FooterMenu
 * @package frontend\widgets
 */
class ChatBlock extends Widget {

    public $width, $height;

    /**
     * @return init
     */
    public function init() {
        parent::init();
    }

    /**
     * @return string
     */
    public function run() {

        return $this->render('chat-block.twig', ['width' => $this->width, 'height' => $this->height]);
    }

}
