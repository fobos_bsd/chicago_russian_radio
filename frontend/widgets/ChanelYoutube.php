<?php

/**
 * Description of ChanelYoutube
 *
 * @author fobos
 */

namespace frontend\widgets;

use yii\base\Widget;
use common\models\GoogleYoutube;

class ChanelYoutube extends Widget {
    /*
     * @var chanel ID меню
     */

    public $id_chanel = 'UCEOPwtJG3ZfFHVqmIUA3FJw';

    public function init() {
        parent::init();
    }

    /**
     * @return string
     */
    public function run() {
        $youtube = new GoogleYoutube();
        $chanel_data = $youtube->getChanelByID($this->id_chanel);

        return $this->render('chanel-youtube.twig', [
                    'chanel_data' => $chanel_data,
        ]);
    }

}
