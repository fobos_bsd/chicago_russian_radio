<?php

namespace frontend\widgets;

use yii\base\Widget;
use common\models\Menu,
    common\models\MenuList;

/**
 * Class FooterMenu
 * @package frontend\widgets
 */
class ViewMenu extends Widget {
    /*
     * @var position позиция на сайте
     */

    public $position = 'header';

    /*
     * @var id меню
     */
    public $id_menu = 1;

    public function init() {
        parent::init();
    }

    /**
     * @return string
     */
    public function run() {
        $menu = MenuList::find()->where(['menu_id' => $this->id_menu, 'status' => 1])->orderBy(['gravity' => SORT_ASC, 'name' => SORT_ASC])->asArray()->all();

        return $this->render('view-menu.twig', [
                    'menulist' => $menu,
        ]);
    }

}
