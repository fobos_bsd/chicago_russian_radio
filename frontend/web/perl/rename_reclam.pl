#!/usr/bin/perl -w
print 'Start rename'."\r\n";
$dir = '../uploads/reclam_sound';
opendir (DIR, $dir) or die $!;
while (my $file = readdir(DIR)) {
    next unless (-f "$dir/$file");
    $oldfile = $file;
    $file =~ s/ /_/g;
    rename $dir . '/' . $oldfile, $dir . '/' . $file;
}
closedir(DIR);
print 'Finish rename'."\r\n";
