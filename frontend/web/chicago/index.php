<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>chicago1330</title>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/wizard.css">
</head>
<body>
<h1>Wizard</h1>

<div class="container-fluid">
    <div id="addvote" class="container">
        <h2>Add Votes</h2>
        <form>
            <div id="votestep1">
                <div class="form-group">
                    <label for="voteName">Name</label>
                    <input type="text" class="form-control" id="voteName" aria-describedby="voteNameHelp" placeholder="Enter Name">
                    <small id="voteNameHelp" class="form-text text-muted">Add Vote title.</small>
                </div>
                <div class="form-group">
                    <label for="voteType">Type vote</label>
                    <select class="form-control" id="voteType">
                        <option value="phone" id="phonetype">Phone</option>
                        <option value="sms" id="smstype">SMS</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="votePeriod">Period between calls</label>
                    <input type="number" class="form-control" id="votePeriod" aria-describedby="votePeriodHelp" placeholder="Enter period">
                    <small id="votePeriodHelp" class="form-text text-muted">(in seconds)</small>
                </div>
                <div class="form-group">
                    <label for="voteDescription">Description</label>
                    <textarea class="form-control" id="voteDescription" rows="3"></textarea>
                </div>
                <a class="btn btn-info" id="nvotestep1">Next</a>
            </div>
            <div id="votestep2phone">
                <div class="form-group">
                    <label for="voteNum1">Vote value for phone 12242494601*</label>
                    <input type="text" class="form-control" id="voteNum1" aria-describedby="voteNum1Help" placeholder="Enter value">
                </div>
                <div class="form-group">
                    <label for="voteNum2">Vote value for phone 12242494602*</label>
                    <input type="text" class="form-control" id="voteNum2" aria-describedby="voteNum2Help" placeholder="Enter value">
                </div>
                <div class="form-group">
                    <label for="voteNum3">Vote value for phone 12242494603*</label>
                    <input type="text" class="form-control" id="voteNum3" aria-describedby="voteNum3Help" placeholder="Enter value">
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input">
                        Create sms votes too
                    </label>
                </div>
                <div class="form-group">
                    <label for="voteStart">Start At</label>
                    <input type="date" class="form-control" id="voteStart" aria-describedby="voteStartHelp" placeholder="Enter start date">
                </div>
                <div class="form-group">
                    <label for="voteFinish">Finish At</label>
                    <input type="date" class="form-control" id="voteFinish" aria-describedby="voteFinishHelp" placeholder="Enter finish date">
                </div>
                <a class="btn btn-info bvotestep1">Back</a>
                <button class="btn btn-success">Add</button>
            </div>
            <div id="votestep2sms">
                <h4>Vote value for sms on phone. Max. variant`s count is 12 for one phone number 12242494600</h4>
                <div class="col-sm-12">
                    <div class="row">
                    <input type="text" class="col-sm-4" id="votesms1" placeholder="Enter value 1">
                    <input type="text" class="col-sm-4" id="votesms2" placeholder="Enter value 2">
                    <input type="text" class="col-sm-4" id="votesms3" placeholder="Enter value 3">
                    <input type="text" class="col-sm-4" id="votesms4" placeholder="Enter value 4">
                    <input type="text" class="col-sm-4" id="votesms5" placeholder="Enter value 5">
                    <input type="text" class="col-sm-4" id="votesms6" placeholder="Enter value 6">
                    <input type="text" class="col-sm-4" id="votesms7" placeholder="Enter value 7">
                    <input type="text" class="col-sm-4" id="votesms8" placeholder="Enter value 8">
                    <input type="text" class="col-sm-4" id="votesms9" placeholder="Enter value 9">
                    <input type="text" class="col-sm-4" id="votesms10" placeholder="Enter value 10">
                    <input type="text" class="col-sm-4" id="votesms11" placeholder="Enter value 11">
                    <input type="text" class="col-sm-4" id="votesms12" placeholder="Enter value 12">
                    </div>
                </div>
                <div class="form-group">
                    <label for="voteStart">Start At</label>
                    <input type="date" class="form-control" id="voteStart" aria-describedby="voteStartHelp" placeholder="Enter start date">
                </div>
                <div class="form-group">
                    <label for="voteFinish">Finish At</label>
                    <input type="date" class="form-control" id="voteFinish" aria-describedby="voteFinishHelp" placeholder="Enter finish date">
                </div>
                <a class="btn btn-info bvotestep1">Back</a>
                <button class="btn btn-success">Add</button>
            </div>
        </form>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script src="js/wizard.js"></script>
</body>
</html>
