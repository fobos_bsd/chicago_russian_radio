/**
 * Created by Server on 31.08.2017.
 */
jQuery(document).ready(function ($) {
    $curtype = 'phone';
    $('#voteType').on('change', function (e) {
        $curtype = this.value;
    });
    $('#nvotestep1').click(function () {
        if ($curtype == 'phone') {
            $('#votestep1').toggle();
            $('#votestep2phone').css('display','block');
        } else {
            $('#votestep1').toggle();
            $('#votestep2sms').css('display','block');
        }
    });
    $('.bvotestep1').click(function () {
        $('#votestep1').toggle();
        $('#votestep2phone').css('display','none');
        $('#votestep2sms').css('display','none');
    })
});