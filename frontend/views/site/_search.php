<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\DatePicker;
?>

<div class="box box-info">
    <div class="box-body">

        <?php
        $form = ActiveForm::begin([
                    'method' => 'get',
                    'options' => [
                        'data-pjax' => 1
                    ],
        ]);
        ?>

        <div class="col-sm-6 col-lg-6 no-padding">
            <?=
            $form->field($model, 'youtube_playlists_id')->widget(Select2::className(), [
                'data' => $playlists,
                'options' => ['multiple' => false, 'placeholder' => 'Выберите программу ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(Yii::t('frontend', 'Program'))
            ?>
        </div>
        <div class="col-sm-6 col-lg-6 no-paddingright">
            <?=
            $form->field($model, 'radio_brodcast_day')->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Выберите дату эфира...'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ])->label(Yii::t('frontend', 'Date'))
            ?>
            <?php // echo $form->field($model, 'name')->label(Yii::t('frontend', 'Name')) ?>
        </div>

        <div class="clearfix"></div>

        <?php // echo $form->field($model, 'long_time') ?>

        <?php // echo $form->field($model, 'radio_brodcast_day') ?>

        <?php // echo $form->field($model, 'radio_brodcast_time')   ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('frontend', 'Search'), ['class' => 'btn btn-warning']) ?>
            <?= Html::a(Yii::t('frontend', 'Reset'), '/archive', ['class' => 'btn btn-default']) ?>
        </div>


        <?php ActiveForm::end(); ?>
    </div>
</div>
