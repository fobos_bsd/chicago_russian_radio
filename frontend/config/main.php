<?php

use yii\web\UrlNormalizer;

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'homeUrl' => '/',
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru-RU',
    'components' => [
        'request' => [
            'baseUrl' => '',
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'csrfParam' => '_csrf-frontend',
        ],
        /*
          'session' => [
          // this is the name of the session cookie used for login on the frontend
          'name' => 'advanced-frontend',
          ],
         * 
         */
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'normalizer' => [
                'class' => 'yii\web\UrlNormalizer',
                'action' => UrlNormalizer::ACTION_REDIRECT_PERMANENT,
            ],
            'rules' => [
                '' => 'site/index',
                'login' => 'user/security/login',
                'programmy' => 'youtube/index',
                'programmy/<plid:[\d]+>' => 'youtube/index/item',
                'broadcasting-grid' => 'site/broadcasting-grid',
                'archive' => 'site/archive',
                'voice-reclam' => 'voicereclam/index',
                'table-actions' => 'site/table-actions',
                'o-nas' => 'site/contact',
                'radio-voice' => 'site/radio-voice',
                'banner-statistic' => 'banner/statistics/ajax-click',
                'our-clients' => 'pages/clients/index',
                'our-clients/<slug:[\w-]+>' => 'pages/clients/item',
                'ajax-click' => 'banner/statistics/ajx-click',
                // Объявления
                'chastnye-obyavleniya' => 'advertisements/index/index',
                'chastnye-obyavleniya/<id:\d+>' => 'advertisements/index/index',
                'chastnye-obyavleniya/item' => 'advertisements/index/item',
                'chastnye-obyavleniya/search' => 'advertisements/index/search',
                'chastnye-obyavleniya/search/<id:\d+>' => 'advertisements/index/search',
                'chastnye-obyavleniya/ajax-deleterekl' => 'advertisements/index/ajax-deleterekl',
                'chastnye-obyavleniya/<category:[a-zа-яА-ЯA-Z0-9-_]+>' => 'advertisements/index/getcategory',
                'chastnye-obyavleniya/<category:[a-zа-яА-ЯA-Z0-9-_]+>/<id:\d+>' => 'advertisements/index/getcategory',
                'chastnye-obyavleniya/<category:[a-zа-яА-ЯA-Z0-9-_]+>/<name:[a-zA-Z0-9-_]+' => 'advertisements/category/info',
                'chastnye-obyavleniya/category/name' => 'advertisements/index/category',
                'chastnye-obyavleniya/management/<action:[a-zA-Z0-9-_]+>' => 'advertisements/management/<action>',
                'chastnye-obyavleniya/management/<action:[a-zA-Z0-9-_]+>/<id:\d+>' => 'advertisements/management/<action>',
                // Правило для отображения новостей без суффикса, всегда последнее!
                '<slug:[\w-]+>' => 'pages/index/index'
            ],
        ],
        'formatter' => [
            'dateFormat' => 'MM/dd/yyyy',
        ],
        'view' => [
            'class' => 'yii\web\View',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [
                        'auto_reload' => true,
                        'debug' => false,
                    ],
                    'extensions' => YII_DEBUG ? ['\Twig_Extension_Debug'] : [],
                    'globals' => [
                        'html' => 'yii\bootstrap\Html',
                        'Url' => 'yii\helpers\Url',
                        'DateTimePicker' => 'kartik\datetime\DateTimePicker'
                    ],
                    'functions' => array(
                        't' => 'Yii::t'
                    ),
                ],
            ],
        ],
        'Siteinfo' => 'frontend\components\Siteinfo',
        'serverHelper' => 'frontend\components\ServerHelper',
        'shortcodes' => [
            'class' => 'tpoxa\shortcodes\Shortcode',
            'callbacks' => [
                'advcam' => ['frontend\widgets\AdvertisingCampaign', 'widget'],
            ]
        ],
    ],
    'modules' => [
        'user' => [
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
        ],
        'pages' => [
            'class' => 'frontend\modules\pages\Module',
        ],
        'youtube' => [
            'class' => 'frontend\modules\youtube\Module',
        ],
        'banner' => [
            'class' => 'frontend\modules\banner\Module',
        ],
        'voicereclam' => [
            'class' => 'frontend\modules\voicereclam\Module',
        ],
        'advertisements' => [
            'class' => 'frontend\modules\advertisements\Module',
        ],
    ],
    'layout' => 'main.twig',
    'params' => $params,
];
