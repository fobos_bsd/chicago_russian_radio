<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://use.fontawesome.com/e5e92b11ab.js',
        'css/font-awesome.min.css',
        'css/bootstrap-theme.min.css',
        'css/flexslider.css',
        'css/mediaelementplayer.min.css',
        'css/main.css',
        'css/layout.css',
        //'css/slick.css',
        //'css/slick-theme.css',
        'css/jquery.fancybox.min.css',
        'load/skin_allsizes_white.css',
        'dzstooltip/dzstooltip.css',
        'audioplayer/audioplayer.css',
        'audioplayer/audioportal-grid.css',
    ];
    public $js = [
        'js/jquery-3.2.1.js',
        'js/vendor/modernizr-2.8.3-respond-1.4.2.min.js',
        'js/FWDSimple3DCoverflow.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\bootstrap\BootstrapThemeAsset',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

}
