<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class FrontendAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/jquery.flexslider-min.js',
        'js/jquery.fancybox.min.js',
        'js/mediaelementplayer.min.js',
        'js/renderers/dailymotion.min.js',
        'js/renderers/facebook.min.js',
        'js/renderers/soundcloud.min.js',
        'js/renderers/twitch.min.js',
        'js/renderers/vimeo.min.js',
        //'js/lang/ru.js',
        //'js/jquery.anoslide.js',
        //'js/jquery.waterwheelCarousel.js',
        //'js/slick.js',
        'audioplayer/audioplayer.js',
        'js/shCore.js',
        'js/shBrushJScript.js',
        'js/main.js',
        'js/layout.js',
    ];
    public $depends = [
        'frontend\assets\AppAsset',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_END];

}
