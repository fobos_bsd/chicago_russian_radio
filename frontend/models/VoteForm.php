<?php

namespace frontend\models;

use common\models\Results;
use common\models\Votes;
use Yii;
use yii\base\Model;

/**
 * Class VoteForm
 * @package frontend\forms
 */
class VoteForm extends Model
{
    /**
     * @var
     */
    public $answerId;

    /**
     * @var
     */
    public $questionId;

    /**
     * @var
     */
    public $site_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answerId', 'questionId', 'site_id'], 'required'],
            [['answerId', 'questionId', 'site_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'answerId' => Yii::t('frontend', 'Вопрос'),
            'questionId' => Yii::t('frontend', 'Ответ'),
        ];
    }

    /**
     * @return bool
     */
    public function vote()
    {
        $result = new Votes([
            'answers_id' => $this->answerId,
            'questions_id' => $this->questionId,
            'ip' => Yii::$app->serverHelper->getRemoteIPAddress(),
            'site_id' => $this->site_id
        ]);

        return $result->save();
    }
}
