<?php

namespace frontend\modules\banner\controllers;

use common\models\BannerStatistic;
use Yii;
use yii\web\Controller;

/**
 * Class StatisticsController
 * @package frontend\modules\banner\controllers
 */
class StatisticsController extends Controller {

    public $enableCsrfValidation = false;


    public function actionAjaxClick() {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $bannerID = (int) $post['banner_id'];

            // Chack is session open
            $session = Yii::$app->session;
            if (!$session->isActive)
                $session->open();

            if (!$session->has('banner_click_' . $bannerID)) {
                $session->set('banner_click_' . $bannerID, 'yes');

                $model = $this->findStatistic($bannerID);
                if ($model !== false) {
                    $model->click_count += 1;
                    $model->update();
                }
            }
        }
    }

    protected function findStatistic($id) {
        if (($model = BannerStatistic::find()->where(['banner_id' => $id])->one()) !== null) {
            return $model;
        } else {
            return false;
        }
    }



}
