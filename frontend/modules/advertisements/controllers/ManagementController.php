<?php

/*
 * Управление добавление и редактирование объявлений
 */

namespace frontend\modules\advertisements\controllers;

use yii;
use yii\helpers\ArrayHelper,
    yii\web\UploadedFile,
    yii\helpers\Json,
    yii\web\Response;
use frontend\modules\advertisements\controllers\BaseController,
    frontend\modules\advertisements\forms\PayQBForm;
use common\models\AdvertisementsTemplates;
use common\models\User,
    common\models\ImageUpload as UploadForm,
    common\models\Settings,
    frontend\forms\ModuleAdvForm,
    frontend\modules\advertisements\forms\TextadForm;

class ManagementController extends BaseController {

    private $userID, $adform, $userToken;
    public $totalsumm, $cookie, $pricetrans, $pricecolor, $priceplace;

    public function beforeAction($action) {
        if ($action->id == 'error') {
            $this->layout = 'error';
        }

        if ($action->id == 'imgboss-upload') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    // Инициируем и получаем параметры объвлений из базы
    public function init() {
        parent::init();

        // Получаем статические переменные
        Yii::$app->authAdvertisement->isAuthCore();
        $this->userID = ($this->session->has('core_userID')) ? $this->session->get('core_userID') : 0;
        $this->userToken = ($this->session->has('core_userToken')) ? $this->session->get('core_userToken') : '';
        //$this->adform = Yii::$app->authAdvertisement->getAdLayoutId();
        // Получаем список ID шаблонов
        $this->adform = AdvertisementsTemplates::find()->where(['status' => 1])->all();
        $this->cookie = Yii::$app->request->cookies;
        $this->cookie->readOnly = false;
    }

    public function actionStart() {

        $params = [];
        return $this->render('start', $params);
    }

    // Создаём новое объявление
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->render('create_auth');
        }

        // Получаем данные учётной записи пользователя, если авторизирован
        $userID = Yii::$app->user->id;
        $userData = User::find()->where(['id' => $userID])->one();

        $fields = array();
        $servises = array();
        $categories = array();
        $this->typeadv = Yii::$app->request->get('type', 'text');

        $arr_cat = $this->getCategories(); // Список категорий
        if (isset($arr_cat) && is_array($arr_cat)) {
            foreach ($arr_cat as $category) {
                $categories[$category->id] = $category->title;
            }
        }

        $ful_platforms = [];
        $platforms = $this->getPlatforms(); // Список площадок
        // Set platrorm services
        if (isset($platforms) && is_array($platforms)) {
            $i = 0;
            foreach ($platforms as $platform) {
                $services = $this->getPlatformServises($platform->id);
                if (isset($services) && is_array($services)) {
                    $ful_platforms[$i]['platform'] = $platform;
                    $ful_platforms[$i]['services'] = $services;
                }
                $i++;
            }
        }

        // Значение кратких правил
        if (isset($this->typeadv) && $this->typeadv === 'banner') {
            $rules = (isset($this->setdesc['short_rules_usege_adv'])) ? $this->setdesc['short_rules_usege_adv'] : '';
        } else {
            $rules = (isset($this->setdesc['short_rules_usege_text_adv'])) ? $this->setdesc['short_rules_usege_text_adv'] : '';
        }

        if ($this->typeadv !== 'banner') {
            // Add temporary form for module advert
            $modadvform = new TextadForm();
            $modadvform->scenario = 'create';
            $modadvform->name = Yii::$app->user->identity['name'] . ' ' . Yii::$app->user->identity['surname'];
            $modadvform->email = Yii::$app->user->identity['email'];
            $modadvform->reg_date = Yii::$app->formatter->asDatetime($userData->created_at, 'php:Y-m-d H:i:s');
            $modadvform->rag_ip = Yii::$app->RequestParams->getIp();
            $modadvform->totalsumm = '0.00';
            $modadvform->source = '7days.us';

            if ($modadvform->load(Yii::$app->request->post())) {
                $modadvform->imageFile = UploadedFile::getInstance($modadvform, 'imageFile');
                $reslt_save = $modadvform->save();
                if (isset($reslt_save->status) && $reslt_save->status === 'success' && isset($reslt_save->invoice)) {
                    return $this->redirect(['management/pay', 'invoice_id' => $reslt_save->invoice, 'summ' => $modadvform->totalsumm]);
                }
            }
        } else {
            $modadvform = new ModuleAdvForm();
        }

        return $this->render('create', [
                    'typeadv' => $this->typeadv,
                    'rules' => $rules,
                    'fields' => $fields,
                    'categories' => $categories,
                    'servises' => $servises,
                    'uplimage' => new UploadForm(),
                    'modadvform' => $modadvform,
                    'platforms' => $ful_platforms
        ]);
    }

    // Запрос на модульную рекламу
    public function actionAjaxReklam() {
        $model = new ModuleAdvForm();

        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [
            'status' => 0,
            'error' => 'Action don`t run'
        ];

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model->validate()) {
            $setting = Settings::find()->byName('noticeEmail')->one();
            if ($model->sendEmail($setting->value) === false) {
                $response = [
                    'status' => 0,
                    'error' => 'Mail not send'
                ];
            } else {
                $response = [
                    'status' => 1,
                    'error' => ''
                ];
            }
        }

        return $response;
    }

    // Оплата объявления
    public function actionPay($invoice_id, $summ) {
        if ($summ <= 0) {
            return $this->redirect(['management/status-pay', 'status' => 'paid', 'totalsumm' => 0.00]);
        }

        $model_pay = new PayQBForm();
        $model_pay->company = '001 Classyfied';
        $model_pay->bstate = 'USA';
        $model_pay->baddress = '325 North Milwaukee Avenue';
        $model_pay->bzip = '60090';
        $model_pay->amount = $summ;
        $model_pay->invoice = $invoice_id;

        // Если переданы данные на оплату
        if ($model_pay->load(Yii::$app->request->post()) && $model_pay->validate()) {
            $result_pay = Json::decode($model_pay->pay());

            if (isset($result_pay['status']) && ($result_pay['status'] === 'success' || $result_pay['status'] === 'paid')) {
                return $this->redirect(['management/status-pay', 'status' => 'paid', 'totalsumm' => $summ]);
            } else {
                Yii::$app->session->setFlash('error', "Оплата не прошла. " . $result_pay['error']);
            }

            /*
              $invoices = $post['summInvoice'];
              foreach ($pdata as $pdt) :
              foreach ($invoices as $key => $val) {
              $post['AutorizeForm']['AMOUNT'] = (float) $val;
              $post['AutorizeForm']['COUNTRY'] = 'US';
              $post['AutorizeForm']['CITY'] = $post['AutorizeForm']['BADDRESS'];

              // Передаём данные по объявлению для создания счёта
              if ($this->session->has('advdata')) {
              try {
              $arradvdata = Json::decode($this->session->get('advdata'));
              } catch (Exception $ex) {
              echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Ошибка передаваемых данных для оплаты. Обратитесь пожалуйста к администратору.')]);
              }

              $post['AutorizeForm']['DATAEMAIL'] = $arradvdata['email'];
              $post['AutorizeForm']['DATAPHONE'] = $arradvdata['phone'];
              } else {
              $post['AutorizeForm']['DATAEMAIL'] = 'noemail@domain.info';
              $post['AutorizeForm']['DATAPHONE'] = '';
              }

              // Проверяем существует ли переменная типа объявления
              $post['AutorizeForm']['TYPEADV'] = (isset($pdt->data->typeadv)) ? $pdt->data->typeadv : 'text';
              // Проверяем тип объявления, если банер, то меняем параметр ключа при наличии
              $this->typeadv = $post['AutorizeForm']['TYPEADV'];
              $this->setTypeAdv($this->typeadv);
              Yii::$app->authAdvertisement->isAuthCore();

              $data = base64_encode(json_encode($post));
              $url = $this->urlapicore . "/site/payinvoice?API_KEY=" . $this->coreKey . "&LANG=ru&USERID=" . $this->userToken . "&INVOICEID=" . $key . "&DATA=" . $data;
              curl_setopt($this->ch, CURLOPT_URL, $url);
              try {
              $rescurl = curl_exec($this->ch);
              //var_dump($rescurl); die();
              $respay = Json::decode($rescurl);
              } catch (Exception $ex) {
              echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось оплатить счёт за объявление. Обратитесь пожалуйста к администратору.')]);
              }

              $status = (isset($respay['data']['status'])) ? $respay['data']['status'] : $respay['data'];
              $paydsumm = + (float) $val;

              if ($status !== 'paid') {
              $status = $respay['data']['CC'][0];
              break;
              }
              }
              break;
              endforeach;

              if ($this->session->has('advdata'))
              unset($_SESSION['advdata']);

              //echo Yii::$app->runAction('/advertisements/management/status-pay', ['status' => $status]);
              $this->redirect(['/chastnye-obyavleniya/management/status-pay', 'status' => $status, 'totalsumm' => $paydsumm]);
             * 
             */
        }

        return $this->render('pay', ['model_pay' => $model_pay]);
    }

    // Сосотояние оплаты
    public function actionStatusPay() {
        $status = (Yii::$app->request->get('status')) ? Yii::$app->request->get('status') : 'error'; // Результат статуса оплаты
        $totalsumm = (Yii::$app->request->get('totalsumm')) ? Yii::$app->request->get('totalsumm') : 0;
        return $this->render('status-pay', [
                    'status' => $status,
                    'totalsumm' => $totalsumm
        ]);
    }

    // Страница ошибки
    public function actionExeption($err_mess) {
        return $this->render('exeption', ['errordata' => $err_mess]);
    }

    // Получаем список категорий объявлений с количеством объявлений в них
    private function getAllCategories() {

        try {
            $urlcat = $this->urlapicore . "/site/getadcategoriesforsite?API_KEY=" . $this->coreKey . "&LANG=ru";
            curl_setopt($this->ch, CURLOPT_URL, $urlcat);
            $categories = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить список категорий объявлений. Обратитесь пожалуйста к администратору.')]);
        }

        if (isset($categories->result) && $categories->result === 'site_data') {
            $catarr = (array) $categories->data;
        } else {
            $catarr = null;
        }

        return $catarr;
    }

    // Получаем список категорий объявлений
    public function getCategories() {
        try {
            $urlcat = $this->urlapicore . "/platforms/get-categories";
            curl_setopt($this->ch, CURLOPT_URL, $urlcat);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, "lang=ru");
            $categories = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить список категорий в объявлении. Обратитесь пожалуйста к администратору.')]);
        }

        return $categories;
    }

    // Получаем список площадок
    public function getPlatforms() {
        try {
            $urlcat = $this->urlapicore . "/platforms/get-platforms";
            curl_setopt($this->ch, CURLOPT_URL, $urlcat);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, "lang=ru");
            $platforms = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить список площадок. Обратитесь пожалуйста к администратору.')]);
        }

        return $platforms;
    }

    // Получаем список сервисов для площадок
    public function getPlatformServises($teml_id) {
        try {
            $urlcat = $this->urlapicore . "/platforms/get-platform-services";
            curl_setopt($this->ch, CURLOPT_URL, $urlcat);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, "lang=ru&tmpl_id=" . $teml_id);
            $services = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить список сервисов для площадок. Обратитесь пожалуйста к администратору.')]);
        }

        return $services;
    }

    // Получаем список категорий объявлений без количе6ства объявлений в них
    private function getOnlyCategories($site_id, $lang = 'ru') {

        try {
            $urlcat = $this->urlapicore . "/site/get-categories?API_KEY=" . $this->coreKey . "&LANG=" . $lang . "&SITE=" . $site_id;
            curl_setopt($this->ch, CURLOPT_URL, $urlcat);
            $categories = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить список категорий в объявлении. Обратитесь пожалуйста к администратору.')]);
        }

        if (isset($categories->result) && $categories->result === 'new_data') {
            $catarr = (array) $categories->data;
        } else {
            $catarr = null;
        }

        return $catarr;
    }

    // Получаем список расценок на объявления
    private function getPriseList($adid, $lang, $userid) {
        $arrprices = array();

        try {
            $urlpr = $this->urlapicore . "/site/pricelist?API_KEY=" . $this->coreKey . "&LANG=" . $lang . "&USERID=" . $userid . "&ADVERTICEID=" . $adid . "&TYPE=json";
            curl_setopt($this->ch, CURLOPT_URL, $urlpr);
            $price = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить список цен в объявлении. Обратитесь пожалуйста к администратору.')]);
        }

        if (isset($price->result) && $price->result === 'new_data') {
            $listprice = (array) $price->data;
        } else {
            $listprice = null;
        }

        // Формируем массив цен
        if ($listprice !== null) {
            $price = (float) $listprice['price'];

            foreach ($listprice['discount'] as $discont) {
                if ($discont->weeks !== '0') {
                    $arrprices[$discont->weeks] = ($discont->type !== 'fixed price') ? round(($discont->weeks * $price) * (100 - $discont->value) / 100, 3) : round(($discont->weeks * $price) - $discont->value, 3);
                    $arrprices['type'] = 'web';
                    $oldprice = round((int) $discont->weeks * (float) $price, 2);
                    $arrprices['listp'][] = ($discont->type !== 'fixed price') ? array('weeks' => $discont->weeks, 'discont' => $discont->value, 'distype' => $discont->type, 'dprice' => round(($discont->weeks * $price) * (100 - $discont->value) / 100, 3), 'oldprice' => $oldprice) :
                            array('weeks' => $discont->weeks, 'discont' => $discont->value, 'distype' => $discont->type, 'dprice' => round(($discont->weeks * $price) - $discont->value, 3), 'oldprice' => $oldprice);
                } else {
                    $arrprices[$discont->issues] = ($discont->type !== 'fixed price') ? round(($discont->issues * $price) * (100 - $discont->value) / 100, 3) : round(($discont->issues * $price) - $discont->value, 3);
                    $arrprices['type'] = 'newspaper';
                    $oldprice = round((int) $discont->issues * (float) $price, 2);
                    $arrprices['listp'][] = ($discont->type !== 'fixed price') ? array('weeks' => $discont->issues, 'discont' => $discont->value, 'distype' => $discont->type, 'dprice' => round(($discont->issues * $price) * (100 - $discont->value) / 100, 3), 'oldprice' => $oldprice) :
                            array('weeks' => $discont->issues, 'discont' => $discont->value, 'distype' => $discont->type, 'dprice' => round(($discont->issues * $price) - $discont->value, 3), 'oldprice' => $oldprice);
                }
            }

            $arrprices['price'][] = $price;
        }

        //var_dump($arrprices); die();

        return $arrprices;
    }

    // Получаем список расценок на сервисы в объявлениях
    private function getServiseList($adid, $lang, $userid) {
        $arrprices = array();

        try {
            $urlpr = $this->urlapicore . "/site/serviselist?API_KEY=" . $this->coreKey . "&LANG=" . $lang . "&USERID=" . $userid . "&ADVERTICEID=" . $adid . "&TYPE=json";
            curl_setopt($this->ch, CURLOPT_URL, $urlpr);
            $price = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить список сервисов в объявлении. Обратитесь пожалуйста к администратору.')]);
        }

        if (isset($price->result) && $price->result === 'new_data') {
            $listprice = (array) $price->data;
        } else {
            $listprice = null;
        }

        return $listprice;
    }

    /**
     * расчитать даты и цены в обьявлениях
     */
    public function actionGetdate() {
        $start_week = Yii::app()->request->getParam('start_week');
        $weeks = Yii::app()->request->getParam('weeks', 1);
        $year = Yii::app()->request->getParam('year', date('Y'));
        echo Json::encode(array(
            'end_date' => date('Y-m-d', strtotime('+' . $weeks . ' week -1 day', Date_helper::get_sunday($start_date, $year)))
        ));
    }

    // Обработчик загрузки дополнительных файлов 
    public function actionImageUpload() {
        $model = new UploadForm();
        $userID = (!Yii::$app->user->isGuest) ? Yii::$app->user->id : 1;

        if (Yii::$app->request->isPost) {
            $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
            $model->upload($userID);

            return true;
        }

        return false;
    }

    // Обработчик загрузки основного изображения
    public function actionImgbossUpload() {
        Yii::$app->controller->enableCsrfValidation = false;
        $userID = (!Yii::$app->user->isGuest) ? Yii::$app->user->id : 1;
        $result = 0;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $folder_hr = Yii::getAlias('@frontend') . '/web/advartise/user_' . Yii::$app->user->id;
            // Проверка на существование каталога, если нет - создаём
            if (!file_exists($folder_hr)) {
                mkdir($folder_hr, 0777, true);
            }

            $folder = Yii::getAlias('@frontend') . '/web/advartise/user_' . Yii::$app->user->id . '/boss';

            // Проверка на существование каталога, если нет - создаём
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }

            $restcrop = Yii::$app->croppic->cropOneImage($post, $folder);
            $result = (isset($restcrop['status']) && $restcrop['status'] === 'success') ? json_encode($restcrop) : 0;
        }

        return $result;
    }

    // Обработчик удаления основного изображения если произведена отмена
    public function actionImgbossDelete() {
        Yii::$app->controller->enableCsrfValidation = false;
        $userID = (!Yii::$app->user->isGuest) ? Yii::$app->user->id : 0;
        $result = 0;

        if ($userID !== 0) {
            $folder = Yii::getAlias('@frontend') . '/web/advartise/user_' . Yii::$app->user->id . '/boss';
            if (file_exists($folder)) {
                foreach (glob($folder . '/*') as $file) {
                    unlink($file);
                }
            }
        }

        return $result;
    }

    // Загрузки основного изображения - оригинала
    /*
      public function actionImgbossOrigupload() {
      $userID = (!Yii::$app->user->isGuest) ? Yii::$app->user->id : 1;
      $result = 0;

      if (Yii::$app->request->isPost) {
      $post = Yii::$app->request->post();
      $folder = Yii::getAlias('@frontend') . '/web/advartise/user_' . Yii::$app->user->id . '/tmp';
      if (!file_exists($folder))
      mkdir($folder, 0777);

      $restcrop = Yii::$app->croppic->loadOneImage($_FILES, $folder);
      $result = (isset($restcrop['status']) && $restcrop['status'] === 'success') ? 1 : 0;
      }

      return $result;
      }
     * 
     */

    // Получение списка загруженных дополнительных файлов
    private function getUserFilesUpload() {
        try {
            $user_folder = Yii::getAlias('@frontend') . '/web/advartise/user_' . Yii::$app->user->id;
        } catch (Exception $ex) {
            return null;
        }

        if (file_exists($user_folder)) {
            return scandir($user_folder);
        } else {
            return array();
        }
    }

    // Получение загруженного основного файла
    private function getBossFileUpload() {
        try {
            $user_folder = Yii::getAlias('@frontend') . '/web/advartise/user_' . Yii::$app->user->id . '/boss';
        } catch (Exception $ex) {
            return null;
        }

        if (file_exists($user_folder)) {
            return scandir($user_folder);
        } else {
            return array();
        }

        return scandir($user_folder);
    }

    // Обработка и кропинг изображенияОчистка папки временных изображений объявлений
    protected function cleanFolder($folder, $patern = null) {
        // Delete old images
        if (file_exists($folder)) {
            foreach (glob($folder . $patern) as $file) {
                unlink($file);
            }
        }
    }

    // Функция определения языка
    protected function getTemplateLang($description = '') {
        if (!empty($description) && (strpos($description, '7days') !== false || strpos($description, '7 days') !== false)) {
            $lang = 'ru';
        } elseif (!empty($description) && (strpos($description, 'Aidas') !== false || strpos($description, 'aidas') !== false)) {
            $lang = 'lt';
        } else {
            $lang = 'ru';
        }

        return $lang;
    }

}
