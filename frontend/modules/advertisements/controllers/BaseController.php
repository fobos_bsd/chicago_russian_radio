<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\advertisements\controllers;

use Yii,
    yii\web\Controller,
    yii\web\Cookie;

/**
 * Description of BaseController
 *
 * @author fobos
 */
class BaseController extends Controller {

    public $catarr = array(), $module, $ch, $session, $settings, $cookies, $typeadv, $setdesc;
    protected $urlcore, $urlapicore, $coreKey;

    public function init() {
        parent::init();

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_NOBODY, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        $this->session = Yii::$app->session;

        // Получаем статические переменные
        $this->coreKey = Yii::$app->params['Auth_key_oncore'];
        $this->urlcore = Yii::$app->params['url_core'];
        $this->urlapicore = Yii::$app->params['url_core_api'];

        // Ставим тип  объявления по умолчанию
        $this->setTypeAdv('text');

        // Получим параметры настроек
        $this->settings = Yii::$app->params;

        // Инициируем куки
        $this->cookies = Yii::$app->response->cookies;
        $this->cookies->readOnly = false;
    }

    // Получение массива категорий
    public function getCategories() {
        $urlcat = $this->urlapicore . "/site/getadcategoriesforsite?API_KEY=" . $this->coreKey . "&LANG=ru";
        curl_setopt($this->ch, CURLOPT_URL, $urlcat);
        $categories = json_decode(curl_exec($this->ch));
        if (isset($categories->result) && $categories->result === 'site_data') {
            $catarr = (array) $categories->data;
        } else {
            $catarr = null;
        }

        return $catarr;
    }

    protected function setCookie($name, $value) {
        $this->cookies->add(new Cookie([
            'name' => $name,
            'value' => $value,
            'expire' => time() + 3600,
            'path' => '/'
        ]));
    }

    public function setTypeAdv($type = 'text') {
        $this->typeadv = $type;

        switch ($type) {
            case 'banner': if (isset($this->settings['Auth_key_core_banner']))
                    $this->coreKey = $this->settings['Auth_key_core_banner'];
                break;
            case 'text': if (empty($this->coreKey))
                    $this->coreKey = '1111111111111111111111111111111111111111';
                break;
            default: if (empty($this->coreKey))
                    $this->coreKey = '1111111111111111111111111111111111111111';
                break;
        }
    }

}
