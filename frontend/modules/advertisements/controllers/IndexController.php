<?php

namespace frontend\modules\advertisements\controllers;

use Yii,
    yii\web\NotFoundHttpException,
    yii\helpers\Url;
use frontend\modules\advertisements\controllers\BaseController,
    frontend\modules\advertisements\models\FilterForm;

class IndexController extends BaseController {

    private $userID, $userToken;

    public function init() {
        parent::init();

        $this->userID = ($this->session->has('core_userID')) ? $this->session->get('core_userID') : 0;
        $this->userToken = ($this->session->has('core_userToken')) ? $this->session->get('core_userToken') : '';
    }

    // Стартовая страница объявлений
    public function actionIndex() {
        $advimgfarr = null;

        if ($this->session->has('searchtext')) {
            $this->session->remove('searchtext');
        }

        // Число элементов на странице
        $count_par_pare = (isset($this->settings['count_items_onpage'])) ? (int) $this->settings['count_items_onpage'] : 10;

        // Получаем номер страници из пагинации
        $page_number = Yii::$app->request->get('id');
        $page = (isset($page_number) && $page_number !== NULL) ? $page_number : 1;

        // Получение всех объявлений
        $advarr = (array) $this->getAllAdv($page, $count_par_pare);

        // Общее число страниц
        $totalpage = (isset($advarr['count_pages'])) ? (int) $advarr['count_pages'] : 0;

        Url::remember();

        return $this->render('index.twig', [
                    'advarr' => $advarr,
                    'page' => (int) $page,
                    'totalpage' => (int) $totalpage
        ]);
    }

    // Промежуточный переход к списку объявлений из категории с транслитерацией и pos зфпросом
    public function actionCategory() {
        $catname = Yii::$app->request->get('catname');
        $id = Yii::$app->request->get('catid');
        //$slug = $this->transliterate($catname);
        $slug = $catname;
        $this->session->set('category_id', $id);
        $this->session->set('category_name', str_replace('"', '', $catname));

        return $this->redirect('/chastnye-obyavleniya/' . $slug);
    }

    // Стартовая страница объявлений
    public function actionGetcategory() {
        if ($this->session->has('searchtext')) {
            $this->session->remove('searchtext');
        }

        // Число элементов на странице
        $count_par_pare = (isset($this->settings['count_items_onpage'])) ? (int) $this->settings['count_items_onpage'] : 20;

        // Получаем номер страници из пагинации
        $page_number = Yii::$app->request->get('id');
        $page = (isset($page_number) && $page_number !== NULL) ? $page_number : 1;

        // Получение всех объявлений
        $advarr = (array) $this->getCategoryAdv($page, $count_par_pare, $this->session->get('category_id'));

        // Общее число страниц
        $totalpage = (isset($advarr['count_pages'])) ? (int) $advarr['count_pages'] : 0;

        $catname = \yii\helpers\Html::encode($this->session->get('category_name'));

        // List of categories
        $arr_cat = $this->getCategoriesAdv();
        foreach ($arr_cat as $item_cat) {
            if ($item_cat->id == (int) $this->session->get('category_id')) {
                $catname = $item_cat->name;
                break;
            }
        }

        Url::remember();

        return $this->render('category.twig', [
                    'advarr' => $advarr,
                    'title' => $catname,
                    'page' => $page,
                    'totalpage' => (int) $totalpage,
                    'slug' => Yii::$app->request->queryParams['category']
        ]);
    }

    // Получение одиночного объявления
    public function actionItem() {
        if ($this->session->has('searchtext')) {
            $this->session->remove('searchtext');
        }

        if (!Yii::$app->request->get('id')) {
            throw new NotFoundHttpException(Yii::t('frontend', 'Данного объявления не существует.'));
        }

        $advID = (int) Yii::$app->request->get('id');

        // Получение данных объявления
        $advarr = (array) $this->getOneAdv($advID);

        $catname = \yii\helpers\Html::encode($this->session->get('category_name'));

        // List of categories
        $arr_cat = $this->getCategoriesAdv();

        foreach ($arr_cat as $item_cat) {
            if ($item_cat->id == (int) $this->session->get('category_id')) {
                $catname = $item_cat->name;
                break;
            }
        }

        return $this->render('item.twig', [
                    'advarr' => $advarr,
                    'title' => $catname,
                    'advID' => $advID
        ]);
    }

    /*
     * @return filter data
     */

    public function actionSearch() {
        $advarr = [];

        // Число элементов на странице
        $count_par_pare = (isset($this->settings['count_items_onpage'])) ? (int) $this->settings['count_items_onpage'] : 10;

        // Получаем номер страници из пагинации
        $page_number = Yii::$app->request->get('id');
        $page = (isset($page_number) && $page_number !== NULL) ? $page_number : 1;

        // Инициируем и получаем данные фильтра
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $this->session->set('searchtext', $post);
        } elseif (!Yii::$app->request->post() && $this->session->has('searchtext')) {
            $post = $this->session->get('searchtext');
        }

        $model = new FilterForm();
        if (isset($post)) {
            $model->load($post);
            if ($model->validate()) {
                // Получение всех объявлений
                $advarr = (array) $this->getSearchAdv($page, $count_par_pare, $model->search_text);

                try {
                    $advertisements = json_decode(curl_exec($this->ch));
                } catch (Exception $ex) {
                    throw new \yii\web\HttpException('Некорректные данные для обработки и вывода результатов поиска');
                }
            }
        }

        // Общее число страниц
        $totalpage = (isset($advarr['count_pages'])) ? (int) $advarr['count_pages'] : 0;

        return $this->render('search.twig', [
                    'advarr' => $advarr,
                    'title' => Yii::t('frontend', 'Результаты поиска'),
                    'page' => $page,
                    'totalpage' => (int) $totalpage
        ]);
    }

    // Получаем список всех объявлений
    public function getAllAdv($page, $count_par_pare) {
        try {
            $urlcat = $this->urlapicore . "/content/data-all";
            curl_setopt($this->ch, CURLOPT_URL, $urlcat);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, "lang=ru&platform=chicago1330&page=" . $page . "&count_par_pare=" . $count_par_pare . "&sort_radio=1");
            $platforms = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить объявления. Обратитесь пожалуйста к администратору.')]);
        }

        return $platforms;
    }

    // Получаем список всех объявлений в категории
    public function getCategoryAdv($page, $count_par_pare, $category_id) {
        try {
            $urlcat = $this->urlapicore . "/content/data-category";
            curl_setopt($this->ch, CURLOPT_URL, $urlcat);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, "lang=ru&platform=chicago1330&category=" . $category_id . "&page=" . $page . "&count_par_pare=" . $count_par_pare . "&sort_radio=1");
            $platforms = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить объявления. Обратитесь пожалуйста к администратору.')]);
        }

        return $platforms;
    }

    // Получаем список всех объявлений по результатам поиска
    public function getSearchAdv($page, $count_par_pare, $search) {
        try {
            $urlcat = $this->urlapicore . "/content/data-search";
            curl_setopt($this->ch, CURLOPT_URL, $urlcat);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, "lang=ru&platform=chicago1330&search=" . $search . "&page=" . $page . "&count_par_pare=" . $count_par_pare);
            $platforms = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить объявления. Обратитесь пожалуйста к администратору.')]);
        }

        return $platforms;
    }

    // Получаем данные одного объявления
    public function getOneAdv($item) {
        try {
            $urlcat = $this->urlapicore . "/content/data-item";
            curl_setopt($this->ch, CURLOPT_URL, $urlcat);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, "lang=ru&platform=chicago1330&item=" . $item . "&sort_radio=1");
            $platforms = json_decode(curl_exec($this->ch));
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить объявления. Обратитесь пожалуйста к администратору.')]);
        }

        return $platforms;
    }

    // Транслитерация строк.
    public function transliterate($st) {
        $s = (string) $st; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "_", $s); // заменяем пробелы знаком минус
        return $s;
    }

    // Ищем миниатюру для объявления
    public function checkFileExist($suffix, $id, $fieldID, $userID, $urlcore) {
        $base_image = str_replace('/api', '/frontend', Yii::app()->basePath);
        $files = glob($base_image . '/www/uploads/pub_image/' . $suffix . '' . $id . '_' . $fieldID . '.{*}', GLOB_NOSORT | GLOB_BRACE);
        if (is_array($files) && count($files) == 0) {
            $files = glob($base_image . '/www/uploads/pub_image/user_' . $userID . '/' . $id . '/' . $suffix . $fieldID . '*.{*}', GLOB_NOSORT | GLOB_BRACE);
        }

        if ($files) {
            $path = explode('/', $files[0]);
            $file = array_pop($path);
            return $file;
        }
        return false;
    }

    protected function galleryImages($images) {
        $sort_type = array();
        $imgarr = array();
        foreach ($images as $image) {
            if (strpos($image, 'tmb_') !== false && strpos($image, 'croppedImg') === false) {
                $sort_type['tmb'][] = $image;
            } elseif (strpos($image, 'midl_') !== false && strpos($image, 'croppedImg') === false) {
                $sort_type['midl'][] = $image;
            } elseif (strpos($image, 'large_') !== false && strpos($image, 'croppedImg') === false) {
                $sort_type['large'][] = $image;
            }
        }

        if (isset($sort_type['tmb'])) {
            foreach ($sort_type['tmb'] as $tmb) {
                $patern = substr($tmb, 3);
                foreach ($sort_type['large'] as $large) {
                    if (strpos($large, $patern) !== false) {
                        $imgarr[$large] = $tmb;
                        break;
                    }
                }
            }
        }

        return $imgarr;
    }

    // Delete advert
    public function actionAjaxDeleterekl() {
        if (Yii::$app->userAccess->isAdmin() && Yii::$app->request->post()) {
            $advID = strip_tags(trim(Yii::$app->request->post('advID')));

            try {
                $urlcat = $this->urlapicore . "/operations/remove-ad";
                curl_setopt($this->ch, CURLOPT_URL, $urlcat);
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, "item=" . $advID);
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Bearer PciIdaRajtKsMmh_Y_3ozZdfUQ_mBBWt'));
                curl_exec($this->ch);
                return true;
            } catch (Exception $ex) {
                echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось получить объявления. Обратитесь пожалуйста к администратору.')]);
            }
        }
    }

    // Get categorias of adv
    private function getCategoriesAdv() {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_NOBODY, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        // Получаем статические переменные
        $this->urlapicore = Yii::$app->params['url_core_api'];

        $urlcat = $this->urlapicore . "/content/get-categories";
        curl_setopt($this->ch, CURLOPT_URL, $urlcat);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, "lang=ru&platform=chicago1330");
        $categories = json_decode(curl_exec($this->ch));

        if (isset($categories)) {
            $catarr = (array) $categories;
        } else {
            $catarr = null;
        }

        // Закрываем соединение
        curl_close($this->ch);

        return $catarr;
    }

}
