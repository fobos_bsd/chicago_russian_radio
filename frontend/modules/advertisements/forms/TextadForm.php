<?php

namespace frontend\modules\advertisements\forms;

use Yii,
    yii\helpers\Json,
    yii\base\Model,
    yii\web\UploadedFile;

/**
 * Description of TextadForm
 *
 * @author fobos
 */
class TextadForm extends Model {

    public $title, $content, $platforms, $services, $weeks, $category, $contact_name, $contact_email, $phone, $address, $name, $email, $reg_date, $rag_ip, $imageFile, $totalsumm, $prices, $source;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['content', 'platforms', 'weeks', 'email'], 'required'],
            ['content', 'string', 'max' => 5000],
            ['category', 'integer', 'max' => 16777215],
            [['platforms'], 'each', 'rule' => ['integer', 'max' => 4294967295]],
            [['services'], 'each', 'rule' => ['each', 'rule' => ['integer', 'max' => 65535]]],
            [['contact_name', 'name'], 'string', 'max' => 255],
            ['address', 'string', 'max' => 500],
            [['contact_email', 'email'], 'email'],
            [['phone', 'source'], 'string', 'max' => 255],
            ['weeks', 'each', 'rule' => ['each', 'rule' => ['integer', 'max' => 4]]],
            ['reg_date', 'date', 'format' => 'php:Y-m-d H:i:s'],
            ['rag_ip', 'ip'],
            ['title', 'string', 'max' => 255],
            ['totalsumm', 'number', 'numberPattern' => '/^\d{1,5}(\.\d{0,3})?$/'],
            ['prices', 'each', 'rule' => ['each', 'rule' => ['string', 'max' => 8]]],
            ['imageFile', 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, gif, png', 'maxSize' => 1024 * 8192, 'on' => 'create'],
            ['imageFile', 'safe', 'on' => 'update'],
            ['totalsumm', 'default', 'value' => 0.00],
            [['content', 'title', 'contact_name', 'name', 'address', 'phone', 'source'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'content' => Yii::t('frontend', 'Текст объявления'),
            'title' => Yii::t('frontend', 'Заголовк объявления'),
            'platforms' => Yii::t('frontend', 'Площадка'),
            'services' => Yii::t('frontend', 'Дополнительные сервисы'),
            'weeks' => Yii::t('frontend', 'Кол-во недель'),
            'category' => Yii::t('frontend', 'Категория объявления'),
            'contact_name' => Yii::t('frontend', 'Контактные данные'),
            'contact_email' => Yii::t('frontend', 'Контактный email'),
            'phone' => Yii::t('frontend', 'Контактный телефон'),
            'address' => Yii::t('frontend', 'Адрес'),
            'name' => Yii::t('frontend', 'ФИО'),
            'email' => Yii::t('frontend', 'Email'),
            'reg_date' => Yii::t('frontend', 'Дата регистрации'),
            'rag_ip' => Yii::t('frontend', 'Регистрационный IP'),
            'imageFile' => Yii::t('frontend', 'Изображение'),
            'totalsumm' => Yii::t('frontend', 'Общая сумма'),
            'prices' => Yii::t('frontend', 'Сумма за площадку'),
            'source' => Yii::t('frontend', 'Источник')
        ];
    }

    /**
     * @param $email
     * @return bool
     */
    public function sendEmail($email) {
        // Send mail
        Yii::$app->mailer->htmlLayout = '@frontend/web/mail/layouts/html';
        Yii::$app->mailer->compose('@frontend/web/mail/advertice/callback', [
                    'email' => $this->email,
                    'name' => $this->name,
                    'message' => $this->comment,
                    'select_size' => $this->select_size])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo(Yii::$app->params['supportEmail'])
                ->setSubject("Запрос на размещение модульной рекламы с сайта 7 Дней")
                ->send();

        return true;
    }

    /**
     * @return bool
     */
    public function save() {
        if ($this->validate()) {
            $data = [];

            // Save image if exist
            if (isset($this->imageFile)) {
                $upload_dir = Yii::getAlias('@frontend') . '/web/uploads/advfiles/';
                $upload_file = time() . '_' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
                $data['imageFile'] = $upload_file;
                $this->imageFile->saveAs($upload_dir . $upload_file);
            }

            // Make body request for core
            $reqbody = 'email=' . $this->email . '&reg_date=' . $this->reg_date . '&rag_ip=' . $this->rag_ip . '&total_sum=' . $this->totalsumm . '&';

            if (isset($this->title) && !empty($this->title)) {
                $data['title'] = $this->title;
            }
            $data['content'] = $this->content;
            $data['category'] = $this->category;
            $data['source'] = $this->source;
            if (isset($this->platforms) && is_array($this->platforms)) {
                foreach ($this->platforms as $platform) {
                    $data['platform'][$platform] = [];
                    if (isset($this->weeks[$platform]) && is_array($this->weeks[$platform])) {
                        $data['platform'][$platform]['weeks'] = $this->weeks[$platform];
                    }
                    if (isset($this->services[$platform]) && is_array($this->services[$platform])) {
                        $data['platform'][$platform]['services'] = $this->services[$platform];
                    }
                    if (isset($this->prices[$platform]) && is_array($this->prices[$platform])) {
                        $data['platform'][$platform]['price'] = $this->prices[$platform];
                    }
                }
            }
            if (isset($this->contact_name) && !empty($this->contact_name)) {
                $data['contact_name'] = $this->contact_name;
            }
            if (isset($this->contact_email) && !empty($this->contact_email)) {
                $data['contact_email'] = $this->contact_email;
            }
            if (isset($this->phone) && !empty($this->phone)) {
                $data['phone'] = $this->phone;
            }
            if (isset($this->address) && !empty($this->address)) {
                $data['address'] = $this->address;
            }

            $string_data = base64_encode(Json::encode($data));
            $reqbody .= 'data=' . $string_data;

            $result = $this->addToCore($reqbody);
            if ($result->status === 'success') {
                return $result;
            } else {
                return false;
            }

            return true;
        } else {
            var_dump($this->getErrors());
            die();
        }
        return false;
    }

    // Add content to Core
    private function addToCore($data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Bearer PciIdaRajtKsMmh_Y_3ozZdfUQ_mBBWt'));

        try {
            $urlcat = Yii::$app->authAdvertisement->getCoreApiUrl() . "/operations/create";
            curl_setopt($ch, CURLOPT_URL, $urlcat);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = json_decode(curl_exec($ch));
            return $result;
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось добавить объявление. Обратитесь пожалуйста к администратору.')]);
        }
    }

}
