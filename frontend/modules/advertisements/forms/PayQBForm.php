<?php

namespace frontend\modules\advertisements\forms;

use Yii,
    yii\helpers\Json,
    yii\base\Model;

/**
 * Description of PayQBForm
 *
 * @author fobos
 */
class PayQBForm extends Model {
    /*
     * @var cc_number
     * credit card nubber
     */

    public $cc_number;

    /*
     * @var expmnth
     * month of end expires the cedit card
     */
    public $expmnth;

    /*
     * @var expyr
     * year of end expires the cedit card
     */
    public $expyr;

    /*
     * @var expmnth
     * csv code of the cedit card
     */
    public $csc;

    /*
     * @var expmnth
     * ardess of payer
     */
    public $baddress;

    /*
     * @var expmnth
     * post code of the payer
     */
    public $bzip;

    /*
     * @var expmnth
     * firstname of the payer
     */
    public $firstname;

    /*
     * @var expmnth
     * lastname of the payer
     */
    public $lastname;

    /*
     * @var expmnth
     * company of the payer
     */
    public $company;

    /*
     * @var expmnth
     * state of the payer
     */
    public $bstate;

    /*
     * @var expmnth
     * amount
     */
    public $amount;

    /*
     * @var expmnth
     * invoice
     */
    public $invoice;

    /**
     * @inheritdoc
     */
    public function rules() {
        $min_year = (int) date('Y');
        $max_year = $min_year + 40;

        return [
            [['cc_number', 'expmnth', 'expyr', 'csc', 'baddress', 'bzip', 'firstname', 'lastname', 'amount', 'invoice'], 'required'],
            ['cc_number', \yii2mod\validators\ECCValidator::className()],
            ['expmnth', 'integer', 'min' => 1, 'max' => 12],
            ['invoice', 'integer'],
            ['expyr', 'integer', 'min' => $min_year, 'max' => $max_year],
            ['csc', 'string', 'length' => [3, 6]],
            ['baddress', 'string', 'length' => [1, 500]],
            ['bzip', 'string', 'length' => [1, 10]],
            [['firstname', 'lastname'], 'string', 'length' => [1, 255]],
            ['amount', 'number', 'numberPattern' => '/^\d{1,5}(\.\d{0,3})?$/'],
            [['company', 'bstate'], 'string', 'max' => 255],
            [['baddress', 'bzip', 'firstname', 'lastname', 'company', 'bstate'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'cc_number' => Yii::t('frontend', 'Номер кредитной карты'),
            'expmnth' => Yii::t('frontend', 'Месяц окончания действия карты, 2 цифры'),
            'expyr' => Yii::t('frontend', 'Год окончания действия карты, 4 цифры'),
            'csc' => Yii::t('frontend', 'CVC код'),
            'baddress' => Yii::t('frontend', 'Адрес плательщика'),
            'bzip' => Yii::t('frontend', 'Почтовый индекс'),
            'firstname' => Yii::t('frontend', 'Имя'),
            'lastname' => Yii::t('frontend', 'Фамилия'),
            'amount' => Yii::t('frontend', 'Сумма'),
            'company' => Yii::t('frontend', 'Компания'),
            'bstate' => Yii::t('frontend', 'Штат'),
            'invoice' => Yii::t('frontend', 'Номер счёта'),
        ];
    }

    /**
     * @return bool
     */
    public function pay() {
        $arr_data = [
            'cc_number' => $this->cc_number,
            'expmnth' => $this->expmnth,
            'expyr' => $this->expyr,
            'csc' => $this->csc,
            'baddress' => $this->baddress,
            'bzip' => $this->bzip,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'company' => $this->company,
            'bstate' => $this->bstate,
            'amount' => $this->amount,
            'invoice' => $this->invoice
        ];
        $data = 'data=' . Json::encode($arr_data);

        $result = $this->sendToCore($data);
        return $result;
    }

    // Add content to Core
    private function sendToCore($data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Bearer PciIdaRajtKsMmh_Y_3ozZdfUQ_mBBWt'));

        try {
            $url = Yii::$app->authAdvertisement->getCoreApiUrl() . "/onlinepay/qb-pay";
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            return $result;
        } catch (Exception $ex) {
            echo Yii::$app->runAction('/chastnye-obyavleniya/management/exeption', ['err_mess' => Yii::t('frontend', 'Не удалось оплатить объявление. Обратитесь пожалуйста к администратору.')]);
        }
    }

}
