<?php

namespace frontend\modules\advertisements;

/**
 * advertisements module definition class
 */
class Module extends \yii\base\Module {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\advertisements\controllers';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        //$this->params['coresitekey'] = 'b5b50167e96a9e7d6ed309daa2df98e3dbf57197'; // Ключ для доступа к core
    }

}
