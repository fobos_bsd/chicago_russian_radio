<?php

namespace frontend\modules\advertisements\models;

use Yii,
    yii\base\Model;

class FilterForm extends Model {

    public $search_text;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['search_text'], 'filter', 'filter' => function($value) {
            return trim(htmlentities(strip_tags($value), ENT_QUOTES, 'UTF-8'));
        }],
            [['search_text'], 'required'],
            [['search_text'], 'string', 'max' => '255']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'search_text' => Yii::t('frontend', 'Текст содержит')
        ];
    }

}
