<?php

use backend\widgets\Breadcrumbs;
use frontend\assets\AdvAsset;

$this->title = Yii::t('common', 'Подача объявления (авторизация)');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->params['homepage'], 'url' => '/'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Подача объявления')];

AdvAsset::register($this);
?>

<?=
Breadcrumbs::widget([
    'links' => $this->params['breadcrumbs'],
]);
?>

<div class="col-md-12 nopadding">
    <h3 style="color: #039be5;"><?= $this->title ?></h3>
    <div class="clearfix" style="height: 3px; border-bottom: 1px solid #f5f5f5; margin-bottom: 15px;"></div>
</div>

<div class="col-md-12 advertisements nopadding">
    <p class="text-center bg-info" style="color: #08669c; padding-top: 20px; padding-bottom: 20px;">
        Для того чтобы иметь возможность подавать объявления,<br/> пожалуйста, авторизируйтесь <br/><br/>
        <a class="btn btn-info waves-effect waves-light btn-flat" data-toggle="modal" data-target="#sing-in"><i class="fa fa-key"></i> Вход</a><br/><br/>
        &nbsp;
        Или зарегистрируйтесь<br/><br/>
        <a class="btn btn-primary waves-effect waves-light btn-flat" data-toggle="modal" data-target="#registrate"><i class="fa fa-pencil"></i> Регистрация</a>
    </p>
</div>