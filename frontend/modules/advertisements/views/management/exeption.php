<?php

use frontend\assets\AdvAsset;

$this->title = Yii::t('common', 'Извините! Произошла ошибка.');

AdvAsset::register($this);
?>
<div class="col-md-9">
    <ol class="breadcrumb advbread">
        <li><a href="/"><?= Yii::$app->params['homepage'] ?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
    <div class="articles advertisements container">
        <p>
            <?= $errordata ?>
        </p>
    </div>
</div>

