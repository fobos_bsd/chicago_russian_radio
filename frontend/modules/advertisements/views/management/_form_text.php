<?php

use frontend\assets\CroppicAsset;
use frontend\assets\PoshytipAsset;
use yii\bootstrap\Html;

CroppicAsset::register($this);
PoshytipAsset::register($this);
?>
<?= Html::beginForm([''], 'post', ['id' => 'addadvertise', 'class' => 'form', 'enctype' => 'multipart/form-data']) ?>
<div class="row classy-body">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="classyText">
                    <h3 class="text-primary"><?= Yii::t('frontend', 'Текст объявления') ?>: <sup><a href="javascript:;" data-toggle="tooltip" title="<?= Yii::t('frontend', 'Текст объявления которое будет размещенно на страницах сайта и (или) печатного издания') ?>"><span class="glyphicon glyphicon-question-sign text-muted"></span></a></sup></h3>
                </label>
                <?= Html::activeInput('text', $modadvform, 'title', ['class' => "form-control", 'placeholder' => Yii::t('frontend', 'Заголовк объявления')]) ?>
                <?= Html::error($modadvform, 'title') ?>
                <?= Html::activeTextarea($modadvform, 'content', ['class' => "form-control", 'rows' => "5"]) ?>
                <?= Html::error($modadvform, 'content') ?>
            </div>
            <div class="form-group">
                <label class="text-muted col-sm-7"><?= Yii::t('frontend', 'Пожалуйста, введите текст') ?> <img src="/vendor/img/kbrd.jpg"></label>
                <label class="input-group col-sm-5">
                    <input type="text" style="background-color:#2977c1; color: #ffffff; max-width: 50px; height: 22px; border-radius: 5px;" class="form-control text-center pull-right" id="InputAmount" value="0" disabled>
                    <span class="pull-right" style="line-height: 22px;"><?= Yii::t('frontend', 'Количество слов') ?>: &nbsp;&nbsp;&nbsp;</span>
                </label>
            </div>
            <div class="form-group">
                <?= Html::activeDropDownList($modadvform, 'category', $categories, ['class' => "form-control", 'value' => 'none', 'prompt' => Yii::t('frontend', 'Выберите категорию')]) ?>
            </div>
            <div class="form-group">
                <?= Html::activeInput('file', $modadvform, 'imageFile', ['id' => 'input-file-max-fs', 'class' => "dropify", 'data-max-file-size' => '2M', 'data-height' => '80', 'placeholder' => Yii::t('frontend', 'Добавить изображение')]) ?>
                <?= Html::error($modadvform, 'imageFile') ?>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="classyFIO">
                    <h3 class="text-primary"><?= Yii::t('frontend', 'Контактные данные') ?> (<small class="text-primary"><?= Yii::t('frontend', 'отображаемые в тексте объявления') ?></small>):</h3>
                </label>
                <?= Html::activeTextInput($modadvform, 'contact_name', ['class' => "form-control", 'placeholder' => Yii::t('frontend', 'ФИО контактного лица')]) ?>
                <?= Html::error($modadvform, 'contact_name') ?>
            </div>
            <div class="form-group">
                <?= Html::activeTextInput($modadvform, 'contact_email', ['class' => "form-control", 'placeholder' => Yii::t('frontend', 'Email контактного лица')]) ?>
                <?= Html::error($modadvform, 'contact_email') ?>
            </div>
            <div class="form-group">
                <?= Html::activeTextInput($modadvform, 'phone', ['class' => "form-control", 'placeholder' => Yii::t('frontend', 'Контактный телефон')]) ?>
                <?= Html::error($modadvform, 'phone') ?>
            </div>
            <div class="form-group">
                <?= Html::activeTextarea($modadvform, 'address', ['rows' => 5, 'style' => 'width: 100%;', 'placeholder' => Yii::t('frontend', 'Адрес')]) ?>
                <?= Html::error($modadvform, 'address') ?>
            </div>
        </div>
        <hr>
    </div>
    <hr>
    <?php
    if (isset($platforms) && is_array($platforms)) {
        foreach ($platforms as $platform) :
            ?>
            <div class="row platforn">
                <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="TextadForm[platforms][]" class="chbox_platform" value="<?= $platform['platform']->id ?>" <?php if (strpos($platform['platform']->name, '7days.us') !== false) { ?>checked<?php } ?>>  <strong><?= $platform['platform']->title ?></strong>
                        </label><br />
                        <label class="checkbox-inline">
                            <?= Yii::t('frontend', 'Кол-во недель') ?>&nbsp; <?= Html::activeInput('number', $modadvform, 'weeks[' . $platform['platform']->id . '][]', ['class' => 'count_weeks', 'style' => "width: 55px;", 'value' => 1, 'min' => "1", 'max' => "4"]) ?>
                            <?= Html::error($modadvform, 'weeks[]') ?>
                        </label>
                        <input type="hidden" class="input_price" name="TextadForm[prices][<?= $platform['platform']->id ?>][]" value="0.00">
                        <span class="price_ad" style="display: none;"><?= $platform['platform']->price ?></span>
                        <?php if (isset($platform['services']) && is_array($platform['services'])) { ?>
                            <div class="classy-featured pl-3">
                                <h5><?= Yii::t('frontend', 'Дополнительные сервисы') ?>:</h5>
                                <p>(<?= Yii::t('frontend', 'цены указаны ') . strtolower($platform['platform']->price_description) ?>)</p>
                                <div class="col-sm-12">
                                    <?php
                                    foreach ($platform['services'] as $service) :
                                        if ($service->type === 'checkbox') {
                                            ?>
                                            <div class="form-group">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" class="platform_service" name="TextadForm[services][<?= $platform['platform']->id ?>][]" value="<?= $service->id ?>" data-price="<?= $service->price ?>" data-platform="<?= $platform['platform']->id ?>"><strong><?= $service->title ?></strong>. <?php if ($service->title !== 'Не переводить (сохранить оригинальный текст)') { ?><?= Yii::t('frontend', 'Цена за 1 неделю') ?>: <strong>$<?= $service->price ?></strong><?php } ?>
                                                </label>
                                            </div>
                                            <?php
                                        }
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="form-group classy-details hidden">
                        <p><?= Yii::t('frontend', 'Размещение в разделе') ?>: <strong><?= $platform['platform']->title ?></strong></p>
                        <p><?= Yii::t('frontend', 'Количество недель размещения') ?>: <strong class="countweeks_info">1</strong></p>
                        <p><?= Yii::t('frontend', 'Цена за слово') ?>: <strong>$<?= $platform['platform']->price ?></strong></p>
                        <?php if (isset($platform['services']) && is_array($platform['services'])) { ?>
                            <p><br><?= Yii::t('frontend', 'Дополнительные сервисы') ?>: <strong></strong></p>
                            <?php
                            foreach ($platform['services'] as $service) :
                                if ($service->type === 'checkbox') {
                                    ?>
                                    <p id="service_info_<?= $platform['platform']->id . '_' . $service->id ?>" style="display: none;"><?= $service->title ?>. <?php if ($service->title !== 'Не переводить (сохранить оригинальный текст)') { ?><?= Yii::t('frontend', 'Цена за 1 неделю') ?>: <strong>$<?= $service->price ?></strong><?php } ?></p>
                                    <?php
                                }
                            endforeach;
                            ?>
                        <?php } ?>
                        <p><br><?= Yii::t('frontend', 'Общая цена размещения за все недели') ?>: <strong>$<span class="price_place">0.00</span></strong></p>
                    </div>
                </div> 
            </div>
            <hr>
            <?php
        endforeach;
    }
    ?>
    <!--
<div class="row">
<div class="col-lg-6 col-md-6">
    <div class="form-group text-right">
        <a href="#" id="classy-featured" class="btn btn-primary"><?= Yii::t('frontend', 'Улучшить вид') ?></a>
    </div>
</div>
<div class="col-lg-6 col-md-6">

</div>
</div>
    -->
    <?= Html::activeHiddenInput($modadvform, 'name') ?>
    <?= Html::activeHiddenInput($modadvform, 'email') ?>
    <?= Html::activeHiddenInput($modadvform, 'reg_date') ?>
    <?= Html::activeHiddenInput($modadvform, 'rag_ip') ?>
    <?= Html::activeHiddenInput($modadvform, 'totalsumm') ?>
</div>
<div class="row classy-foot">
    <div class="col-lg-4">
        <label class="input-group">
            <h5 class="col-sm-8"><?= Yii::t('common', 'Общее количество слов в объявлении') ?>: &nbsp;&nbsp;&nbsp;</h5>
            <input type="text" style="background-color:#2977c1; color: #ffffff; max-width: 60px; height: 30px; border-radius: 5px;" class="form-control text-center col-sm-4" id="InputAmountTotal" value="0" disabled>
        </label>
    </div>
    <div class="col-lg-4 text-center">
        <button type="submit" class="btn btn-primary btn-lg"><?= Yii::t('common', 'Подать') ?></button>
    </div>
    <div class="col-lg-offset-1 col-lg-3">
        <label class="classy-total-amount pull-right" style="width: 100%;">
            <h5 class="pull-left" style="width: auto;"><?= Yii::t('common', 'Общая сумма') ?><br> <?= Yii::t('common', 'к оплате') ?>:</h5>
            <p class="pull-right">$<span id="totalprice">0.00</span></p>
        </label>
    </div>
</div>

<?= Html::endForm() ?>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).scroll(function () {
            s_top = $("html").scrollTop();
            yes = $("#bussinesnav").offset().top;
            console.log(s_top);
            console.log(yes);
            if (s_top > yes) {
                $("#bussinesnav").addClass('active');
            }
            ;
            if (s_top < 400) {
                $("#bussinesnav").removeClass('active');
            }
            ;
        });

        // Translated
        $('.dropify').dropify({
            messages: {
                default: 'Выберите изображение',
                replace: 'Заменить',
                remove: 'Удалить',
                error: 'Недопустимое значение'
            }
        });

        $(".scrolltonext").on("click", function (event) {
            event.preventDefault();
            var id = $(this).attr('href'),
                    top = $(id).offset().top;
            $('body,html').animate({scrollTop: top - 120}, 1000);
        });

        $('#classy-featured').on("click", function () {
            if ($('.classy-featured').hasClass('active')) {
                $('.classy-featured').removeClass('active');
            } else {
                $('.classy-featured').addClass('active');
            }

        });
        $('.week-count').on("click", function () {
            $('.classy-featured').addClass('active');
        });

        $("#textadform-content").on('change', function () {
            checkPlatforms();
            checkOnePlatforms();
        });

        $(".chbox_platform").on('change', function () {
            checkPlatforms();
            checkOnePlatforms();
            showDescription();
        });

        $(".count_weeks").on('change', function () {
            checkPlatforms();
            checkOnePlatforms();
        });

        $(".platform_service").on('change', function () {
            checkPlatforms();
            checkOnePlatforms();
        });

        checkPlatforms();
        checkOnePlatforms();
        showDescription();
    });

    function strip_tags(str) {
        return str.replace(/<\/?[^>]+>/gi, '');
    }

    function checkPlatforms() {
        countWords();
        $("#totalprice").text('0.00');
        $(".platforn").each(function () {
            if ($(this).find(".chbox_platform").prop("checked") === true) {
                var platform_price = parseFloat($(this).find('.price_ad').text());
                platform_weeks = parseInt($(this).find('.count_weeks').val());

                if (platform_price > 0) {
                    var cnt_words = countWords();
                    totalsumm(cnt_words * platform_price * platform_weeks);
                }

                $(this).closest('.platforn').find(".platform_service").each(function () {
                    if ($(this).prop("checked") === true) {
                        var serv_price = parseFloat($(this).data('price'));
                        var price = serv_price * platform_weeks;
                        totalsumm(price);
                    }
                });
            }
        });
    }

    function checkOnePlatforms() {
        countWords();
        $(".platforn").each(function () {
            if ($(this).find(".chbox_platform").prop("checked") === true) {
                var platform_price = parseFloat($(this).find('.price_ad').text());
                one_platform_weeks = parseInt($(this).find('.count_weeks').val());
                $(this).find('.countweeks_info').empty().text(one_platform_weeks);

                $(this).find('.price_place').text('0.00');

                if (platform_price > 0) {
                    var cnt_words = countWords();
                    var place_sum = cnt_words * platform_price * one_platform_weeks;

                    $(this).find('.price_place').text(place_sum);
                    $(this).find('.input_price').val(place_sum);
                }

                $(this).find(".platform_service").each(function () {
                    var serv_info_id = '#service_info_' + $(this).data('platform') + '_' + $(this).val();
                    if ($(this).prop("checked") === true) {
                        var serv_price = parseFloat($(this).data('price'));
                        var price = serv_price * one_platform_weeks;
                        var curent_summ = parseFloat($(this).closest('.platforn').find('.price_place').text());
                        var new_summ = curent_summ + price;
                        $(this).closest('.platforn').find('.price_place').text(new_summ);
                        $(this).closest('.platforn').find('.input_price').val(new_summ);

                        $(serv_info_id).show(100);
                    } else {
                        $(serv_info_id).hide(10);
                    }
                });
            }
        });
    }

    function countWords() {
        var countwc = 0;
        var text_adv = $.trim(strip_tags($("#textadform-content").val()));

        var wc = text_adv.replace(/\s\s+/gi, " ");
        if (wc === '') {
            countwc = 0;
        } else {
            var arrwc = wc.split(" ");
            countwc = parseInt(arrwc.length);
        }
        $("#InputAmount").val(countwc);
        $("#InputAmountTotal").val(countwc);

        return countwc;
    }

    function totalsumm(summ) {
        var curent_summ = parseFloat($("#totalprice").text());
        var new_summ = curent_summ + summ;
        $("#totalprice").text(new_summ);
        $("#textadform-totalsumm").val(new_summ);
    }

    function showDescription() {
        $(".platforn").each(function () {
            if ($(this).find("input.chbox_platform").prop("checked") === true) {
                $(this).find(".classy-details").removeClass('hidden');
                $(this).find('.classy-featured').addClass('active');
            } else {
                $(this).find(".classy-details").removeClass('hidden');
                $(this).find(".classy-details").addClass('hidden');
                $(this).find('.classy-featured').removeClass('active');
            }
        });
    }

</script>