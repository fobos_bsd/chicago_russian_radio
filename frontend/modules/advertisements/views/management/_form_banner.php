<?php

use frontend\assets\AdvAsset;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Html;

AdvAsset::register($this);
?>

<?php $form = ActiveForm::begin(['id' => 'advmodal-form', 'method' => 'post', 'action' => '/chastnye-obyavleniya/management/ajax-reklam']); ?>
<div class="row classy-body">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?= $form->field($modadvform, 'name')->textInput() ?>

            <?= $form->field($modadvform, 'phone')->textInput(['placeholder' => '79781243587']) ?>

            <?= $form->field($modadvform, 'email')->textInput(['placeholder' => 'your@mail.net']) ?>

            <?= $form->field($modadvform, 'comment')->textarea(['rows' => 5]) ?>

            <?=
            $form->field($modadvform, 'select_size')->widget(Select2::classname(), [
                'data' => [
                    'Full Page' => '32 Unit  Full Page',
                    '16 units H' => '16 Units H  1/2 Page H',
                    '16 units V' => '16 Units V  1/2 Page V',
                    '8 units' => '8 Units  1/4 Page',
                    '4 units' => '4 Units  1/8 Page V or H',
                    '2 units' => '2 Units  1/16 Page',
                    '1 units' => '1 Units  1/32 Page',
                ],
                'options' => ['placeholder' => 'Выберите размер изображения на странице ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('frontend', 'Отправить заявку'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<div class="col-sm-12 nopadding margin-top-20">
    <span class="" style="">Чтобы ознакомится с ценами на модульную рекламу, перейдите пожалуйста по ссылке <a href="http://ethnicmedia.us/#price" target="_blanck" class="" style="color: #0277bd;">ethnicmedia.us</a></span>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("form#advmodal-form").on('change', function () {
            $(this).valid();
        });

        $("form#advmodal-form").validate({
            debug: true,
            rules: {
                'ModuleAdvForm[name]': {
                    required: false,
                    minlength: 2,
                    maxlength: 1500
                },
                'ModuleAdvForm[phone]': {
                    required: false,
                    minlength: 2,
                    maxlength: 150
                },
                'ModuleAdvForm[email]': {
                    required: false,
                    email: true
                }
            },
            messages: {
                'ModuleAdvForm[name]': {
                    minlength: "Длинна данных не менее 2 символов",
                    maxlength: "Длинна данных не более 255 символов"
                },
                'ModuleAdvForm[phone]': {
                    required: "Пожалуйста, введите номер телефона",
                    minlength: "Длинна данных не менее 10 символов",
                    maxlength: "Длинна данных не более 15символов",
                    //number: "Допускаются только цифры"
                },
                'ModuleAdvForm[email]': {
                    email: "Не правильный формат email адреса"
                }
            },
            submitHandler: function (form) {
                $(form).on('submit', function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    event.stopImmediatePropagation();
                    var action = $(form).attr("action");
                    var formcontent = $(form).html();
                    $.ajax({
                        dataType: "json",
                        url: action,
                        method: 'POST',
                        data: $(form).serialize(),
                        beforeSend: function () {
                            $(form).empty().html("<p style='color: red; text-align: center; font-size: 1.5em;'>Ваш запрос отправляется!</p>");
                        },
                        success: function (response) {
                            if (response.status === '1' || response.status === 1) {
                                $(form).empty().html("<p style='color: green; text-align: center; font-size: 1.5em;'>Спасибо! Ваш запрос отправлен.</p>");
                            } else {
                                $(form).empty().html(formcontent);
                            }
                        },
                        error: function () {
                            $(form).empty().html(formcontent);
                        }
                    });
                });

                return false;
            }
        });
    });
</script>