<?php

use backend\widgets\Breadcrumbs;
use frontend\assets\AdvAsset;
use yii\bootstrap\Html,
    yii\widgets\ActiveForm;

$this->title = Yii::t('common', 'Оплата размещения объявления');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->params['homepage'], 'url' => '/'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Объявления'), 'url' => '/chastnye-obyavleniya'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];

AdvAsset::register($this);
?>

<?=
Breadcrumbs::widget([
    'links' => $this->params['breadcrumbs'],
]);
?>
<div class="col-md-12 nopadding">
    <h3 style="color: #039be5;"><?= $this->title ?></h3>
    <div class="clearfix" style="height: 3px; border-bottom: 1px solid #ccc; margin-bottom: 15px;"></div>
</div>

<div class="col-md-12 advertisements nopadding">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <?= $form->field($model_pay, 'cc_number')->textInput(['placeholder' => '11112222333344445555']) ?>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <?= $form->field($model_pay, 'expmnth')->input('number', ['placeholder' => 11, 'min' => 1, 'max' => 12])->label(Yii::t('frontend', 'Действует до, месяц')) ?>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <?= $form->field($model_pay, 'expyr')->input('number', ['placeholder' => date('Y'), 'min' => date('Y')])->label(Yii::t('frontend', 'Действует до, год')) ?>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <?= $form->field($model_pay, 'csc')->textInput(['placeholder' => '123', 'maxlength' => true]) ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?= $form->field($model_pay, 'firstname')->textInput(['placeholder' => Yii::t('frontend', 'Имя'), 'maxlength' => true]) ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?= $form->field($model_pay, 'lastname')->textInput(['placeholder' => Yii::t('frontend', 'Фамилия'), 'maxlength' => true]) ?>
    </div>

    <?= $form->field($model_pay, 'amount')->hiddenInput()->label(false) ?>
    <?= $form->field($model_pay, 'baddress')->hiddenInput()->label(false) ?>
    <?= $form->field($model_pay, 'bzip')->hiddenInput()->label(false) ?>
    <?= $form->field($model_pay, 'company')->hiddenInput()->label(false) ?>
    <?= $form->field($model_pay, 'bstate')->hiddenInput()->label(false) ?>
    <?= $form->field($model_pay, 'invoice')->hiddenInput()->label(false) ?>

    <div class="col-lg-offset-6 col-md-offset-6 col-sm-offset-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="form-group text-right">
            <?= Html::submitButton('<i class="fa fa-money"></i> ' . Yii::t('frontend', 'Оплатить'), ['class' => 'btn btn-success', 'data-toggle' => "modal", 'data-target' => "#watepaysend"]) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
</div>

<div id="watepaysend" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Отправка данных оплаты</h4>
            </div>
            <div class="modal-body">
                <p style="text-align: center;">
                    <i class="fa fa-spinner" aria-hidden="true"></i>
                </p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("form#payadvform").on('change', function () {
            $(this).valid();
        });
        $("form#payadvform").on('submit', function (event) {
            event.preventDefault();
            isVal = $(this).valid();
            if (isVal === true) {
                $("#watepaysend").modal('show');
                $.post('/chastnye-obyavleniya/management/pay', $(this).serialize(), function (data) {});
                //$("form#payadvform").submit();
            }
        });

        $("form#payadvform").validate({
            rules: {
                'AutorizeForm[CC]': {
                    required: true,
                    creditcard: true
                },
                'AutorizeForm[EXPMNTH]': {
                    required: true,
                    number: true,
                    min: 1,
                    max: 12
                },
                'AutorizeForm[EXPYR]': {
                    required: true,
                    number: true,
                    min: 2018,
                    max: 2100
                },
                'AutorizeForm[CSC]': {
                    required: true,
                    number: true,
                    minlength: 3,
                    min: 1,
                    max: 999
                },
                'AutorizeForm[BADDRESS]': {
                    required: true,
                    minlength: 1,
                    maxlength: 255
                },
                'AutorizeForm[BZIP]': {
                    number: true,
                    minlength: 3,
                    maxlength: 8,
                    min: 1,
                    max: 99999999
                },
                'AutorizeForm[FIRSTNAME]': {
                    required: true,
                    minlength: 1,
                    maxlength: 255
                },
                'AutorizeForm[LASTNAME]': {
                    required: true,
                    minlength: 1,
                    maxlength: 255
                },
                'AutorizeForm[COMPANY]': {
                    required: false,
                    minlength: 1,
                    maxlength: 255
                },
                'AutorizeForm[BSTATE]': {
                    required: false,
                    minlength: 1,
                    maxlength: 255
                },
            },
            messages: {
                'AutorizeForm[CC]': {
                    required: "Введите номер кредитной карты",
                    creditcard: "Не правильный номер карты",
                },
                'AutorizeForm[EXPMNTH]': {
                    required: "Укажите номер месяца",
                    number: "Допускаются только цифры",
                    min: "Число должно быть не меньше 1",
                    max: "Число должно быть не больше 12"
                },
                'AutorizeForm[EXPYR]': {
                    required: "Укажите год",
                    number: "Допускаются только цифры",
                    min: "Число должно быть не меньше 2018",
                    max: "Число должно быть не больше 2100"
                },
                'AutorizeForm[CSC]': {
                    required: "Укажите csv код",
                    number: "Допускаются только цифры",
                    minlength: "Длинна данных не менее 3 символов",
                    min: "Число должно быть не меньше 001",
                    max: "Число должно быть не больше 999"
                },
                'AutorizeForm[BADDRESS]': {
                    required: "Укажите город или населённый пункт проживания",
                    minlength: "Длинна данных не менее 1 символа",
                    maxlength: "Длинна данных не более 255 символов"
                },
                'AutorizeForm[BZIP]': {
                    number: "Допускаются только цифры",
                    minlength: "Длинна данных не менее 3 символов",
                    maxlength: "Длинна данных не более 8 символов",
                    min: "Число должно быть не меньше 1",
                    max: "Число должно быть не больше 99999999"
                },
                'AutorizeForm[FIRSTNAME]': {
                    required: "Укажите ваше имя",
                    minlength: "Длинна данных не менее 1 символа",
                    maxlength: "Длинна данных не более 255 символов"
                },
                'AutorizeForm[LASTNAME]': {
                    required: "Укажите вашу фамилию",
                    minlength: "Длинна данных не менее 3 символа",
                    maxlength: "Длинна данных не более 255 символов"
                },
                'AutorizeForm[COMPANY]': {
                    //required: "Укажите название фирмы",
                    minlength: "Длинна данных не менее 3 символа",
                    maxlength: "Длинна данных не более 255 символов"
                },
                'AutorizeForm[BSTATE]': {
                    //required: "Укажите адрес",
                    minlength: "Длинна данных не менее 3 символа",
                    maxlength: "Длинна данных не более 255 символов"
                },
                submitHandler: function (form) {
                    isValid = true;
                    return isVaid;
                },
            }
        });
    });
</script>



