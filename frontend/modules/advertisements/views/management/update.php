<?php


use frontend\assets\AdvAsset;

$this->title = Yii::t('common', 'Редактирование объявления');

AdvAsset::register($this);
?>
<div class="col-md-9">
    <ol class="breadcrumb advbread">
        <li><a href="/"><?= Yii::$app->params['homepage'] ?></a></li>
        <li class="active"><?= Yii::t('common', 'Редактирование объявления') ?></li>
    </ol>
    <div class="articles advertisements container">
        <?= $this->render('_form', ['fields' => $fields, 'arrcat' => $arrcat, 'prices' => $prices, 'servises' => $servises, 'typeadv' => $typeadv]) ?>
    </div>
</div>
