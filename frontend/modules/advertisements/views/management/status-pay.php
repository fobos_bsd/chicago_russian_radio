<?php

use backend\widgets\Breadcrumbs;
use frontend\assets\AdvAsset;

AdvAsset::register($this);

$this->title = Yii::t('common', 'Состояние оплаты');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->params['homepage'], 'url' => '/'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Объявления'), 'url' => '/chastnye-obyavleniya'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?=
Breadcrumbs::widget([
    'links' => $this->params['breadcrumbs'],
]);
?>
<div class="col-md-12 nopadding">
    <h3 style="color: #039be5;"><?= $this->title ?></h3>
    <div class="clearfix" style="height: 3px; border-bottom: 1px solid #ccc; margin-bottom: 15px;"></div>
</div>

<div class="col-md-12 advertisements nopadding">
    <?php if (isset($status) && $status === 'paid') { ?>
        <p class="text-center bg-success" style="color: #007600; padding-top: 10px; padding-bottom: 10px;">
            Благодарим Вас за размещение объявления!<br/>
            С вашего баланса было успешно списанно $<?= $totalsumm ?> в счёт оплаты за размещение
        </p>
    <?php } else { ?>
        <p class="text-center bg-danger" style="color: #880000; padding-top: 10px; padding-bottom: 10px;">
            Произошла ошибка во время оплаты объявления.<br/>
            Описание ошибки: <b><?php if (Yii::$app->session->hasFlash('success')): echo Yii::$app->session->getFlash('success');
    endif; ?>
            </b><br/>
            Пожалуйста обратитес к администратору!
        </p> 
<?php } ?>
</div>

