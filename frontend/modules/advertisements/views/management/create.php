<?php

use backend\widgets\Breadcrumbs;
use frontend\assets\AdvAsset;

AdvAsset::register($this);

switch ($typeadv) {
    case 'banner': $titadv = 'модульное';
        break;
    default: $titadv = 'текстовое';
        break;
}

$this->title = Yii::t('common', 'Подать {typeadv} объявление', ['typeadv' => $titadv]);
$this->params['breadcrumbs'][] = ['label' => Yii::$app->params['homepage'], 'url' => '/'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Объявления'), 'url' => '/chastnye-obyavleniya'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?=
Breadcrumbs::widget([
    'links' => $this->params['breadcrumbs'],
]);
?>

<div class="classy-box">
    <div class="row classy-header">
        <p id="dXjZtcVNnblT" class="text-danger"><?= Yii::t('frontend', 'Для корректной работы данной страницы, пожалуйста, отключите AdBlock и/или другие блокирующие скрипты сервисы.') ?></p>
        <div class="col-lg-6">
            <h2 class="classy-title"><span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> <?= $this->title ?></h2>
        </div>
        <div class="col-lg-6">
            <p style="padding: 5px 5px 0 0;"><?= $rules ?></p>
        </div>
    </div>

    <?php
    if (isset($typeadv) && $typeadv === 'banner') {
        /*
          echo $this->render('_form_banner', [
          'fields' => $fields,
          'templates' => $templates,
          'categories' => $categories,
          'prices' => $prices,
          'servises' => $servises,
          'uemail' => $uemail,
          'start_time' => $start_time,
          'uplimage' => $uplimage,
          'typeadv' => $typeadv,
          'startnumer' => $startnumer
          ]);
         * 
         */
        echo $this->render('_form_banner', [
            'modadvform' => $modadvform
        ]);
    } else {
        echo $this->render('_form_text', [
            'fields' => $fields,
            'categories' => $categories,
            'servises' => $servises,
            'typeadv' => $typeadv,
            'modadvform' => $modadvform,
            'platforms' => $platforms
        ]);
    }
    ?>
</div>

<?php
$script = <<< JS
if(!document.getElementById('TPxCJFVHNZKm')){
  document.getElementById('dXjZtcVNnblT').style.display='block';
}   
JS;

$this->registerJs($script, yii\web\View::POS_END);
?>

