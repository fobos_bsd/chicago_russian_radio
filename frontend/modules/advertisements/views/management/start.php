<?php

use backend\widgets\Breadcrumbs;
use frontend\assets\AdvAsset;
use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = Yii::t('common', 'Подать объявление');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->params['homepage'], 'url' => '/'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Объявления'), 'url' => '/chastnye-obyavleniya'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];

AdvAsset::register($this);
?>

<?=
Breadcrumbs::widget([
    'links' => $this->params['breadcrumbs'],
]);
?>

<div class="row">
    <div class="select_advtype col-sm-6">
        <?= Html::a(Yii::t('common', 'Модульное'), Url::to('/chastnye-obyavleniya/management/create?type=banner'), ['class' => 'btn btn-md btn-success btn-block']) ?>
        <p>
            <?= Yii::t('common', 'Модульное объявление — это графическое объявление определённого размера в виде отдельного блока, дизайн которого не нарушает стилистическое оформление общего вида полосы и в целом газеты.') ?>
        </p>
    </div>
    <div class="select_advtype col-sm-6">
        <?= Html::a(Yii::t('common', 'Текстовое'), Url::to('/chastnye-obyavleniya/management/create?type=text'), ['class' => 'btn btn-md btn-info btn-block']) ?>
        <p>
            <?= Yii::t('common', 'Текстовое объявление — это блок текстовой информации с оформлением или фоновым рисунком, расположенный в соответствующем разделе газеты/сайта.') ?>
        </p>
    </div>
</div>
