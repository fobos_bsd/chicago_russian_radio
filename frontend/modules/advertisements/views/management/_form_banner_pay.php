<?php

use frontend\assets\CroppicAsset;
use frontend\assets\PoshytipAsset;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;

CroppicAsset::register($this);
PoshytipAsset::register($this);
?>

<?= Html::beginForm(['management/update'], 'post', ['id' => 'addadvertisbanner', 'class' => 'form', 'enctype' => 'multipart/form-data']) ?>
<div class="row">
    <div id="boxtextadv" class="col-md-5 col-sm-12 nopadding-left">
        <!-- Начало блока основного изображения -->
        <div class="form-group col-sm-12">
            <strong><?= Yii::t('frontend', 'Заголовок и основное изображение') ?>:&nbsp;<sup id="info-contact-mod" class="question" title="<?= Yii::t('frontend', 'Основное изображение, которое будет размещено на страницах сайта и (или) печатного издания. Загрузите изображение, нажав на иконку в верхнем правом углу поля') ?>"><i class="fa fa-question-circle"></i></sup></strong>
            <p><?= Html::input('text', 'field_title', '', ['id' => 'input-field-title', 'class' => 'form-control', 'placeholder' => Yii::t('frontend', 'Заголовок объявления'), 'title' => Yii::t('frontend', 'В первую очередь, читатель обращает внимание на заголовок. Пишите просто, понятно и кратко')]) ?></p>
            <div class="form-group">
                <div id="cropContainerModalBan"></div>
                <div class="banimgerr hided"><?= Yii::t('frontend', 'Добавьте основное изображение!') ?></div>
            </div>
        </div>
        <!-- Окончание блока основного изображения -->

        <!-- Начало дополнительных сервисов -->
        <div class="form-group col-sm-12 extservises">
            <strong><?= Yii::t('frontend', 'Дополнительные сервисы') ?>:&nbsp;<sup id="info-extserv-mod" class="question" title="<?= Yii::t('frontend', 'Дополнительные платные сервисы при подаче объявления') ?>"><i class="fa fa-question-circle"></i></sup></strong>
            <div class="radio">
                <label class="checkbox-inline">
                    <input type="checkbox" name="fieldselcolor" value="serv_Полноцветное">
                    <?= Yii::t('frontend', 'Полноцветное') ?>
                </label>
            </div>
            <div class="radio">
                <label class="checkbox-inline">
                    <input type="checkbox" name="fieldgarpos" value="serv_Гарантированная">
                    <?= Yii::t('frontend', 'Гарантированная позиция') ?>
                </label>
            </div>
            <div class="radio">
                <label class="checkbox-inline">
                    <input type="checkbox" name="fieldtranslate" value="serv_Professional">
                    <?= Yii::t('frontend', 'Профессиональный перевод') ?>
                </label>
            </div>
            <div id="transdirect" class="radio">
                <h7 style="margin-bottom: 0.8em; display: inline-block;"><?= Yii::t('frontend', 'Направление перевода') ?></h7><br/>
                <label class="checkbox-inline">
                    <input type="radio" name="fildTranslateTo" value="en -> lt"> <?= Yii::t('frontend', 'Англо - Литовский') ?>
                </label><br/>
                <label class="checkbox-inline">
                    <input type="radio" name="fildTranslateTo" value="en -> ru"> <?= Yii::t('frontend', 'Англо - Русский') ?>
                </label><br/>
                <label class="checkbox-inline">
                    <input type="radio" name="fildTranslateTo" value="ru -> en"> <?= Yii::t('frontend', 'Русско - Английский') ?>
                </label><br/>
                <label class="checkbox-inline">
                    <input type="radio" name="fildTranslateTo" value="ru -> lt"> <?= Yii::t('frontend', 'Русско - Литовский') ?>
                </label><br/>
                <label class="checkbox-inline">
                    <input type="radio" name="fildTranslateTo" value="lt -> ru"> <?= Yii::t('frontend', 'Литовско - Русский') ?>
                </label><br/>
                <label class="checkbox-inline">
                    <input type="radio" name="fildTranslateTo" value="lt -> en"> <?= Yii::t('frontend', 'Литовско - Английский') ?>
                </label>
            </div>
            <input type="hidden" name="acountmail" value="<?= $uemail ?>"/>
        </div>
        <!-- Окончание дополнительных сервисов -->

        <!-- Начало блока дополнительной информации -->
        <div class="form-group col-sm-12 fieldsadv">
            <strong><?= Yii::t('frontend', 'Информация и контактные данные') ?>:&nbsp;<sup id="info-contact-mod" class="question" title="<?= Yii::t('frontend', 'Контактная и дополнительная информация для объявления') ?>"><i class="fa fa-question-circle"></i></sup></strong>

            <div class="form-group">
                <?= Html::input('text', 'field_phone', '', ['id' => 'input-field-phone', 'class' => 'form-control', 'placeholder' => Yii::t('frontend', 'Контактный телефон'), 'title' => Yii::t('frontend', 'Номер телефона, по которому с вами смогут связаться заинтересовавшиеся в вашем объявлении')]) ?>
            </div>
            <div class="form-group">
                <?= Html::input('text', 'field_company', '', ['id' => 'input-field-company', 'class' => 'form-control', 'placeholder' => Yii::t('frontend', 'Название бизнеса'), 'title' => Yii::t('frontend', 'Название вашего бизнеса, компания')]) ?>
            </div>
            <div class="form-group">
                <?= Html::input('text', 'field_address', '', ['id' => 'input-field-address', 'class' => 'form-control', 'placeholder' => Yii::t('frontend', 'Адрес'), 'title' => Yii::t('frontend', 'Почтовый адрес, по которому находится компания')]) ?>
            </div>
            <div class="form-group">
                <?= Html::input('text', 'field_manypictures', '', ['id' => 'input-field-extlink', 'class' => 'form-control', 'placeholder' => Yii::t('frontend', 'Ссылка для скачивания изибражений'), 'title' => Yii::t('frontend', 'Ссылка для скачивания изображений с внешних ресурсов')]) ?>
            </div>
            <div class="form-group">
                <?= Html::input('text', 'field_email', '', ['id' => 'input-field-email', 'class' => 'form-control', 'placeholder' => Yii::t('frontend', 'E-mail контактного лица'), 'title' => Yii::t('frontend', 'E-mail, по которому с вами смогут связаться заинтересовавшиеся в вашем объявлении')]) ?>
            </div>
            <div class="form-group">
                <textarea id="content_chars" class="form-control keyboardInput" name="field[content]" rows="4" value="0" data-name="Content" placeholder="Дополнительное содержание объявления или коментарии" title="<?= Yii::t('frontend', 'Введите текст или описание объявления. Здесь вы можете написать, каким бы вы хотели видеть свое объявление. Пишите все, что посчитаете нужным. Чем более точным будет описание, тем проще нам будет понять выши пожелания') ?>"></textarea>
            </div>
            <div class="form-group text-center" style="display: none; padding-top: 15px;">
                <p>
                    <?= Yii::t('frontend', 'Текст слишком большой! Вы привысили 5000 символов.') ?>
                    <span id="countwords" class="text-center" style="display: none;">5000</span>
                </p>
            </div>
        </div>
        <!-- Окончание блока дополнительной информации -->
    </div>
    <!-- Начало блока выбора площадки -->
    <div class="col-md-4 col-sm-12 box_templates">
        <strong><?= Yii::t('frontend', 'Виды модульной рекламы') ?>:&nbsp;<sup id="info-part-mod" class="question" title="<?= Yii::t('frontend', 'Выберите один вид модульной рекламы для вашего объявления') ?>"><i class="fa fa-question-circle"></i></sup></strong>
        <?php
        $templID = 0;
        foreach ($templates as $template) :
            if ($template->site_id !== $templID) {
                ?>
                <h3><?php
                    if ($template->site_id == 8) {
                        echo Yii::t('frontend', 'Газета «7 Дней»');
                    } elseif ($template->site_id == 9) {
                        echo Yii::t('frontend', 'Газета «Aidas»');
                    }
                    ?></h3>
                <?php
            }
            $templID = $template->site_id;
            ?>
            <div class="col-md-12 nopadding showtemplateblock" data-item="<?= $template->template_id ?>">
                <label class="checkbox" style="width: 80%;">
                    <input class="checktemplate" type="checkbox" name="template[<?= $template->template_id ?>]" value="<?= $template->template_id ?>" data-site="<?= $template->site_id ?>" data-name="<?= Html::encode($template->description) ?>">
                    <?= Html::encode($template->description) ?>
                </label>
                <span style="display: inline-block; position: absolute; top: 10px; right: 25px; cursor: pointer;"><i class="fa fa-angle-down"></i></span>
                <input type="hidden" name="Templ_<?= $template->template_id ?>[AdvertiseUser[siteid]]" value="<?= $template->site_id ?>">
                <input type="hidden" name="Templ_<?= $template->template_id ?>[AdvertiseUser[typeadv]]" value="<?= $typeadv ?>">
            </div>
            <div id="tmpate_box_<?= $template->template_id ?>" class="col-md-12 nopadding boxtemplate_one boxtemplate_<?= $template->site_id ?>" style="margin-bottom: 25px; display: none;">
                <?php
                if (isset($prices[$template->template_id])) {
                    ?>
                    <ul class="price_<?= $template->template_id ?>" style="display: none;">
                        <?php
                        foreach ($prices[$template->template_id] as $key => $val) :
                            if ($key === 'price') {
                                ?>
                                <li class="dataprice" data-weeks="1"><?= $val[0] ?></li>
                                <?php
                            }
                            if ($key !== 'type' && $key !== 'listp' && $key !== 'price') {
                                ?>
                                <li class="dataweeks" data-weeks="<?= $key ?>"><?= $val ?></li>
                                <?php
                            }
                        endforeach;
                        ?>
                    </ul>
                    <?php
                }
                ?>
                <!-- Модальное окно цен -->
                <div id="modalpr_<?= $template->template_id ?>" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title"><?= Yii::t('frontend', 'Ценовое предложение') ?></h4>
                            </div>
                            <div class="modal-body">
                                <p>
                                    <?php
                                    foreach ($prices[$template->template_id] as $key => $val) :
                                        if ($key === 'listp') {
                                            ?>
                                        <table class="table table-responsive">
                                            <tr>
                                                <td><?= Yii::t('frontend', 'Недель') ?></td>
                                                <td><?= Yii::t('frontend', 'Размер скидки') ?></td>
                                                <td><?= Yii::t('frontend', 'Ваша выгода') ?></td>
                                            </tr>
                                            <?php
                                            foreach ($val as $prlist) :
                                                ?>
                                                <tr>
                                                    <td><?= $prlist['weeks'] ?></td>
                                                    <td><?= $prlist['discont'] . '' . $prlist['distype'] ?></td>
                                                    <td><strike>$<?= $prlist['oldprice'] ?></strike>&nbsp;&nbsp;<b>$<?= $prlist['dprice'] ?></b></td>
                                                </tr>
                                                <?php
                                            endforeach;
                                            ?>
                                        </table>
                                        <?php
                                    }
                                endforeach;
                                ?>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><?= Yii::t('frontend', 'Закрыть') ?></button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="seect_weeks">
                    <div class="form-group col-sm-12">
                        <label><?= Yii::t('frontend', 'Недель публикации') ?></label>
                        <input id="contweek_<?= $template->template_id ?>" class="weeks" name="Templ_<?= $template->template_id ?>[AdvertiseUser[weeks]]" type="number" value="0" min="0" max="30" data-item="<?= $template->template_id ?>" />
                        <?php $prtype = (isset($prices[$template->template_id]['type'])) ? $prices[$template->template_id]['type'] : 'banner'; ?>
                        <input id="type_price_<?= $template->template_id ?>" type="hidden" name="typeSite" value="<?= $prtype ?>"/>
                    </div>
                    <input name="Templ_<?= $template->template_id ?>[AdvertiseUser[year]]" type="hidden" value="<?= date('Y') ?>" />
                    <div class="form-group col-sm-12">
                        <label><?= Yii::t('frontend', 'Начиная с номер выпуска') ?></label>
                        <input class="startweek" name="Templ_<?= $template->template_id ?>[AdvertiseUser[start_date]]" type="number" value="<?= $startnumer ?>" min="<?= $startnumer ?>" max="<?= $startnumer + 300 ?>" data-item="<?= $template->template_id ?>" data-cweek="<?= $startnumer ?>" data-nowe="<?= date('m/d/Y', $start_time) ?>" />
                    </div>
                    <!--
                    <div class="col-md-3">
                    <?php // echo Html::button(Yii::t('frontend', 'Цена'), ['class' => 'btn btn-primary showprice', 'data-item' => $template->template_id])     ?>
                    </div>
                    -->
                    <div class="form-group col-sm-12 finishdate" style="display: none;">
                        <div class="col-md-5 col-sm-12 col-xs-12 nopadding dateplace" id="start_date_<?= $template->template_id ?>"><?= date('m/d/Y', $start_time) ?></div>
                        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-2 nopadding dateplace" id="finish_date_<?= $template->template_id ?>"><?= date('m/d/Y', $start_time + 604800) ?></div>
                    </div>
                </div>
                <div cass="clearfix"></div>
                <div class="col-md-6">
                    <?php
                    if (isset($categories[$template->template_id])) {
                        ?>
                        <select name="Templ_<?= $template->template_id ?>[AdvertiseUser[category_id]]" class="selectbancat form-control select2-hidden-accessible" style="display:none">
                            <?php
                            foreach ($categories[$template->template_id] as $key => $val) {
                                ?>
                                <option value="<?= $key ?>" <?php if (!next($categories[$template->template_id])) echo 'selected=selected' ?> ><?= $val ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>
                </div>
                <div class="clearfix"></div>
                <div>
                    <?php
                    if (isset($fields[$template->template_id]['fields'])) {
                        foreach ($fields[$template->template_id]['fields'] as $field) :
                            if ($field->type !== 'textarea' && $field->type !== 'file') {
                                ?>
                                <?= Html::input('hidden', 'Templ_' . $template->template_id . '[field[' . $field->id . ']]', '', ['class' => 'form-control field_' . $field->slug, 'data-name' => $field->name, 'placeholder' => Html::encode($field->description)]) ?>
                                <?php
                            }
                            if ($field->type === 'textarea' && $field->slug === 'content') {
                                ?>
                                <?= Html::input('hidden', 'Templ_' . $template->template_id . '[field_content[' . $field->id . ']]', '', ['data-name' => $field->name]) ?>
                                <?php
                            }
                            if ($field->type !== 'textarea' && $field->type === 'file') {
                                ?>
                                <?= Html::input('hidden', 'Templ_' . $template->template_id . '[field_file[' . $field->id . ']]', '', ['data-name' => $field->name]) ?>
                                <?php
                            }
                        endforeach;
                    }
                    ?>
                </div>
                <div cass="clearfix"></div>
                <div class="col-md-12 serviceslist">
                    <?php
                    if (isset($servises[$template->template_id])) {
                        $j = 0;
                        foreach ($servises[$template->template_id] as $service) :
                            if ($service->app_area === 'local') {
                                ?>
                                <div class="checkbox"><label><?= Html::checkbox('Templ_' . $template->template_id . '[servises[' . $service->id . ']]', false, ['data-priceser' => $service->price, 'class' => 'service', 'data-item' => $template->template_id, 'data-names' => $service->name, 'data-typesev' => $service->app_area]) ?> <b><?= $service->name ?></b> &nbsp; $<?= $service->price . ' ' . Yii::t('common', 'за объявление') ?></label></div>
                                <?php
                            } elseif ($service->app_area === 'local_on_word') {
                                ?>
                                <div class="checkbox"><label><?= Html::checkbox('Templ_' . $template->template_id . '[servises[' . $service->id . ']]', false, ['data-priceser' => $service->price, 'class' => 'service', 'data-item' => $template->template_id, 'data-names' => $service->name, 'data-typesev' => $service->app_area]) ?> <b><?= $service->name ?></b> &nbsp; $<?= $service->price . ' ' . Yii::t('common', 'за слово в объявлении') ?></label></div>
                                <?php
                            } else {
                                ?>
                                <div id="transer_<?= $j ?>" class="hidden-md hidden serv_<?= $service->name ?>"><?= Html::checkbox('Templ_' . $template->template_id . '[servises[' . $service->id . ']]', false, ['data-priceser' => $service->price, 'class' => 'service', 'data-item' => $template->template_id, 'data-names' => $service->name, 'data-typesev' => $service->app_area]) ?></div>
                                <?php
                            }
                            $j++;
                        endforeach;
                    }
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 bannersize">
                    <?= Yii::t('frontend', 'Размер модуля в дюймах') ?>: <b><?= $template->block_width ?>x<?= $template->block_height ?></b> 
                </div>
                <div class="clearfix"></div>
                <div id="pricerate_<?= $template->template_id ?>" class="col-md-12 pricerate">
                    $<span></span>
                </div>
                <input id="priceadv_<?= $template->template_id ?>" type="hidden" name="Templ_<?= $template->template_id ?>[advprice]" value="0"/>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- Окончание блока выбора площадки -->
    <div class="col-md-3 col-sm-12 nopadding-right">
        <div id="fixedrigh">
            <div id="prices_box" class="priceview"></div>
            <button class="btn btn-sm btn-default btn-block margin-top-10 btmsend" type="submit"><?= Yii::t('common', 'Завершить и перейти к оплате') ?></button>
        </div>
    </div>

    <!-- Начало дополнительных изображений -->
    <div class="col-md-12 col-sm-12">
        <strong><?= Yii::t('frontend', 'Дополнительные изображения') ?>:&nbsp;<sup id="info-extimage-adv" class="question" title="<?= Yii::t('frontend', 'Дополнительные изображения, которые будут размещены только на страницах сайта') ?>"><i class="fa fa-question-circle"></i></sup></strong>
        <div class="form-group col-md-12 nopadding">
            <?php
            if (isset($uplimage)) {
                echo FileInput::widget([
                    'model' => $uplimage,
                    'attribute' => 'imageFiles',
                    'pluginOptions' => [
                        'showCaption' => false,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="fa fa-photo"></i>',
                        'browseLabel' => Yii::t('frontend', 'Добавить изображения'),
                        'uploadUrl' => Url::to(['/chastnye-obyavleniya/management/image-upload']),
                        'overwriteInitial' => false,
                        'showRemove' => false,
                        'uploadLabel' => Yii::t('frontend', 'Загрузить подготовленные изображения'),
                        'uploadClass' => 'btn btn-success',
                        'fileActionSettings' => [
                            'showUpload' => false,
                        ],
                    ],
                    'options' => ['multiple' => true]
                ]);
            }
            ?>
        </div>
    </div>
    <!-- Окончание дополнительных изображений -->

    <div class="col-sm-12 col-lg-12">
        <strong><?= Yii::t('common', 'Предпросмотр объявления') ?>:</strong>
        <div class="col-md-12 nopadding etantion">
            <div class="col-md-3 nopadding-left">

            </div>
            <div class="col-md-9">

            </div>
        </div>
        <div class="col-md-12 nopadding contentadvert">
            <div class="col-lg-3 col-md-5 col-sm-8 col-xs-12 wrapper nopadding">
                <div class="imgResize"><img class="imagepreview img-responsive" src="/uploads/advartise/preview/7day.jpg" alt="7day" /></div>
            </div>
        </div>
    </div>

    <div class="col-md-12 text-right totalsull">$<span id="totalsull"></span></div>
    <?= Html::input('hidden', 'totalprice', '', ['id' => 'totalprice', 'value' => 0]) ?>
    <?= Html::input('hidden', 'pricefortranslate', '', ['id' => 'pricefortranslate', 'value' => 0]) ?>
    <?= Html::input('hidden', 'priceforcolor', '', ['id' => 'priceforcolor', 'value' => 0]) ?>
    <?= Html::input('hidden', 'priceforplace', '', ['id' => 'priceforplace', 'value' => 0]) ?>
    <button class="btn btn-defult btn-sm pull-right" type="submit"><?= Yii::t('common', 'Завершить и перейти к оплате') ?></button>
    <!-- Окончание cодержимого для второго шага -->

</div>
<?= Html::endForm() ?>

<?= Html::jsFile('@web/vendor/js/formwizard_banner.js') ?>
<?= Html::jsFile('@web/vendor/js/jquery.BlackAndWhite.js') ?>
<script type="text/javascript">
    $(document).ready(function () {

        if ($("input[name='fieldselcolor']").is(':checked')) {
            $('img.imagepreview').removeClass('imagegrey');
            $('img.imagepreview').addClass('imagecolor');
        } else {
            $('img.imagepreview').removeClass('imagecolor');
            $('img.imagepreview').addClass('imagegrey');
        }

        $("input[name='fieldselcolor']").change(function () {
            if ($(this).is(':checked')) {
                $('img.imagepreview').removeClass('imagegrey');
                $('img.imagepreview').addClass('imagecolor');
            } else {
                $('img.imagepreview').removeClass('imagecolor');
                $('img.imagepreview').addClass('imagegrey');
            }
        });

        $("input.checktemplate").prop('checked', false);
        $("input.checktemplate").on('click', function () {
            var onsite = $(this).data("site");
            $("input.checktemplate").each(function () {
                var thsite = $(this).data("site");
                if (thsite === onsite) {
                    $(this).prop('checked', false);
                    $(".boxtemplate_" + onsite).hide();
                }
            });
            //$("input.checktemplate").prop('checked', false);
            //$(".boxtemplate_one").hide();
            clearPrice();
            $(this).prop('checked', true);
            var idtml = parseInt($(this).val());
            var unit = $(this).data('name');
            changePrevImage(unit);
            if ($(this).prop('checked') === true) {
                $("input#contweek_" + idtml).val(1);
                convertDate(idtml, 1);
                price_of_one(idtml);
                var sonprice = selectPrice(idtml, 1);
                change_one_price(idtml, sonprice);
                var showelem = '#tmpate_box_' + idtml;
                $(showelem).toggle();
            } else {
                $("input#contweek_" + idtml).val(0);
                convertDate(idtml, 0);
                price_of_one(idtml);
                change_one_price(idtml, onprice);
                var sonprice = selectPrice(idtml, 0);
                change_one_price(idtml, sonprice);
            }
        });
        $("#content_chars").change(function () {
            changeContent();
        });
        $("#content_chars").keypress(function () {
            changeContent();
        });
        $("div.showtemplateblock span").click(function () {
            var tempid = $(this).parent().data('item');
            var showelem = '#tmpate_box_' + tempid;
            price_of_one(tempid);
            $(showelem).toggle();
        });
        $("input.weeks").on('change', function () {
            var temID = parseInt($(this).data('item'));
            var onprice = selectPrice(temID, $(this).val());
            change_one_price(temID, onprice)
        });
        select_service();
        totalsumm();
        informationPay();
        $("form#addadvertisbanner button.nextBtn").on('click', function () {
            payStep();
        });
        $("form#addadvertisbanner a[href='#step-2']").on('click', function () {
            payStep();
        });

        setEmail();

        $("input.weeks").on('change', function () {
            var idtmpl = parseInt($(this).data('item'));
            var weeks = parseInt($(this).val());
            convertDate(idtmpl, weeks);
        });
        $("input.startweek").on('change', function () {
            var idtmpl = parseInt($(this).data('item'));
            var wasweek = parseInt($(this).data('cweek'));
            var weeks = parseInt($(this).val());
            var startdate = $(this).data('nowe');
            changeStartDate(idtmpl, wasweek, weeks, startdate);
        });
        $("button.showprice").on('click', function () {
            var tID = $(this).data('item');
            $("#modalpr_" + tID).modal('show');
        });
        $("input[name='field_title']").change(function () {
            var valtitle = $(this).val();
            $("input.field_title").val(valtitle);
        });
        $("input[name='field_company']").change(function () {
            var valcomp = $(this).val();
            $("input.field_company").val(valcomp);
        });
        $("input[name='field_phone']").change(function () {
            var valphone = $(this).val();
            $("input.field_phone").val(valphone);
        });
        $("input[name='field_address']").change(function () {
            var valaddress = $(this).val();
            $("input.field_adress").val(valaddress);
        });
        $("input[name='field_manypicture']").change(function () {
            var valmpict = $(this).val();
            $("input.field_manypicture").val(valmpict);
        });
        $("input[name='fieldtranslate']").change(function () {
            var nametype = $("input[name='fieldtranslate']:checked").val();
            if (nametype === 'serv_Professional') {
                $("#transdirect").fadeIn(500);
            } else {
                $("#transdirect").fadeOut(10);
            }
            $("div." + nametype).children("input").attr('checked', 'checked');
            totalsumm();
            informationPay();
        });
        $("input[name='fieldselcolor']").change(function () {
            var nametype = $("input[name='fieldselcolor']:checked").val();
            $("div." + nametype).children("input").attr('checked', 'checked');
            totalsumm();
            informationPay();
        });
        $("input[name='fieldgarpos']").change(function () {
            var nametype = $("input[name='fieldgarpos']:checked").val();
            $("div." + nametype).children("input").attr('checked', 'checked');
            totalsumm();
            informationPay();
        });
    });
    function strip_tags(str) {
        return str.replace(/<\/?[^>]+>/gi, '');
    }

    function clearPrice() {
        $(".showtemplateblock").each(function () {
            if ($(this).find("input.checktemplate").prop('checked') === false) {
                idtml = parseInt($(this).find("input.checktemplate").val());
                change_one_price(idtml, 0);
            }
        });
    }

    function countWords() {
        var countwc = 5000;
        var text_adv = $.trim(strip_tags($("#content_chars").val()));
        var wc = text_adv.replace(/\s\s+/gi, " ");
        if (wc !== '') {
            countwc = 5000 - parseInt(wc.length);
        }
        if (countwc <= 0) {
            $("#countwords").closest("div.text-center").show();
        } else {
            $("#countwords").closest("div.text-center").hide();
        }
        $("#countwords").text(countwc);
        return countwc;
    }

    function price_of_one(tmpid) {
        var onprice = selectPrice(tmpid, 1);
        var count_week = parseInt($("div#tmpate_box_" + tmpid).find('input.weeks').val());
        if (isNaN(onprice) || count_week == 0) {
            onprice = 0;
        }
        $("#pricerate_" + tmpid + " span").text(onprice.toFixed(2));
        $("#priceadv_" + tmpid).val(onprice.toFixed(2));
        selected_service(tmpid);
        totalsumm();
        informationPay();
    }

    function change_one_price(temID, onprice) {
        if (isNaN(onprice)) {
            onprice = 0;
        }

        $("#pricerate_" + temID + " span").text(onprice.toFixed(2));
        $("#priceadv_" + temID).val(onprice.toFixed(2));
        selected_service(temID);
        totalsumm();
        informationPay();
    }

    function select_service() {
        $("input.service").on('click', function () {
            var isChecked = $(this).prop('checked');
            var tmplID = parseInt($(this).data('item'));
            var count_week = parseInt($("div#tmpate_box_" + tmplID + " input.weeks").val());
            var srvprice = parseFloat($(this).data('priceser')) * count_week;
            var currprice = parseFloat($("#pricerate_" + tmplID + " span").text());
            if (isChecked === true) {
                var totprice = currprice + srvprice;
            } else {
                var totprice = currprice - srvprice;
            }

            $("#pricerate_" + tmplID + " span").text(totprice.toFixed(2));
            $("#priceadv_" + tmplID).val(totprice.toFixed(2));
            totalsumm();
            informationPay();
        });
    }

    function selected_service(tid) {
        $("div#tmpate_box_" + tid + " input.service:checked").each(function () {
            var isGlobal = $(this).data('typesev');
            if (isGlobal !== 'global') {
                var count_week = parseInt($("div#tmpate_box_" + tid).find('input.weeks').val());
                var srvprice = parseFloat($(this).data('priceser')) * count_week;
                var currprice = parseFloat($("#pricerate_" + tid + " span").text());
                var totprice = currprice + srvprice;
                $("#pricerate_" + tid + " span").text(totprice.toFixed(2));
                $("#priceadv_" + tid).val(totprice.toFixed(2));
            }
        });
    }

    function totalsumm() {
        var tsum = 0;
        $("div.pricerate").each(function () {
            var onesum = parseFloat($(this).find("span").text());
            if (!isNaN(onesum)) {
                tsum += onesum;
            }
        });
        var summ = tsum.toFixed(2);
        $("#totalsull").text(summ);
        var pricetrn = 0;
        var nametype = $("input[name='fieldtranslate']:checked").val();
        if (nametype !== undefined) {
            pricetrn += parseFloat($("div." + nametype).children("input").data('priceser'));
        }

        nametype = $("input[name='fieldselcolor']:checked").val();
        if (nametype !== undefined) {
            pricetrn += parseFloat($("div." + nametype).children("input").data('priceser'));
        }

        nametype = $("input[name='fieldgarpos']:checked").val();
        if (nametype !== undefined) {
            pricetrn += parseFloat($("div." + nametype).children("input").data('priceser'));
            console.log(pricetrn);
        }

        var totlsumm = parseFloat($("#totalsull").text());
        var nsumm = pricetrn + totlsumm;
        $("#totalsull").text(nsumm.toFixed(2));
        return nsumm;
    }

    function changeContent() {
        countWords();
    }

    function payStep() {
        var text_adv = strip_tags($("#content_chars").val());
        $(".contentadvert p.contadv").text(text_adv);
        var totsum = $("#totalsull").text();
        $("input#totalprice").val(totsum);
        var nametype = $("input[name='fieldtranslate']:checked").val();
        $("div." + nametype).children("input").attr('checked', 'checked');
        nametype = $("input[name='fieldselcolor']:checked").val();
        $("div." + nametype).children("input").attr('checked', 'checked');
        nametype = $("input[name='fieldgarpos']:checked").val();
        $("div." + nametype).children("input").attr('checked', 'checked');
        $(".listcont").html("<li>Итого: <span>$" + totsum + "</span></li>");
    }

    function selectPrice(trmID, wks) {
        result = 0;
        maxprice = parseFloat($("ul.price_" + trmID + " li.dataweeks").last().text());
        minweeks = parseFloat($("ul.price_" + trmID + " li.dataweeks").first().data('weeks'));
        minprice = parseFloat($("ul.price_" + trmID + " li.dataprice").text());
        maxweek = parseFloat($("ul.price_" + trmID + " li.dataweeks").last().data('weeks'));

        if (minweeks > wks) {
            result = minprice * wks;
        } else if (maxweek <= wks) {
            var onprc = maxprice / maxweek;
            result = onprc * wks;
        } else {
            $("ul.price_" + trmID + " li.dataweeks").each(function () {
                weeks = parseInt($(this).data('weeks'));
                price = parseFloat($(this).text());

                if (weeks <= wks) {
                    var saleprice = minprice * weeks - price;

                    result = (minprice * wks) - (saleprice * wks);
                }
            });
        }

        return result;
    }

    function informationPay() {

        var textinfo = '';
        var countwords = countWords();
        var tsumm = parseFloat($("#totalsull").text());
        var nametype = $("input[name='fieldtranslate']:checked").val();
        var nametype2 = $("input[name='fieldselcolor']:checked").val();
        var nametype3 = $("input[name='fieldgarpos']:checked").val();
        var pricetrn = parseFloat($("div." + nametype).children("input").data('priceser'));
        var pricetrn2 = parseFloat($("div." + nametype2).children("input").data('priceser'));
        var pricetrn3 = parseFloat($("div." + nametype3).children("input").data('priceser'));
        $("div.box_templates div.showtemplateblock").each(function () {
            var price = 0;
            var typePlace = 'Цена указана за объявление';
            var temID = $(this).find("input.checktemplate:checked").val();
            var temName = $(this).find("input.checktemplate:checked").data('name');
            var siteID = $(this).find("input.checktemplate:checked").data('site');

            if (siteID == 8) {
                siteName = '7 Дней';
            } else if (siteID == 9) {
                siteName = 'Aidas';
            } else {
                siteName = '7 Дней';
            }

            if (temID !== undefined) {
                var weeks = $("div#tmpate_box_" + temID).find('input.weeks').val();
                var onprice = selectPrice(temID, weeks);
                price = onprice.toFixed(2);
                textinfo += 'Размещено в <b>' + temName + ' (газета «' + siteName + '»)</b><br/>';
                textinfo += 'Число недель размещения:  <b>' + weeks + '</b><br/>';
                textinfo += typePlace + ' <b>$' + selectPrice(temID, 1) + '</b><br/>';
                textinfo += 'Общая цена в данном разделе: <b>' + price + '</b><br/>';
                textinfo += 'Дополнительные сервисы:<br/>';
                $("div#tmpate_box_" + temID + " input.service:checked").each(function () {
                    var isGlobal = $(this).data('typesev');
                    if (isGlobal !== 'global') {
                        var service_price = parseFloat($(this).data('priceser'));
                        var sename = $(this).data('names');
                        var srvprice = service_price * weeks;
                        textinfo += 'Сервис -  <b>' + sename + '</b>:<br/>';
                        textinfo += 'Цена 1 недели сервиса: <b>$' + service_price + '</b><br/>';
                        textinfo += 'Общая цена за все недели: <b>$' + srvprice + '</b><br/>';
                    }
                });
                textinfo += '<br/>';
            }
        });
        if (pricetrn > 0) {
            $("input#pricefortranslate").val(pricetrn);
            textinfo += 'Перевод текста: <b>$' + pricetrn + '</b><br/><br/>';
        }

        if (pricetrn2 > 0) {
            $("input#priceforcolor").val(pricetrn2);
            textinfo += 'Полноцветные: <b>$' + pricetrn2 + '</b><br/><br/>';
        }

        if (pricetrn3 > 0) {
            $("input#priceforplace").val(pricetrn3);
            textinfo += 'Гарантированная позиция: <b>$' + pricetrn3 + '</b><br/><br/>';
        }

        textinfo += 'Общая сумма к оплате: <b>$' + tsumm + '</b>;<br/>';
        $("#prices_box").html(textinfo);
    }

    function setEmail() {
        var usemail = $("input[name='acountmail']").val();
        $("input[type='email']").val(usemail);
        $("input[name='field_email']").val(usemail);
    }

    function convertDate(idtmpl, weeks) {
        var dateparse = $("div#start_date_" + idtmpl).text();
        var fdateparse = parseDateFrom(dateparse);
        var timcrnt = parseInt(Date.parse(fdateparse));
        var date = new Date(timcrnt);
        var newdate = add_weeks(date, weeks);
        var fndate = newdate.getFullYear() + '-' + newdate.getMonth() + '-' + newdate.getDate();
        var endate = parseDateTo(fndate, 1);
        $("div#finish_date_" + idtmpl).text(endate);
        $("div#finish_date_" + idtmpl).parent().show();
    }

    function changeStartDate(idtmpl, wasweek, weeks, startdate) {
        var caounweek = weeks - wasweek;
        var fdateparse = parseDateFrom(startdate);
        var timcrnt = parseInt(Date.parse(fdateparse));
        var dateTime = new Date(timcrnt);
        var date = add_weeks(dateTime, caounweek);
        var fndate = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();
        var endate = parseDateTo(fndate, 1);
        $("div#start_date_" + idtmpl).text(endate);
        $("div#start_date_" + idtmpl).parent().show();
        var count_weeks = parseInt($("input#contweek_" + idtmpl).val());
        convertDate(idtmpl, count_weeks);
    }

    function parseDateFrom(datefrom) {
        var strfrom = datefrom.split('/');
        var month = parseInt(strfrom[0]);
        month = month;
        if (month < 10) {
            month = '0' + month;
        }

        var result = month + '/' + strfrom[1] + '/' + strfrom[2];
        return result;
    }

    function parseDateTo(datefrom, tms) {
        var strfrom = datefrom.split('-');
        var month = parseInt(strfrom[1]);
        var day = parseInt(strfrom[2]);
        month = month + tms
        if (month < 10) {
            month = '0' + month;
        }
        if (day < 10) {
            day = '0' + day;
        }

        var result = month + '/' + day + '/' + strfrom[0];
        return result;
    }

    function add_weeks(dt, n)
    {
        return new Date(dt.setDate(dt.getDate() + (n * 7)));
    }

    function chackTranslate(nametype) {
        var pricetrn = parseFloat($("div." + nametype).children("input").data('priceser'));
        var totlsumm = parseFloat($("#totalsull").text());
        var nsumm = pricetrn + totlsumm;
        $("#totalsull").text(nsumm.toFixed(2));
    }

    function changePrevImage(unit) {
        selectorImg = ".contentadvert .wrapper .imgResize";
        imgp = ".contentadvert .wrapper .imgResize img.imagepreview";

        switch (unit) {
            case '1 Units':
                //$(selectorImg).css({width: '3.05em', height: '3.05em'});
                //$(imgp).css({width: '3.05em', height: '3.05em'});
                $(imgp).attr('src', '/vendor/img/u1.png');
                break;
            case '2 Units':
                //$(selectorImg).css({width: '3.05em', height: '6.01em'});
                //$(imgp).css({width: '3.05em', height: '6.01em'});
                $(imgp).attr('src', '/vendor/img/u2.png');
                break;
            case '4 Units':
                //$(selectorImg).css({width: '3.05em', height: '12.2em'});
                //$(imgp).css({width: '3.05em', height: '12.2em'});
                $(imgp).attr('src', '/vendor/img/u4.png');
                break;
            case '8 Units':
                //$(selectorImg).css({width: '6.01em', height: '12.2em'});
                //$(imgp).css({width: '6.01em', height: '12.2em'});
                $(imgp).attr('src', '/vendor/img/u8.png');
                break;
            case '16 Units H':
                //$(selectorImg).css({width: '12.2em', height: '12.2em'});
                //$(imgp).css({width: '12.2em', height: '12.2em'});
                $(imgp).attr('src', '/vendor/img/u16h.png');
                break;
            case '16 Units V':
                //$(selectorImg).css({width: '6.01em', height: '73.2em'});
                //$(imgp).css({width: '6.01em', height: '73.2em'});
                $(imgp).attr('src', '/vendor/img/u16v.png');
                break;
            case 'Full Page':
                //$(selectorImg).css({width: '12.2em', height: '73.2em'});
                //$(imgp).css({width: '12.2em', height: '73.2em'});
                $(imgp).attr('src', '/vendor/img/u32.png');
                break;
            default:
                //$(selectorImg).css({width: '3.05em', height: '19.15em'});
                //$(imgp).css({width: '3.05em', height: '3.05em'});
                $(imgp).attr('src', '/vendor/img/u1.png');
                break;
        }

        $(".contentadvert .wrapper").show();
    }
</script>

<script>
    (function () {
        var a = document.querySelector('#fixedrigh'), b = null, P = 0;
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);
        function Ascroll() {
            if (b == null) {
                var Sa = getComputedStyle(a, ''), s = '';
                for (var i = 0; i < Sa.length; i++) {
                    if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                        s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                    }
                }
                b = document.createElement('div');
                b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                a.insertBefore(b, a.firstChild);
                var l = a.childNodes.length;
                for (var i = 1; i < l; i++) {
                    b.appendChild(a.childNodes[1]);
                }
                a.style.height = b.getBoundingClientRect().height + 'px';
                a.style.padding = '0';
                a.style.border = '0';
            }
            var Ra = a.getBoundingClientRect(),
                    R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('#boxtextadv').getBoundingClientRect().bottom);  // селектор блока, при достижении нижнего края которого нужно открепить прилипающий элемент
            if ((Ra.top - P) <= 0) {
                if ((Ra.top - P) <= R) {
                    b.className = 'stop';
                    b.style.top = -R + 'px';
                } else {
                    b.className = 'sticky';
                    b.style.top = P + 'px';
                }
            } else {
                b.className = '';
                b.style.top = '';
            }
            window.addEventListener('resize', function () {
                a.children[0].style.width = getComputedStyle(a, '').width
            }, false);
        }
    })()
</script>

<?php
$script = <<< JS
    var croppicContainerModalBannOptions = {
        //uploadUrl: '/advertisements/management/imgboss-origupload',
        cropUrl: '/advertisements/management/imgboss-upload',
        imgEyecandyOpacity: 0.2,
        processInline: true,
        modal: true,
        enableMousescroll: true
    };
    var cropContainerModalBann = new Croppic('cropContainerModalBan', croppicContainerModalBannOptions);
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_END);
?>
