<?php

namespace frontend\modules\advertisements\widgets;

use yii,
    yii\base\Widget;
use common\models\User;

/**
 * Class AuthorMiniList
 * @package frontend\widgets
 */
class CategoriesList extends Widget {

    /**
     * @var User[]
     */
    public $catarr = array();
    public $module, $ch, $session;
    protected $urlapicore;

    public function init() {
        parent::init();

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_NOBODY, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        $this->session = Yii::$app->session;

        // Получаем статические переменные
        $this->urlapicore = Yii::$app->authAdvertisement->getCoreApiUrl();

        $urlcat = $this->urlapicore . "/content/get-categories";
        curl_setopt($this->ch, CURLOPT_URL, $urlcat);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, "lang=ru&platform=7days");
        $categories = json_decode(curl_exec($this->ch));

        if (isset($categories)) {
            $this->catarr = (array) $categories;
        } else {
            $this->catarr = null;
        }

        // Закрываем соединение
        curl_close($this->ch);
    }

    /**
     * @return string
     */
    public function run() {
        return $this->render('categories-list', [
                    'catarr' => $this->catarr,
        ]);
    }

}
