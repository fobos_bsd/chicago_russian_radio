<?php

use common\models\User;
use yii\helpers\Url,
    yii\helpers\Html,
    rmrevin\yii\fontawesome\FA,
    yii\helpers\HtmlPurifier;

/** @var User[] $items */
// Транслитерация строк.
function transliterate($st) {
    $s = (string) $st; // преобразуем в строковое значение
    $s = strip_tags($s); // убираем HTML-теги
    $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
    $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
    $s = trim($s); // убираем пробелы в начале и конце строки
    $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
    $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
    $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
    $s = str_replace(" ", "_", $s); // заменяем пробелы знаком минус
    return $s;
}
?>

<div class="classy-cats">
    <?php
    if (isset($catarr) && is_array($catarr) && count($catarr) > 0) {
        foreach ($catarr as $cat) :

            switch ($cat->name) {
                case 'Купля-Продажа': $faicon = 'fa-money';
                    break;
                case 'Бизнес на продажу': $faicon = 'fa-suitcase';
                    break;
                case 'Ищу работу': $faicon = 'fa-user-secret';
                    break;
                case 'Требуются на работу': $faicon = 'fa-users';
                    break;
                case 'Недвижимость': $faicon = 'fa-home';
                    break;
                case 'Недвижимость за рубежом': $faicon = 'fa-home';
                    break;
                case 'Сдается': $faicon = 'fa-building-o';
                    break;
                case 'Ищу руммейта': $faicon = 'fa-male';
                    break;
                case 'Услуги': $faicon = 'fa-bullhorn';
                    break;
                case 'Ищу рент': $faicon = 'fa-key';
                    break;
                case 'Продажа автомобилей': $faicon = 'fa-car';
                    break;
                case 'Уроки': $faicon = 'fa-graduation-cap';
                    break;
                case 'Знакомства': $faicon = 'fa-venus-mars';
                    break;
                case 'Разное': $faicon = 'fa-newspaper-o';
                    break;
                default: $faicon = 'fa-suitcase';
                    break;
            }
            ?>
            <?php $countad = (isset($cat->countad)) ? (int) $cat->countad : 0; ?>
            <?php if ($countad > 0) : ?>
                <a href="<?= '/chastnye-obyavleniya/category/name?catid=' . $cat->id . '&catname=' . transliterate($cat->name) ?>" class="btn btn-default btn-block" data-item="<?= $cat->id ?>">
                    <i class="fa <?= $faicon ?> text-primary"></i>
                    <?= $cat->name ?>

                    <span class="text-primary">(<?= $countad ?>)</span></a>
                    <?php
                endif;
            endforeach;
        }
        ?>
</div>