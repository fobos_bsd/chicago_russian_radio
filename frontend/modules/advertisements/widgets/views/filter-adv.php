<?php

use yii\bootstrap\Html,
    yii\helpers\Url;
?>

<div class="panel card-box col-sm-12">
    <h2 class="text-primary"><?= Yii::t('frontend', Yii::t('frontend', 'Частные объявления')); ?></h2>
    <p class="margin-bottom-20"><?=
        Yii::t('frontend', 'Ищете работу? Хотите познакомиться? Хотите снять квартиру? Просмотрите наши объявления. 
                    Ищете сотрудников? Хотите сдать квартиру в рент? Разместите свое объявление.'
        );
        ?></p>
    <?= Html::beginForm('/chastnye-obyavleniya/search', 'post', ['class' => 'form-inline']); ?>
    <div class="input-group col-md-7">
        <?= Html::activeInput('text', $model, 'search_text', ['class' => 'form-control', 'placeholder' => Yii::t('frontend', 'Поиск по объявлениям')]) ?>
        <span class="input-group-btn">
            <?= Html::submitButton('<i class="fa fa-search fa-lg"></i>', ['class' => 'btn btn-primary']) ?>
        </span>
    </div>
    <div class="input-group col-md-4 text-right">
        <?= Html::a(Yii::t('common', Yii::t('frontend', 'Подать объявление')), ['management/start'], ['class' => 'btn btn-md btn-info']) ?>
    </div>

    <?= Html::endForm(); ?>
</div>

