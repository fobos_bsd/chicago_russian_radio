<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\advertisements\widgets;

use yii,
    yii\helpers\ArrayHelper,
    yii\base\Widget;
use frontend\modules\advertisements\models\FilterForm;

class FilterAdv extends Widget {

    public $session;

    /*
     * @return data
     */

    public function init() {
        $this->session = Yii::$app->session;
        parent::init();
    }

    /**
     * @return string
     */
    public function run() {
        $model = new FilterForm();

        if ($this->session->has('searchtext')) {
            $model->load($this->session->get('searchtext'));
        }

        return $this->render('filter-adv', [
                    'model' => $model,
        ]);
    }

}
