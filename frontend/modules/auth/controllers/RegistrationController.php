<?php

namespace frontend\modules\auth\controllers;

use dektrium\user\controllers\RegistrationController as BaseRegistrationController;

/**
 * Default controller for the `user` module
 */
class RegistrationController extends BaseRegistrationController {

    public function beforeAction($action) {
        $this->layout = '/auth'; //your layout name
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionRegister() {
        return parent::actionRegister();
    }

    public function actionConnect($code) {
        $return = parent::actionConnect($code);
        return $return;
    }

    public function actionConfirm($id, $code) {
        return parent::actionConfirm($id, $code);
    }

    public function actionResend() {
        return parent::actionResend();
    }

}
