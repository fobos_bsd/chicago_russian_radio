<?php

namespace frontend\modules\auth\controllers;

use Yii;
use dektrium\user\controllers\SecurityController as BaseSecurityController;

/**
 * Default controller for the `user` module
 */
class SecurityController extends BaseSecurityController {

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLogin() {

        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/admin');
        }

        $this->layout = '/auth.twig';

        return parent::actionLogin();
    }
    
    /**
     * Logs the user out and then redirects to the homepage.
     *
     * @return Response
     */
    public function actionLogout()
    {
        return parent::actionLogout();
    }

}
