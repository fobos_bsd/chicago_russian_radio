<?php

namespace frontend\modules\auth\controllers;

use dektrium\user\controllers\RecoveryController as BaseRecoveryController;

/**
 * Default controller for the `user` module
 */
class RecoveryController extends BaseRecoveryController {

    public function beforeAction($action) {
        $this->layout = '/auth'; //your layout name
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionRequest() {
        return parent::actionRequest();
    }

    public function actionReset($id, $code) {
        return parent::actionRequest($id, $code);
    }

}
