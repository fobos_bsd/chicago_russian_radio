<?php

namespace frontend\modules\pages\controllers;

use yii,
    yii\data\ActiveDataProvider,
    yii\web\NotFoundHttpException;
use common\models\ClientsPage;

class ClientsController extends \yii\web\Controller {

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex() {
        $query = ClientsPage::find()->where(['status' => 1]);
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'title' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('index.twig', ['dataProvider' => $provider]);
    }

    public function actionItem($slug) {
        $this->layout = '@app/views/layouts/main_clientpage.twig';
        $model = $this->findModel($slug);

        return $this->render('item.twig', ['model' => $model]);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug) {
        if (($model = ClientsPage::find()->where(['slug' => $slug, 'status' => 1])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
