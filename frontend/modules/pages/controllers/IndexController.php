<?php

namespace frontend\modules\pages\controllers;

use yii\web\NotFoundHttpException;
use common\models\Pages;

class IndexController extends \yii\web\Controller {

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($slug) {
        $data = $this->findModel($slug);

        return $this->render('index.twig', ['dataPage' => $data]);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug) {
        if (($model = Pages::find()->where(['slug' => $slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
