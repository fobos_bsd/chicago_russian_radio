<?php

namespace frontend\modules\voicereclam\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\helpers\ArrayHelper;
use common\models\AudioAdvUsed,
    common\models\search\AudioAdvUsedSearch,
    common\models\Pages;

class IndexController extends Controller {

    public $daysofweek;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->daysofweek = [
            'Mo' => Yii::t('common', 'Monday'),
            'Tu' => Yii::t('common', 'Tuesday'),
            'We' => Yii::t('common', 'Wednesday'),
            'Th' => Yii::t('common', 'Thursday'),
            'Fr' => Yii::t('common', 'Friday'),
            'Sa' => Yii::t('common', 'Saturday'),
            'Su' => Yii::t('common', 'Sunday')
        ];
    }

    /**
     * Lists all Timetable models.
     * @return mixed
     */
    public function actionIndex() {
        // Set meta SEO if exists home page
        $pagedata = Pages::find()->where(['like', 'slug', 'tekstovyj-arhiv-vesania'])->one();
        $title = (isset($pagedata->meta_title)) ? $pagedata->meta_title : Yii::t('frontend', 'Voice reclam');
        $keywords = (isset($pagedata->meta_key)) ? $pagedata->meta_key : Yii::t('frontend', 'Voice,reclam');
        $meta_desc = (isset($pagedata->meta_description)) ? $pagedata->meta_description : Yii::t('frontend', 'Voice reclam');

        $searchModel = new AudioAdvUsedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index.twig', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'daysofweek' => $this->daysofweek,
                    'title' => $title,
                    'keywords' => $keywords,
                    'metadesc' => $meta_desc,
        ]);
    }

}
