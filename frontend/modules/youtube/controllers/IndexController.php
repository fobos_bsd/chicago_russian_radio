<?php

namespace frontend\modules\youtube\controllers;

use Yii,
    yii\helpers\Url,
    yii\web\NotFoundHttpException,
    yii\helpers\ArrayHelper,
    yii\filters\VerbFilter,
    yii\data\ActiveDataProvider;
use common\models\YoutubeChannels,
    common\models\YoutubePlaylists,
    common\models\YoutubeVideo,
    common\models\Soundcloud,
    common\models\PlaylistFiles;

class IndexController extends \yii\web\Controller {

    public $siteID, $siteLang;

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['POST', 'GET'],
                    'item' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    public function init() {
        parent::init();

        // Set global site params
        $siteArr = Yii::$app->Siteinfo->getSite();
        $this->siteID = $siteArr['id'];
        $this->siteLang = $siteArr['lang'];
    }

    public function actionIndex() {
        $arrchanl = YoutubeChannels::find()->where(['status' => 1, 'site_id' => $this->siteID])->asArray()->all();

        // Get all channels of this site
        $chanelsArr = ArrayHelper::getColumn($arrchanl, 'id');
        if (isset($chanelsArr) && count($chanelsArr) > 0) {
            $query = YoutubePlaylists::find()->where(['youtube_channels_id' => $chanelsArr, 'status' => 1]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 12,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'priority' => SORT_ASC,
                        'publish_at' => SORT_DESC,
                        'title' => SORT_ASC,
                    ]
                ],
            ]);
        } else {
            $dataProvider = null;
        }

        // Set meta params for SEO
        if (isset($arrchanl) && count($arrchanl) > 0) {
            $lastchanel = end($arrchanl);
            $title = (isset($lastchanel['name'])) ? $lastchanel['name'] : Yii::t('frontend', 'Video channel');
            $keywords = (isset($lastchanel['meta_key'])) ? $lastchanel['meta_key'] : Yii::t('frontend', 'Video,channel');
            $meta_desc = (isset($lastchanel['meta_description'])) ? $lastchanel['meta_description'] : Yii::t('frontend', 'Video channel');
        } else {
            $title = Yii::t('frontend', 'Video channel');
            $keywords = Yii::t('frontend', 'Video,channel');
            $meta_desc = Yii::t('frontend', 'Video channel');
        }

        return $this->render('index.twig', ['dataProvider' => $dataProvider, 'title' => $title, 'keywords' => $keywords, 'metadesc' => $meta_desc]);
    }

    public function actionItem($plid) {
        // Get playlist data
        $playlist = YoutubePlaylists::findOne(['id' => $plid]);

        // Get souncloud audio of the playlist
        $soundcloud = Soundcloud::find()->where(['youtube_playlists_id' => $plid]);
        $dataSoundcloud = new ActiveDataProvider([
            'query' => $soundcloud,
            'pagination' => [
                'pageSize' => 12,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'name' => SORT_ASC,
                ]
            ],
        ]);

        // Get playlist video
        $query = YoutubeVideo::find()->where(['youtube_playlists_id' => $plid, 'status' => 1]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 12,
            ],
            'sort' => [
                'defaultOrder' => [
                    'publish_at' => SORT_DESC,
                    'title' => SORT_ASC,
                ]
            ],
        ]);

        // Get last video
        $video = YoutubeVideo::find()->where(['youtube_playlists_id' => $plid, 'status' => 1])->orderBy(['id' => SORT_DESC])->one();

        // Get audio files
        $audiofiles = PlaylistFiles::find()->where(['youtube_playlists_id' => $plid, 'status' => 1]);
        $dataAudio = new ActiveDataProvider([
            'query' => $audiofiles,
            'pagination' => [
                'pageSize' => 12,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                    'name' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('item.twig', ['playlist' => $playlist, 'dataProvider' => $dataProvider, 'dataSoundcloud' => $dataSoundcloud, 'dataAudio' => $dataAudio, 'video' => $video]);
    }

}
