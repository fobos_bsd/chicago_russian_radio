<?php

namespace frontend\controllers;

use Yii,
    yii\base\InvalidParamException,
    yii\web\BadRequestHttpException,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper,
    yii\data\ActiveDataProvider;
use common\models\LoginForm,
    common\models\Timetable,
    common\models\YoutubePlaylists,
    common\models\YoutubeChannels,
    common\models\Pages,
    common\models\Settings,
    common\models\DifferentData,
    common\models\search\RadioArchiveSearch;
use frontend\models\PasswordResetRequestForm,
    frontend\models\ResetPasswordForm,
    frontend\models\SignupForm,
    frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller {

    public $siteID, $siteLang, $settings;

    const DAYS_OF_WEEK = [
        0 => 'Su',
        1 => 'Mo',
        2 => 'Tu',
        3 => 'We',
        4 => 'Th',
        5 => 'Fr',
        6 => 'Sa'
    ];

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'error'],
                'rules' => [
                    [
                        'actions' => ['signup', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function init() {
        parent::init();

        $siteArr = Yii::$app->Siteinfo->getSite();
        $this->siteID = $siteArr['id'];
        $this->siteLang = $siteArr['lang'];
        $this->settings = Settings::find()->where(['site_id' => $this->siteID])->asArray()->all();
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {

        // Set meta SEO if exists home page
        $pagedata = Pages::find()->where(['like', 'slug', 'home'])->one();
        $title = (isset($pagedata->meta_title)) ? $pagedata->meta_title : Yii::t('frontend', 'Home page');
        $keywords = (isset($pagedata->meta_key)) ? $pagedata->meta_key : Yii::t('frontend', 'Home,page');
        $meta_desc = (isset($pagedata->meta_description)) ? $pagedata->meta_description : Yii::t('frontend', 'Home page');


        // Get all programs
        $playlists = YoutubePlaylists::find()->select(['youtube_playlists.*'])->leftJoin('youtube_channels', 'youtube_channels.id = youtube_playlists.youtube_channels_id')->where(['youtube_channels.site_id' => $this->siteID, 'youtube_playlists.status' => 1])->asArray()->all();

        // Setting youtobe video
        if (isset($this->settings) && count($this->settings) > 0) {
            foreach ($this->settings as $param) {
                if ($param['name'] === 'youtobe_home_channel') {
                    $videocode = strip_tags($param['value']);
                    break;
                } else {
                    $videocode = '7oyuhHcQ_SA';
                }
            }
        } else {
            $videocode = '7oyuhHcQ_SA';
        }

        // Get curent timetable program status
        if (($model_tt = Timetable::find()->where(['<=', 'from_time', date('H:i:s')])->andWhere(['>=', 'to_time', date('H:i:s')])->andWhere(['day_of_week' => self::DAYS_OF_WEEK[date('w')]])->andWhere(['or', ['timetable_tags_id' => 1], ['onair' => 1]])->one()) !== null) {
            $live = 1;
        } else {
            $live = 0;
        }

        return $this->render('index.twig', ['playlists' => $playlists, 'title' => $title, 'keywords' => $keywords,
                    'metadesc' => $meta_desc, 'videocode' => $videocode, 'live' => $live]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/admin');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login.twig', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
        $model = new ContactForm();
        $model->subject = Yii::t('frontend', 'Feedback and wishes');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // Get settings e-mail
            if (isset($this->settings) && count($this->settings) > 0) {
                foreach ($this->settings as $param) {
                    if ($param['name'] === 'contact_email') {
                        $email = strip_tags($param['value']);
                        break;
                    } else {
                        $email = 'radio@ethnicmedia.us';
                    }
                }
            } else {
                $email = 'radio@ethnicmedia.us';
            }
            if ($model->sendEmail($email)) {
                Yii::$app->session->setFlash('success', Yii::t('frontend', 'Thank you for contacting us. We will reply to you as soon as possible.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'There was an error sending your message.'));
            }

            return $this->refresh();
        } else {
            return $this->render('contact.twig', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout() {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup.twig', [
                    'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken.twig', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword.twig', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionBroadcastingGrid() {
        // Set meta SEO if exists home page
        $pagedata = Pages::find()->where(['like', 'slug', 'setka-radio-vesania'])->one();
        $title = (isset($pagedata->meta_title)) ? $pagedata->meta_title : Yii::t('frontend', 'Home page');
        $keywords = (isset($pagedata->meta_key)) ? $pagedata->meta_key : Yii::t('frontend', 'Home,page');
        $meta_desc = (isset($pagedata->meta_description)) ? $pagedata->meta_description : Yii::t('frontend', 'Home page');

        $result = array();
        $timetable = Timetable::find()->with('timetableTags')->where(['site_id' => $this->siteID, 'lang_id' => $this->siteLang])->orderBy(['from_time' => SORT_ASC, 'day_of_week' => SORT_ASC, 'name' => SORT_ASC])->asArray()->all();
        $i = 0;
        foreach ($timetable as $timetb) {
            switch ($timetb['day_of_week']) {
                case 'Mo': $result['mo'][$i] = [
                        'from_time' => $timetb['from_time'],
                        'to_time' => $timetb['to_time'],
                        'name' => $timetb['name'],
                        'onair' => $timetb['onair'],
                        'description' => $timetb['description'],
                        'youtube_playlists_id' => $timetb['youtube_playlists_id'],
                        'fileImage' => $timetb['fileImage'],
                        'tags' => $timetb['timetableTags']['name']
                    ];
                    break;
                case 'Tu': $result['tu'][$i] = [
                        'from_time' => $timetb['from_time'],
                        'to_time' => $timetb['to_time'],
                        'name' => $timetb['name'],
                        'onair' => $timetb['onair'],
                        'description' => $timetb['description'],
                        'youtube_playlists_id' => $timetb['youtube_playlists_id'],
                        'fileImage' => $timetb['fileImage'],
                        'tags' => $timetb['timetableTags']['name']
                    ];
                    break;
                case 'We': $result['we'][$i] = [
                        'from_time' => $timetb['from_time'],
                        'to_time' => $timetb['to_time'],
                        'name' => $timetb['name'],
                        'onair' => $timetb['onair'],
                        'description' => $timetb['description'],
                        'youtube_playlists_id' => $timetb['youtube_playlists_id'],
                        'fileImage' => $timetb['fileImage'],
                        'tags' => $timetb['timetableTags']['name']
                    ];
                    break;
                case 'Th': $result['th'][$i] = [
                        'from_time' => $timetb['from_time'],
                        'to_time' => $timetb['to_time'],
                        'name' => $timetb['name'],
                        'onair' => $timetb['onair'],
                        'description' => $timetb['description'],
                        'youtube_playlists_id' => $timetb['youtube_playlists_id'],
                        'fileImage' => $timetb['fileImage'],
                        'tags' => $timetb['timetableTags']['name']
                    ];
                    break;
                case 'Fr': $result['fr'][$i] = [
                        'from_time' => $timetb['from_time'],
                        'to_time' => $timetb['to_time'],
                        'name' => $timetb['name'],
                        'onair' => $timetb['onair'],
                        'description' => $timetb['description'],
                        'youtube_playlists_id' => $timetb['youtube_playlists_id'],
                        'fileImage' => $timetb['fileImage'],
                        'tags' => $timetb['timetableTags']['name']
                    ];
                    break;
                case 'Sa': $result['sa'][$i] = [
                        'from_time' => $timetb['from_time'],
                        'to_time' => $timetb['to_time'],
                        'name' => $timetb['name'],
                        'onair' => $timetb['onair'],
                        'description' => $timetb['description'],
                        'youtube_playlists_id' => $timetb['youtube_playlists_id'],
                        'fileImage' => $timetb['fileImage'],
                        'tags' => $timetb['timetableTags']['name']
                    ];
                    break;
                case 'Su': $result['su'][$i] = [
                        'from_time' => $timetb['from_time'],
                        'to_time' => $timetb['to_time'],
                        'name' => $timetb['name'],
                        'onair' => $timetb['onair'],
                        'description' => $timetb['description'],
                        'youtube_playlists_id' => $timetb['youtube_playlists_id'],
                        'fileImage' => $timetb['fileImage'],
                        'tags' => $timetb['timetableTags']['name']
                    ];
                    break;
            }
            $i++;
        }

        return $this->render('broadcasting-grid.twig', [
                    'timetable' => $timetable,
                    'models' => $result,
                    'dayofweek' => date('w'),
                    'currenttime' => $this->_date('H:i:s', false, 'America/Chicago'),
                    'title' => $title,
                    'keywords' => $keywords,
                    'metadesc' => $meta_desc
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionArchive() {
        $searchModel = new RadioArchiveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $arrchanl = YoutubeChannels::find()->where(['status' => 1, 'site_id' => $this->siteID])->asArray()->all();

        // Get all channels of this site
        $chanelsArr = ArrayHelper::getColumn($arrchanl, 'id');

        return $this->render('archive.twig', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'playlists' => ArrayHelper::map(YoutubePlaylists::find()->where(['youtube_channels_id' => $chanelsArr[0], 'status' => 1])->orderBy(['priority' => SORT_ASC, 'publish_at' => SORT_DESC, 'title' => SORT_ASC,])->asArray()->all(), 'id', 'title'),
                    'dayofweek' => date('w'),
                    'currenttime' => $this->_date('H:i:s', false, 'America/Chicago'),
        ]);
    }

    /**
     * Displays different data page.
     *
     * @return mixed
     */
    public function actionTableActions() {
        $query = DifferentData::find()->where(['site_id' => $this->siteID]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'action_date' => SORT_DESC,
                    'name' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('table-actions.twig', [
                    'dataProvider' => $dataProvider,
                    'title' => Yii::t('frontend', 'Table actions'),
                    'keywords' => Yii::t('frontend', 'table,actions'),
                    'metadesc' => Yii::t('frontend', 'Table actions')
        ]);
    }

    public function actionRadioVoice() {
        $this->layout = 'radio.twig';

        return $this->render('radio-voice.twig');
    }

    protected function _date($format = "r", $timestamp = false, $timezone = false) {
        $userTimezone = new \DateTimeZone(!empty($timezone) ? $timezone : 'GMT');
        $gmtTimezone = new \DateTimeZone('GMT');
        $myDateTime = new \DateTime(($timestamp != false ? date("r", (int) $timestamp) : date("r")), $gmtTimezone);
        $offset = $userTimezone->getOffset($myDateTime);
        return date($format, ($timestamp != false ? (int) $timestamp : $myDateTime->format('U')) + $offset);
    }

    public function actionError() {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error.twig', ['exception' => $exception]);
        }
    }

}
