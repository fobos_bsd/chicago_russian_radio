<?php

namespace frontend\components;

use Yii,
    yii\base\Component,
    yii\helpers\ArrayHelper,
    yii\helpers\Url,
    yii\web\Cookie,
    common\models\Site;

class Siteinfo extends Component {

    public $cookies, $lang;

    public function init() {
        parent::init();

        $this->cookies = Yii::$app->request->cookies;

        // Set site selector cookie
        if (!$this->cookies->has('site')) {
            $this->setSite();
        }
    }

    // Set site ID cookie
    protected function setSite() {
        $cookies = Yii::$app->response->cookies;

        $relativeHomeUrl = Url::home(true);
        // Get sites
        $sites = Site::find()->where(['status' => 1])->asArray()->all();

        foreach ($sites as $site) {
            if (strpos($relativeHomeUrl, $site['domain'])) {
                $siteID = (int) $site['id'];
                $siteLang = (int) $site['lang_id'];
                break;
            } else {
                $siteID = 1;
                $siteLang = 1;
            }
        }

        $cookies->add(new Cookie([
            'name' => 'site',
            'value' => $siteID,
            'path' => '/',
            'expire' => time() + 60 * 60 * 24 * 7
        ]));

        $cookies->add(new Cookie([
            'name' => 'lang',
            'value' => $siteLang,
            'path' => '/',
            'expire' => time() + 60 * 60 * 24 * 7
        ]));
    }

    public function getSite() {
        $site = [
            'id' => $this->cookies->getValue('site'),
            'lang' => $this->cookies->getValue('lang')
        ];

        return $site;
    }

}
