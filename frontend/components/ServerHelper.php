<?php

namespace frontend\components;

use Yii;
use yii\base\Component;


class ServerHelper extends Component {

    public function getRemoteIPAddress() {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $strip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            $arrip = explode(', ', $strip);
            $ip = (isset($arrip[1])) ? $arrip[1] : $arrip[0];
            return $ip;
        }
        return Yii::$app->request->userIP;
    }
}
