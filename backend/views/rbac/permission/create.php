<?php

use yii\helpers\Html;

$this->title = Yii::t('rbac', 'Create new permission');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac', 'Permission'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-create">
    <h1><?= $this->title ?></h1>
    <?php $this->beginContent('@dektrium/rbac/views/layout.php') ?>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

    <?php $this->endContent() ?>
</div>

