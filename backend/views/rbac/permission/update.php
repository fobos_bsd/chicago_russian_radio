<?php

use yii\helpers\Html;

$this->title = Yii::t('rbac', 'Update permission');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac', 'Permission'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-create">
    <h1><?= $this->title ?></h1>
    <?php $this->beginContent('@backend/views/rbac/layout.php') ?>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

    <?php $this->endContent() ?>
</div>
