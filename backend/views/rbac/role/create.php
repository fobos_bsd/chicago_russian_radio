<?php

use yii\helpers\Html;

$this->title = Yii::t('rbac', 'Create new role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac', 'Role'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-create">

    <h1><?= Yii::t('rbac', 'Create role form') ?></h1>
    <?php $this->beginContent('@backend/views/rbac/layout.php') ?>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

    <?php $this->endContent() ?>
</div>