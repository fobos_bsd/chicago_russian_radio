<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var $this  \yii\web\View
 * @var $model \dektrium\rbac\models\Rule
 */
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
?>

<div class="box box-danger">
    <div class="box-body">
<?php
$form = ActiveForm::begin([
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
        ])
?>

        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'class') ?>

        <?= Html::submitButton(Yii::t('rbac', 'Save'), ['class' => 'btn btn-danger']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>