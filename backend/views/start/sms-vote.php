<?php

use yii\bootstrap\Html;

$jsonCount = (isset($objSmsCount) && $objSmsCount !== '[{}]') ? $objSmsCount : '[{"label":"Votes (0)","value":1,"color":"#ccc"}]';

if (!isset($jsonCount) || $totalcount == 0)
    $jsonCount = '[{"label":"Votes (0)","value":1,"color":"#ccc"}]';
?>

<section class="panel">
    <?php if (isset($totalcount) && $totalcount > 0) : ?>
        <header class="panel-heading">
            <?= $theme ?>
        </header>
        <div class="panel-body text-center">
            <div id="pieSmsChart"></div>
        </div>
    <?php else : ?>
        <header class="panel-heading">
            <?= Yii::t('frontend', 'No active votes'); ?>
        </header>
        <div class="panel-body text-center">
            <?= Yii::t('frontend', 'No active SMS votes or voices'); ?>
        </div>
    <?php endif; ?>
</section>

<script type="text/javascript">
    var pie = new d3pie("pieSmsChart", {
        "header": {
            "title": {
                "text": "<?= $theme ?>",
                "fontSize": 22,
                "font": "open sans"
            },
            "subtitle": {
                "text": "<?= strip_tags($descript) . ' / Total votes (' . $totalcount . ')' ?>",
                "color": "#999999",
                "fontSize": 15,
                "font": "open sans"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 12,
            "font": "open sans",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 1200,
            "pieOuterRadius": "80%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": <?= $jsonCount ?>
        },
        "labels": {
            "outer": {
                "pieDistance": 5
            },
            "inner": {
                "hideWhenLessThanPercentage": 1
            },
            "mainLabel": {
                "fontSize": 13
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0,
                "fontSize": 15,
            },
            "value": {
                "color": "#adadad",
                "fontSize": 13
            },
            "lines": {
                "enabled": true
            },
            "truncation": {
                "enabled": true
            }
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8
            }
        },
        "misc": {
            "gradient": {
                "enabled": true,
                "percentage": 100
            }
        }
    });
</script>