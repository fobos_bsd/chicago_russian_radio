<?php
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.4.4/d3.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/js/d3pie.js' ?>"></script>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= $neworders ?></h3>
                <p><?= Yii::t('backend', 'New orders') ?></p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="/admin/advertising/adv-orders/index?AdvOrdersSearch[status]=New&AdvOrdersSearch[site_id]=<?= $site_id ?>" class="small-box-footer"><?= Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= $airorders; ?></h3>
                <p><?= Yii::t('backend', 'Current orders') ?></p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <a href="/admin/advertising/adv-orders/index?AdvOrdersSearch[status]=On+Air&AdvOrdersSearch[site_id]=<?= $site_id ?>" class="small-box-footer"><?= Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3></h3>

                <p><?= Yii::t('backend', 'Users on month') ?></p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="/admin/google/index" class="small-box-footer"><?= Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?= $totalcountph ?></h3>

                <p><?= $phtheme ?></p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?php if ($vote_id !== 0) { ?>/admin/onlinevotes/index/statistic/<?= $vote_id ?><?php } else { ?>javascript:;<?php } ?>" class="small-box-footer"><?= Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>
<!-- /.row -->
<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
        <!-- quick email widget -->
        <div class="box box-info">
            <div class="box-header">
                <i class="fa fa-pie-chart"></i>

                <h3 class="box-title">Last Online Vote Result</h3>
            </div>
            <div class="box-body">
                <?=
                $this->render('phone-vote', [
                    'objPhoneCount' => $objPhoneCount,
                    'theme' => $phtheme,
                    'descript' => $phdescript,
                    'totalcount' => $totalcountph,
                    'curntname' => $curntname
                ])
                ?>
                <?=
                $this->render('sms-vote', [
                    'objSmsCount' => $objSmsCount,
                    'theme' => $smstheme,
                    'descript' => $smsdescript,
                    'totalcount' => $totalcountsms,
                ])
                ?>
            </div>
        </div>

    </section>
</div>
<!-- /.row (main row) -->