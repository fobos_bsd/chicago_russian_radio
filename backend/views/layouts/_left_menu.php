<?php

use yii\bootstrap\Html,
    yii\helpers\Url;
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/backend/web/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header"><?= Yii::t('backend', 'NAVIGATION MENU') ?></li>
            <li>
                <a href="/admin">
                    <i class="fa fa-dashboard"></i> <span><?= Yii::t('backend', 'Dashboard') ?></span>
                </a>
            </li>
            <?php if (Yii::$app->userAccess->userCan()) { ?>
                <li>
                    <a href="/admin/lang/index">
                        <i class="fa fa-font"></i> <span><?= Yii::t('backend', 'Languges') ?></span>
                    </a>
                </li>
                <li>
                    <a href="/admin/site/index">
                        <i class="fa fa-globe"></i> <span><?= Yii::t('backend', 'Sites') ?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-file-text"></i>
                        <span><?= Yii::t('backend', 'Pages') ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/pages/index"><i class="fa fa-circle-o"></i> <?= Yii::t('backend', 'Pages') ?></a></li>
                        <li><a href="/admin/pages/clients-page"><i class="fa fa-circle-o"></i> <?= Yii::t('backend', 'Client Pages') ?></a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-sitemap"></i>
                        <span><?= Yii::t('backend', 'Menu') ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/menu/index"><i class="fa fa-circle-o"></i> <?= Yii::t('backend', 'Menu') ?></a></li>
                        <li><a href="/admin/menu/menu"><i class="fa fa-circle-o"></i> <?= Yii::t('backend', 'Menu list') ?></a></li>
                    </ul>
                </li>
                <li>
                    <a href="/admin/blocks/index">
                        <i class="fa fa-file-code-o"></i> <span><?= Yii::t('backend', 'Content Blocks') ?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-picture-o"></i>
                        <span><?= Yii::t('backend', 'Banners') ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/banner/position"><i class="fa fa-list-alt"></i> <?= Yii::t('backend', 'Position') ?></a></li>
                        <li><a href="/admin/banner/banner"><i class="fa fa-list-ol"></i> <?= Yii::t('backend', 'Banner list') ?></a></li>
                        <li><a href="/admin/banner/company"><i class="fa fa-list-alt"></i> <?= Yii::t('backend', 'Adv companies') ?></a></li>
                        <li><a href="/admin/banner/statistics"><i class="fa fa-history"></i> <?= Yii::t('backend', 'Banner statistics') ?></a></li>
                    </ul>
                </li>
                <li>
                    <a href="/admin/playlist_file/archive">
                        <i class="fa fa-archive"></i> <span><?= Yii::t('backend', 'Archive') ?></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (Yii::$app->userAccess->userCan() || Yii::$app->userAccess->isRadioAdmin()) { ?>
                <li>
                    <a href="#">
                        <i class="fa fa-youtube"></i>
                        <span><?= Yii::t('backend', 'Programs') ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <!--<li><a href="/admin/youtube/channels"><i class="fa fa-video-camera"></i> <?php // Yii::t('backend', 'Programs channels')        ?></a></li>-->
                        <li><a href="/admin/youtube/playlists"><i class="fa fa-list-ol"></i> <?= Yii::t('backend', 'Progrems lists') ?></a></li>
                        <?php if (Yii::$app->userAccess->userCan()) { ?>
                            <li><a href="/admin/youtube/folders-archive"><i class="fa fa-youtube-play"></i> <?= Yii::t('backend', 'Programs folder') ?></a></li>
                        <?php } ?>
    <!--<li><a href="/admin/youtube/video"><i class="fa fa-youtube-play"></i> <?php // Yii::t('backend', 'Programs video')        ?></a></li>-->
                    </ul>
                </li>
            <?php } ?>
            <?php if (Yii::$app->userAccess->userCan() || Yii::$app->userAccess->isRadioAdmin() || Yii::$app->userAccess->isRadioManager()) { ?>
                <li>
                    <a href="#">
                        <i class="fa fa-question-circle"></i>
                        <span><?= Yii::t('backend', 'Questions') ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/contacts/online"><i class="fa fa-circle-o"></i> <?= Yii::t('backend', 'Online questions') ?></a></li>
                        <li><a href="/admin/contacts/email"><i class="fa fa-envelope-o"></i> <?= Yii::t('backend', 'Email questions') ?></a></li>
                    </ul>
                </li>
                <!--
                <li>
                    <a href="#">
                        <i class="fa fa-soundcloud"></i>
                        <span><?php // Yii::t('backend', 'Soundcloud')             ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/soundcloud/index"><i class="fa fa-th-list"></i> <?php // Yii::t('backend', 'Soundcloud list')             ?></a></li>
                    </ul>
                </li>
                -->
                <li>
                    <a href="/admin/playlist_file/index">
                        <i class="fa fa-music"></i> <span><?= Yii::t('backend', 'Broadcasts') ?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-bar-chart"></i>
                        <span><?= Yii::t('backend', 'Vote') ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/vote/questions"><i class="fa fa-question"></i> <?= Yii::t('backend', 'Questions') ?></a></li>
                        <li><a href="/admin/vote/answers"><i class="fa fa-list-ol"></i> <?= Yii::t('backend', 'Answers') ?></a></li>
                        <li><a href="/admin/vote/votes"><i class="fa fa-check"></i> <?= Yii::t('backend', 'Votes') ?></a></li>
                    </ul>
                </li>
                <li>
                    <a href="/admin/top_video/index">
                        <i class="fa fa-file-code-o"></i> <span><?= Yii::t('backend', 'Top Video') ?></span>
                    </a>
                </li>
                <li>
                    <a href="/admin/radioquiz/index">
                        <i class="fa fa-file-code-o"></i> <span><?= Yii::t('backend', 'Radioquiz') ?></span>
                    </a>
                </li>
                <li>
                    <a href="/admin/songs/index">
                        <i class="fa fa-file-code-o"></i> <span><?= Yii::t('backend', 'Songs list') ?></span>
                    </a>
                </li>
            <?php } ?>
            <li>
                <a href="#">
                    <i class="fa fa-bullhorn"></i>
                    <span><?= Yii::t('backend', 'Votes on Air') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/onlinevotes/index/index"><i class="fa fa-bar-chart"></i> <?= Yii::t('backend', 'Votes') ?></a></li>
                    <li><a href="/admin/onlinevotes/index/create"><i class="fa fa-plus-circle"></i> <?= Yii::t('backend', 'Create') ?></a></li>
                </ul>
            </li>
            <li>
                <a href="/admin/opinions/index">
                    <i class="fa fa-cogs"></i> <span><?= Yii::t('backend', 'Opinions and wishs') ?></span>
                </a>
            </li>
            <?php if (Yii::$app->userAccess->userCan()) { ?>
                <li>
                    <a href="/admin/seo_content/index">
                        <i class="fa fa-archive"></i> <span><?= Yii::t('backend', 'SEO') ?></span>
                    </a>
                </li>
                <li>
                    <a href="/admin/tags/index">
                        <i class="fa fa-font"></i> <span><?= Yii::t('backend', 'Tags') ?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-image"></i>
                        <span><?= Yii::t('backend', 'Gallery') ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/gallery/index/index"><i class="fa fa-sitemap"></i> <?= Yii::t('backend', 'Galleries') ?></a></li>
                        <li><a href="/admin/gallery/content/index"><i class="fa fa-image"></i> <?= Yii::t('backend', 'Images') ?></a></li>
                    </ul>
                </li>
            <?php } ?>
            <?php if (Yii::$app->userAccess->userCan() || Yii::$app->userAccess->isRadioAdmin()) { ?>
                <li>
                    <a href="/admin/timetable/index">
                        <i class="fa fa-calendar"></i> <span><?= Yii::t('backend', 'Timetable') ?></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (Yii::$app->userAccess->userCan()) { ?>
                <!--
                <li>
                    <a href="/admin/sitemap/sitemap/index">
                        <i class="fa fa-map-o"></i> <span><?php // Yii::t('backend', 'Site map')             ?></span>
                    </a>
                </li>
                -->
                <li>
                    <a href="/admin/settings/index">
                        <i class="fa fa-cogs"></i> <span><?= Yii::t('backend', 'Settings') ?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span><?= Yii::t('user', 'Users') ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/user/admin/index"><i class="fa fa-user"></i> <?= Yii::t('backend', 'User') ?></a></li>
                        <li><a href="/admin/rbac/role/index"><i class="fa fa-street-view"></i> <?= Yii::t('user', 'Roles') ?></a></li>
                        <li><a href="/admin/rbac/permission/index"><i class="fa fa-unlock-alt"></i> <?= Yii::t('user', 'Permissions') ?></a></li>
                        <li><a href="/admin/rbac/rule/index"><i class="fa fa-file-text-o"></i> <?= Yii::t('user', 'Rules') ?></a></li>
                        <li>
                            <a href="#"><i class="fa fa-plus"></i> <?= Yii::t('user', 'Create') ?>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/admin/user/admin/create"><i class="fa fa-user-plus"></i> <?= Yii::t('user', 'New user') ?></a></li>
                                <li><a href="/admin/rbac/role/create"><i class="fa fa-street-view"></i> <?= Yii::t('user', 'New role') ?></a></li>
                                <li><a href="/admin/rbac/permission/create"><i class="fa fa-unlock-alt"></i> <?= Yii::t('user', 'New permission') ?></a></li>
                                <li><a href="/admin/rbac/rule/create"><i class="fa fa-file-text-o"></i> <?= Yii::t('user', 'New rule') ?></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-rocket"></i>
                        <span><?= Yii::t('backend', 'Advertising') ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/advertising/adv-calendar/index"><i class="fa fa-calendar"></i> <?= Yii::t('backend', 'Advertising schedule') ?></a></li>
                        <li><a href="/admin/advertising/adv-calendar/index-by-date"><i class="fa fa-calendar-times-o"></i> <?= Yii::t('backend', 'Fulfillment') ?></a></li>
                        <li><a href="/admin/advertising/adv-orders/index"><i class="fa fa-circle-o"></i> <?= Yii::t('backend', 'Advertising Orders') ?></a></li>
                        <li><a href="/admin/advertising/clients/index"><i class="fa fa-users"></i> <?= Yii::t('backend', 'Advertisers') ?></a></li>
                        <li><a href="/admin/reclams/audio"><i class="fa fa-volume-up"></i> <?= Yii::t('backend', 'Advertisings Archive') ?></a></li>
                        <li><a href="/admin/advertising/adv-ordersorting"><i class="fa fa-sort"></i> <?= Yii::t('backend', 'List of orders in blocks') ?></a></li>
                        <li><a href="/admin/advertising/adv-mesh/index"><i class="fa fa-calendar-check-o"></i> <?= Yii::t('backend', 'Advertising blocks') ?></a></li>
                        <li><a href="/admin/advertising/adv-settings/index"><i class="fa fa-cogs"></i> <?= Yii::t('backend', 'Advertising Settings') ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <?php if (Yii::$app->userAccess->userCan()) { ?>
                <!--
                <li>
                    <a href="/admin/different/index">
                        <i class="fa fa-file-code-o"></i> <span><?php // Yii::t('backend', 'Content Table')             ?></span>
                    </a>
                </li>
                -->
            <?php } ?>
            <!--
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Layout Options</span>
                    <span class="pull-right-container">
                        <span class="label label-primary pull-right">4</span>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
                    <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
                    <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
                </ul>
            </li>
            <li>
                <a href="pages/widgets.html">
                    <i class="fa fa-th"></i> <span>Widgets</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">new</small>
                    </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Charts</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                    <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                    <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                    <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>UI Elements</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                    <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                    <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                    <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                    <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                    <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Forms</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                    <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                    <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Tables</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                    <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
                </ul>
            </li>
            <li>
                <a href="pages/calendar.html">
                    <i class="fa fa-calendar"></i> <span>Calendar</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-red">3</small>
                        <small class="label pull-right bg-blue">17</small>
                    </span>
                </a>
            </li>
            <li>
                <a href="pages/mailbox/mailbox.html">
                    <i class="fa fa-envelope"></i> <span>Mailbox</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-yellow">12</small>
                        <small class="label pull-right bg-green">16</small>
                        <small class="label pull-right bg-red">5</small>
                    </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Examples</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
                    <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
                    <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
                    <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
                    <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
                    <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
                    <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
                    <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
                    <li><a href="pages/examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-share"></i> <span>Multilevel</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                    <li>
                        <a href="#"><i class="fa fa-circle-o"></i> Level One
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                            <li>
                                <a href="#"><i class="fa fa-circle-o"></i> Level Two
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                </ul>
            </li>
            <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
            -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

