<?php

namespace backend\components;

use Yii,
    yii\base\Component,
    yii\helpers\ArrayHelper,
    common\models\Site;

class Siteselector extends Component {

    public $cookies;

    public function init() {
        parent::init();

        $this->cookies = Yii::$app->request->cookies;
    }

    public function getSites() {
        return ArrayHelper::map(Site::find()->where(['status' => 1])->asArray()->all(), 'id', 'name');
    }

    public function getSitesCookie() {
        $siteID = (isset($_COOKIE['siteID'])) ? (int) $_COOKIE['siteID'] : 1;
        return $siteID;
    }

    public function getSiteLangugeID() {
        $objsite = Site::findOne($this->getSitesCookie());
        $lagid = $objsite->lang->id;

        return $lagid;
    }

    public function getSiteName() {
        $objsite = Site::findOne($this->getSitesCookie());
        $name = $objsite->name;

        return $name;
    }

}
