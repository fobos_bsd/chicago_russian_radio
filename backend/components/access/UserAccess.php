<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\components\access;

use Yii,
    yii\base\BaseObject;

class UserAccess extends BaseObject {

    public $userID, $role;

    public function init() {
        parent::init();

        if (!Yii::$app->user->isGuest) {
            $this->userID = Yii::$app->user->id;
            $this->role = Yii::$app->authManager->getRolesByUser($this->userID);
        } else {
            $this->userID = 0;
            $this->role = 'guest';
        }
    }

    // Get all user roles
    public function getRoles() {
        $result = array();

        foreach ($this->role as $role) {
            array_push($result, $role->name);
        }

        return $result;
    }

    // If user is admin
    public function userCan() {
        $result = false;
        foreach ($this->role as $role) {
            if ($role->name === 'Администраторы') {
                $result = true;
                break;
            }
        }
        return $result;
    }

    // If user Radio admin
    public function isRadioAdmin() {
        $result = false;
        foreach ($this->role as $role) {
            if ($role->name === 'Radioadmins') {
                $result = true;
                break;
            }
        }
        return $result;
    }

    // If user Radio manager
    public function isRadioManager() {
        $result = false;
        foreach ($this->role as $role) {
            if ($role->name === 'Менеджеры') {
                $result = true;
                break;
            }
        }
        return $result;
    }

}
