<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        'css/AdminLTE.css',
        'css/skins/_all-skins.min.css',
        'plugins/iCheck/flat/blue.css',
        'plugins/morris/morris.css',
        'plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        //'plugins/datepicker/datepicker3.css',
        'plugins/daterangepicker/daterangepicker.css',
        //'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'plugins/datatables/dataTables.bootstrap.css',
        'css/jquery-ui.min.css',
        'css/main.css',
        'css/ace.min.css',
        'css/colorbox.min.css',
        'css/ace-skins.min.css',
        'css/ace-rtl.min.css'
    ];
    public $js = [
        'js/ace-extra.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\bootstrap\BootstrapThemeAsset',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

}
