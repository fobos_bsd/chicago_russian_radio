<?php

namespace backend\controllers;

use Yii,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\web\NotFoundHttpException,
    yii\helpers\ArrayHelper,
    yii\helpers\Json;
use common\models\LoginForm,
    common\models\OnlineVotes,
    common\models\Settings,
    common\models\CurrentPhoneVote,
    common\models\CurrentSmsVote,
    common\models\ArhPhoneVote,
    common\models\ArhSmsVote,
    common\models\AdvOrders,
    common\models\GoogleStatistic;

/**
 * Site controller
 */
class StartController extends Controller {

    protected $settings;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'index', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function init() {
        $this->settings = ArrayHelper::map(Settings::find()->all(), 'name', 'value');
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '@backend/views/start/error.php'
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/login');
        }

        $roles = Yii::$app->userAccess->getRoles();

        if (array_search('advertising_managers', $roles) !== false) {
            return $this->redirect('/admin/advertising/adv-calendar');
        }

        // Get last phone vote ID
        $objpid = $this->getLastPhoneVoteId();
        if (isset($objpid->online_votes_id)) {
            $phid = (int) $objpid->online_votes_id;
        }

        // Get last sms vote ID
        $objpid = $this->getLastSmsVoteId();
        if (isset($objpid->online_votes_id)) {
            $smsid = (int) $objpid->online_votes_id;
        }

        if (!isset($this->settings['vote_timeout']) || empty($this->settings['vote_timeout']))
            $this->settings['vote_timeout'] = 3600;

        $totalcountph = 0;
        if (isset($phid)) {
            $phvoteobj = $this->findVoteData($phid);

            // Direct type
            $timearh = (int) $phvoteobj->start_at + (int) $this->settings['vote_timeout'];

            // Get phone votes data
            $arrparams = (isset($phvoteobj->params) && $phvoteobj->vtype === 'phone') ? Json::decode($phvoteobj->params) : array();
            if (count($arrparams) > 0) :
                $i = 0;
                foreach ($arrparams as $key => $val) {
                    if ($timearh >= time() || $phvoteobj->status == 1) {
                        $count = (int) CurrentPhoneVote::find()->where(['online_votes_id' => $phid, 'vote_phone' => trim($key)])->count();
                    } elseif ($timearh < time() && $phvoteobj->status == 0) {
                        $count = (int) ArhPhoneVote::find()->where(['online_votes_id' => $phid, 'vote_phone' => trim($key)])->count();
                    }
                    $totalcountph += $count;
                    $arrcount[$i]['label'] = (string) trim($val) . ' (' . $count . ')';
                    $arrcount[$i]['value'] = $count;
                    $i++;
                }
            endif;
        }
        $objPhoneCount = (isset($arrcount) && count($arrcount) > 0) ? Json::encode($arrcount) : '[{}]';

        // Title active votes
        $titleobj = OnlineVotes::find()->where(['status' => 1, 'site_id' => Yii::$app->Siteselector->getSitesCookie()])->andWhere(['<=', 'start_at', time()])->orderBy(['finish_at' => SORT_DESC])->one();
        if (isset($titleobj->name))
            $totalcountsms = 0;
        if (isset($smsid)) {
            $smsvoteobj = $this->findVoteData($smsid);

            // Get sms votes data
            $arrparams = (isset($smsvoteobj->params) && $smsvoteobj->vtype === 'sms') ? Json::decode($smsvoteobj->params) : array();
            if (count($arrparams) > 0) :
                foreach ($arrparams as $params) {
                    $j = 0;
                    foreach ($params as $key => $val) {
                        $code = (int) trim($key);
                        if (!isset($timearh))
                            $timearh = time();
                        if ($timearh >= time() || $smsvoteobj->status == 1) {
                            $countsms = (int) CurrentSmsVote::find()->where(['online_votes_id' => $smsid, 'vote_sms' => $code])->count();
                        } elseif ($timearh < time() && $smsvoteobj->status == 0) {
                            $countsms = (int) ArhSmsVote::find()->where(['online_votes_id' => $smsid, 'vote_sms' => $code])->count();
                        }
                        $totalcountsms += $countsms;
                        $arrsmscount[$j]['label'] = (string) trim($val) . ' (' . $countsms . ')';
                        $arrsmscount[$j]['value'] = $countsms;
                        $j++;
                    }
                    break;
                }
            endif;
        }
        $objSmsCount = (isset($arrsmscount) && count($arrsmscount) > 0) ? Json::encode($arrsmscount) : '[{}]';

        // Google analytics total users
        //$analytics = new GoogleStatistic();
        //$statistic = $analytics->getStatistic(1);

        return $this->render('index', [
                    'objPhoneCount' => $objPhoneCount,
                    'objSmsCount' => $objSmsCount,
                    'phtheme' => (isset($phvoteobj->name)) ? $phvoteobj->name : '',
                    'phdescript' => (isset($phvoteobj->description)) ? $phvoteobj->description : '',
                    'smstheme' => (isset($smsvoteobj->name)) ? $smsvoteobj->name : '',
                    'smsdescript' => (isset($smsvoteobj->description)) ? $smsvoteobj->description : '',
                    'totalcountph' => (isset($totalcountph)) ? $totalcountph : 0,
                    'totalcountsms' => (isset($totalcountsms)) ? $totalcountsms : 0,
                    'curntname' => (isset($titleobj->name)) ? $titleobj->name : '',
                    'neworders' => AdvOrders::find()->where(['status' => 'New'])->count(),
                    'airorders' => AdvOrders::find()->where(['status' => 'On Air'])->andWhere(['>=', 'finish_at', date('Y-m-d')])->count(),
                    'site_id' => Yii::$app->Siteselector->getSitesCookie(),
                    //'users_year' => $statistic['total_sessions'],
                    'vote_id' => (isset($titleobj->id)) ? $titleobj->id : 0
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    // Get last phone vote
    private function getLastPhoneVoteId() {
        if (($model = CurrentPhoneVote::find()->select(['id', 'online_votes_id'])->orderBy(['id' => SORT_DESC])->one()) !== null) {
            return $model;
        } else {
            if (($model = ArhPhoneVote::find()->select(['id', 'online_votes_id'])->orderBy(['id' => SORT_DESC])->one()) !== null) {
                return $model;
            } else {
                return null;
            }
        }
    }

    // Get last phone vote
    private function getLastSmsVoteId() {
        if (($model = CurrentSmsVote::find()->select(['id', 'online_votes_id'])->orderBy(['id' => SORT_DESC])->one()) !== null) {
            return $model;
        } else {
            if (($model = ArhSmsVote::find()->select(['id', 'online_votes_id'])->orderBy(['id' => SORT_DESC])->one()) !== null) {
                return $model;
            } else {
                return false;
            }
        }
    }

    // Find vote data by ID
    protected function findVoteData($id) {
        if (($model = OnlineVotes::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
