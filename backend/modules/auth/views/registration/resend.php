<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Html;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\ResendForm $model
 */
$this->title = Yii::t('user', 'Request new confirmation message');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- PAGE CONTENT --> 
<div class="container">
    <div class="text-center">
        <?= Html::img(Yii::$app->basePath . '/img/logo.png', ['id' => 'logoimg', 'alt' => 'Logo']) ?>
    </div>
    <div class="tab-content">
        <?= Html::beginForm('', 'post', ['class' => 'form-signin']) ?>

        <?=
        Html::activeTextInput($model, 'email', [
            'autofocus' => 'autofocus',
            'class' => 'form-control',
            'placeholder' => 'Your E-mail'
        ])
        ?>
        <?= Html::error($model, 'email', ['class' => 'help-block']) ?>
        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken()) ?>

        <?= Html::submitButton(Yii::t('user', 'Continue'), ['class' => 'btn text-muted text-center btn-success']) ?><br>

        <?= Html::endForm() ?>
    </div>
</div>
