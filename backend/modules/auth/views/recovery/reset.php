<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Html;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\RecoveryForm $model
 */
$this->title = Yii::t('user', 'Reset your password');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- PAGE CONTENT --> 
<div class="container">
    <div class="text-center">
        <?= Html::img(Yii::$app->basePath . '/img/logo.png', ['id' => 'logoimg', 'alt' => 'Logo']) ?>
    </div>
    <div class="tab-content">
        <?= Html::beginForm('', 'post', ['class' => 'form-signin']) ?>
        <?=
        Html::activePasswordInput($model, 'password', [
            'autofocus' => 'autofocus',
            'class' => 'form-control',
            'placeholder' => 'Your Password',
        ])
        ?>
        <?= Html::error($model, 'password', ['class' => 'help-block']) ?>
        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken()) ?>

        <?= Html::submitButton(Yii::t('user', 'Finish'), ['class' => 'btn btn-success btn-block']) ?><br>

        <?= Html::endForm() ?>
    </div>
    <div class="text-center">
        <ul class="list-inline">
            <li><?= Html::a(Yii::t('user', 'Already registered? Login!'), ['/user/security/login'], ['class' => 'text-muted']) ?></li>
            <li><a class="text-muted" href="#forgot" data-toggle="tab">Forgot Password</a></li>
        </ul>
    </div>
</div>