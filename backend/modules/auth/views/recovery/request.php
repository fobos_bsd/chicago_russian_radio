<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Html,
    yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\RecoveryForm $model
 */
$this->title = Yii::t('user', 'Recover your password');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- PAGE CONTENT --> 
<div class="container">
    <div class="text-center">
        <?= Html::img(Yii::$app->basePath . '/img/logo.png', ['id' => 'logoimg', 'alt' => 'Logo']) ?>
    </div>
    <div class="tab-content">
        <?= Html::beginForm('', 'post', ['id' => 'password-recovery-form', 'class' => 'form-signin']) ?>

        <p class="text-muted text-center btn-block btn btn-primary btn-rect"><?= Html::encode($this->title) ?></p>

        <?=
        Html::activeTextInput($model, 'email', [
            'autofocus' => 'autofocus',
            'class' => 'form-control',
            'placeholder' => 'Your E-mail'
        ])
        ?>
        <?= Html::error($model, 'email', ['class' => 'help-block']) ?>

        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken()) ?>

        <?= Html::submitButton(Yii::t('user', 'Continue'), ['class' => 'btn text-muted text-center btn-success']) ?><br>

        <?= Html::endForm() ?>
    </div>
    <div class="text-center">
        <ul class="list-inline">
            <li>
            <li><?= Html::a(Yii::t('user', 'Already registered? Login!'), Url::to('/user/security/login'), ['class' => 'text-muted']) ?></li>
            </li>
            <li>
                <?= Html::a(Yii::t('user', 'Registered'), Url::to('/user/registration/register'), ['class' => 'text-muted']) ?>
            </li>
        </ul>
    </div>
</div>
