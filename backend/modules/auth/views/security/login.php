<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect,
    dektrium\user\models\LoginForm;
use yii\bootstrap\Html,
    yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */
$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<!-- PAGE CONTENT --> 
<div class="container">
    <div class="text-center">
        <?= Html::img('/frontend/web/img/logo.png', ['id' => 'logoimg', 'alt' => 'Logo']) ?>
    </div>
    <div class="tab-content">
        <div id="login" class="tab-pane active">
            <?= Html::beginForm('', 'post', ['class' => 'form-signin']) ?>

            <p class="text-muted text-center text-btn">
                Enter your username and password
            </p>

            <?=
            Html::activeTextInput($model, 'login', [
                'autofocus' => 'autofocus',
                'class' => 'form-control',
                'placeholder' => 'Username',
            ])
            ?>
            <?= Html::error($model, 'login', ['class' => 'help-block']) ?>

            <?=
            Html::activePasswordInput($model, 'password', [
                'class' => 'form-control',
                'placeholder' => 'Password'
            ])
            ?>
            <?= Html::error($model, 'password', ['class' => 'help-block']) ?>

            <?= Html::activeHiddenInput($model, 'rememberMe') ?>
            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken()) ?>

            <?=
            Html::submitButton(
                    Yii::t('user', 'Sign in'), ['class' => 'btn text-muted text-center btn-block btn-danger', 'tabindex' => '4']
            )
            ?>

            <?= Html::endForm() ?>
        </div>
    </div>
    <div class="text-center">
        <ul class="list-inline">
            <li>
                <?= Html::a(Yii::t('user', 'Forgot Password'), Url::to('/user/recovery/request'), ['class' => 'text-muted']) ?>
            </li>
            <li>
                <?= Html::a(Yii::t('user', 'Registered'), Url::to('/user/registration/register'), ['class' => 'text-muted']) ?>
            </li>
        </ul>
    </div>


</div>

<!--END PAGE CONTENT -->