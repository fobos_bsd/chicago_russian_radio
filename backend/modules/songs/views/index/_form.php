<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\Songs */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Song') : Yii::t('backend', 'Update Song') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="col-lg-6">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'url') ?>
        </div>      
        <div class="col-lg-3">
            <?= $form->field($model, 'rank') ?>
        </div>
        <div class="col-lg-2">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12">
            <?= $form->field($model, 'description')->textarea(['size' => 3]) ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'dropbox_id') ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'fileAudio')->input('file', ['class' => 'fileImage', 'data-default-file' => '/backend/web/uploads/songs/' . $model->fileAudio]) ?>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
$this->registerCssFile('admin/plugins/bower_components/dropify/dist/css/dropify.min.css', ['depends' => [backend\assets\AppAsset::className()]]);
$this->registerJsFile('admin/plugins/bower_components/dropify/dist/js/dropify.min.js', ['depends' => [backend\assets\BackendAsset::className()], 'position' => yii\web\View::POS_END]);

$script = <<< JS
    $(document).ready(function() {
        $('.fileImage').dropify();
    });
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);
?>
