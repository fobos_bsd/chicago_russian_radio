<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Songs */

$this->title = Yii::t('backend', 'Create Songs');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Songs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timetable-tags-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
