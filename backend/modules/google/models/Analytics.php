<?php

namespace backend\modules\google\models;

use Yii;
use Google_Client,
    Google_Service_Analytics,
    Google_Service_AnalyticsReporting,
    Google_Service_AnalyticsReporting_DateRange,
    Google_Service_AnalyticsReporting_Metric,
    Google_Service_AnalyticsReporting_ReportRequest,
    Google_Service_AnalyticsReporting_GetReportsRequest,
    Google_Service_AnalyticsReporting_Dimension;
use common\models\Settings;

/**
 * Description of Analytics
 *
 * @author fobos
 */
class Analytics extends \yii\base\Object {

    public $file_key = 'Ethnicmedia-f6f2176161c8.json';
    public $profileID = '150769003';
    public $analytics_v3, $analytics_v4, $results;

    /*
     * @start
     */

    public function init() {
        parent::init();

        // Get settings of the current site
        if (isset(Yii::$app->Siteselector)) {
            $siteID = Yii::$app->Siteselector->getSitesCookie();
        } else {
            $siteArr = Yii::$app->Siteinfo->getSite();
            $siteID = $siteArr['id'];
        }

        $settings = Settings::find()->where(['site_id' => $siteID])->asArray()->all();
        foreach ($settings as $par) {
            if ($par['name'] === 'analytic_profile_id')
                $this->profileID = $par['value'];
            if ($par['name'] === 'google_serviceacount_filename')
                $this->file_key = $par['value'];
        }

        $analytics = $this->initializeAnalytics();
        $this->analytics_v4 = $analytics['analytics_v4'];
        $this->analytics_v3 = $analytics['analytics_v3'];
    }

    public function getReportOneDay() {

        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("today");
        $dateRange->setEndDate("today");

        // Create the Metrics object.
        $sessions = new Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:users");
        $sessions->setAlias("users");

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($sessions));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));

        return $this->analytics_v4->reports->batchGet($body);
    }

    public function getReportMonthSessions() {

        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("31daysAgo");
        $dateRange->setEndDate("today");

        // Create the Metrics object.
        $users = new Google_Service_AnalyticsReporting_Metric();
        $users->setExpression("ga:sessions");
        $users->setAlias("sessions");

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($users));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));

        return $this->analytics_v4->reports->batchGet($body);
    }

    public function getReportMonthUsers() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("31daysAgo");
        $dateRange->setEndDate("today");

        // Create the Metrics object.
        $users = new Google_Service_AnalyticsReporting_Metric();
        $users->setExpression("ga:users");

        $sessions = new Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:sessions");

        //$source = new Google_Service_AnalyticsReporting_Dimension();
        //$source->setName('ga:source');
        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($users, $sessions));
        //$request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportWeekUsers() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("7daysAgo");
        $dateRange->setEndDate("today");

        // Create the Metrics object.
        $users = new Google_Service_AnalyticsReporting_Metric();
        $users->setExpression("ga:users");

        $sessions = new Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:sessions");

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($users, $sessions));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportDayUsers() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("today");
        $dateRange->setEndDate("today");

        // Create the Metrics object.
        $users = new Google_Service_AnalyticsReporting_Metric();
        $users->setExpression("ga:users");

        $sessions = new Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:sessions");

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($users, $sessions));
        //$request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportYearUsers() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("365daysAgo");
        $dateRange->setEndDate("today");

        // Create the Metrics object.
        $users = new Google_Service_AnalyticsReporting_Metric();
        $users->setExpression("ga:users");

        $sessions = new Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:sessions");

        //$source = new Google_Service_AnalyticsReporting_Dimension();
        //$source->setName('ga:source');
        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($users, $sessions));
        //$request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportYearLocation() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("365daysAgo");
        $dateRange->setEndDate("today");

        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName('ga:country');

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        //$request->setMetrics(array($users, $sessions));
        $request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportMonthLocation() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("31daysAgo");
        $dateRange->setEndDate("today");

        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName('ga:country');

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        //$request->setMetrics(array($users, $sessions));
        $request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportWeekLocation() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("7daysAgo");
        $dateRange->setEndDate("today");

        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName('ga:country');

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        //$request->setMetrics(array($users, $sessions));
        $request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportDayLocation() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("today");
        $dateRange->setEndDate("today");

        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName('ga:country');

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        //$request->setMetrics(array($users, $sessions));
        $request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportYearSource() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("365daysAgo");
        $dateRange->setEndDate("today");

        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName('ga:medium');

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        //$request->setMetrics(array($users, $sessions));
        $request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportMonthSource() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("31daysAgo");
        $dateRange->setEndDate("today");

        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName('ga:medium');

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        //$request->setMetrics(array($users, $sessions));
        $request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportWeekSource() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("7daysAgo");
        $dateRange->setEndDate("today");

        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName('ga:medium');

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        //$request->setMetrics(array($users, $sessions));
        $request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    public function getReportDaySource() {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("today");
        $dateRange->setEndDate("today");

        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName('ga:medium');

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->profileID);
        $request->setDateRanges($dateRange);
        //$request->setMetrics(array($users, $sessions));
        $request->setDimensions(array($source));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $response = $this->analytics_v4->reports->batchGet($body);
        return $response->getReports();
    }

    /*
     * @return real time results
     */

    public function realTimeReporting() {
        $optParams = array(
            'dimensions' => 'rt:medium');

        $result = null;
        try {
            $result = $this->analytics_v3->data_realtime->get(
                    'ga:' . $this->profileID, 'rt:activeUsers', $optParams);
            // Success. 
        } catch (apiServiceException $e) {
            // Handle API service exceptions.
            $error = $e->getMessage();
        }

        return $result;
    }

    /*
     * @return analytics object
     */

    private function initializeAnalytics() {
        /* Creates and returns the Analytics Reporting service object.

          Use the developers console and download your service account
          credentials in JSON format. Place them in this directory or
          change the key file location if necessary.
         * */

        $KEY_FILE_LOCATION = __DIR__ . '/' . $this->file_key;

        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("Hello Analytics Reporting");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);

        $analytics = [
            'analytics_v3' => new Google_Service_Analytics($client),
            'analytics_v4' => new Google_Service_AnalyticsReporting($client),
        ];

        return $analytics;
    }

}
