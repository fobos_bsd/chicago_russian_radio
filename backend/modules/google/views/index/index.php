<?php

use yii\bootstrap\Html,
    yii\helpers\Url;

$this->title = Yii::t('backend', 'Google Analytics');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-bordered googleanalytic">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('backend', 'Статистика посещений') ?></h3>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For year') ?></th>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For month') ?></th>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For week') ?></th>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For day') ?></th>
                </tr>
                <tr>
                    <th><?= Yii::t('backend', 'Users') ?></th><th><?= Yii::t('backend', 'Sessions') ?></th>
                    <th><?= Yii::t('backend', 'Users') ?></th><th><?= Yii::t('backend', 'Sessions') ?></th>
                    <th><?= Yii::t('backend', 'Users') ?></th><th><?= Yii::t('backend', 'Sessions') ?></th>
                    <th><?= Yii::t('backend', 'Users') ?></th><th><?= Yii::t('backend', 'Sessions') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <?php
                    foreach ($users_year as $report) {
                        $rows = $report->getData()->getRows();

                        foreach ($rows as $row) {
                            $dimensions = $row->getDimensions();
                            $metrics = $row->getMetrics();

                            foreach ($metrics as $values) {
                                foreach ($values->getValues() as $value) {
                                    ?>
                                    <td><?= $value ?></td>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                    <?php
                    foreach ($users_month as $report) {
                        $rows = $report->getData()->getRows();

                        foreach ($rows as $row) {
                            $dimensions = $row->getDimensions();
                            $metrics = $row->getMetrics();

                            foreach ($metrics as $values) {
                                foreach ($values->getValues() as $value) {
                                    ?>
                                    <td><?= $value ?></td>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                    <?php
                    foreach ($users_week as $report) {
                        $rows = $report->getData()->getRows();

                        foreach ($rows as $row) {
                            $dimensions = $row->getDimensions();
                            $metrics = $row->getMetrics();

                            foreach ($metrics as $values) {
                                foreach ($values->getValues() as $value) {
                                    ?>
                                    <td><?= $value ?></td>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                    <?php
                    foreach ($users_day as $report) {
                        $rows = $report->getData()->getRows();

                        foreach ($rows as $row) {
                            $dimensions = $row->getDimensions();
                            $metrics = $row->getMetrics();

                            foreach ($metrics as $values) {
                                foreach ($values->getValues() as $value) {
                                    ?>
                                    <td><?= $value ?></td>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="clearfix"></div>
<div class="panel panel-bordered">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('backend', 'Статистика по странам') ?></h3>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For year') ?></th>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For month') ?></th>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For week') ?></th>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For day') ?></th>
                </tr>
                <tr>
                    <th><?= Yii::t('backend', 'Country') ?></th><th><?= Yii::t('backend', 'Users') ?></th>
                    <th><?= Yii::t('backend', 'Country') ?></th><th><?= Yii::t('backend', 'Users') ?></th>
                    <th><?= Yii::t('backend', 'Country') ?></th><th><?= Yii::t('backend', 'Users') ?></th>
                    <th><?= Yii::t('backend', 'Country') ?></th><th><?= Yii::t('backend', 'Users') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">
                        <table class="table table-bordered">
                            <tbody>
                                <?php
                                foreach ($location_year as $report) {
                                    $header = $report->getColumnHeader();
                                    $rows = $report->getData()->getRows();
                                    foreach ($rows as $row) {
                                        ?>
                                        <tr>
                                            <?php
                                            $dimensions = $row->getDimensions();
                                            $metrics = $row->getMetrics();
                                            foreach ($dimensions as $dimension) {
                                                ?>
                                                <td><?= $dimension ?></td>
                                                <?php
                                            }

                                            foreach ($metrics as $values) {
                                                foreach ($values->getValues() as $value) {
                                                    ?>
                                                    <td><?= $value ?></td>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                    <td colspan="2">
                        <table class="table table-bordered">
                            <tbody>
                                <?php
                                foreach ($location_month as $report) {
                                    $header = $report->getColumnHeader();
                                    $rows = $report->getData()->getRows();
                                    foreach ($rows as $row) {
                                        ?>
                                        <tr>
                                            <?php
                                            $dimensions = $row->getDimensions();
                                            $metrics = $row->getMetrics();
                                            foreach ($dimensions as $dimension) {
                                                ?>
                                                <td><?= $dimension ?></td>
                                                <?php
                                            }

                                            foreach ($metrics as $values) {
                                                foreach ($values->getValues() as $value) {
                                                    ?>
                                                    <td><?= $value ?></td>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                    <td colspan="2">
                        <table class="table table-bordered">
                            <tbody>
                                <?php
                                foreach ($location_week as $report) {
                                    $header = $report->getColumnHeader();
                                    $rows = $report->getData()->getRows();
                                    foreach ($rows as $row) {
                                        ?>
                                        <tr>
                                            <?php
                                            $dimensions = $row->getDimensions();
                                            $metrics = $row->getMetrics();
                                            foreach ($dimensions as $dimension) {
                                                ?>
                                                <td><?= $dimension ?></td>
                                                <?php
                                            }

                                            foreach ($metrics as $values) {
                                                foreach ($values->getValues() as $value) {
                                                    ?>
                                                    <td><?= $value ?></td>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                    <td colspan="2">
                        <table class="table table-bordered">
                            <tbody>
                                <?php
                                foreach ($location_day as $report) {
                                    $header = $report->getColumnHeader();
                                    $rows = $report->getData()->getRows();
                                    foreach ($rows as $row) {
                                        ?>
                                        <tr>
                                            <?php
                                            $dimensions = $row->getDimensions();
                                            $metrics = $row->getMetrics();
                                            foreach ($dimensions as $dimension) {
                                                ?>
                                                <td><?= $dimension ?></td>
                                                <?php
                                            }

                                            foreach ($metrics as $values) {
                                                foreach ($values->getValues() as $value) {
                                                    ?>
                                                    <td><?= $value ?></td>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
<div class="clearfix"></div>
<div class="panel panel-bordered">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('backend', 'Статистика по источникам перехода') ?></h3>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For year') ?></th>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For month') ?></th>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For week') ?></th>
                    <th colspan="2" class="text-center"><?= Yii::t('backend', 'For day') ?></th>
                </tr>
                <tr>
                    <th><?= Yii::t('backend', 'Source') ?></th><th><?= Yii::t('backend', 'Count') ?></th>
                    <th><?= Yii::t('backend', 'Source') ?></th><th><?= Yii::t('backend', 'Count') ?></th>
                    <th><?= Yii::t('backend', 'Source') ?></th><th><?= Yii::t('backend', 'Count') ?></th>
                    <th><?= Yii::t('backend', 'Source') ?></th><th><?= Yii::t('backend', 'Count') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">
                        <table class="table table-bordered">
                            <tbody>
                                <?php
                                foreach ($source_year as $report) {
                                    $header = $report->getColumnHeader();
                                    $rows = $report->getData()->getRows();
                                    foreach ($rows as $row) {
                                        ?>
                                        <tr>
                                            <?php
                                            $dimensions = $row->getDimensions();
                                            $metrics = $row->getMetrics();
                                            foreach ($dimensions as $dimension) {
                                                ?>
                                                <td><?= $dimension ?></td>
                                                <?php
                                            }

                                            foreach ($metrics as $values) {
                                                foreach ($values->getValues() as $value) {
                                                    ?>
                                                    <td><?= $value ?></td>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                    <td colspan="2">
                        <table class="table table-bordered">
                            <tbody>
                                <?php
                                foreach ($source_month as $report) {
                                    $header = $report->getColumnHeader();
                                    $rows = $report->getData()->getRows();
                                    foreach ($rows as $row) {
                                        ?>
                                        <tr>
                                            <?php
                                            $dimensions = $row->getDimensions();
                                            $metrics = $row->getMetrics();
                                            foreach ($dimensions as $dimension) {
                                                ?>
                                                <td><?= $dimension ?></td>
                                                <?php
                                            }

                                            foreach ($metrics as $values) {
                                                foreach ($values->getValues() as $value) {
                                                    ?>
                                                    <td><?= $value ?></td>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                    <td colspan="2">
                        <table class="table table-bordered">
                            <tbody>
                                <?php
                                foreach ($source_week as $report) {
                                    $header = $report->getColumnHeader();
                                    $rows = $report->getData()->getRows();
                                    foreach ($rows as $row) {
                                        ?>
                                        <tr>
                                            <?php
                                            $dimensions = $row->getDimensions();
                                            $metrics = $row->getMetrics();
                                            foreach ($dimensions as $dimension) {
                                                ?>
                                                <td><?= $dimension ?></td>
                                                <?php
                                            }

                                            foreach ($metrics as $values) {
                                                foreach ($values->getValues() as $value) {
                                                    ?>
                                                    <td><?= $value ?></td>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                    <td colspan="2">
                        <table class="table table-bordered">
                            <tbody>
                                <?php
                                foreach ($source_day as $report) {
                                    $header = $report->getColumnHeader();
                                    $rows = $report->getData()->getRows();
                                    foreach ($rows as $row) {
                                        ?>
                                        <tr>
                                            <?php
                                            $dimensions = $row->getDimensions();
                                            $metrics = $row->getMetrics();
                                            foreach ($dimensions as $dimension) {
                                                ?>
                                                <td><?= $dimension ?></td>
                                                <?php
                                            }

                                            foreach ($metrics as $values) {
                                                foreach ($values->getValues() as $value) {
                                                    ?>
                                                    <td><?= $value ?></td>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


