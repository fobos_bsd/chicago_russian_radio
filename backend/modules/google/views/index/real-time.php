<?php

use yii\bootstrap\Html,
    yii\helpers\Url;
?>

<?php
if (count($data->getRows()) > 0) {
    $table = '<table>';

    // Print headers.
    $table .= '<tr>';

    foreach ($data->getColumnHeaders() as $header) {
      $table .= '<th>' . $header->name . '</th>';
    }
    $table .= '</tr>';

    // Print table rows.
    foreach ($data->getRows() as $row) {
      $table .= '<tr>';
        foreach ($row as $cell) {
          $table .= '<td>'
                 . htmlspecialchars($cell, ENT_NOQUOTES)
                 . '</td>';
        }
      $table .= '</tr>';
    }
    $table .= '</table>';

  } else {
    $table .= '<p>No Results Found.</p>';
  }
  print $table;

?>
