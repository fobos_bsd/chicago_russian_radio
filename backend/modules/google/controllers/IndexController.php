<?php

namespace backend\modules\google\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl;
use backend\modules\google\models\Analytics,
    common\models\Settings;

/**
 * Description of IndexController
 *
 * @author fobos
 */
class IndexController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'real-time'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all GoogleAnalitics models.
     * @return mixed
     */
    public function actionIndex() {
        $analitic = new Analytics();
        //var_dump($analitic->realTimeReporting());

        return $this->render('index', [
                    'users_year' => $analitic->getReportYearUsers(),
                    'users_month' => $analitic->getReportMonthUsers(),
                    'users_week' => $analitic->getReportWeekUsers(),
                    'users_day' => $analitic->getReportDayUsers(),
                    'location_year' => $analitic->getReportYearLocation(),
                    'location_month' => $analitic->getReportMonthLocation(),
                    'location_week' => $analitic->getReportWeekLocation(),
                    'location_day' => $analitic->getReportDayLocation(),
                    'source_year' => $analitic->getReportYearSource(),
                    'source_month' => $analitic->getReportMonthSource(),
                    'source_week' => $analitic->getReportWeekSource(),
                    'source_day' => $analitic->getReportDaySource(),
        ]);
    }

    /**
     * Lists all GoogleAnalitics real time.
     * @return mixed
     */
    public function actionRealTime() {
        $analitic = new Analytics();

        return $this->render('real-time', [
                    'data' => $analitic->realTimeReporting()
        ]);
    }

}
