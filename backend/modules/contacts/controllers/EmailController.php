<?php

namespace backend\modules\contacts\controllers;

use backend\modules\contacts\models\ReplyForm;
use common\models\search\UserQuestionsSearch;
use common\models\UserQuestions;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter,
    yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * EmailController implements the CRUD actions for UserQuestions model.
 */
class EmailController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'reply', 'view', 'delete'],
                        'roles' => ['Администраторы', 'Менеджеры','Radioadmins'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actionIndex() {
        $searchModel = new UserQuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReply($id) {
        $data = $this->findModel($id);
        $data->scenario = UserQuestions::SCENARIO_UPDATE_TIME;

        $model = new ReplyForm();

        $model->email = $data->email;
        $model->question = $data->question;

        if ($model->load(Yii::$app->request->post())) {
            $data->answer_at = time();
            if ($data->save()) {
                $model->sendEmail();
                return $this->redirect(['index']);
            }
        }

        return $this->render('reply-form', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);

        $model->scenario = UserQuestions::SCENARIO_UPDATE_STATUS;

        if (!$model->status) {
            $model->status = 1;
            $model->save();
        }

        return $this->render('view', [
                    'model' => $model
        ]);
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the BannerItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserQuestions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = UserQuestions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
