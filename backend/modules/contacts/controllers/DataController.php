<?php

namespace backend\modules\contacts\controllers;

use Yii,
    yii\web\Controller,
    yii\web\Response,
    yii\filters\AccessControl,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter;
use common\models\OnlineQuestions,
    common\models\UserQuestions;

class DataController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'ajax-data', 'email-ajax-data'],
                        'roles' => ['Администраторы','Radioadmins'],
                    ]
                ],
            ],
        ];
    }

    public function actionAjaxData() {
        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $post) {

            $data = OnlineQuestions::find()->where(['online_questions' => $post['online_questions']])->one();
            $data->scenario = OnlineQuestions::SCENARIO_UPDATE_STATUS;

            if (!$data->status) {
                $data->status = 1;
                $data->save();
            }

            $response = [
                'name' => $data->username,
                'phone' => ($data->phone) ? $data->phone : null,
                'time' => date('H:s', $data->created_at),
                'message' => $data->question
            ];

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $response;
        }
    }

    public function actionEmailAjaxData() {
        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $post) {

            $data = UserQuestions::find()->where(['id' => $post['id']])->one();
            $data->scenario = UserQuestions::SCENARIO_UPDATE_STATUS;

            if (!$data->status) {
                $data->status = 1;
                $data->save();
            }

            $response = [
                'name' => $data->usename,
                'phone' => ($data->phone) ? $data->phone : null,
                'time' => date('H:s', $data->created_at),
                'message' => $data->question,
            ];

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $response;
        }
    }

}
