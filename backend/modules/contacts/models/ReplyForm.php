<?php

namespace backend\modules\contacts\models;

use Yii;
use yii\base\Model;

/**
 * ReplyForm is the model behind the contact form.
 */
class ReplyForm extends Model
{
    public $question;
    public $email;
    public $reply;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email', 'question','reply'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose('reply-html',['data' => $this])
            ->setTo($this->email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Reply for you question.')
            ->send();
    }
}
