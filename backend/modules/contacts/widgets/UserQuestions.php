<?php
namespace backend\modules\contacts\widgets;

use Yii;
use yii\base\Widget;

/**
 * UserQuestions
 * @package backend\modules\contacts\widgets
 */
class UserQuestions extends Widget
{

    public function run()
    {
        $data = \common\models\UserQuestions::find()->where(['status' => 0, 'site_id' => Yii::$app->Siteselector->getSitesCookie()])->all();

        return $this->render('user-questions',['data' => $data]);
    }
}
