

<li class="dropdown messages-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-envelope-o"></i>
        <span class="label label-success"><?= count($data) ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <?= count($data) ?> messages</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <?php if ($data) : ?>
                    <?php foreach ($data as $item) : ?>
                        <li><!-- start message -->
                            <a href="#" data-toggle="modal" data-target="#email-question-modal" onclick="getData(<?= $item->id ?>)">
                                <h4 style="margin-left: 10px">
                                    <span class="phone-name"><?= $item->usename ?></span>
                                    <?php if ($item->phone) : ?>
                                    <br>
                                    <span class="phone-online"><?= $item->phone ?></span>
                                    <?php endif; ?>
                                    <small><i class="fa fa-clock-o" style="padding-right: 3px;"></i><?= date('H:s', $item->created_at) ?></small>
                                </h4>
                                <p style="margin-left: 10px;white-space: normal;"><?= substr($item->question, 0, 60) ?>...</p>
                            </a>
                        </li>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li><div class="no-message text-center" style="margin-top: 30%;">No messages.</div></li>
                <?php endif; ?>
                <!-- end message -->
            </ul>
        </li>
        <li class="footer"><a href="/admin/contacts/online">See All Messages</a></li>
    </ul>
</li>

<div id="email-question-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Question from : Loading...</h4>
                <div class="other-info-online">
                    <i class="fa fa-clock-o" aria-hidden="true" style="padding-right: 3px;"></i><small class="time">Loading...</small>
                    <i class="fa fa-phone-square" aria-hidden="true" style="padding-right: 3px;"></i><small class="phone">Loading...</small>
                </div>
            </div>
            <div class="modal-body">
                <div class="loader-online text-center"><img src="/frontend/web/img/b-loader.gif" alt=""></div>
                <p class="online-message"></p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default"><?= Yii::t('backend','Reply') ?></t></a>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<< JS

    function getData(id) {

        let action = '/admin/contacts/data/email-ajax-data';

        $.ajax({
           method: 'POST',
           dataType: 'text',
           url: action,
           data: {id: id},

           success: function (response) {
               $('.loader-online').hide('fast');
               let data = JSON.parse(response);

               $('.modal-title').text('Question from: ' + data.name);
               $('.online-message').text(data.message);
               $('.other-info-online>.time').text(data.time);
               $('.modal-footer>a').attr('href', '/admin/contacts/email/reply/'+ id);

               if(data.phone){
                   $('.other-info-online>.phone').text(data.phone);
               }
               else
                   $('.other-info-online>.phone').text('none');
           },
           error: function(e) {
             console.log(e);
           }
        });
    }
JS;

$this->registerJs($script, yii\web\View::POS_END);
?>
