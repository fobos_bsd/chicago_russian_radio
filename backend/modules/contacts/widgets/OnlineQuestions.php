<?php
namespace backend\modules\contacts\widgets;

use Yii;
use yii\base\Widget;

/**
 * OnlineQuestions
 * @package backend\modules\contacts\widgets
 */
class OnlineQuestions extends Widget
{

    public function run()
    {
        $data = \common\models\OnlineQuestions::find()->where(['status' => 0, 'site_id' => Yii::$app->Siteselector->getSitesCookie()])->all();

        return $this->render('online-questions',['data' => $data]);
    }
}
