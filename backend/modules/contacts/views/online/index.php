<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Online Questions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box">
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    [
                        'attribute' => 'online_questions',
                        'label' => 'ID',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    [
                        'attribute' => 'username',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    [
                        'attribute' => 'question',
                        'value' => function ($data) {
                            return  substr($data->question, 0, 60) . '...';
                        },
                    ],
                    [
                        'header' => Yii::t('common', 'Статус'),
                        'format' => 'raw',
                        'value' => function($data){
                            return ($data->status) ? '<span class="label label-success">'.Yii::t('backend','Просмотренно').'</span>'
                                : '<span class="label label-warning">'.Yii::t('backend','Не просмотренно').'</span>';
                        }
                    ],
                    [
                        'attribute' => 'created_at',
                        'headerOptions' => ['class' => 'sorting'],
                        'value' => function ($data) {
                            return  date('H:s', $data->created_at);
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{view} &nbsp; {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye" style="color: #0073b7;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url);
                            },
                        ],
                    ],
                ],
            ])
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
