<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Email Questions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box">
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'ID',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    [
                        'attribute' => 'usename',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    [
                        'attribute' => 'email',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    [
                        'attribute' => 'question',
                        'value' => function ($data) {
                            return substr($data->question, 0, 60) . '...';
                        },
                    ],
                    [
                        'header' => Yii::t('common', 'Статус'),
                        'format' => 'raw',
                        'value' => function($data) {
                            return ($data->status) ? '<span class="label label-success">' . Yii::t('backend', 'Просмотренно') . '</span>' : '<span class="label label-warning">' . Yii::t('backend', 'Не просмотренно') . '</span>';
                        }
                    ],
                    [
                        'header' => Yii::t('common', 'Ответ'),
                        'format' => 'raw',
                        'value' => function($data) {
                            return ($data->answer_at) ? '<span class="label label-success">' . Yii::t('backend', 'Отвечено ') . date('d.m.Y', $data->answer_at) . '</span>' : '<span class="label label-warning">' . Yii::t('backend', 'Ответ не был дан.') . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'created_at',
                        'headerOptions' => ['class' => 'sorting'],
                        'value' => function ($data) {
                            return date('m/d/Y H:i:s', $data->created_at);
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{reply} &nbsp; {view} &nbsp; {delete}',
                        'buttons' => [
                            'reply' => function ($url, $model) {
                                return Html::a('<i class="fa fa-envelope-o" style="color: #0073b7;" aria-hidden="true"></i>', $url);
                            },
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye" style="color: #0073b7;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url);
                            },
                        ],
                    ],
                ],
            ])
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
