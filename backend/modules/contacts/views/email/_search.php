<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-md-4 no-paddingleft">
            <?= $form->field($model, 'status')->dropDownList([
                '0' => 'Не просмотренно',
                '1' => 'Просмотренно',
            ]); ?>
        </div>

        <div class="clearfix"></div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/contacts/email', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
