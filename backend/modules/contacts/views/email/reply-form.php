<?php

use kartik\widgets\DatePicker;
use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\FileInput;
?>

<div class="box box-primary">
    <div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly' => true]) ?>
        <?= $form->field($model, 'question')->textarea(['readonly' => true]) ?>

        <?= $form->field($model, 'reply')->textarea() ?>


        <div class="clearfix"></div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Reply'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
