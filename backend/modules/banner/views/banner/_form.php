<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\DatePicker,
    kartik\widgets\FileInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Banner') : Yii::t('backend', 'Update Banner') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?=
            $form->field($model, 'user_id')->widget(Select2::className(), [
                'data' => $users,
                'options' => ['multiple' => false, 'placeholder' => 'Select author ...']
            ])
            ?>
        </div>
        <div class="col-sm-2 col-lg-2">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...']
            ])
            ?>
        </div>
        <div class="col-sm-8 col-lg-9 no-padding">
            <?=
            $form->field($model, 'banner_position_id')->widget(Select2::className(), [
                'data' => $position,
                'options' => ['multiple' => false, 'placeholder' => 'Select position ...']
            ])
            ?>
        </div>

        <div class="clearfix"></div>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'url')->textInput() ?>

        <?= $form->field($model, 'url_title')->textInput() ?>

        <div class="col-lg-2 col-sm-4 no-padding">
            <?= $form->field($model, 'priority')->input('number', ['min' => 0, 'max' => 1000, 'step' => 1]) ?>
        </div>
        <div class="col-lg-2 col-sm-4 no-paddingright">
            <?=
            $form->field($model, 'from_date')->widget(DatePicker::className(), [
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])
            ?>
        </div>
        <div class="col-lg-2 col-sm-4 no-paddingright">
            <?=
            $form->field($model, 'to_date')->widget(DatePicker::className(), [
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])
            ?>
        </div>

        <div class="col-sm-12 no-padding thumbbox">
            <?php if (isset($model->image)) { ?>
                <?= Html::img('/frontend/web/uploads/banners/' . $model->image, ['class' => 'img-responsive']) ?>
                <a class="remimg" href="javascript:;" data-item="<?= $model->id ?>" data-file="<?= $model->image ?>" data-type="image"><i class="fa fa-remove"></i></a>
            <?php } ?>

            <?=
            $form->field($model, 'image')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'showCaption' => false,
                    'showUpload' => false,
                    'showRemove' => false,
                    'fileActionSettings' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        'showCaption' => false,
                    ],
                ],
            ])
            ?>
        </div>

        <div class="col-sm-4 col-lg-2 no-padding">
            <?=
            $form->field($model, 'active')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>

        <div class="clearfix"></div>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
$modelID = (isset($model->id)) ? $model->id : 0;

$script = <<< JS
   $("a.remimg").on('click', function(event){
        event.preventDefault();
        
        var idad = $modelID;
        var filename = $(this).data("file");
        typerm = $(this).data("type");
        
        if (confirm("Are you sure you want to delete this image?")) {
            $.post('/admin/banner/banner/delete-image',{idadv: idad, filename: filename, typerm: typerm}, function(){
                if(typerm === 'image') {
                    $(".thumbbox").children("img.img-responsive").remove();
                    $(".thumbbox").children("a.remimg").remove();
                }
            });
        }
   });
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>
