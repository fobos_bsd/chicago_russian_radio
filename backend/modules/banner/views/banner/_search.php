<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-paddingleft">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-lg-2 col-sm-2">
            <?=
            $form->field($model, 'user_id')->widget(Select2::className(), [
                'data' => $users,
                'options' => ['multiple' => false, 'placeholder' => 'Select author ...']
            ])
            ?>
        </div>

        <div class="col-sm-2 col-lg-2">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...']
            ])
            ?>
        </div>

        <div class="col-lg-3">
            <?= $form->field($model, 'name') ?>
        </div>

        <div class="col-sm-3 col-lg-2">
            <?=
            $form->field($model, 'active')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>

        <div class="clearfix"></div>
        <div class="col-sm-6 col-lg-6 no-padding">
            <?=
            $form->field($model, 'banner_position_id')->widget(Select2::className(), [
                'data' => $position,
                'options' => ['multiple' => false, 'placeholder' => 'Select position ...'],
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/banner/banner', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
