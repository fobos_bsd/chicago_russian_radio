<?php

use kartik\widgets\DatePicker;
use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\FileInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Company') : Yii::t('backend', 'Update Company') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?php

        echo '<label class="control-label" for="w1">Banners</label>';
        echo \kartik\widgets\Select2::widget([
            'name' => 'AdvCompany[bIds]',
            'data' => $banners,
            'value' =>  $model->getBanners()->select('id')->column(),
            'options' => ['multiple' => true],
        ]);

        ?>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="clearfix"></div>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>