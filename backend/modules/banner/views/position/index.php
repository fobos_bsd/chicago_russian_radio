<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\BannerPositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Banner Positions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-position-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Banner Position'), ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'label' => '#',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'name',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'width',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'height',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{update} &nbsp; {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url);
                            },
                        ],
                    ],
                ],
            ])
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
