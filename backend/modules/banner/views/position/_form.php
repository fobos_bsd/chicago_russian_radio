<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BannerPosition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Position') : Yii::t('backend', 'Update Position') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <div class="clearfix"></div>
        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'width')->input('number', ['min' => 10, 'max' => 65365, 'step' => 1]) ?>
        </div>
        <div class="col-sm-2 col-lg-1">
            <?= $form->field($model, 'height')->input('number', ['min' => 10, 'max' => 65365, 'step' => 1]) ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
