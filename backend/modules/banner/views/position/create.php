<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BannerPosition */

$this->title = Yii::t('backend', 'Create Banner Position');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Banner Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-position-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
