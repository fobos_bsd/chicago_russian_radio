<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Banner Statistic');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'banners' => $banners]); ?>
</div>

<?php if ($searchModel->banner_id): ?>
    <?php
        $model = $dataProvider->getModels();
        $ctr = 0;
        if($model[0]->click_count > 0 && $model[0]->show_count > 0){
            $ctr = ($model[0]->click_count / $model[0]->show_count) * 100 ;
        }
    ?>
    <div class="box box-primary">
        <div class="box-body" style="padding: 20px 10px;">
            <span class="label label-info label-lg" style="font-size: 18px;margin-right: 20px;">CTR : <?= round($ctr,2) ?> %</span>
            <span class="label label-warning label-lg" style="font-size: 18px;margin-right: 20px;">VIEWS : <?= $model[0]->show_count ?></span>
            <span class="label label-warning label-lg" style="font-size: 18px;">PRESSING : <?= $model[0]->click_count ?></span>
        </div>
    </div>
<?php endif; ?>