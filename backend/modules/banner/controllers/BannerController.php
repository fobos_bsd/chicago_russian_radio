<?php

namespace backend\modules\banner\controllers;

use Yii,
    yii\helpers\ArrayHelper,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\web\NotFoundHttpException,
    yii\web\UploadedFile,
    yii\filters\AccessControl;
use common\models\BannerItem,
    common\models\BannerStatistic,
    common\models\Lang,
    common\models\search\BannerItemSearch,
    common\models\User,
    common\models\BannerPosition;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends Controller {

    public $users, $langs, $position;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-image'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->users = ArrayHelper::map(User::find()->where(['is not', 'confirmed_at', null])->andWhere(['is', 'blocked_at', null])->asArray()->all(), 'id', 'username');
        $this->langs = ArrayHelper::map(Lang::find()->asArray()->all(), 'id', 'name');
        $this->position = ArrayHelper::map(BannerPosition::find()->asArray()->all(), 'id', 'name');
    }

    public function actionIndex() {
        $searchModel = new BannerItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'users' => $this->users,
                    'langs' => $this->langs,
                    'position' => $this->position,
        ]);
    }

    public function actionCreate() {
        $model = new BannerItem();
        $model->site_id = Yii::$app->Siteselector->getSitesCookie();
        $model->user_id = Yii::$app->user->id;
        $model->lang_id = Yii::$app->Siteselector->getSiteLangugeID();
        $model->active = 1;
        $model->priority = 0;
        $model->from_date = date('Y-m-d');
        $model->to_date = date('Y-m-d', strtotime("+30 days"));

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');

            if ($model->insert()) {
                $statistic = new BannerStatistic();

                $statistic->banner_id = $model->id;
                $statistic->save();
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'users' => $this->users,
                        'langs' => $this->langs,
                        'position' => $this->position,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->update())
                return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'users' => $this->users,
                        'langs' => $this->langs,
                        'position' => $this->position,
            ]);
        }
    }

    /**
     * Displays a single BannerItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing BannerItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /*
     * @return strng
     */
    public function actionDeleteImage() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $post = Yii::$app->request->post();

            if (isset($post['idadv']) && isset($post['filename']) && isset($post['typerm'])) {
                $id = (int) strip_tags(trim($post['idadv']));
                $file = (string) strip_tags(trim($post['filename']));
                $typerm = (string) strip_tags(trim($post['typerm']));

                $fullpath = Yii::getAlias('@frontend') . '/web/uploads/banners/' . $file;

                if (file_exists($fullpath))
                    unlink($fullpath);

                $model = $this->findModel($id);
                $model->$typerm = null;

                $model->save();
            }
        }
    }

    /**
     * Finds the BannerItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BannerItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = BannerItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
