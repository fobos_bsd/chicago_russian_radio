<?php

namespace backend\modules\banner\controllers;

use Yii,
    yii\helpers\ArrayHelper,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl;
use common\models\BannerItem,
    common\models\search\BannerStatisticSearch;

/**
 * StatisticsController implements the CRUD actions for BannerStatistics model.
 */
class StatisticsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['Администраторы'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new BannerStatisticSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $banners = ArrayHelper::map(BannerItem::find()->where(['site_id' => Yii::$app->Siteselector->getSitesCookie()])->asArray()->all(), 'id', 'name');

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'banners' => $banners
        ]);
    }

}
