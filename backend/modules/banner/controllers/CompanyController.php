<?php

namespace backend\modules\banner\controllers;

use Yii,
    yii\data\ActiveDataProvider,
    yii\helpers\ArrayHelper,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\web\NotFoundHttpException,
    yii\filters\AccessControl;
use common\models\AdvCompany,
    common\models\BannerItem;

/**
 * FormatController implements the CRUD actions for CompanyController model.
 */
class CompanyController extends Controller {

    public $banners;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        $this->banners = ArrayHelper::map(BannerItem::find()->asArray()->where(['active' => 1])->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => AdvCompany::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {

        $model = new AdvCompany();
        $model->site_id = Yii::$app->Siteselector->getSitesCookie();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'banners' => $this->banners,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'banners' => $this->banners,
            ]);
        }
    }

    /**
     * Displays a single BannerFormat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing BannerFormat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = AdvCompany::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
