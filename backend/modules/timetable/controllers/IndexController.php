<?php

namespace backend\modules\timetable\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper,
    yii\web\UploadedFile;
use common\models\Timetable,
    common\models\search\TimetableSearch,
    common\models\Lang,
    common\models\YoutubePlaylists,
    common\models\TimetableTags;

/**
 * IndexController implements the CRUD actions for Timetable model.
 */
class IndexController extends Controller {

    public $playlists, $langs, $daysofweek, $tags;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-image'],
                        'roles' => ['Администраторы', 'Radioadmins'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->playlists = ArrayHelper::map(YoutubePlaylists::find()->where(['status' => 1])->asArray()->all(), 'id', 'title');
        $this->langs = ArrayHelper::map(Lang::find()->asArray()->all(), 'id', 'name');

        // Set tags
        $tags = TimetableTags::find()->asArray()->all();
        if (isset($tags) && is_array($tags) && count($tags) > 0) {
            foreach ($tags as $tag) {
                $this->tags[$tag['id']] = $tag['name'] . ' (' . $tag['archive_name'] . ')';
            }
        } else {
            $this->tags = [];
        }

        $this->daysofweek = [
            'Mo' => Yii::t('common', 'Monday'),
            'Tu' => Yii::t('common', 'Tuesday'),
            'We' => Yii::t('common', 'Wednesday'),
            'Th' => Yii::t('common', 'Thursday'),
            'Fr' => Yii::t('common', 'Friday'),
            'Sa' => Yii::t('common', 'Saturday'),
            'Su' => Yii::t('common', 'Sunday')
        ];
    }

    /**
     * Lists all Timetable models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TimetableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'langs' => $this->langs,
                    'playlists' => $this->playlists,
                    'tags' => $this->tags,
                    'daysofweek' => $this->daysofweek
        ]);
    }

    /**
     * Displays a single Timetable model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Timetable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Timetable();
        $model->site_id = Yii::$app->Siteselector->getSitesCookie();
        $model->lang_id = Yii::$app->Siteselector->getSiteLangugeID();
        $model->onair = 0;

        if ($model->load(Yii::$app->request->post())) {
            //$model->fileImage = UploadedFile::getInstance($model, 'fileImage');
            if ($model->insert()) {
                return $this->redirect(['index']);
            } else {
                var_dump($model->getErrors());
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'langs' => $this->langs,
                        'playlists' => $this->playlists,
                        'tags' => $this->tags,
                        'daysofweek' => $this->daysofweek
            ]);
        }
    }

    /**
     * Updates an existing Timetable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            //$model->fileImage = UploadedFile::getInstance($model, 'fileImage');
            if ($model->save()) {
                return $this->redirect(['index']);
            } else {
                $model->getErrors();
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'langs' => $this->langs,
                        'playlists' => $this->playlists,
                        'tags' => $this->tags,
                        'daysofweek' => $this->daysofweek
            ]);
        }
    }

    /**
     * Deletes an existing Timetable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Timetable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timetable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Timetable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * @return strng
     */

    public function actionDeleteImage() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $post = Yii::$app->request->post();

            if (isset($post['idadv']) && isset($post['filename']) && isset($post['typerm'])) {
                $id = (int) strip_tags(trim($post['idadv']));
                $file = (string) strip_tags(trim($post['filename']));
                $typerm = (string) strip_tags(trim($post['typerm']));

                $fullpath = Yii::getAlias('@frontend') . '/web/uploads/timetable/' . $file;

                if (file_exists($fullpath))
                    unlink($fullpath);

                $model = $this->findModel($id);
                $model->$typerm = null;

                $model->save();
            }
        }
    }

}
