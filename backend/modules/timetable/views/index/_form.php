<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\Select2,
    kartik\time\TimePicker,
    kartik\widgets\SwitchInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Timetable') : Yii::t('backend', 'Update Timetable') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-3 col-lg-2 no-paddingleft">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...']
            ])
            ?>
        </div>

        <div class="col-sm-6 col-lg-6 no-padding">
            <?=
            $form->field($model, 'youtube_playlists_id')->widget(Select2::className(), [
                'data' => $playlists,
                'options' => ['multiple' => false, 'placeholder' => 'Select playlist ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-4 no-paddingright">
            <?=
            $form->field($model, 'day_of_week')->widget(Select2::className(), [
                'data' => $daysofweek,
                'options' => ['multiple' => false, 'placeholder' => 'Select day of week ...']
            ])
            ?>
        </div>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <div class="col-sm-4 col-lg-3 no-paddingleft">
            <?=
            $form->field($model, 'from_time')->widget(TimePicker::classname(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                ]
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-3 no-paddingright">
            <?=
            $form->field($model, 'to_time')->widget(TimePicker::classname(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                ]
            ])
            ?>
        </div>
        <div class="col-sm-2 col-lg-1 col-sm-offset-2 col-lg-offset-5">
            <?=
            $form->field($model, 'onair')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12 no-padding">
            <?=
            $form->field($model, 'description')->widget(Redactor::className(), [
                'clientOptions' => [
                    'options' => [
                        'style' => 'height: 300px;',
                    ],
                    'convertDivs' => false,
                    'replaceDivs' => false,
                    'removeEmptyTags' => false,
                    'paragraphize' => false,
                    'pastePlainText' => true,
                    'autoresize' => true,
                    'convertVideoLinks' => true,
                    //'buttons' => ['format', 'bold', 'italic', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule', 'underline', 'ol', 'ul', 'indent', 'outdent'],
                    'plugins' => ['clips', 'fontcolor', 'imagemanager', 'advanced']
                ]
            ])
            ?>
        </div>

        <?=
        $form->field($model, 'timetable_tags_id')->widget(Select2::className(), [
            'data' => $tags,
            'options' => ['multiple' => false, 'placeholder' => 'Select tag ...']
        ])
        ?>

        <!--
        <div class="col-sm-12 no-padding thumbbox">
        <?php
        // if (isset($model->fileImage)) {
        ?>
        <?php // echo Html::img('/frontend/web/uploads/timetable/' . $model->fileImage, ['class' => 'img-responsive']) ?>
                <a class="remimg" href="javascript:;" data-item="<?php // echo $model->id    ?>" data-file="<?php // echo $model->fileImage    ?>" data-type="fileImage"><i class="fa fa-remove"></i></a>
        <?php
        //}
        ?>
        <?php /* echo
          $form->field($model, 'fileImage')->widget(FileInput::classname(), [
          'options' => ['accept' => 'image/*'],
          'pluginOptions' => [
          'showCaption' => false,
          'showUpload' => false,
          'showRemove' => false,
          'fileActionSettings' => [
          'showUpload' => false,
          'showRemove' => false,
          'showCaption' => false,
          ],
          ],
          ]) */
        ?>
        </div>
        -->

        <?= $form->field($model, 'fileImage')->hiddenInput()->label(false); ?>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$modelID = (isset($model->id)) ? $model->id : 0;

$script = <<< JS
   $("a.remimg").on('click', function(event){
        event.preventDefault();
        
        var idad = $modelID;
        var filename = $(this).data("file");
        typerm = $(this).data("type");
        
        if (confirm("Вы действительно хотите удалить это изображение?")) {
            $.post('/admin/timetable/index/delete-image',{idadv: idad, filename: filename, typerm: typerm}, function(){
                if(typerm === 'fileImage') {
                    $(".thumbbox").children("img.img-responsive").remove();
                    $(".thumbbox").children("a.remimg").remove();
                }
            });
        }
   });
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>
