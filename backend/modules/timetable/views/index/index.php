<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

$this->title = Yii::t('backend', 'Timetables');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timetable-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'langs' => $langs, 'playlists' => $playlists, 'tags' => $tags, 'daysofweek' => $daysofweek]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Timetable'), ['create'], ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'label' => 'ID',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'lang_id',
                        'label' => 'Language',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->lang->name;
                        }
                    ],
                    ['attribute' => 'name',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'site_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->site->name;
                        }
                    ],
                    ['attribute' => 'youtube_playlists_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->youtubePlaylists->title;
                        }
                    ],
                    ['attribute' => 'from_time',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->from_time;
                        }
                    ],
                    ['attribute' => 'to_time',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->to_time;
                        }
                    ],
                    ['attribute' => 'timetable_tags_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            $tag = (isset($data->timetableTags->name)) ? $data->timetableTags->name : '';
                            return $tag;
                        }
                    ],
                    ['attribute' => 'day_of_week',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) use ($daysofweek) {
                            $week = $daysofweek[$data->day_of_week];
                            return $week;
                        }
                    ],
                    ['attribute' => 'onair',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            if ($data->onair == 1) {
                                return '<i class="fa fa-toggle-on" style="color: #00a65a;" aria-hidden="true"></i>';
                            } else {
                                return '<i class="fa fa-toggle-off" style="color: #dd4b39;" aria-hidden="true"></i>';
                            }
                        }
                    ],
                    /*
                      [
                      'attribute' => 'fileImage',
                      'format' => 'raw',
                      'content' => function($data) {
                      return Html::img('/frontend/web/uploads/timetable/' . $data->fileImage, [
                      'alt' => 'картинка',
                      'style' => 'width:35px;',
                      'class' => 'img-responsive'
                      ]);
                      }
                      ],
                     * 
                     */
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{update} &nbsp; {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ])
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>