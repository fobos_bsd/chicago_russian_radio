<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-2 col-lg-1">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-5 no-padding">
            <?=
            $form->field($model, 'youtube_playlists_id')->widget(Select2::className(), [
                'data' => $playlists,
                'options' => ['multiple' => false, 'placeholder' => 'Select youtube playlist ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-5 no-paddingright">
            <?=
            $form->field($model, 'day_of_week')->widget(Select2::className(), [
                'data' => $daysofweek,
                'options' => ['multiple' => false, 'placeholder' => 'Select day of weeke ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-8 col-lg-9 no-padding">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-sm-2 col-lg-1 col-sm-offset-2 col-lg-offset-2">
            <?=
            $form->field($model, 'onair')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <?php // echo $form->field($model, 'site_id')  ?>

        <?php // echo $form->field($model, 'description') ?>

        <?php // echo $form->field($model, 'fileImage') ?>

        <?php // echo $form->field($model, 'from_time') ?>

        <?php // echo $form->field($model, 'to_time')  ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/timetable', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div> 
</div>
