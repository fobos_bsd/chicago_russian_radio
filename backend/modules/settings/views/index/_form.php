<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\FileInput;
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Setting') : Yii::t('backend', 'Update Setting') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-5 col-lg-4 no-paddingleft">
            <?=
            $form->field($model, 'site_id')->widget(Select2::className(), [
                'data' => $sites,
                'options' => ['multiple' => false, 'disabled' => 'disabled']
            ])
            ?>
        </div>
        <div class="col-sm-7 col-lg-8 no-paddingright">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-danger' : 'btn btn-danger']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
