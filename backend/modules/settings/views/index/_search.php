<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2;
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-3 col-lg-2 no-paddingleft">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-sm-9 col-lg-10 no-paddingright">
            <?= $form->field($model, 'name') ?>

        </div>

        <?php // echo $form->field($model, 'site_id') ?>

        <?= $form->field($model, 'value') ?>

        <?= $form->field($model, 'description') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-danger']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/settings/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
