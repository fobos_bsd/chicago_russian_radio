<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TopVideos */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Tag') : Yii::t('backend', 'Update Tag') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-lg-5">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'url') ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'rank') ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'description')->textarea(['size' => 3]) ?>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
