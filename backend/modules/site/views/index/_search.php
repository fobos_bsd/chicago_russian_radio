<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-4 no-padding">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-sm-4">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $arrlang,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-4 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-6 no-paddingleft">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-sm-6 no-paddingright">
            <?= $form->field($model, 'domain') ?>
        </div>
        <div class="clearfix"></div>
        <?= $form->field($model, 'description') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-info']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/site/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
