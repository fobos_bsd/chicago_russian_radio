<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Site */

$this->title = Yii::t('backend', 'Create Site');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Sites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'langs' => $langs
    ])
    ?>

</div>
