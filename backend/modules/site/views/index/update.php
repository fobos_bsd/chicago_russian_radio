<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Site */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
            'modelClass' => 'Site',
        ]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Sites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="site-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'langs' => $langs
    ])
    ?>

</div>
