<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Sites');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'arrlang' => $arrlang]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Site'), ['create'], ['class' => 'btn btn-warning']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'label' => 'ID',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'lang_id',
                        'label' => 'Language',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->lang->name;
                        }
                    ],
                    ['attribute' => 'name',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'domain',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'description',
                        'format' => 'raw',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'status',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{update} &nbsp; {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ])
            ?>
            <?php Pjax::end(); ?></div></div>
</div>
