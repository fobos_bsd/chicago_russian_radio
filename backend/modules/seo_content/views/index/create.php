<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SeoContent */

$this->title = Yii::t('backend', 'Create Seo Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Seo Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
