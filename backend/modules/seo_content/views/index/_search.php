<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\SeoContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-5 col-lg-6">
            <?= $form->field($model, 'page_name') ?>
        </div>
        <div class="col-sm-3 col-lg-1 no-paddingleft">
           <?= $form->field($model, 'seo_title') ?>
        </div>
        <div class="col-sm-4 col-lg-4 no-padding">
            <?= $form->field($model, 'seo_keywords') ?>
        </div>
        <div class="clearfix"></div>

        <?= $form->field($model, 'seo_description') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/seo_content/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
