<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\SeoContent */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Page') : Yii::t('backend', 'Update Page') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-4 col-lg-4 no-padding">
            <?= $form->field($model, 'page_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4 col-lg-4">
            <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4 col-lg-4 no-padding">
            <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="clearfix"></div>


        <?= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
