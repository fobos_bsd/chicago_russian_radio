<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserQuestions */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'User Questions',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'User Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="user-questions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
