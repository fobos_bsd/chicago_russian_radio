<?php

namespace backend\modules\onlinevotes\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper,
    yii\helpers\Json,
    yii\data\ActiveDataProvider;
use common\models\OnlineVotes,
    common\models\search\OnlineVoteSearch,
    common\models\Settings,
    common\models\CurrentPhoneVote,
    common\models\CurrentSmsVote,
    common\models\ArhPhoneVote,
    common\models\ArhSmsVote;

/**
 * IndexController implements the CRUD actions for OnlineVotes model.
 */
class IndexController extends Controller {

    protected $settings, $time;
    public $session;
    

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'statistic'],
                        'roles' => ['Администраторы', 'Менеджеры', 'Radioadmins'],
                    ]
                ],
            ],
        ];
    }

    public function init() {
        parent::init();

        $this->settings = ArrayHelper::map(Settings::find()->all(), 'name', 'value');
        $this->session = Yii::$app->session;
        
        $this->time = time() - 5*3600;

        $this->oldvoteDisactivate();
    }

    /**
     * Lists new OnlineVotes models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new OnlineVoteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OnlineVotes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OnlineVotes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new OnlineVotes();

        // Set default params
        $model->created_at = $this->time;
        $model->params = '{"status":"error"}';
        $model->status = 1;
        $model->site_id = Yii::$app->Siteselector->getSitesCookie();
        $model->vtype = 'phone';

        // Get settings for OnlineVotes
        if (isset($this->settings['phones_for_votes']) && !empty($this->settings['phones_for_votes'])) {
            $setphone = explode(',', $this->settings['phones_for_votes']);
        } else {
            $setphone = array();
        }

        if (isset($this->settings['phone_for_smsvotes']) && !empty($this->settings['phone_for_smsvotes'])) {
            $setsms = explode(',', $this->settings['phone_for_smsvotes']);
        } else {
            $setsms = array();
        }

        // Is post of create vote
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $post = Yii::$app->request->post();

            if (isset($post['OnlineVotes']['vtype']) && $post['OnlineVotes']['vtype'] === 'phone') {
                foreach ($post['Valuespone'] as $key => $val) {
                    if (!empty($val))
                        $arrpar[$key] = $val;
                }
                if (isset($arrpar))
                    $model->params = $this->createVoteParams('phone', $arrpar);
            } else {
                if (isset($post['Valuesms']))
                    $model->params = $this->createVoteParams('sms', $post['Valuesms']);
            }

            // Test opening OnlineVotes of the type
            $cntopenPhone = $this->testActivePhoneVote($model->start_at);
            $cntopenSms = $this->testActiveSmsVote($model->start_at);


            if ($post['OnlineVotes']['vtype'] === 'phone' && (isset($cntopenPhone) && $cntopenPhone > 0)) {
                $this->session->setFlash('errorVote', 'Error! You already have an open vote of this type');
            } elseif ($post['OnlineVotes']['vtype'] === 'sms' && (isset($cntopenSms) && $cntopenSms > 0)) {
                $this->session->setFlash('errorVote', 'Error! You already have an open vote of this type');
            } elseif (!isset($model->params) || empty($model->params) || $model->params === '{"status":"error"}') {
                $this->session->setFlash('errorVote', 'Error! You are not wrote data for phone online votes number');
            } else {
                // Seve vote data
                if ($model->insert()) {
                    if (isset($post['OnlineVotes']['createsms']) && $post['OnlineVotes']['createsms'] === 'on') {
                        $this->createSmsVoteToo($post);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }
        return $this->render('create', [
                    'model' => $model,
                    'setphone' => $setphone,
                    'setsms' => $setsms,
                    'isexitactive' => (int) $this->testActiveVote()
        ]);
    }

    // Create parallel sms vote
    public function createSmsVoteToo($post) {
        $model = new OnlineVotes(['scenario' => 'createVote']);

        // Set default params
        $model->created_at = $this->time;
        $model->params = '{"status":"error"}';
        $model->status = 1;

        if ($model->load($post) && $model->validate()) {
            $model->name = $model->name . ' sms';
            $model->vtype = 'sms';
            $model->params = $this->createVoteSmsParams($post['Valuesms']);

            // Test opening OnlineVotes of the type
            $cntopenSms = $this->testActiveSmsVote($model->start_at);
            if (isset($cntopenSms) && $cntopenSms > 0) {
                $this->session->setFlash('errorVote', 'Error! You already have an open vote of this type');
            } else {
                // Seve vote data
                if (!$model->insert()) {
                    var_dump($model->getErrors());
                }
            }
        }
    }

    /**
     * Updates an existing OnlineVotes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        // Covert time
        $model->start_at = (isset($model->start_at)) ? date('d/m/Y h:i', $model->start_at) : date('d/m/Y h:i');

        if (!isset($this->settings['vote_timeout']) || empty($this->settings['vote_timeout'])) {
            $this->settings['vote_timeout'] = 10;
        }
        $model->finish_at = (isset($model->finish_at)) ? date('d/m/Y h:i', $model->finish_at) : date('d/m/Y h:i', $this->time + $this->settings['vote_timeout']);

        // Get params
        if (!isset($model->params) || empty($model->params)) {
            $model->params = '{"status":"error"}';
        }

        if (isset($this->settings['phones_for_votes']) && !empty($this->settings['phones_for_votes'])) {
            $setphone = explode(',', $this->settings['phones_for_votes']);
        } else {
            $setphone = array();
        }

        if (isset($this->settings['phone_for_smsvotes']) && !empty($this->settings['phone_for_smsvotes'])) {
            $setsms = explode(',', $this->settings['phone_for_smsvotes']);
        } else {
            $setsms = array();
        }

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();

            $model->start_at = (isset($post['OnlineVotes']['start_at'])) ? strtotime($post['OnlineVotes']['start_at']) : $this->time;
            $model->finish_at = (isset($post['OnlineVotes']['finish_at'])) ? strtotime($post['OnlineVotes']['finish_at']) : $this->time + $this->settings['vote_timeout'];
            if (isset($post['OnlineVotes']['vtype']) && $post['OnlineVotes']['vtype'] === 'phone') {
                $model->params = $this->createVoteParams('phone', $post['Valuespone']);
            } else {
                $model->params = $this->createVoteParams('sms', $post['Valuesms']);
            }

            // Seve vote data
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                var_dump($model->getErrors());
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'setphone' => $setphone,
                    'setsms' => $setsms
        ]);
    }

    /**
     * Deletes an existing OnlineVotes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /*
     * Show current vote
     * statistic
     */

    public function actionStatistic($id) {
        $voteobj = $this->findModel($id);
        $totalcount = 0;

        // Direct type
        $timearh = (int) $voteobj->start_at + (int) $this->settings['vote_timeout'];

        // Get phone OnlineVotes data
        $arrparams = (isset($voteobj->params) && $voteobj->vtype === 'phone') ? Json::decode($voteobj->params) : array();
        if (count($arrparams) > 0) :
            $i = 0;
            foreach ($arrparams as $key => $val) {
                if ($timearh >= $this->time || $voteobj->status == 1) {
                    $count = (int) CurrentPhoneVote::find()->where(['online_votes_id' => $id, 'vote_phone' => trim($key)])->count();
                } elseif ($timearh < $this->time && $voteobj->status == 0) {
                    $count = (int) ArhPhoneVote::find()->where(['online_votes_id' => $id, 'vote_phone' => trim($key)])->count();
                }
                $totalcount += $count;
                $arrcount[$i]['label'] = (string) trim($val) . ' (' . $count . ')';
                $arrcount[$i]['value'] = $count;
                $i++;
            }
        endif;
        $objPhoneCount = (isset($arrcount) && count($arrcount) > 0) ? Json::encode($arrcount) : '[{}]';

        // Get sms OnlineVotes data
        $arrparams = (isset($voteobj->params) && $voteobj->vtype === 'sms') ? Json::decode($voteobj->params) : array();
        if (count($arrparams) > 0) :
            foreach ($arrparams as $params) {
                $j = 0;
                foreach ($params as $key => $val) {
                    $code = (int) trim($key);
                    if ($timearh >= $this->time || $voteobj->status == 1) {
                        $countsms = (int) CurrentSmsVote::find()->where(['online_votes_id' => $id, 'vote_sms' => $code])->count();
                    } elseif ($timearh < $this->time && $voteobj->status == 0) {
                        $countsms = (int) ArhSmsVote::find()->where(['online_votes_id' => $id, 'vote_sms' => $code])->count();
                    }
                    $totalcount += $countsms;
                    $arrsmscount[$j]['label'] = (string) trim($val) . ' (' . $countsms . ')';
                    $arrsmscount[$j]['value'] = $countsms;
                    $j++;
                }
                break;
            }
        endif;
        $objSmsCount = (isset($arrsmscount) && count($arrsmscount) > 0) ? Json::encode($arrsmscount) : '[{}]';

        // Data of OnlineVotes
        if ($timearh >= $this->time || $voteobj->status == 1) {
            $modelPhone = new ActiveDataProvider([
                'query' => CurrentPhoneVote::find()->where(['online_votes_id' => $id]),
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);

            $modelSms = new ActiveDataProvider([
                'query' => CurrentSmsVote::find()->where(['online_votes_id' => $id]),
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } elseif ($timearh < $this->time && $voteobj->status == 0) {
            $modelPhone = new ActiveDataProvider([
                'query' => ArhPhoneVote::find()->where(['online_votes_id' => $id]),
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
            $modelSms = new ActiveDataProvider([
                'query' => ArhSmsVote::find()->where(['online_votes_id' => $id]),
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        }

        return $this->render('statistic', [
                    'objPhoneCount' => $objPhoneCount,
                    'objSmsCount' => $objSmsCount,
                    'objvote' => $voteobj,
                    'theme' => $voteobj->name,
                    'descript' => $voteobj->description,
                    'totalcount' => $totalcount,
                    'modelPhone' => $modelPhone,
                    'modelSms' => $modelSms
        ]);
    }

    /**
     * Finds the OnlineVotes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OnlineVotes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = OnlineVotes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * Create params OnlineVotes
     */

    protected function createVoteParams($type, $params) {
        $result = '';
        if ($type === 'phone') {
            $result = Json::encode($params);
        } else {
            foreach ($params as $key => $val) {
                foreach ($val as $pkey => $pval) {
                    if (!empty($pval)) {
                        $arrparams[$key][trim($pkey)] = trim($pval);
                    }
                }
            }
            $result = Json::encode($arrparams);
        }

        return $result;
    }

    /*
     * Create params OnlineVotes for sms from phone
     */

    protected function createVoteSmsParams($params) {
        $result = '';
        $i = 1;

        // Create key sms
        $arrsmsnumber = explode(',', $this->settings['phone_for_smsvotes']);

        foreach ($params as $key => $val) {
            $arrparams[trim($arrsmsnumber[0])][$i] = trim($val);
            $i++;
        }
        $result = Json::encode($arrparams);

        return $result;
    }

    // Disactivate old OnlineVotes
    public function oldvoteDisactivate() {
        if (!isset($this->settings['vote_timeout']) || empty($this->settings['vote_timeout'])) {
            $this->settings['vote_timeout'] = 10;
        }

        // Direct type
        $maxtime = $this->time - (int) $this->settings['vote_timeout'];
        $arrold = OnlineVotes::find()->select('id')->where(['status' => 1])->andWhere(['<', 'finish_at', $this->time])->asArray()->all();


        if (isset($arrold) && count($arrold) > 0) {
            $oldvotes = array();
            foreach ($arrold as $old) {
                foreach ($old as $key => $val) {
                    $oldvotes[] = $val;
                }
            }
        }
        if (isset($oldvotes) && count($oldvotes) > 0) {
            foreach ($oldvotes as $oldvote) {
                $oldmodel = $this->findModel($oldvote);
                $oldmodel->status = 0;

                if (!$oldmodel->save()) {
                    var_dump($oldmodel->getErrors());
                }
            }
        }


        // Not closet vote
        $maxtime = $this->time - (int) $this->settings['vote_timeout'];
        $arrold = OnlineVotes::find()->select('id')->where(['status' => 1])->andWhere(['<', 'start_at', $maxtime])->asArray()->all();

        if (isset($arrold) && count($arrold) > 0) {
            $oldvotes = array();
            foreach ($arrold as $old) {
                foreach ($old as $key => $val) {
                    $oldvotes[] = $val;
                }
            }
        }
        if (isset($oldvotes) && count($oldvotes) > 0) {
            foreach ($oldvotes as $oldvote) {
                $oldmodel = $this->findModel($oldvote);
                $oldmodel->status = 0;

                if (!$oldmodel->save()) {
                    //var_dump($oldmodel->getErrors());
                }
            }
        }
    }

    // Test is opening OnlineVotes
    public function testActiveVote() {
        $count = OnlineVotes::find()->where(['status' => 1])->count();
        return $count;
    }

    // Test is opening phone OnlineVotes
    public function testActivePhoneVote($start) {
        $count = OnlineVotes::find()->where(['status' => 1, 'vtype' => 'phone'])->andWhere(['<=', 'start_at', $start])->andWhere(['>=', 'finish_at', $start])->count();
        return $count;
    }

    // Test is opening sms OnlineVotes
    public function testActiveSmsVote($start) {
        $count = OnlineVotes::find()->where(['status' => 1, 'vtype' => 'sms'])->andWhere(['<=', 'start_at', $start])->andWhere(['>=', 'finish_at', $start])->count();
        return $count;
    }

}
