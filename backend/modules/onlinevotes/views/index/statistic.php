<?php

use yii\grid\GridView,
    yii\bootstrap\Html,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Statistic Online Vote');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel-body no-padding">
    <div class="tab-pane" id="chartjs">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <?= Html::encode($this->title) ?>
                    </header>
                    <div class="panel-body text-center">
                        <div id="pieChart"></div>
                    </div>
                </section>
            </div>
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <?= Yii::t('backend', 'Detals of the vote') ?>
                    </header>
                    <?php
                    Pjax::begin();
                    if (isset($modelPhone)) {
                        $params = (array) json_decode($objvote->params);
                        echo GridView::widget([
                            'dataProvider' => $modelPhone,
                            'layout' => "{pager}\n{items}",
                            'tableOptions' => [
                                'class' => 'table table-striped table-advance table-hover'
                            ],
                            'options' => ['class' => 'table-responsive'],
                            'columns' => [
                                [
                                    'attribute' => 'votes_id',
                                    'label' => Yii::t('backend', 'Vote name'),
                                    'content' => function($data) {
                                        return $data->votes->name;
                                    }
                                ],
                                'phone:ntext',
                                [
                                    'attribute' => 'created_at',
                                    'content' => function($data) {
                                        return date('d.m.Y h:i:s', $data->created_at);
                                    }
                                ],
                                [
                                    'attribute' => 'vote_phone',
                                    'label' => Yii::t('backend', 'Vote`s result'),
                                    'content' => function($data) use ($params) {
                                        $reslt = '';
                                        $phn = trim($data->vote_phone);
                                        foreach ($params as $key => $val) {
                                            if (trim($key) === $phn) {
                                                $reslt = trim($val);
                                                break;
                                            } else {
                                                $reslt = $phn;
                                            }
                                        }
                                        return $reslt;
                                    }
                                ],
                            ]
                        ]);
                    }
                    if (isset($modelSms)) {
                        echo GridView::widget([
                            'dataProvider' => $modelSms,
                            'layout' => "{pager}\n{items}",
                            'tableOptions' => [
                                'class' => 'table table-striped table-advance table-hover'
                            ],
                            'options' => ['class' => 'table-responsive'],
                            'columns' => [
                                [
                                    'attribute' => 'votes_id',
                                    'label' => Yii::t('backend', 'Vote name'),
                                    'content' => function($data) {
                                        return $data->votes->name;
                                    }
                                ],
                                'phone:ntext',
                                [
                                    'attribute' => 'created_at',
                                    'content' => function($data) {
                                        return date('d.m.Y h:i:s', $data->created_at);
                                    }
                                ],
                                [
                                    'attribute' => 'vote_sms',
                                    'content' => function ($data) use($params) {
                                        $phn = trim($data->vote_sms);
                                        foreach ($params as $prm) {
                                            $arprm = (array) $prm;
                                            foreach ($arprm as $key => $val) {
                                                if (trim($key) === $phn) {
                                                    $reslt = trim($val);
                                                    break;
                                                } else {
                                                    $reslt = $phn;
                                                }
                                            }
                                            break;
                                        }
                                        return $reslt;
                                    }
                                ]
                            ],
                        ]);
                    }
                    Pjax::end();
                    ?>
                </section>
            </div>
        </div>
    </div>
</div>
<?php
if (isset($objPhoneCount) && $objPhoneCount !== '[{}]')
    $jsonCount = $objPhoneCount;
if (isset($objSmsCount) && $objSmsCount !== '[{}]')
    $jsonCount = $objSmsCount;

if (!isset($jsonCount) || $totalcount == 0)
    $jsonCount = '[{"label":"Votes (0)","value":1,"color":"#ccc"}]';
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.4.4/d3.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/js/d3pie.js' ?>"></script>
<script type="text/javascript">
    window.onload = function () {
        var pie = new d3pie("pieChart", {
            "header": {
                "title": {
                    "text": "<?= $theme ?>",
                    "fontSize": 22,
                    "font": "open sans"
                },
                "subtitle": {
                    "text": "<?= strip_tags($descript) . ' / Total votes (' . $totalcount . ')' ?>",
                    "color": "#999999",
                    "fontSize": 15,
                    "font": "open sans"
                },
                "titleSubtitlePadding": 9
            },
            "footer": {
                "color": "#999999",
                "fontSize": 12,
                "font": "open sans",
                "location": "bottom-left"
            },
            "size": {
                "canvasWidth": 1200,
                "pieOuterRadius": "90%"
            },
            "data": {
                "sortOrder": "value-desc",
                "content": <?= $jsonCount ?>
            },
            "labels": {
                "outer": {
                    "pieDistance": 5
                },
                "inner": {
                    "hideWhenLessThanPercentage": 1
                },
                "mainLabel": {
                    "fontSize": 13
                },
                "percentage": {
                    "color": "#ffffff",
                    "decimalPlaces": 0,
                    "fontSize": 15,
                },
                "value": {
                    "color": "#adadad",
                    "fontSize": 13
                },
                "lines": {
                    "enabled": true
                },
                "truncation": {
                    "enabled": true
                }
            },
            "effects": {
                "pullOutSegmentOnClick": {
                    "effect": "linear",
                    "speed": 400,
                    "size": 8
                }
            },
            "misc": {
                "gradient": {
                    "enabled": true,
                    "percentage": 100
                }
            }
        });
    }
</script>