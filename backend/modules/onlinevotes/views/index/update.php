<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Votes */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
            'modelClass' => 'OnlineVotes',
        ]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Online Votes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="onlinevotes-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    $this->render('_form', [
        'model' => $model,
        'setphone' => $setphone,
        'setsms' => $setsms
    ])
    ?>
</div>
