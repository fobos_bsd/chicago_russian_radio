<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Votes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Votes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="panel">
    <header class="panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <div class="panel-body">
        <p>
            <?= Html::a(Yii::t('backend', 'Goback'), Url::to('/admin/onlinevotes/index/index'), ['class' => 'btn btn-default']) ?>
        </p>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'slug',
                'description:ntext',
                [
                    'attribute' => 'created_at',
                    'label' => 'Date of created',
                    'value' => (int) $model->created_at + 5 * 3600,
                    'format' => 'datetime'
                ],
                [
                    'attribute' => 'start_at',
                    'label' => 'Date of start',
                    'value' => (int) $model->start_at + 5 * 3600,
                    'format' => 'datetime'
                ],
                [
                    'attribute' => 'finish_at',
                    'label' => 'Date of finish',
                    'value' => (int) $model->finish_at + 5 * 3600,
                    'format' => 'datetime'
                ],
                'status',
                'params:ntext',
            ],
        ])
        ?>
    </div>

</div>
