<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Votes */

$this->title = Yii::t('backend', 'Create Online Votes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Online Votes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="onlinevotes-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (isset($isexitactive) && $isexitactive > 0) : ?>
        <div class="alert alert-block alert-warning fade in">
            <?= Yii::t('backend', 'Attention! You have other open voting. Make sure that there will be two or more parallel SMS or Phone to avoid mistakes.') ?>
        </div>
        <?php
    endif;
    if (Yii::$app->session->hasFlash('errorVote')) {
        ?>
        <div class="alert alert-block alert-danger fade in">
            <?= Yii::$app->session->getFlash('errorVote') ?>
        </div>
        <?php
    }
    echo $this->render('_form', [
        'model' => $model,
        'setphone' => $setphone,
        'setsms' => $setsms
    ]);
    ?>
</div>
