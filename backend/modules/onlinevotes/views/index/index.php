<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Online Votes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="votes-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Yii::t('backend', 'Дата и время на данный момент: ') . date('Y/d/m H:i') ?>
    </p>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Votes'), ['create'], ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    'id',
                    ['attribute' => 'site_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->site->name;
                        }
                    ],
                    'name',
                    //'slug',
                    //'params:ntext',
                    //'description:ntext',
                    //'created_at',
                    [
                        'attribute' => 'start_at',
                        'content' => function($data) {
                            return date('m/d/Y H:i', $data->start_at + 5*3600);
                        }
                    ],
                    [
                        'attribute' => 'finish_at',
                        'content' => function($data) {
                            return date('m/d/Y H:i', $data->finish_at  + 5*3600);
                        }
                    ],
                    'vtype',
                    [
                        'attribute' => 'status',
                        'content' => function($data) {
                            $tmplstatus = ($data->status == 1) ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-close" style="color:red"></i>';
                            return $tmplstatus;
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('backend', 'Actions'),
                        'headerOptions' => ['width' => '140'],
                        'template' => '<div class="btn-group">{statistic}{view}{delete}</div>',
                        'buttons' => [
                            'statistic' => function ($url, $model) {
                                return Html::a('<i class="fa fa-pie-chart"></i>', $url, ['class' => 'btn btn-success']);
                            },
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-cog"></i>', $url, ['class' => 'btn btn-primary']);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times-circle-o"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ],
                                            'class' => 'btn btn-danger'
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
