<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Votes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Online Vote') : Yii::t('backend', 'Update Online Voteg') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'slug')->textInput(['disabled' => 'disabled', 'placeholder' => 'Url']) ?>

        <div class="col-sm-6 no-paddingleft">
            <?= $form->field($model, 'vtype')->dropDownList(array('phone' => 'phone', 'sms' => 'sms')) ?>
        </div>
        <div class="col-sm-6 no-paddingright">
            <?= $form->field($model, 'period_call')->textInput(['placeholder' => 0])->label(Yii::t('backend', 'Period between calls (on seconds)')) ?>
        </div>

        <div id="phoneparams">
            <?php
            if (isset($setphone)) {
                foreach ($setphone as $phone) :
                    ?>
                    <div class="form-group">
                        <label class="control-label"><?= Yii::t('backend', 'Vote value for phone ') . $phone ?><sup style="color: red;">*</sup></label>
                        <input class="form-control" type="text" name="Valuespone[<?= $phone ?>]" data-phone="<?= $phone ?>" value="" />
                    </div>
                    <?php
                endforeach;
            }
            ?>
        </div>
        <div id="smsparams">
            <?php
            if (isset($setsms)) {
                foreach ($setsms as $phone) :
                    ?>
                    <div class="col-sm-12 no-padding">
                        <label class="control-label"><?= Yii::t('backend', 'Vote value for sms on phone. Max. variant`s count is 12 for one phone number ') . $phone ?></label> 
                    </div>
                    <?php
                    $k = 1;
                    while ($k <= 12) {
                        ?>
                        <div class="form-group col-sm-3 no-padding">
                            <input class="form-control" type="text" name="Valuesms[<?= $phone ?>][<?= $k ?>]" data-phone="<?= $phone ?>" value="" placeholder="values for code <?= $k ?>" />
                        </div>
                        <?php
                        $k++;
                    }
                    ?>
                </div>
                <div class="clearfix"></div>
                <?php
            endforeach;
        }
        ?>

        <?= $form->field($model, 'params')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <div class="col-sm-4 no-paddingleft">
            <?=
            $form->field($model, 'start_at')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Start date and time'],
                'convertFormat' => true,
                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                'layout' => '{picker}{input}{remove}',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'MM/d/yyyy H:i',
                    'startDate' => gmdate('m/d/Y H:i', time() - 3600 * 5),
                    'todayHighlight' => false,
                    'timezone' => '-0600',
                    'showTimezone' => true,
                    'LocalTimezone' => true
                ]
            ])
            ?> 
        </div>

        <div class="col-sm-4 no-padding">
            <?php $endTime = time() - (3600 * 5) + 300; ?>
            <?=
            $form->field($model, 'finish_at')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Finish date and time'],
                'convertFormat' => true,
                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                'layout' => '{picker}{input}{remove}',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'MM/d/yyyy H:i',
                    'startDate' => gmdate('m/d/Y H:i', $endTime),
                    'todayHighlight' => false,
                    'timezone' => '-0600',
                    'showTimezone' => true,
                    'LocalTimezone' => true
                ]
            ])
            ?> 
        </div>

        <div class="col-sm-4 no-paddingright">
            <dev style="display: inline-block; min-width: 100%; height: 2.2em;">&nbsp;</dev>
            <label class="label_check">
                <input name="OnlineVotes[createsms]" id="checkbox" type="checkbox" /> <?= Yii::t('backend', 'Create sms votes too') ?>
            </label>
        </div>


        <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="clearfix"></div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary', 'data-toggle' => 'modal', 'href' => '#myModal3']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= Yii::t('backend', 'Waite send') ?></h4>
            </div>
            <div class="modal-body">
                Please wait!
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button"> <?= Yii::t('backend', 'Close') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->

<?php
$script = <<< JS
    $(document).ready(function () {
        var stypevote = $("select#onlinevotes-vtype option:selected").val();
        if (stypevote === 'phone') {
            $("#smsparams").hide();
        } else {
            $("#phoneparams").hide();
        }

        $("select#onlinevotes-vtype").change(function () {
            var typevote = $(this).find('option:selected').val();
            if (typevote === 'phone') {
                $("#smsparams").fadeOut(10);
                $("#phoneparams").fadeIn(100);
            } else {
                $("#phoneparams").fadeOut(10);
                $("#smsparams").fadeIn(100);
            }
        });
    });
JS;

$this->registerJs($script, yii\web\View::POS_END);
?>
