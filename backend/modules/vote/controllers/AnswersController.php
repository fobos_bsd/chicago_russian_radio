<?php

namespace backend\modules\vote\controllers;

use Yii,
    yii\helpers\ArrayHelper,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\web\NotFoundHttpException;
use common\models\Answers,
    common\models\Questions,
    common\models\search\AnswersSearch;

/**
 * AnswersController implements the CRUD actions for Answers model.
 */
class AnswersController extends Controller {

    public $questions;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['Администраторы', 'Radioadmins'],
                    ]
                ],
            ],
        ];
    }

    public function init() {
        parent::init();
        $this->questions = ArrayHelper::map(Questions::find()->where(['site_id' => Yii::$app->Siteselector->getSitesCookie()])->asArray()->all(), 'id', 'name');
    }

    public function actionIndex() {
        $searchModel = new AnswersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'questions' => $this->questions
        ]);
    }

    public function actionCreate() {
        $model = new Answers();
        $model->site_id = Yii::$app->Siteselector->getSitesCookie();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->insert()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'questions' => $this->questions
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->update())
                return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'questions' => $this->questions
            ]);
        }
    }

    /**
     * Displays a single Answers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing Answers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the BannerItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Answers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Answers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
