<?php

use kartik\widgets\DatePicker;
use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\FileInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Banner') : Yii::t('backend', 'Update Banner') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?=
        $form->field($model, 'questions_id')->widget(Select2::className(), [
            'data' => $questions,
            'options' => ['multiple' => false, 'placeholder' => 'Select question...']
        ])
        ?>

        <div class="clearfix"></div>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
