<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Answers */

$this->title = Yii::t('backend', 'Update Answer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Answer'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="pages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'questions' => $questions
    ])
    ?>

</div>
