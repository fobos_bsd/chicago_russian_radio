<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Votes */

$this->title = Yii::t('backend', 'Update Vote');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Votes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="pages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'questions' => $questions,
        'answers' => $answers
    ])
    ?>

</div>
