<?php

use kartik\widgets\Select2;
use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-4 col-lg-4 no-paddingleft">
            <?= $form->field($model, 'ip') ?>
        </div>
        <div class="col-sm-4 col-lg-4">
            <?=
            $form->field($model, 'questions_id')->widget(Select2::className(), [
                'data' => $questions,
                'options' => ['multiple' => false, 'placeholder' => 'Select question...']
            ])
            ?>
        </div>

        <div class="col-sm-4 col-lg-4">
            <?=
            $form->field($model, 'answers_id')->widget(Select2::className(), [
                'data' => $answers,
                'options' => ['multiple' => false, 'placeholder' => 'Select answer...']
            ])
            ?>
        </div>

        <div class="clearfix"></div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/vote/votes', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
