<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Votes */

$this->title = Yii::t('backend', 'Create Vote');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Votes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'questions' => $questions,
        'answers' => $answers
    ])
    ?>

</div>
