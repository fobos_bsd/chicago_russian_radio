<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Questions */

$this->title = Yii::t('backend', 'Create Question');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Question'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model
    ])
    ?>

</div>
