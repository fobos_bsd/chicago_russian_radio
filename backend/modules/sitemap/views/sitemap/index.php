<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::t('backend', 'Site map generator');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-questions-index">

    <p class="text-center">Site Map generator</p>
    <p class="text-center" style="margin-top: 40px;">
        <button class="btn btn-success" onclick="generateSiteMap()">Generate</button>
    </p>

    <div class="generate-success alert alert-success">
        <?= Yii::t('backend','Карта сгенерированна успешно') ?>
        <span class="close-button" onclick="$('.generate-success').hide('fast');">&#215;</span>
    </div>
    <div class="generate-error alert alert-danger">
        Error: See the console.
        <span class="close-button" onclick="$('.generate-error').hide('fast');">&#215;</span>
    </div>

</div>

<?php
$script = <<< JS
    
    function generateSiteMap() {
        
        let action = '/admin/sitemap/sitemap/ajax-generate';
        
        $.ajax({
           method: 'POST',
           dataType: 'text',
           url: action,
           data: {generate: 1},

           success: function (response) {
              
               let data = JSON.parse(response);
               
               if(data.success)
               {
                   $('.generate-success').show('fast');        
               }
               else {
                   $('.generate-error').show('fast');
                   console.log(data.error);
               }
               
           },
           error: function(e) {
             console.log(e);
           }
        });
    }
JS;

$this->registerJs($script, yii\web\View::POS_END);
?>
