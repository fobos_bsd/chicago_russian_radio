<?php

namespace backend\modules\sitemap\controllers;

use Yii,
    yii\base\Exception,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\web\Response;
use common\models\MenuList,
    samdark\sitemap\Sitemap;

/**
 * SitemapController generate site map
 */
class SitemapController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['ajax-generate'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionAjaxGenerate() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            try {
                $webPath = Yii::getAlias('@frontend/web/');
                $url = str_replace(Yii::$app->request->getUrl(), '', Yii::$app->request->getAbsoluteUrl());

                $siteMap = new Sitemap($webPath . "sitemap.xml");

                foreach (MenuList::find()->all() as $item) {
                    if ($item->alias[0] !== '/')
                        continue;

                    $siteMap->addItem($url . $item->alias, time(), Sitemap::DAILY);
                }

                $siteMap->write();

                return ['success' => true];
            } catch (Exception $exception) {
                return ['success' => false, 'error' => $exception->getMessage()];
            }
        }
    }

}
