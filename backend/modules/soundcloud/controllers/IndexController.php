<?php

namespace backend\modules\soundcloud\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper;
use common\models\Soundcloud,
    common\models\search\SoundcloudSearch,
    common\models\YoutubePlaylists;

/**
 * IndexController implements the CRUD actions for Soundcloud model.
 */
class IndexController extends Controller {

    public $playlists;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // Get all active playlists ID
        $this->playlists = ArrayHelper::map(YoutubePlaylists::find()->where(['status' => 1])->asArray()->all(), 'id', 'title');
    }

    /**
     * Lists all Soundcloud models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SoundcloudSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'playlists' => $this->playlists
        ]);
    }

    /**
     * Displays a single Soundcloud model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Soundcloud model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Soundcloud();
        $model->created_at = time();
        $model->updated_at = time();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'playlists' => $this->playlists
            ]);
        }
    }

    /**
     * Updates an existing Soundcloud model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->updated_at = time();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'playlists' => $this->playlists
            ]);
        }
    }

    /**
     * Deletes an existing Soundcloud model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Soundcloud model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Soundcloud the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Soundcloud::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
