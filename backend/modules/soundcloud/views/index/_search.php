<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                    'options' => [
                        'data-pjax' => 1
                    ],
        ]);
        ?>

        <div class="col-sm-3 col-lg-1 no-paddingleft">
            <?= $form->field($model, 'id') ?>
        </div>

        <div class="col-sm-9 col-lg-11 no-paddingright">
            <?=
            $form->field($model, 'youtube_playlists_id')->widget(Select2::className(), [
                'data' => $playlists,
                'options' => ['multiple' => false, 'placeholder' => 'Select youtube playlist ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>

        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'description') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'sclink')   ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-info']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/soundcloud/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
