<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Soundloud Note') : Yii::t('backend', 'Update Soundloud Note') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'sclink')->textInput(['maxlength' => true]) ?>

        <?=
        $form->field($model, 'youtube_playlists_id')->widget(Select2::className(), [
            'data' => $playlists,
            'options' => ['multiple' => false, 'placeholder' => 'Select youtube playlist ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-info']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
