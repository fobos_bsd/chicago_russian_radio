<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SoundcloudSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Soundclouds');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soundcloud-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'playlists' => $playlists]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Soundcloud'), ['create'], ['class' => 'btn btn-info']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'youtube_playlists_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            $result = (isset($data->youtubePlaylists->title)) ? $data->youtubePlaylists->title : '';
                            return $result;
                        }
                    ],
                    ['attribute' => 'name',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'description',
                        'format' => 'raw',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'created_at',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return date('m/d/Y', $data->created_at);
                        }
                    ],
                    /*
                      ['attribute' => 'sclink',
                      'headerOptions' => ['class' => 'sorting'],
                      ],
                     * 
                     */
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{view} &nbsp; {update} &nbsp; {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye" style="color: #0073b7;" aria-hidden="true"></i>', $url);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
