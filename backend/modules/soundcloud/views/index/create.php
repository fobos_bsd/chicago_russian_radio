<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Soundcloud */

$this->title = Yii::t('backend', 'Create Soundcloud');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Soundclouds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soundcloud-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'playlists' => $playlists
    ])
    ?>

</div>
