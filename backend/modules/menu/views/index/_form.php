<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Menu') : Yii::t('backend', 'Update Menu') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-2 no-padding">
            <?= $form->field($model, 'priority')->input('number', ['min' => 0, 'max' => 100, 'step' => 1]) ?>
        </div>
        <div class="col-sm-4">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="col-sm-6 no-padding">
            <?=
            $form->field($model, 'position')->widget(Select2::className(), [
                'data' => ['header' => "Header", 'footer' => 'Footer', 'leftsidebar' => 'Sidebar Left', 'rightsidebar' => 'Sidebar Right'],
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <?=
        $form->field($model, 'parent_id')->widget(Select2::className(), [
            'data' => $menus,
            'options' => ['multiple' => false, 'placeholder' => 'Select parent menu ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
