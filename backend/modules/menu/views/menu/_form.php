<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Menu Item') : Yii::t('backend', 'Update Menu Item') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-2 no-padding">
            <?= $form->field($model, 'gravity')->input('number', ['min' => 0, 'max' => 100, 'step' => 1]) ?>
        </div>
        <div class="col-sm-2">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="col-sm-5 no-paddingleft">
            <?=
            $form->field($model, 'menu_id')->widget(Select2::className(), [
                'data' => $menus,
                'options' => ['multiple' => false, 'placeholder' => 'Select menu ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-3 no-paddingright">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'alias')->textInput(['maxlength' => true, 'list' => 'pages_list']) ?>
        <datalist id="pages_list">
            <?php foreach ($pages as $key => $val) { ?>
                <option><?= $key ?></option>
            <?php } ?>
        </datalist>

        <?=
        $form->field($model, 'parent_id')->widget(Select2::className(), [
            'data' => $menulist,
            'options' => ['multiple' => false, 'placeholder' => 'Select parent menu ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'params')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
