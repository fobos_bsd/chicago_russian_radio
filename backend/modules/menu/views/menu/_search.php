<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-2 col-lg-2">
            <?= $form->field($model, 'gravity')->input('number', ['min' => 0, 'max' => 100, 'step' => 1]) ?>
        </div>
        <div class="col-sm-2 col-lg-2 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="col-sm-3 col-lg-4">
            <?=
            $form->field($model, 'menu_id')->widget(Select2::className(), [
                'data' => $menus,
                'options' => ['multiple' => false, 'placeholder' => 'Select menu ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-3 col-lg-3 no-paddingright">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>

        <?= $form->field($model, 'name') ?>
        <?php // echo $form->field($model, 'parent_id') ?>

        <?= $form->field($model, 'description') ?>
        <?= $form->field($model, 'alias')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/menu/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>   
</div>
