<?php

namespace backend\modules\menu\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper;
use common\models\MenuList,
    common\models\search\MenuListSearch,
    common\models\Menu,
    common\models\Lang,
    common\models\Pages;

/**
 * IndexController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller {

    public $menus, $langs, $pages, $menulist;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->menus = ArrayHelper::map(Menu::find()->where(['status' => 1])->asArray()->all(), 'id', 'name');
        $this->menulist = ArrayHelper::map(MenuList::find()->where(['status' => 1])->asArray()->all(), 'id', 'name');
        $this->langs = ArrayHelper::map(Lang::find()->asArray()->all(), 'id', 'name');
        $this->pages = ArrayHelper::map(Pages::find()->where(['status' => 1])->asArray()->all(), 'slug', 'name');
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MenuListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'menus' => $this->menus,
                    'menulist' => $this->menulist,
                    'langs' => $this->langs
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new MenuList();
        $model->lang_id = Yii::$app->Siteselector->getSiteLangugeID();
        $model->status = 1;
        $model->gravity = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'menus' => $this->menus,
                        'menulist' => $this->menulist,
                        'langs' => $this->langs,
                        'pages' => $this->pages,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'menus' => $this->menus,
                        'menulist' => $this->menulist,
                        'langs' => $this->langs,
                        'pages' => $this->pages,
            ]);
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MenuList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
