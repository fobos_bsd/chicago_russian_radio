<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Radioquiz */

$this->title = Yii::t('backend', 'Update Radioquiz: ' . $model->title, [
            'nameAttribute' => '' . $model->title,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Radioquizzes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="opinions-and-wishs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>