<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\search\RadioquizSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">

        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                    'options' => [
                        'data-pjax' => 1
                    ],
        ]);
        ?>

        <div class="col-lg-1">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'prize') ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'participant') ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'questions') ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'answers') ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'rules') ?>
        </div>
        <div class="col-lg-2">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-6">
            <?= $form->field($model, 'advertiser_name') ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'advertiser_site') ?>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/radioquiz/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div> 
</div>
