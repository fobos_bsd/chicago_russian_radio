<?php

namespace backend\modules\playlist_file\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\helpers\ArrayHelper,
    yii\filters\AccessControl,
    yii\web\UploadedFile;
use common\models\PlaylistFiles,
    common\models\search\PlaylistFileSearch,
    common\models\YoutubePlaylists;

/**
 * IndexController implements the CRUD actions for PlaylistFiles model.
 */
class IndexController extends Controller {

    public $playlists;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['Администраторы', 'Менеджеры', 'Radioadmins'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->playlists = ArrayHelper::map(YoutubePlaylists::find()->where(['status' => 1])->asArray()->all(), 'id', 'title');
    }

    /**
     * Lists all PlaylistFiles models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PlaylistFileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'playlists' => $this->playlists
        ]);
    }

    /**
     * Displays a single PlaylistFiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PlaylistFiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new PlaylistFiles();
        $model->setScenario('insert');
        $model->status = 1;

        if ($model->load(Yii::$app->request->post())) {
            $model->fileName = UploadedFile::getInstance($model, 'fileName');
            if ($model->save()) {
                return $this->redirect(['index']);
            } else {
                var_dump($model->getErrors());
                die();
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'playlists' => $this->playlists
            ]);
        }
    }

    /**
     * Updates an existing PlaylistFiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $uploaded = $model->fileName;
        $model->setScenario('update');

        if ($model->load(Yii::$app->request->post())) {

            $model->fileName = UploadedFile::getInstance($model, 'fileName');
            if (!isset($model->fileName))
                $model->fileName = $uploaded;

            if ($model->save()) {
                return $this->redirect(['index']);
            } else {
                var_dump($model->getErrors());
                die();
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'playlists' => $this->playlists
            ]);
        }
    }

    /**
     * Deletes an existing PlaylistFiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PlaylistFiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PlaylistFiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PlaylistFiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
