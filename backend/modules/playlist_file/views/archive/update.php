<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PlaylistFiles */

$this->title = Yii::t('backend', 'Update Archive file: {nameAttribute}', [
            'nameAttribute' => $model->name,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Archive'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="playlist-files-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'playlists' => $playlists
    ])
    ?>

</div>
