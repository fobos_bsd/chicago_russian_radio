<?php

use yii\bootstrap\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PlaylistFiles */

$this->title = Yii::t('backend', 'Create Broadcast File');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Broadcasts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="playlist-files-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'playlists' => $playlists
]) ?>

</div>
