<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\DatePicker,
    kartik\widgets\TimePicker,
    kartik\widgets\FileInput;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Add file of broadcast') : Yii::t('backend', 'Update file of broadcast') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-6 col-lg-6 no-paddingleft">
            <?=
            $form->field($model, 'youtube_playlists_id')->widget(Select2::className(), [
                'data' => $playlists,
                'options' => ['multiple' => false, 'placeholder' => 'Select youtube playlist ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-6 col-lg-6 no-paddingright">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-3 col-lg-2 no-paddingleft">
            <?= $form->field($model, 'long_time')->input('number', ['step' => 1, 'min' => 0, 'max' => 86400]) ?>
        </div>
        <div class="col-sm-5 col-lg-4 no-padding">
            <?=
            $form->field($model, 'radio_brodcast_day')->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Select broadcast date...'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-2 col-lg-offset-4 no-paddingright">
            <?=
            $form->field($model, 'radio_brodcast_time')->widget(TimePicker::className(), [
                'options' => ['placeholder' => 'Select broadcast date...'],
                'pluginOptions' => [
                    'showSeconds' => false,
                    'autoclose' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                ]
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <?php if (isset($model->fileName)) { ?>
            <br/>
            <p class="bg-info" style="padding: 10px 10px; color: black;">
                <audio src="/uploads/sound_playlist/<?= $model->fileName ?>" controls></audio>&nbsp;&nbsp;<?= $model->fileName ?>
            </p>
        <?php } ?>
        <?=
        $form->field($model, 'fileName')->widget(FileInput::className(), [
            'options' => ['accept' => 'audio/*'],
            'pluginOptions' => [
                'showPreview' => false,
                'showCaption' => true,
                'showRemove' => false,
                'showUpload' => false,
                'maxFileSize' => 153600,
            ],
        ])
        ?>
        <div class="clearfix"></div>
        <?=
        $form->field($model, 'status')->widget(SwitchInput::className(), [
            'type' => SwitchInput::CHECKBOX
        ])
        ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
