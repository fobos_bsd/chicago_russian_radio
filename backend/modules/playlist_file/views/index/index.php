<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PlaylistFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Broadcasts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="playlist-files-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel, 'playlists' => $playlists]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Broadcast File'), ['create'], ['class' => 'btn btn-info']) ?>
        </div>
        <div class="box-body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'youtube_playlists_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            $result = (isset($data->youtubePlaylists->title)) ? $data->youtubePlaylists->title : '';
                            return $result;
                        }
                    ],
                    ['attribute' => 'name',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'fileName',
                        'format' => 'raw',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'radio_brodcast_day',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            $date_br = (isset($data->radio_brodcast_day)) ? Yii::$app->formatter->asDate($data->radio_brodcast_day) : '';
                            return $date_br;
                        }
                    ],
                    ['attribute' => 'radio_brodcast_time',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            $time_br = (isset($data->radio_brodcast_time)) ? Yii::$app->formatter->asTime($data->radio_brodcast_time) : '';
                            return $time_br;
                        }
                    ],
                    ['attribute' => 'status',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            if ($data->status == 1) {
                                return '<i class="fa fa-toggle-on" style="color: #00a65a;" aria-hidden="true"></i>';
                            } else {
                                return '<i class="fa fa-toggle-off" style="color: #dd4b39;" aria-hidden="true"></i>';
                            }
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{update} &nbsp; {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
