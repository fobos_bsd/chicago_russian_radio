<?php

namespace backend\modules\reclams\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper;
use common\models\AudioAdvTimetable,
    common\models\search\AudioAdvTimetableSearch,
    common\models\AudioAdvertising;

/**
 * TimetableController implements the CRUD actions for AudioAdvTimetable model.
 */
class TimetableController extends Controller {

    public $reclams, $daysofweek;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->reclams = ArrayHelper::map(AudioAdvertising::find()->where(['site_id' => Yii::$app->Siteselector->getSitesCookie()])->asArray()->all(), 'id', 'name');
        $this->daysofweek = [
            'Mo' => Yii::t('common', 'Monday'),
            'Tu' => Yii::t('common', 'Tuesday'),
            'We' => Yii::t('common', 'Wednesday'),
            'Th' => Yii::t('common', 'Thursday'),
            'Fr' => Yii::t('common', 'Friday'),
            'Sa' => Yii::t('common', 'Saturday'),
            'Su' => Yii::t('common', 'Sunday')
        ];
    }

    /**
     * Lists all AudioAdvTimetable models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AudioAdvTimetableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'reclams' => $this->reclams,
                    'daysofweek' => $this->daysofweek
        ]);
    }

    /**
     * Displays a single AudioAdvTimetable model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AudioAdvTimetable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AudioAdvTimetable();
        $model->status = 1;
        $model->lasttime_read = time();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'reclams' => $this->reclams,
                        'daysofweek' => $this->daysofweek
            ]);
        }
    }

    /**
     * Updates an existing AudioAdvTimetable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'reclams' => $this->reclams,
                        'daysofweek' => $this->daysofweek
            ]);
        }
    }

    /**
     * Deletes an existing AudioAdvTimetable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AudioAdvTimetable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AudioAdvTimetable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AudioAdvTimetable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
