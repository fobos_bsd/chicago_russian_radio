<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\SwitchInput;
?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Audio Advertising') : Yii::t('backend', 'Update Audio Advertising') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'audio_file')->textInput(['maxlength' => true]) ?>

        <div class="col-sm-12 no-padding">
            <?=
            $form->field($model, 'text_content')->widget(Redactor::className(), [
                'clientOptions' => [
                    'options' => [
                        'style' => 'height: 300px;',
                    ],
                    'convertDivs' => false,
                    'replaceDivs' => false,
                    'removeEmptyTags' => false,
                    'paragraphize' => false,
                    'pastePlainText' => true,
                    'autoresize' => true,
                    'convertVideoLinks' => true,
                    //'buttons' => ['format', 'bold', 'italic', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule', 'underline', 'ol', 'ul', 'indent', 'outdent'],
                    'plugins' => ['clips', 'fontcolor', 'imagemanager', 'advanced']
                ]
            ])
            ?>
        </div>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

        <div class="col-sm-2 col-lg-2 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-warning']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
