<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\SwitchInput;
?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                    'options' => [
                        'data-pjax' => 1
                    ],
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-5 col-lg-5">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-sm-5 col-lg-6 no-padding">
            <?= $form->field($model, 'audio_file') ?>
        </div>

        <?= $form->field($model, 'text_content') ?>

        <?= $form->field($model, 'description') ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>

        <div class="clearfix"></div>

        <?php // echo $form->field($model, 'site_id') ?>

        <?php // echo $form->field($model, 'created_at')  ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-warning']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/reclams/audio', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?> 
    </div>
</div>
