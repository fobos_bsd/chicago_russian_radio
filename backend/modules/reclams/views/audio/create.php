<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AudioAdvertising */

$this->title = Yii::t('backend', 'Create Audio Advertising');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Audio Advertisings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audio-advertising-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
