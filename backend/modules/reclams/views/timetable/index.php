<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

$this->title = Yii::t('backend', 'Audio Adv Timetables');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audio-adv-timetable-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'reclams' => $reclams, 'daysofweek' => $daysofweek]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Timetable'), ['create'], ['class' => 'btn btn-warning']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'label' => 'ID',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'audio_advertising_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->audioAdvertising->name;
                        }
                    ],
                    ['attribute' => 'day_of_week',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) use ($daysofweek) {
                            $week = $daysofweek[$data->day_of_week];
                            return $week;
                        }
                    ],
                    ['attribute' => 'start_time',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'from_date',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return date('m/d/Y', strtotime($data->from_date));
                        }
                    ],
                    ['attribute' => 'to_date',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return date('m/d/Y', strtotime($data->to_date));
                        }
                    ],
                    ['attribute' => 'status',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            if ($data->status == 1) {
                                return '<i class="fa fa-toggle-on" style="color: #00a65a;" aria-hidden="true"></i>';
                            } else {
                                return '<i class="fa fa-toggle-off" style="color: #dd4b39;" aria-hidden="true"></i>';
                            }
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{update} &nbsp; {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ])
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
