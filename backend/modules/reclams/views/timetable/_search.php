<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\time\TimePicker;
?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                    'options' => [
                        'data-pjax' => 1
                    ],
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>

        <div class="col-sm-4 col-lg-5">
            <?=
            $form->field($model, 'day_of_week')->widget(Select2::className(), [
                'data' => $daysofweek,
                'options' => ['multiple' => false, 'placeholder' => 'Select day of weeke ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>

        <div class="col-sm-4 col-lg-5 no-paddingleft">
            <?=
            $form->field($model, 'start_time')->widget(TimePicker::classname(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                ]
            ])
            ?>
        </div>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>

        <?=
        $form->field($model, 'audio_advertising_id')->widget(Select2::className(), [
            'data' => $reclams,
            'options' => ['multiple' => false, 'placeholder' => 'Select reclam ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ?>

        <?php // echo $form->field($model, 'from_date') ?>

        <?php // echo $form->field($model, 'to_date')  ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-warning']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/reclams/timetable', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
