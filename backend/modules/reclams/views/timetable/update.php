<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AudioAdvTimetable */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
            'modelClass' => 'Audio Adv Timetable',
        ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Audio Adv Timetables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="audio-adv-timetable-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'reclams' => $reclams,
        'daysofweek' => $daysofweek
    ])
    ?>

</div>
