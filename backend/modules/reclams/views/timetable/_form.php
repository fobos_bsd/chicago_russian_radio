<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\time\TimePicker,
    kartik\date\DatePicker;
?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Timetable') : Yii::t('backend', 'Update Timetable') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-4 col-lg-4 no-padding">
            <?=
            $form->field($model, 'from_date')->widget(DatePicker::className(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'Select date from...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-4">
            <?=
            $form->field($model, 'to_date')->widget(DatePicker::className(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'Select date to...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-4 no-padding">
            <?=
            $form->field($model, 'start_time')->widget(TimePicker::classname(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                ]
            ])
            ?>
        </div>
        <div class="col-sm-6 col-lg-6 no-padding">
            <?=
            $form->field($model, 'audio_advertising_id')->widget(Select2::className(), [
                'data' => $reclams,
                'options' => ['multiple' => false, 'placeholder' => 'Select reclam ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-4">
            <?=
            $form->field($model, 'day_of_week')->widget(Select2::className(), [
                'data' => $daysofweek,
                'options' => ['multiple' => false, 'placeholder' => Yii::t('backend', 'Select day of weeke ...')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-2 col-lg-2 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>

        <?= $form->field($model, 'lasttime_read')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-warning']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
