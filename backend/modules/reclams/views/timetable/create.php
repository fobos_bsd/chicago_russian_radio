<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AudioAdvTimetable */

$this->title = Yii::t('backend', 'Create Audio Adv Timetable');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Audio Adv Timetables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audio-adv-timetable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'reclams' => $reclams,
        'daysofweek' => $daysofweek
    ])
    ?>

</div>
