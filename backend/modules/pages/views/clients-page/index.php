<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

$this->title = Yii::t('backend', 'Clients Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-page-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel, 'langs' => $langs]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Clients Page'), ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="box-body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'label' => 'ID',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'site_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->site->name;
                        }
                    ],
                    ['attribute' => 'lang_id',
                        'label' => 'Language',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->lang->name;
                        }
                    ],
                    ['attribute' => 'clients_id',
                        'label' => 'Client',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->clients->name;
                        }
                    ],
                    ['attribute' => 'title',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'slug',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'status',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            if ($data->status == 1) {
                                return '<i class="fa fa-toggle-on" style="color: #00a65a;" aria-hidden="true"></i>';
                            } else {
                                return '<i class="fa fa-toggle-off" style="color: #dd4b39;" aria-hidden="true"></i>';
                            }
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{update} &nbsp; {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ])
            ?>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
