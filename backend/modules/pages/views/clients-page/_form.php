<?php

use yii\bootstrap\Html,
    yii\helpers\Url,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\FileInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Client Page') : Yii::t('backend', 'Update Client Page') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(['id' => 'client_page']); ?>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="col-sm-4 col-lg-5 no-padding">
            <?=
            $form->field($model, 'clients_id')->widget(Select2::className(), [
                'data' => $clients,
                'options' => ['multiple' => false, 'placeholder' => 'Select client ...']
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-5">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...']
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-2 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'slug')->hiddenInput(['maxlength' => true, 'disabled' => 'disabled'])->label(false) ?>

        <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

        <div class="col-sm-12 no-padding">
            <?=
            $form->field($model, 'content')->widget(Redactor::className(), [
                'clientOptions' => [
                    'options' => [
                        'style' => 'height: 300px;',
                    ],
                    'convertDivs' => false,
                    'replaceDivs' => false,
                    'removeEmptyTags' => false,
                    'paragraphize' => false,
                    'pastePlainText' => true,
                    'autoresize' => true,
                    'convertVideoLinks' => true,
                    //'buttons' => ['format', 'bold', 'italic', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule', 'underline', 'ol', 'ul', 'indent', 'outdent'],
                    'plugins' => ['clips', 'fontcolor', 'imagemanager', 'advanced']
                ]
            ])
            ?>
        </div>

        <?= $form->field($model, 'text_css')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'text_js')->textarea(['rows' => 6]) ?>

        <div class="col-md-12 thumbbox">
            <?php
            if (isset($model->thumb)) {
                ?>
                <?= Html::img('/frontend/web/uploads/clients_page/' . $model->thumb, ['class' => 'img-responsive']) ?>
                <a class="remimg" href="javascript:;" data-item="<?= $model->id ?>" data-file="<?= $model->thumb ?>" data-type="thumb"><i class="fa fa-remove"></i></a>
                <?php
            }
            ?>
            <?=
            $form->field($model, 'thumb')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'showCaption' => false,
                    'showUpload' => false,
                    'showRemove' => false,
                    'fileActionSettings' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        'showCaption' => false,
                    ],
                ],
            ])
            ?>
        </div>

        <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save and Close'), ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('backend', 'Cancel'), Url::toRoute('index'), ['class' => 'btn btn-default']); ?>
            <?php if (!$model->isNewRecord) echo Html::a(Yii::t('backend', 'Delete'), Url::toRoute(['delete', 'id' => $model->id]), ['class' => 'btn btn-danger', 'method' => 'post']); ?>
            <?php
            if (!$model->isNewRecord)
                echo Html::a(Yii::t('backend', 'Preview'), '/our-clients/' . $model->slug, ['id' => 'seve-previe', 'class' => 'btn btn-warning', 'target' => '_blank']);
            ?>
            <?= Html::a(Yii::t('backend', 'Save'), 'javascript:;', ['id' => 'only_save', 'class' => 'btn btn-warning']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
$modelID = (isset($model->id)) ? $model->id : 0;

$script = <<< JS
   $("a.remimg").on('click', function(event){
        event.preventDefault();
        
        var idad = $modelID;
        var filename = $(this).data("file");
        
        if (confirm("Вы действительно хотите удалить это изображение?")) {
            $.post('/admin/pages/clients-page/delete-image',{idadv: idad, filename: filename}, function(){
                    $(".thumbbox").children("img.img-responsive").remove();
                    $(".thumbbox").children("a.remimg").remove();
            });
        }
   });
        
   $("a#only_save").on('click', function(event){
        event.preventDefault();
        
        var action = $("form#client_page").attr("action");
        var formdata = $("form#client_page").serialize();

        $.post(action,(formdata +'&'+$.param({'onlysave': 'yes'})));
   });
JS;

$this->registerJs($script, yii\web\View::POS_READY);
