<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ClientsPage */

$this->title = Yii::t('backend', 'Create Clients Page');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Clients Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'clients' => $clients,
        'langs' => $langs
    ])
    ?>

</div>
