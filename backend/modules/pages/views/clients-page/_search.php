<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\search\ClientsPageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                    'options' => [
                        'data-pjax' => 1
                    ],
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-4 col-lg-4">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...']
            ])
            ?>
        </div>
        <div class="col-sm-3 col-lg-1 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
            <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-6 col-lg-6 no-paddingleft">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-sm-6 col-lg-6 no-padding">
            <?= $form->field($model, 'slug') ?>
        </div>

        <?php // echo $form->field($model, 'clients_id') ?>

        <?php // echo $form->field($model, 'keywords') ?>

        <?php // echo $form->field($model, 'description') ?>

        <?php // echo $form->field($model, 'content') ?>

        <?php // echo $form->field($model, 'created_at')  ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
