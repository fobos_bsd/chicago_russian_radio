<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ClientsPage */

$this->title = Yii::t('backend', 'Update Clients Page: {nameAttribute}', [
            'nameAttribute' => $model->title,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Clients Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="clients-page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'clients' => $clients,
        'langs' => $langs
    ])
    ?>

</div>
