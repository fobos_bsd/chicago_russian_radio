<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-5 col-lg-6">
            <?=
            $form->field($model, 'user_id')->widget(Select2::className(), [
                'data' => $users,
                'options' => ['multiple' => false, 'placeholder' => 'Select author ...']
            ])
            ?>
        </div>
        <div class="col-sm-3 col-lg-1 no-paddingleft">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
            <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>
        </div>
        <div class="col-sm-4 col-lg-4 no-padding">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...']
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <?= $form->field($model, 'name') ?>

        <?php // echo $form->field($model, 'slug') ?>



        <?= $form->field($model, 'content') ?>

        <?php // echo $form->field($model, 'meta_title') ?>

        <?php // echo $form->field($model, 'meta_key') ?>

        <?php // echo $form->field($model, 'meta_description') ?>

        <?php // echo $form->field($model, 'params') ?>

        <?php // echo $form->field($model, 'thumb') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_at')  ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/pages/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
