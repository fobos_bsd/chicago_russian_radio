<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\FileInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Page') : Yii::t('backend', 'Update Page') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="col-sm-4 col-lg-5 no-padding">
            <?=
            $form->field($model, 'user_id')->widget(Select2::className(), [
                'data' => $users,
                'options' => ['multiple' => false, 'placeholder' => 'Select author ...']
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-5">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...']
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-2 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'slug')->hiddenInput(['maxlength' => true, 'disabled' => 'disabled'])->label(false) ?>

        <div class="col-sm-12 no-padding">
            <?=
            $form->field($model, 'content')->widget(Redactor::className(), [
                'clientOptions' => [
                    'options' => [
                        'style' => 'height: 300px;',
                    ],
                    'convertDivs' => false,
                    'replaceDivs' => false,
                    'removeEmptyTags' => false,
                    'paragraphize' => false,
                    'pastePlainText' => true,
                    'autoresize' => true,
                    'convertVideoLinks' => true,
                    //'buttons' => ['format', 'bold', 'italic', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule', 'underline', 'ol', 'ul', 'indent', 'outdent'],
                    'plugins' => ['clips', 'fontcolor', 'imagemanager', 'advanced']
                ]
            ])
            ?>
        </div>



        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_key')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'params')->textarea(['rows' => 6]) ?>

        <div class="col-sm-12 no-padding thumbbox">
            <?php
            if (isset($model->thumb)) {
                ?>
                <?= Html::img('/frontend/web/uploads/pages/' . $model->thumb, ['class' => 'img-responsive']) ?>
                <a class="remimg" href="javascript:;" data-item="<?= $model->id ?>" data-file="<?= $model->thumb ?>" data-type="thumb"><i class="fa fa-remove"></i></a>
                <?php
            }
            ?>
            <?=
            $form->field($model, 'thumb')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'showCaption' => false,
                    'showUpload' => false,
                    'showRemove' => false,
                    'fileActionSettings' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        'showCaption' => false,
                    ],
                ],
            ])
            ?>
        </div>

        <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
$modelID = (isset($model->id)) ? $model->id : 0;

$script = <<< JS
   $("a.remimg").on('click', function(event){
        event.preventDefault();
        
        var idad = $modelID;
        var filename = $(this).data("file");
        typerm = $(this).data("type");
        
        if (confirm("Вы действительно хотите удалить это изображение?")) {
            $.post('/admin/pages/index/delete-image',{idadv: idad, filename: filename, typerm: typerm}, function(){
                if(typerm === 'thumb') {
                    $(".thumbbox").children("img.img-responsive").remove();
                    $(".thumbbox").children("a.remimg").remove();
                }
            });
        }
   });
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>
