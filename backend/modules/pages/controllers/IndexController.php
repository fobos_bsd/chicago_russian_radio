<?php

namespace backend\modules\pages\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper,
    yii\web\UploadedFile;
use common\models\Pages,
    common\models\search\PageSearch,
    common\models\User,
    common\models\Lang;

/**
 * IndexController implements the CRUD actions for Pages model.
 */
class IndexController extends Controller {

    public $users, $langs;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-image'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->users = ArrayHelper::map(User::find()->where(['is not', 'confirmed_at', null])->andWhere(['is', 'blocked_at', null])->asArray()->all(), 'id', 'username');
        $this->langs = ArrayHelper::map(Lang::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'users' => $this->users,
                    'langs' => $this->langs
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Pages();
        $model->site_id = Yii::$app->Siteselector->getSitesCookie();
        $model->created_at = time();
        $model->updated_at = time();
        $model->lang_id = Yii::$app->Siteselector->getSiteLangugeID();
        $model->user_id = Yii::$app->user->id;
        $model->status = 1;

        if ($model->load(Yii::$app->request->post())) {
            $model->thumb = UploadedFile::getInstance($model, 'thumb');
            if ($model->insert()) {
                return $this->redirect(['index']);
            } else {
                var_dump($model->getErrors());
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'users' => $this->users,
                        'langs' => $this->langs
            ]);
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->updated_at = time();

        if ($model->load(Yii::$app->request->post())) {
            $model->thumb = UploadedFile::getInstance($model, 'thumb');
            if ($model->update())
                return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'users' => $this->users,
                        'langs' => $this->langs
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * @return strng
     */

    public function actionDeleteImage() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $post = Yii::$app->request->post();

            if (isset($post['idadv']) && isset($post['filename']) && isset($post['typerm'])) {
                $id = (int) strip_tags(trim($post['idadv']));
                $file = (string) strip_tags(trim($post['filename']));
                $typerm = (string) strip_tags(trim($post['typerm']));

                $fullpath = Yii::getAlias('@frontend') . '/web/uploads/pages/' . $file;

                if (file_exists($fullpath))
                    unlink($fullpath);

                $model = $this->findModel($id);
                $model->$typerm = null;

                $model->save();
            }
        }
    }

}
