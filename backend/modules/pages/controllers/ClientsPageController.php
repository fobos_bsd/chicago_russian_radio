<?php

namespace backend\modules\pages\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\AccessControl,
    yii\filters\VerbFilter,
    yii\helpers\ArrayHelper,
    yii\helpers\Url,
    yii\web\UploadedFile;
use common\models\ClientsPage,
    common\models\search\ClientsPageSearch,
    common\models\Clients,
    common\models\Lang;

/**
 * ClientsPageController implements the CRUD actions for ClientsPage model.
 */
class ClientsPageController extends Controller {

    public $clients, $langs;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-image'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->clients = ArrayHelper::map(Clients::find()->asArray()->all(), 'id', 'name');
        $this->langs = ArrayHelper::map(Lang::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * Lists all ClientsPage models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ClientsPageSearch();
        $searchModel->site_id = Yii::$app->Siteselector->getSitesCookie();
        $searchModel->lang_id = Yii::$app->Siteselector->getSiteLangugeID();
        $searchModel->status = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'langs' => $this->langs,
                    'clients' => $this->clients
        ]);
    }

    /**
     * Displays a single ClientsPage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ClientsPage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new ClientsPage();
        $model->status = 1;
        $model->site_id = Yii::$app->Siteselector->getSitesCookie();
        $model->lang_id = Yii::$app->Siteselector->getSiteLangugeID();
        $model->created_at = date('Y-m-d');

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $model->thumb = UploadedFile::getInstance($model, 'thumb');
            if ($model->save()) {
                self::saveStylesData($model->id, $model->text_css, $model->text_js);
                if (!isset($post['onlysave']) || $post['onlysave'] !== 'yes') {
                    return $this->redirect(['index']);
                } else {
                    return $this->redirect(Url::toRoute('clients-page/update/' . $model->id));
                }
            } else {
                var_dump($model->getErrors());
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'langs' => $this->langs,
                        'clients' => $this->clients
            ]);
        }
    }

    /**
     * Updates an existing ClientsPage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $curent_thumb = $model->thumb;

        // styles data
        $pagedir = Yii::getAlias('@frontend') . '/web/uploads/clients_page/' . $id;
        $model->text_css = file_get_contents($pagedir . '/client.css');
        $model->text_js = file_get_contents($pagedir . '/client.js');

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();

            $new_thumb = UploadedFile::getInstance($model, 'thumb');
            $model->thumb = (isset($new_thumb)) ? $new_thumb : $curent_thumb;
            if ($model->save()) {
                self::saveStylesData($model->id, $model->text_css, $model->text_js);
                if (!isset($post['onlysave']) || $post['onlysave'] !== 'yes') {
                    return $this->redirect(['index']);
                }
            } else {
                var_dump($model->getErrors());
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'langs' => $this->langs,
                        'clients' => $this->clients
            ]);
        }
    }

    /**
     * Deletes an existing ClientsPage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClientsPage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientsPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ClientsPage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected static function saveStylesData($id, $data_style = '', $data_js = '') {
        $pagedir = Yii::getAlias('@frontend') . '/web/uploads/clients_page/' . $id;
        if (!is_dir($pagedir)) {
            mkdir($pagedir, 0775, true);
        }
        $file_style = $pagedir . '/client.css';
        $file_js = $pagedir . '/client.js';

        file_put_contents($file_style, $data_style);
        file_put_contents($file_js, $data_js);
    }

    public function actionDeleteImage() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $post = Yii::$app->request->post();

            if (isset($post['idadv']) && isset($post['filename'])) {
                $id = (int) strip_tags(trim($post['idadv']));
                $file = (string) strip_tags(trim($post['filename']));

                $fullpath = Yii::getAlias('@frontend') . '/web/uploads/clients_page/' . $file;

                if (file_exists($fullpath))
                    unlink($fullpath);

                $model = $this->findModel($id);
                $model->thumb = null;
                $model->save();
            }
        }
    }

}
