<?php

namespace backend\modules\advertising\models;

use Yii,
    yii\base\Model,
    yii\helpers\ArrayHelper,
    yii\helpers\Json,
    yii\bootstrap\Html,
    yii\data\ActiveDataProvider;
use common\models\AdvCalendar,
    common\models\AdvOrders,
    common\models\AdvMesh,
    common\models\Clients,
    common\models\AdvUsedmesh;

class Calendar extends Model {

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function getCalendarData($fromday, $today) {
        $query = AdvCalendar::find()->where(['>=', 'online_day', $fromday])->andWhere(['<=', 'online_day', $today]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'online_day' => SORT_DESC,
                    'gravity' => SORT_ASC,
                    'adv_mesh_id' => SORT_ASC,
                ]
            ]
        ]);

        return $dataProvider;
    }

    public function getUsedMeshDate($from, $to) {
        if (($model = AdvUsedmesh::find()->where(['>=', 'used_date', $from])->andWhere(['<=', 'used_date', $to])->all()) !== null) {
            return $model;
        } else {
            return false;
        }
    }

    public function getUsedMeshOneDate($sdate) {
        if (($model = AdvUsedmesh::find()->where(['used_date' => $sdate])->all()) !== null) {
            return $model;
        } else {
            return false;
        }
    }

    public function getUsedmesh() {
        if (($model = AdvUsedmesh::find()->all()) !== null) {
            return $model;
        } else {
            return false;
        }
    }

    public function findCalendarOne($sdate) {
        if (($models = AdvCalendar::find()->where(['online_day' => $sdate])->orderBy(['adv_orders_id' => SORT_ASC])->all()) !== null) {
            return $models;
        } else {
            return null;
        }
    }

    public function getCalendarOne($sdate) {
        $data = false;
        $result = array();
        $models = $this->findCalendarOne($sdate);
        if (isset($models) && count($models) > 0) {
            foreach ($models as $model) {
                if (!array_key_exists($model->adv_orders_id, $result)) {
                    $result[$model->adv_orders_id]['id'] = $model->id;
                    $result[$model->adv_orders_id]['order_id'] = $model->adv_orders_id;
                    $result[$model->adv_orders_id]['client'] = $model->advOrders->clients->name;
                    $result[$model->adv_orders_id]['title'] = $model->advOrders->title;
                    $result[$model->adv_orders_id]['audio'] = $model->advOrders->fileAudio;
                    $result[$model->adv_orders_id]['how_seconds'] = $model->advOrders->how_seconds;
                    $result[$model->adv_orders_id]['count_days'] = $model->advOrders->count_days;
                    $result[$model->adv_orders_id]['count_onday'] = $model->advOrders->count_onday;
                    $result[$model->adv_orders_id]['used_days'] = $model->advOrders->used_days;
                    $result[$model->adv_orders_id]['mesh_id'][] = ['id' => $model->adv_mesh_id, 'count_time' => $model->count_time];
                } else {
                    $result[$model->adv_orders_id]['mesh_id'][] = ['id' => $model->adv_mesh_id, 'count_time' => $model->count_time];
                }
            }
        }

        if (count($result) > 0) {
            $i = 0;
            foreach ($result as $res) {
                $data[$i] = $res;
                $i++;
            }
        }

        return $data;
    }

    public function findUsedMesh($id, $date) {
        if (($model = AdvUsedmesh::find()->where(['adv_mesh_id' => $id, 'used_date' => $date])->one()) !== null) {
            return $model;
        } else {
            return false;
        }
    }

    public function updateMersh($model) {
        $order = AdvOrders::find()->where(['id' => $model->adv_orders_id])->one();
        $msh = AdvMesh::find()->where(['id' => $model->adv_mesh_id])->one();

        $mesh = AdvUsedmesh::find()->where(['adv_mesh_id' => $model->adv_mesh_id, 'used_date' => $model->online_day])->one();
        $mesh->used = (int) $mesh->used - ((int) $order->how_seconds * (int) $model->count_time);
        $mesh->less_seconds = (int) $mesh->less_seconds + ((int) $order->how_seconds * (int) $model->count_time);
        $pocent = 100 * (int) $mesh->used / (int) $msh->long;
        $mesh->used_procent = round($pocent, 0);

        if (!$mesh->save()) {
            var_dump($mesh->getErrors());
        }
    }

    public function updateOrders($model) {
        $order = AdvOrders::find()->where(['id' => $model->adv_orders_id])->one();
        if ((int) $order->used_days > 0) {
            $order->used_days = (int) $order->used_days - 1;
        }

        $order->update();
    }

    public function getSummViews($id, $date, $oldid) {
        $summ = (int) AdvCalendar::find()->where(['online_day' => $date, 'adv_orders_id' => $id])->andWhere(['<>', 'id', $oldid])->sum('count_time');

        return $summ;
    }

}
