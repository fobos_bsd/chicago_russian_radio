<?php

namespace backend\modules\advertising\models;

use Yii,
    yii\base\Model;
use common\models\AdvCalendar,
    common\models\AdvOrders,
    common\models\AdvMesh,
    common\models\AdvUsedmesh;

class CalendarOrders extends Model {

    public $order_id, $mesh_id, $start, $finish, $count_days, $status;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['order_id', 'mesh_id', 'start', 'status', 'count_days'], 'required'],
            [['order_id', 'mesh_id', 'status', 'count_days'], 'integer'],
            [['start', 'finish'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'order_id' => Yii::t('frontend', 'Order ID'),
            'mesh_id' => Yii::t('frontend', 'Mesh ID'),
            'start' => Yii::t('frontend', 'Start date'),
            'finish' => Yii::t('frontend', 'Finish date'),
            'count_days' => Yii::t('frontend', 'Count days'),
            'status' => Yii::t('frontend', 'Status'),
        ];
    }

    public function saveChangeOrder() {
        // Get order data
        $oderdata = AdvOrders::find()->where(['id' => $this->order_id])->one();

        // Get mesh data
        $meshdata = AdvMesh::find()->where(['id' => $this->mesh_id])->one();

        if (!isset($this->finish))
            $this->finish = date('Y-m-d', strtotime($this->start . '+61 days'));

        $date = $this->start;

        while ($date <= $this->finish) {
            if ($this->status == 1) {
                switch ($this->count_days) {
                    case 7:
                        $model = new AdvCalendar();
                        $model->adv_orders_id = $this->order_id;
                        $model->adv_mesh_id = $this->mesh_id;
                        $model->online_day = $date;
                        $model->gravity = 1;
                        $model->count_time = 1;

                        if (!$model->save()) {
                            var_dump($model->getErrors());
                        }
                        $this->updateUsedMesh($model->adv_mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                        break;
                    case 5: if (date('w', strtotime($date)) !== '0' && date('w', strtotime($date)) !== '6') {
                            $model = new AdvCalendar();
                            $model->adv_orders_id = $this->order_id;
                            $model->adv_mesh_id = $this->mesh_id;
                            $model->online_day = $date;
                            $model->gravity = 1;
                            $model->count_time = 1;

                            $model->insert();
                            $this->updateUsedMesh($model->adv_mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                        }
                        break;
                    case 2: if (date('w', strtotime($date)) === '0' || date('w', strtotime($date)) === '6') {
                            $model = new AdvCalendar();
                            $model->adv_orders_id = $this->order_id;
                            $model->adv_mesh_id = $this->mesh_id;
                            $model->online_day = $date;
                            $model->gravity = 1;
                            $model->count_time = 1;

                            $model->insert();
                            $this->updateUsedMesh($model->adv_mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                        }
                        break;
                    case 4:
                        if (date('w', strtotime($date)) === '1' || date('w', strtotime($date)) === '3' || date('w', strtotime($date)) === '5' || date('w', strtotime($date)) === '0') {
                            $model = new AdvCalendar();
                            $model->adv_orders_id = $this->order_id;
                            $model->adv_mesh_id = $this->mesh_id;
                            $model->online_day = $date;
                            $model->gravity = 1;
                            $model->count_time = 1;

                            $model->insert();
                            $this->updateUsedMesh($model->adv_mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                        }
                        break;
                    case 3:
                        if (date('w', strtotime($date)) === '2' || date('w', strtotime($date)) === '4' || date('w', strtotime($date)) === '6') {
                            $model = new AdvCalendar();
                            $model->adv_orders_id = $this->order_id;
                            $model->adv_mesh_id = $this->mesh_id;
                            $model->online_day = $date;
                            $model->gravity = 1;
                            $model->count_time = 1;

                            $model->insert();
                            $this->updateUsedMesh($model->adv_mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                        }
                        break;
                }
            } else {
                switch ($this->count_days) {
                    case 7: $this->daleteUsedMesh($this->mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                        $model_del = AdvCalendar::find()->where(['adv_orders_id' => $this->order_id, 'adv_mesh_id' => $this->mesh_id, 'online_day' => $date])->one();
                        if (!$model_del->delete()) {
                            var_dump($model_del->getErrors());
                            die();
                        } else {
                            $model_del = AdvCalendar::find()->where(['adv_orders_id' => $this->order_id, 'adv_mesh_id' => $this->mesh_id, 'online_day' => $date])->one();
                        }
                        break;
                    case 5: if (date('w', strtotime($date)) !== '0' && date('w', strtotime($date)) !== '6') {
                            $model_del = AdvCalendar::find()->where(['adv_orders_id' => $this->order_id, 'adv_mesh_id' => $this->mesh_id, 'online_day' => $date])->one();
                            if (!$model_del->delete()) {
                                var_dump($model_del->getErrors());
                                die();
                            } else {
                                $this->daleteUsedMesh($this->mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                            }
                        }
                        break;
                    case 2: if (date('w', strtotime($date)) === '0' || date('w', strtotime($date)) === '6') {
                            $model_del = AdvCalendar::find()->where(['adv_orders_id' => $this->order_id, 'adv_mesh_id' => $this->mesh_id, 'online_day' => $date])->one();
                            if (!$model_del->delete()) {
                                var_dump($model_del->getErrors());
                                die();
                            } else {
                                $this->daleteUsedMesh($this->mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                            }
                        }
                        break;
                    case 4: if (date('w', strtotime($date)) === '1' || date('w', strtotime($date)) === '3' || date('w', strtotime($date)) === '5' || date('w', strtotime($date)) === '0') {
                            $model_del = AdvCalendar::find()->where(['adv_orders_id' => $this->order_id, 'adv_mesh_id' => $this->mesh_id, 'online_day' => $date])->one();
                            if (!$model_del->delete()) {
                                var_dump($model_del->getErrors());
                                die();
                            } else {
                                $this->daleteUsedMesh($this->mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                            }
                        }
                        break;
                    case 3: if (date('w', strtotime($date)) === '2' || date('w', strtotime($date)) === '4' || date('w', strtotime($date)) === '6') {
                            $model_del = AdvCalendar::find()->where(['adv_orders_id' => $this->order_id, 'adv_mesh_id' => $this->mesh_id, 'online_day' => $date])->one();
                            if (!$model_del->delete()) {
                                var_dump($model_del->getErrors());
                                die();
                            } else {
                                $this->daleteUsedMesh($this->mesh_id, $oderdata->how_seconds, $meshdata->long, $date);
                            }
                        }
                        break;
                }
            }

            $date = date('Y-m-d', strtotime($date . '+1 day'));
        }

        return $date;
    }

    private function updateUsedMesh($mesh_id, $how_seconds, $long, $date) {

        $model = AdvUsedmesh::find()->where(['adv_mesh_id' => $mesh_id, 'used_date' => $date])->one();

        if (!isset($model) || $model == false) {
            $model_mesh = new AdvUsedmesh();
            $model_mesh->adv_mesh_id = $mesh_id;
            $model_mesh->used = $how_seconds;
            $model_mesh->less_seconds = $long - $how_seconds;
            $model_mesh->used_procent = round(100 * $long / $how_seconds, 0);
            $model_mesh->used_date = $date;
            if (!$model->save()) {
                var_dump($model_mesh->getErrors());
                die();
            }
        }
    }

    private function daleteUsedMesh($mesh_id, $how_seconds, $long, $date) {

        $model = AdvUsedmesh::find()->where(['adv_mesh_id' => $mesh_id, 'used_date' => $date])->one();

        if (isset($model) && $model !== false && $model->used > 0) {
            $less_seconds = (int) $model->less_seconds;
            $used = (int) $model->used;
            $used_procent = (float) $model->used_procent;

            $model->used = $used - $how_seconds;
            $model->less_seconds = $long - $model->used;
            $model->used_procent = round(100 * $model->used / $long, 0);

            if (!$model->save()) {
                var_dump($model->getErrors());
                die();
            }
        }
    }

}
