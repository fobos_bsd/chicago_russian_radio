<?php

namespace backend\modules\advertising\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper,
    yii\data\Pagination;
use common\models\AdvCalendar,
    backend\modules\advertising\models\Calendar,
    common\models\AdvOrders,
    common\models\AdvMesh,
    backend\modules\advertising\models\CalendarOrders,
    common\models\AdvUsedmesh;

/**
 * AdvCalendarController implements the CRUD actions for AdvCalendar model.
 */
class AdvCalendarController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'ajax-calendar', 'view', 'create', 'update', 'delete', 'index-by-date'],
                        'roles' => ['Администраторы', 'Менеджеры'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'index-by-date'],
                        'roles' => ['Radioadmins'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Vie all AdvCalendar models by date.
     * @return mixed
     */
    public function actionIndexByDate() {
        if (Yii::$app->request->queryParams) {
            $params = Yii::$app->request->queryParams;
            $startday = (isset($params['start_day']) && !empty($params['start_day'])) ? $params['start_day'] : date('Y/d/m');
            $finishday = (isset($params['end_day']) && !empty($params['end_day'])) ? $params['end_day'] : date('Y/d/m', strtotime('+7 days'));
        } else {
            $startday = date('Y/d/m');
            $finishday = date('Y/d/m', strtotime('+7 days'));
        }

        // Convert date to normaldate

        $sdate = date_create_from_format('Y/d/m', $startday);
        $cstart = date_format($sdate, 'Y-m-d');
        $unixstart = strtotime($cstart);
        $fdate = date_create_from_format('Y/d/m', $finishday);
        $cend = date_format($fdate, 'Y-m-d');
        $unixfinish = strtotime($cend);

        // Get all mesh
        $meshs = $this->getMesh();

        // Get orders
        $orders = $this->getOrders();


        $search = new Calendar();
        $dataProvider = $search->getCalendarData($startday, $finishday);

        return $this->render('index-bydate', [
                    'dataProvider' => $dataProvider,
                    'startday' => $startday,
                    'finishday' => $finishday,
                    'unixstart' => $unixstart,
                    'unixfinish' => $unixfinish,
                    'meshs' => $meshs,
                    'orders' => $orders,
                    'usedmeshs' => $search->getUsedMeshDate($cstart, $cend)
        ]);
    }

    /**
     * Lists all AdvCalendar models New.
     * @return mixed
     */
    public function actionIndex($sort = 1) {
        // Get all mesh
        $meshs = $this->getMesh($sort);

        // Get all orders
        $query = AdvOrders::find()->where(['>=', 'finish_at', date('Y-m-d')])->andWhere(['site_id' => Yii::$app->Siteselector->getSitesCookie()])->orderBy(['start_at' => SORT_DESC, 'finish_at' => SORT_DESC, 'id' => SORT_DESC]);

        // Make Dataprovider for export
        $fullQuery = clone $query;
        $fullQuery->with('advCalendars')->all();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $fullQuery
        ]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => 8
        ]);
        $orders = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        // Get calendar
        $calendar = $this->findCalendar();

        // Get meshs used
        $meshdata = AdvUsedmesh::find()->orderBy(['adv_mesh_id' => SORT_ASC])->asArray()->all();

        // Summ mashtimes
        $arrmeshsum = array();
        foreach ($meshdata as $meshu) {
            if (isset($old_key) && $old_key == $meshu['adv_mesh_id']) {
                $arrmeshsum[$meshu['adv_mesh_id']] += (int) $meshu['used'];
            } else {
                $arrmeshsum[$meshu['adv_mesh_id']] = (int) $meshu['used'];
            }
            $old_key = $meshu['adv_mesh_id'];
        }

        return $this->render('index', [
                    'meshs' => $meshs,
                    'orders' => $orders,
                    'calendar' => $calendar,
                    'meshdata' => $meshdata,
                    'arrmeshsum' => $arrmeshsum,
                    'pages' => $pages,
                    'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single AdvCalendar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($sdate) {
        // Get events data
        $search = new Calendar();

        // Get all mesh
        $meshs = $this->getMesh();

        return $this->render('view', [
                    'models' => $search->getCalendarOne($sdate),
                    'sdate' => $sdate,
                    'meshs' => $meshs,
                    'usedmeshs' => $search->getUsedMeshOneDate($sdate)
        ]);
    }

    /**
     * Creates a new AdvCalendar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AdvCalendar();
        $model->count_time = 1;

        // Get orders
        $orders = ArrayHelper::toArray($this->getOrders());
        // Get mesh
        $meshs = ArrayHelper::toArray($this->getMesh());

        if ($model->load(Yii::$app->request->post())) {
            // Convert to mysql date format
            $sdate = date_create_from_format('Y/d/m', $model->online_day);
            $model->online_day = date_format($sdate, 'Y-m-d');

            //Test is new date
            if (AdvCalendar::find()->where(['adv_orders_id' => $model->adv_orders_id, 'online_day' => $model->online_day, 'adv_mesh_id' => $model->adv_mesh_id])->count() == 0) {
                if ($model->validate() && $model->save()) {
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Maybe you over limit use of day of times on the day/time');
                }
            } else {
                Yii::$app->getSession()->setFlash('error', 'Maybe you used the same day and time for this advertise');
            }
        }

        if (isset($model->online_day))
            $model->online_day = date("Y/m/d", strtotime($model->online_day));
        return $this->render('create', [
                    'model' => $model,
                    'orders' => ArrayHelper::map($orders, 'id', 'title'),
                    'meshs' => ArrayHelper::map($meshs, 'id', 'start_time')
        ]);
    }

    /**
     * Updates an existing AdvCalendar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        // Get orders
        $orders = ArrayHelper::toArray($this->getOrders());
        // Get mesh
        $meshs = ArrayHelper::toArray($this->getMesh());

        if ($model->load(Yii::$app->request->post())) {
            // Convert to mysql date format
            $sdate = date_create_from_format('Y/d/m', $model->online_day);
            $model->online_day = date_format($sdate, 'Y-m-d');

            // Get events data
            $search = new Calendar();

            $old_model = $this->findModel($id);

            // Update suport noutes
            $search->updateMersh($old_model);
            $search->updateOrders($old_model);

            if ($model->validate() && $model->save()) {
                return $this->redirect(['index']);
            } else {
                Yii::$app->getSession()->setFlash('error', 'Maybe you over limit use of day of times on the day/time');
            }
        }

        $model->online_day = date('Y/d/m', strtotime($model->online_day));

        return $this->render('update', [
                    'model' => $model,
                    'orders' => ArrayHelper::map($orders, 'id', 'title'),
                    'meshs' => ArrayHelper::map($meshs, 'id', 'start_time')
        ]);
    }

    /**
     * Deletes an existing AdvCalendar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        // Get events data
        $search = new Calendar();

        // Update suport noutes
        $search->updateMersh($model);
        $search->updateOrders($model);

        // Remove griate note
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAjaxCalendar() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $post = Yii::$app->request->post();

            $model = new CalendarOrders();
            $model->order_id = (int) $post['order_id'];
            $model->mesh_id = (int) $post['mesh_id'];
            $model->start = $post['start'];
            $model->finish = $post['finish'];
            $model->count_days = (int) $post['count_days'];
            $model->status = (int) $post['status'];

            if ($model->validate()) {
                // Save change model status
                $model->saveChangeOrder();
            } else {
                var_dump($model->getErrors());
            }
        }
    }

    /**
     * Finds the AdvCalendar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdvCalendar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AdvCalendar::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getOrders() {
        if (($model = AdvOrders::find()->where(['site_id' => Yii::$app->Siteselector->getSitesCookie()])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getMesh($sort = 1) {
        switch ($sort) {
            case 0: $arrsort = ['start_time' => SORT_ASC];
                break;
            case 1: $arrsort = ['spacialy' => SORT_DESC, 'start_time' => SORT_ASC];
                break;
            default: $arrsort = ['spacialy' => SORT_DESC, 'start_time' => SORT_ASC];
                break;
        }

        if (($model = AdvMesh::find()->where(['site_id' => Yii::$app->Siteselector->getSitesCookie()])->orderBy($arrsort)->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findCalendar() {
        if (($model = AdvCalendar::find()->asArray()->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
