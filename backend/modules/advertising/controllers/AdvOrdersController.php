<?php

namespace backend\modules\advertising\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper,
    yii\web\UploadedFile;
use common\models\AdvOrders,
    common\models\search\AdvOrdersSearch,
    common\models\Clients,
    common\models\AdvCalendar,
    common\models\AdvMesh,
    backend\modules\advertising\models\CalendarOrders;

/**
 * AdvOrdersController implements the CRUD actions for AdvOrders model.
 */
class AdvOrdersController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['Администраторы', 'Менеджеры', 'Radioadmins'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all AdvOrders models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AdvOrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Array of clients
        $clients = ArrayHelper::map($this->findClients(), 'id', 'name');

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'clients' => $clients
        ]);
    }

    /**
     * Displays a single AdvOrders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AdvOrders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $dir = Yii::getAlias('@backend/web/uploads/sound_reklam');
        $model = new AdvOrders();
        $model->setScenario('insert');
        $model->created_at = time();
        $model->used_days = 0;
        $model->site_id = Yii::$app->Siteselector->getSitesCookie();

        // Array of clients
        $clients = ArrayHelper::map($this->findClients(), 'id', 'name');

        // Array meshs
        $meshes = ArrayHelper::map(AdvMesh::find()->where(['site_id' => Yii::$app->Siteselector->getSitesCookie()])->orderBy(['start_time' => SORT_ASC])->asArray()->all(), 'id', 'start_time');

        if ($model->load(Yii::$app->request->post())) {
            //Upload file
            $model->fileAudio = UploadedFile::getInstance($model, 'fileAudio');

            // Upload to DB
            $sdate = date_create_from_format('Y/d/m', $model->start_at);
            $model->start_at = date_format($sdate, 'Y-m-d');
            $fdate = date_create_from_format('Y/d/m', $model->finish_at);
            $model->finish_at = date_format($fdate, 'Y-m-d');

            $post = Yii::$app->request->post();

            if ($model->save()) {
                $modelID = (int) $model->id;

                if (isset($post['AdvOrders']['time_period']) && count($post['AdvOrders']['time_period']) > 0) {
                    foreach ($post['AdvOrders']['time_period'] as $mesh) {
                        $this->Calendar($modelID, (int) $mesh, $model->start_at, $model->finish_at, $model->count_days, 1);
                    }
                }

                return $this->redirect(['index']);
            } else {
                var_dump($model->getErrors());
                die();
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'clients' => $clients,
                        'isused' => 0,
                        'meshes' => $meshes,
                        'usedmesh' => array()
            ]);
        }
    }

    /**
     * Updates an existing AdvOrders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        // Get calendar by order
        $calendar = AdvCalendar::find()->where(['adv_orders_id' => $id])->andWhere(['<=', 'online_day', date('Y-m-d')])->one();
        $isused = ($calendar !== null) ? 1 : 0;

        $uploaded = $model->fileAudio;
        $model->setScenario('update');
        $model->start_at = date('Y/d/m', strtotime($model->start_at));
        $model->finish_at = date('Y/d/m', strtotime($model->finish_at));

        // Array of clients
        $clients = ArrayHelper::map($this->findClients(), 'id', 'name');

        // Array meshs
        $meshes = ArrayHelper::map(AdvMesh::find()->where(['site_id' => Yii::$app->Siteselector->getSitesCookie()])->orderBy(['start_time' => SORT_ASC])->asArray()->all(), 'id', 'start_time');

        // Array meshe data
        $usedmesh = ArrayHelper::getColumn(AdvCalendar::find()->where(['adv_orders_id' => $model->id])->orderBy(['adv_mesh_id' => SORT_ASC])->asArray()->all(), 'adv_mesh_id');

        if ($model->load(Yii::$app->request->post())) {
            //Upload file
            $model->fileAudio = UploadedFile::getInstance($model, 'fileAudio');
            if (!isset($model->fileAudio))
                $model->fileAudio = $uploaded;

            // Upload to DB
            $sdate = date_create_from_format('Y/d/m', $model->start_at);
            $model->start_at = date_format($sdate, 'Y-m-d');
            $fdate = date_create_from_format('Y/d/m', $model->finish_at);
            $model->finish_at = date_format($fdate, 'Y-m-d');

            $post = Yii::$app->request->post();

            if ($model->save()) {
                $modelID = (int) $model->id;

                if (isset($post['AdvOrders']['time_period']) && count($post['AdvOrders']['time_period']) > 0) {
                    $arrmesh = array_unique(ArrayHelper::getColumn(AdvCalendar::find()->where(['adv_orders_id' => $modelID])->asArray()->all(), 'adv_mesh_id'));
                    foreach ($arrmesh as $oldmesh) {
                        $this->Calendar($modelID, (int) $oldmesh, $model->start_at, $model->finish_at, $model->count_days, 0);
                    }

                    foreach ($post['AdvOrders']['time_period'] as $mesh) {
                        $this->Calendar($modelID, (int) $mesh, $model->start_at, $model->finish_at, $model->count_days, 1);
                    }
                }

                return $this->redirect(['index']);
            } else {
                var_dump($model->getErrors());
                die();
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'clients' => $clients,
                        'isused' => $isused,
                        'meshes' => $meshes,
                        'usedmesh' => (isset($usedmesh) && count($usedmesh) > 0) ? array_unique($usedmesh) : array()
            ]);
        }
    }

    /**
     * Deletes an existing AdvOrders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function Calendar($order_id, $mesh_id, $start, $finish = null, $count_days, $status = 1) {

        $post = Yii::$app->request->post();

        $model = new CalendarOrders();
        $model->order_id = (int) $order_id;
        $model->mesh_id = (int) $mesh_id;
        $model->start = $start;
        $model->finish = $finish;
        $model->count_days = (int) $count_days;
        $model->status = (int) $status;

        if ($model->validate()) {
            // Save change model status
            $model->saveChangeOrder();
        } else {
            var_dump($model->getErrors());
        }
    }

    /**
     * Finds the AdvOrders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdvOrders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AdvOrders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findClients() {
        if (($model = Clients::find()->asArray()->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
