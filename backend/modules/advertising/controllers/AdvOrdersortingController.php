<?php

namespace backend\modules\advertising\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\data\Pagination,
    yii\helpers\ArrayHelper;
use common\models\AdvCalendar,
    common\models\AdvMesh,
    common\models\AdvOrders,
    backend\modules\advertising\models\Calendar,
    backend\modules\advertising\models\CalendarOrders,
    backend\modules\advertising\forms\CalendarFilter;

class AdvOrdersortingController extends Controller {

    public $site_id;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'order-time', 'ajax-sotable', 'ajax-removecalendar', 'ajax-addorder'],
                        'roles' => ['Администраторы', 'Менеджеры'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->site_id = Yii::$app->Siteselector->getSitesCookie();
    }

    /**
     * Lists all AdvMeshes models.
     * @return mixed
     */
    public function actionIndex() {
        // Set use date for request to database
        $filter = new CalendarFilter();
        $use_date = date('Y-m-d');

        if (Yii::$app->request->post()) {
            $filter->load(Yii::$app->request->post());
            if ($filter->validate()) {
                $use_date = $filter->date_start;
            }
        }

        // Get all orders by the day
        $weeks = $this->getWeekDate($use_date);
        $orders = AdvCalendar::find()->with('advOrders')->where(['online_day' => $use_date])->orderBy(['gravity' => SORT_ASC])->asArray()->all();
        $allnotorders = AdvCalendar::find()->with('advOrders')->where(['>=', 'online_day', $weeks['monday']])->andWhere(['<=', 'online_day', $weeks['friday']])->andWhere(['<>', 'online_day', $use_date])->orderBy(['gravity' => SORT_ASC])->asArray()->all();
        $notorders = $this->filterDublicate($allnotorders, $orders);

        // Get all adv by the day
        $objadv = AdvOrders::find()->where(['<=', 'start_at', $use_date])->andWhere(['>=', 'finish_at', $use_date])->andWhere(['site_id' => $this->site_id])->groupBy('id')->orderBy(['title' => SORT_ASC])->asArray()->all();

        $query = AdvMesh::find()->where(['site_id' => $this->site_id])->orderBy(['spacialy' => SORT_DESC, 'start_time' => SORT_ASC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->defaultPageSize = 15;
        $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->render('index', [
                    'orders' => $orders,
                    'objadv' => $objadv,
                    'models' => $models,
                    'pages' => $pages,
                    'filter' => $filter,
                    'use_date' => $use_date,
                    'notorders' => $notorders
        ]);
    }

    /**
     * Lists all Orders of this time.
     * @return mixed
     */
    public function actionOrderTime($id) {
        // Get title and other data of time
        $timedata = AdvMesh::find()->where(['id' => $id])->asArray()->one();

        // Get all data of orders
        $orders = AdvCalendar::find()->where(['adv_mesh_id' => $id])->orderBy(['gravity' => SORT_ASC, 'adv_orders_id' => SORT_ASC, 'online_day' => SORT_ASC])->all();

        // Get all date
        $arralldate = ArrayHelper::getColumn($orders, 'online_day');
        $arrdate = array_unique($arralldate);

        return $this->render('order-time', ['timedata' => $timedata, 'orders' => $orders, 'arrdate' => $arrdate]);
    }

    // Sort orders on calendar
    public function actionAjaxSotable() {
        if (Yii::$app->request->post() && Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            if (isset($post['foo'])) {
                $i = 0;
                foreach ($post['foo'] as $item) {
                    $model = AdvCalendar::findOne($item);
                    $model->gravity = $i;
                    $model->update();
                    $i++;
                }
            }
        }
    }

    // Add new order to calendar
    public function actionAjaxAddorder() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = [
            'status' => 0,
            'error' => 'Error',
            'order_id' => 0,
            'mesh_id' => 0,
            'calendar_id' => 0
        ];

        if (Yii::$app->request->post() && Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $orderData = AdvOrders::find()->where(['id' => $post['adv_orders_id']])->one();

            if (isset($orderData)) {
                $model = new CalendarOrders();
                $model->order_id = (int) $post['adv_orders_id'];
                $model->mesh_id = (int) $post['adv_mesh_id'];
                $model->start = $orderData->start_at;
                $model->finish = $orderData->finish_at;
                $model->count_days = (int) $orderData->count_days;
                $model->status = 1;

                if ($model->validate()) {
                    // Save change model status
                    $model->saveChangeOrder();
                }
            }

            $calendar_id = AdvCalendar::find()->where(['adv_orders_id' => $post['adv_orders_id'], 'adv_mesh_id' => $post['adv_mesh_id']])->orderBy(['id' => SORT_DESC])->one();

            $result = [
                'status' => 1,
                'error' => 'No error',
                'order_id' => (int) $post['adv_orders_id'],
                'mesh_id' => (int) $post['adv_mesh_id'],
                'calendar_id' => (isset($calendar_id->id)) ? (int) $calendar_id->id : 0
            ];
        }

        return $result;
    }

    // Remove order from calendar
    public function actionAjaxRemovecalendar() {
        if (Yii::$app->request->post() && Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            if (isset($post['order_id']) && isset($post['mesh_id'])) {
                $models = AdvCalendar::find()->where(['adv_orders_id' => $post['order_id'], 'adv_mesh_id' => $post['mesh_id']])->all();

                foreach ($models as $model) {
                    // Get events data
                    $search = new Calendar();

                    // Update suport noutes
                    $search->updateMersh($model);
                    $search->updateOrders($model);

                    $model->delete();
                }
            }
        }
    }

    // Get start and end current week date
    private function getWeekDate($usedate) {
        $day = date('w', strtotime($usedate)) - 1;
        $week['monday'] = date('Y-m-d', strtotime('-' . $day . ' days'));
        $week['friday'] = date('Y-m-d', strtotime('+' . (6 - $day) . ' days'));

        return $week;
    }

    // Filter dublicate orders
    private function filterDublicate($arrModelCalendar = array(), $arrOrders = array()) {
        $newArray = array();
        $ordidArr = [];

        if (count($arrModelCalendar) > 0) {
            foreach ($arrModelCalendar as $calendars) {
                if (array_search($calendars['adv_mesh_id'], $ordidArr) === false) {
                    $ordidArr[] = $calendars['adv_mesh_id'];
                    foreach ($arrOrders as $arord) {
                        if ($calendars['adv_orders_id'] !== $arord['adv_orders_id']) {
                            $newArray[] = $calendars;
                            break;
                        }
                    }
                }
            }
        }

        return $newArray;
    }

}
