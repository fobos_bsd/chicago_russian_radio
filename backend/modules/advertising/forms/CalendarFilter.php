<?php

namespace backend\modules\advertising\forms;

use Yii,
    yii\base\Model;

class CalendarFilter extends Model {

    public $date_start;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['date_start'], 'date', 'format' => 'yyyy-MM-dd'],
            [['date_start'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'date_start' => Yii::t('backend', 'Date of start'),
        ];
    }

}
