<?php

use yii\bootstrap\Html,
    yii\helpers\Url;

$this->title = Yii::t('backend', 'List of orders at {timedata}', ['timedata' => Yii::$app->formatter->asTime($timedata['start_time'])]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-blocks-index">
    <div class="box">
        <h1><?= Html::encode($this->title) ?></h1>

        <?php if (isset($orders) && count($orders) > 0) : ?>
            <div class="box-body">
                <?php foreach ($arrdate as $advdate) { ?>
                <h3 class="text-center"><?= Yii::$app->formatter->asDate($advdate) . '  ' . date('D', strtotime($advdate)) ?></h3>
                    <ul id="sortadv_<?= $advdate ?>" class="sortable list-unstyled">
                        <?php foreach ($orders as $order) : if ($order->online_day === $advdate) { ?>
                                <li id="foo_<?= $order->id ?>"><span class="fa fa-sort" style="color: red;"></span>&nbsp;&nbsp;<?= $order->advOrders->title . ' (' . Yii::$app->formatter->asDate($order->online_day) . '  ' . date('D', strtotime($order->online_day)) . ')' ?></li>
                                <?php } endforeach; ?>
                    </ul>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php
if(isset($arrdate) && count($arrdate) > 0) { foreach ($arrdate as $advd) {
$js = <<< JS
    $(function() {
        $("#sortadv_$advd").sortable({
            axis: 'y',
            stop: function(event, ui) {
                var url = '/admin/advertising/adv-ordersorting/ajax-sotable';
                var fieldData = $(this).sortable("serialize");
                $.ajax({
                    data: fieldData,
                    type: 'POST',
                    url: url
                });
            }
        });
    });
JS;

$this->registerJs($js, yii\web\View::POS_END);
}}
?>