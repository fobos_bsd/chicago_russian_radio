<?php

use yii\bootstrap\Html,
    yii\helpers\Url,
    yii\widgets\ActiveForm,
    kartik\widgets\DatePicker,
    yii\widgets\LinkPager;

$this->title = Yii::t('backend', 'List of adv blocks');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-body ordermeshs col-sm-12 no-padding">




    <h1 class="page-header"><?= Html::encode($this->title) ?>&nbsp;&nbsp;&nbsp;<span class="currdate"><?= Yii::$app->formatter->asDate($use_date) ?></span></h1>

    <div class="box box-primary">
        <div class="box-body">

            <?php $form = ActiveForm::begin(['id' => 'filter-form']); ?>
            <div class="col-sm-6 col-md-4 col-lg-3 no-padding-left">
                <?=
                $form->field($filter, 'date_start')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Start date'],
                    'convertFormat' => true,
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'layout' => '{picker}{input}{remove}',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-MM-dd',
                        'startDate' => gmdate('Y-m-d', time()),
                        'todayHighlight' => false,
                        'timezone' => '-0600',
                        'showTimezone' => true,
                        'LocalTimezone' => true
                    ]
                ])
                ?>
            </div>

            <div class="col-sm-3 col-md-2 col-lg-2 no-padding-right">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary search-btn', 'name' => 'filter-button']) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-5 col-xs-6 no-padding">
            <ul class="roworderscalendar">
                <?php
                if (isset($objadv) && count($objadv) > 0) :
                    $i = 0;
                    foreach ($objadv as $arradv) :
                        ?>
                        <li id="foo_x<?= $i ?>" class="draggable"><span id="<?= $arradv['id'] ?>" class="elemadv"><i class="fa fa-arrows" style="color: red;"></i>&nbsp;&nbsp;<?= $arradv['title'] ?></span></li>
                        <?php
                        $i++;
                    endforeach;
                endif;
                ?>
            </ul>
        </div>
        <div class="col-lg-10 col-md-9 col-sm-7 col-xs-6 no-padding">
            <?php
            if (isset($models)) :
                foreach ($models as $model) :
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-1 text-center <?php if ($model->spacialy == 1) echo 'spacialy'; ?>">
                        <div class="boxtime">
                            <h4><a href="<?= Url::toRoute(['order-time', 'id' => $model->id]) ?>"><?= Yii::$app->formatter->asTime($model->start_time) ?></a></h4>
                            <ul id="sortadv_<?= $model->id ?>" class="sortable droppable list-unstyled" data-item="<?= $model->id ?>">
                                <?php foreach ($orders as $order) : ?>
                                    <?php if ($order['adv_mesh_id'] == $model->id) : ?>
                                        <li id="foo_<?= $order['id'] ?>"><span class="orderone"><i class="fa fa-sort" style="color: red;"></i>&nbsp;&nbsp;<?= $order['advOrders']['title'] ?></span><span class="remorder fa fa-remove" style="color: red;" onclick="removeOrder(<?= $order['adv_orders_id'] ?>,<?= $order['adv_mesh_id'] ?>,<?= $order['id'] ?>)"></span></li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php
                                if (isset($notorders)) {
                                    foreach ($notorders as $norder) :
                                        ?>
                                        <?php if ((int) $norder['adv_mesh_id'] == $model->id) : ?>
                                            <li id="foo_<?= $norder['id'] ?>"><span class="orderone"><i class="fa fa-sort" style="color: gray;"></i>&nbsp;&nbsp;<?= $norder['advOrders']['title'] ?> (<?= Yii::$app->formatter->asDate($norder['online_day']) ?>)</span><span class="remorder fa fa-remove" style="color: gray;" onclick="removeOrder(<?= $norder['adv_orders_id'] ?>,<?= $norder['adv_mesh_id'] ?>,<?= $norder['id'] ?>)"></span></li>
                                        <?php endif; ?>
                                        <?php
                                    endforeach;
                                }
                                ?>
                            </ul>
                        </div>
                    </div>  
                    <?php
                endforeach;
            endif;
            ?>
        </div>
        <div class="clearfix"></div>
        <nav class="">
            <?=
            LinkPager::widget([
                'pagination' => $pages,
            ])
            ?>
        </nav>
    </div>
</div>

<?php
if (isset($models) && count($models) > 0) {
    foreach ($models as $model) {
        $js = <<< JS
    $(function() {
        $("#sortadv_$model->id").sortable({
            axis: 'y',
            stop: function(event, ui) {
                var url = '/admin/advertising/adv-ordersorting/ajax-sotable';
                var fieldData = $(this).sortable("serialize");
                $.ajax({
                    data: fieldData,
                    type: 'POST',
                    url: url
                });
            }
        });
    });
JS;
        $this->registerJs($js, yii\web\View::POS_END);
    }
}
?>

<?php
$js2 = <<< JS
    $(function() {               
        $('.draggable').draggable({
            revert: "invalid",
            stack: ".draggable",
            helper: 'clone'
        });
        $('.droppable').droppable({
            accept: ".draggable",
            drop: function (event, ui) {
                var droppable = $(this);
                var draggable = ui.draggable;
                // Move draggable into droppable
                draggable.clone().appendTo(droppable);
                var mash_id = droppable.data("item");
                var order_id = draggable.find("span.elemadv").attr("id");
                var foo_id = draggable.attr("id");
                var online_day = $(".currdate").text();
                var url = '/admin/advertising/adv-ordersorting/ajax-addorder';
                $.ajax({
                    data: { adv_mesh_id:mash_id, adv_orders_id:order_id, online_day:online_day },
                    type: 'POST',
                    url: url,
                    success: function(response) {
                        if(response.status === 1) {
                            $("ul.sortable").find("li#" + foo_id).find("span.elemadv").after('<span class="remorder fa fa-remove" style="color: red;" onclick="removeOrder(' + response.order_id + ',' + response.mesh_id + ')"></span>');
                            $("ul.sortable").find("li#" + foo_id).find("span i.fa-arrows").removeClass("fa-arrows").addClass("fa-sort");
                            $("ul.sortable").find("li#" + foo_id).attr("id", "foo_" + response.calendar_id);
                            $("ul.sortable").find("li#foo_" + response.calendar_id).attr("id", "foo_" + response.calendar_id);
                            $("ul.sortable li").find("span.elemadv").removeClass("elemadv").addClass("orderone");
                        }
                    }
                });
            }
        });
    });
                
    function removeOrder(orderID,meshID,ID) {
        var url = '/admin/advertising/adv-ordersorting/ajax-removecalendar';
        $.ajax({
            data: { order_id: orderID, mesh_id: meshID },
            type: 'POST',
            url: url,
            success: function() {
                $("li#foo_" + ID).remove();
            }
        });
    }
JS;

$this->registerJs($js2, yii\web\View::POS_END);
?>
