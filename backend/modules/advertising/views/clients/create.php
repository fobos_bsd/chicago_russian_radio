<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Clients */

$this->title = Yii::t('backend', 'Create Advertiser');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Advertisers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model
    ]);
    ?>
</div>
