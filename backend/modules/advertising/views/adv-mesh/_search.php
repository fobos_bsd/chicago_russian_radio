<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\TimePicker,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\search\AdvMeshSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">

        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-5 col-lg-5">
            <?=
            $form->field($model, 'start_time')->widget(TimePicker::className(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1
                ]
            ])
            ?>
        </div>
        <div class="col-sm-5 col-lg-6 no-padding">
            <?=
            $form->field($model, 'end_time')->widget(TimePicker::className(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1
                ]
            ])
            ?>
        </div>
        <div class="col-sm-4 no-paddingleft">
            <?= $form->field($model, 'long')->input('number', ['min' => 1, 'max' => 3600, 'step' => 1]) ?>
        </div>
        <div class="col-sm-5 no-padding">
            <?=
            $form->field($model, 'important')->widget(Select2::className(), [
                'data' => [
                    1 => Yii::t('backend', 'Moning'),
                    2 => Yii::t('backend', 'Day'),
                    3 => Yii::t('backend', 'Evening'),
                    4 => Yii::t('backend', 'Night'),
                ],
                'options' => ['placeholder' => 'Select part of day'],
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ])
            ?>
        </div>
        <div class="col-sm-3 no-paddingright">
            <?=
            $form->field($model, 'spacialy')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>
        <div class="clearfix"></div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/advertising/adv-mesh/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>