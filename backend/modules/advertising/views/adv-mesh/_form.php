<?php

use yii\bootstrap\Html,
    yii\bootstrap\ActiveForm,
    kartik\widgets\TimePicker,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\AdvMesh */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Advertising block') : Yii::t('backend', 'Update Advertising block') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-6">
            <?=
            $form->field($model, 'start_time')->widget(TimePicker::className(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1
                ]
            ])
            ?>
        </div>
        <div class="col-sm-6">
            <?=
            $form->field($model, 'end_time')->widget(TimePicker::className(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1
                ]
            ])
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'long')->input('number', ['min' => 1, 'max' => 3600, 'step' => 1]) ?>
        </div>
        <div class="col-sm-5">
            <?=
            $form->field($model, 'important')->widget(Select2::className(), [
                'data' => [
                    1 => Yii::t('backend', 'Moning'),
                    2 => Yii::t('backend', 'Day'),
                    3 => Yii::t('backend', 'Evening'),
                    4 => Yii::t('backend', 'Night'),
                ],
                'options' => ['placeholder' => 'Select part of day'],
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ])
            ?>
        </div>
        <div class="col-sm-3">
            <?=
            $form->field($model, 'spacialy')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
