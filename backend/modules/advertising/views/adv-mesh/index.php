<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AdvMeshSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Advertising blocks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adv-mesh-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Advertising block'), ['create'], ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    'id',
                    ['attribute' => 'site_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->site->name;
                        }
                    ],
                    'start_time',
                    'end_time',
                    [
                        'attribute' => 'long',
                        'label' => 'Duration in seconds',
                    ],
                    [
                        'attribute' => 'important',
                        'label' => 'Priority',
                        'content' => function($data) {
                            switch ($data->important) {
                                case 1: $text = Yii::t('backend', 'Moning');
                                    break;
                                case 2: $text = Yii::t('backend', 'Day');
                                    break;
                                case 3: $text = Yii::t('backend', 'Evening');
                                    break;
                                case 4: $text = Yii::t('backend', 'Night');
                                    break;
                                default: $text = Yii::t('backend', 'Night');
                                    break;
                            }

                            return $text;
                        }
                    ],
                    ['attribute' => 'spacialy',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            if ($data->spacialy == 1) {
                                return '<i class="fa fa-toggle-on" style="color: #00a65a;" aria-hidden="true"></i>';
                            } else {
                                return '<i class="fa fa-toggle-off" style="color: #dd4b39;" aria-hidden="true"></i>';
                            }
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '140'],
                        'template' => '<div class="btn-group">{view}{update}{delete}</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-primary']);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit"></i>', $url, ['class' => 'btn btn-success']);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times-circle-o"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ],
                                            'class' => 'btn btn-danger'
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
