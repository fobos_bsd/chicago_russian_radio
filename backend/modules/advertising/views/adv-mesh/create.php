<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AdvMesh */

$this->title = Yii::t('backend', 'Create Advertising block');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Advertising blocks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advmash-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?=
    $this->render('_form', [
        'model' => $model
    ]);
    ?>
</div>
