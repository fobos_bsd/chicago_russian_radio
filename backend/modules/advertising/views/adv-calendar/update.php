<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AdvCalendar */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
            'modelClass' => 'Advertising schedule',
        ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Advertising schedule'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <?= Html::encode($this->title) ?>
            </header>
            <div class="panel-body">
                <p class="bg-danger text-center" style="padding: 15px 0; color: red;"><?= Yii::$app->session->getFlash('error'); ?></p>
                <?=
                $this->render('_form', [
                    'model' => $model,
                    'orders' => $orders,
                    'meshs' => $meshs
                ])
                ?>
            </div>
        </section>
    </div>
</div>
