<?php

use yii\bootstrap\Html,
    yii\helpers\Url,
    kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AdvCalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Fulfillment');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adv-calendar-index">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <div class="box">
        <div class="box-body">
            <div class="col-sm-12 no-padding">
                <form id="filterdate" class="" >
                    <div class="col-sm-11">
                        <div class="form-group">
                            <?=
                            DatePicker::widget([
                                'name' => 'start_day',
                                'value' => $startday,
                                'type' => DatePicker::TYPE_RANGE,
                                'name2' => 'end_day',
                                'value2' => $finishday,
                                'options' => ['placeholder' => 'Start date'],
                                'options2' => ['placeholder' => 'End date'],
                                'pluginOptions' => [
                                    'format' => 'yyyy/dd/mm',
                                    'todayHighlight' => true
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('backend', 'Go'), ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </form>
            </div>
            <?php $models = $dataProvider->getModels(); ?>
            <div class="col-sm-12 no-padding table-responsive calendarbydate">
                <table class="table table-bordered">
                    <tr>
                        <?php
                        if (isset($meshs) && count($meshs) > 0) : foreach ($meshs as $mesh) :
                                switch ($mesh->important) {
                                    case 1: $color = '#C6F3FF';
                                        $partday = 'Moning';
                                        break;
                                    case 2: $color = '#3FB2FF';
                                        $partday = 'Day';
                                        break;
                                    case 3: $color = '#C7C7C7';
                                        $partday = 'Evening';
                                        break;
                                    case 4: $color = '#B1B1B1';
                                        $partday = 'Night';
                                        break;
                                    default: $color = '#B1B1B1';
                                        $partday = 'Night';
                                        break;
                                }
                                ?>
                                <th style="background-color: <?= $color ?>"><?= $partday ?><br/><?= Yii::$app->formatter->asTime($mesh->start_time) ?></th>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tr>

                    <?php
                    while ($unixstart <= $unixfinish) {
                        ?>
                        <tr>
                            <?php
                            if (isset($meshs) && count($meshs) > 0) : foreach ($meshs as $mesh) :
                                    $usedsecond = 0;
                                    $usedproc = 0;
                                    $sellsecond = $mesh->long;
                                    if (isset($usedmeshs) && count($usedmeshs) > 0) {
                                        foreach ($usedmeshs as $usedmesh) {
                                            if ($usedmesh->adv_mesh_id == $mesh->id && $usedmesh->used_date === date('Y-m-d', $unixstart)) {
                                                $usedsecond = $usedmesh->used;
                                                $usedproc = $usedmesh->used_procent;
                                                $sellsecond = $usedmesh->less_seconds;

                                                break;
                                            }
                                        }
                                    }

                                    if ($usedproc < 20) {
                                        $colort = '#00F700';
                                    } elseif (20 < $usedproc && $usedproc <= 50) {
                                        $colort = '#00C300';
                                    } elseif (50 < $usedproc && $usedproc <= 70) {
                                        $colort = '#fffc00';
                                    } elseif (70 < $usedproc && $usedproc <= 99) {
                                        $colort = '#FF3F14';
                                    } else {
                                        $colort = '#9b0000';
                                    }
                                    ?>
                                    <td style="background-color: <?= $colort ?>" title="Used in seconds: <?= $usedsecond ?>sec;  Used procant: <?= $usedproc ?>%; Less seconds: <?= $sellsecond ?>sec."><?= Html::a(date('Y/d/m', $unixstart), Url::to(['/advertising/adv-calendar/view', 'sdate' => date('Y-m-d', $unixstart)]), ['class' => 'btn']) ?></td>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tr>
                        <?php
                        $unixstart += 86400;
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>