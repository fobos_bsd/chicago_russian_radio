<?php

use yii\bootstrap\Html,
    yii\helpers\Url,
    yii\grid\GridView,
    yii\widgets\Pjax,
    kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AdvCalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Fulfillment');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adv-calendar-index">

    <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <div class="box">
        <div class="box-body">
            <div id="tableDiv_Arrays" class="table-responsive boxdetalcalendar">
                <table id="Open_Text_Arrays" class="table table-bordered table-hover">
                    <thead>
                        <tr class="text-center">
                            <td style="background-color: #367CFF; color: white;">
                                <?= Yii::t('backend', 'Company name') ?>
                            </td>
                            <td style="background-color: #367CFF; color: white;">
                                <?= Yii::t('backend', 'Ads title') ?>
                            </td>
                            <td style="background-color: #367CFF; color: white;">
                                <?= Yii::t('backend', 'Audio') ?>
                            </td>
                            <td style="background-color: #367CFF; color: white;">
                                <?= Yii::t('backend', 'Ads lengtd') ?>
                            </td>
                            <td style="background-color: #367CFF; color: white;">
                                <?= Yii::t('backend', 'Number of days') ?>
                            </td>
                            <td style="background-color: #367CFF; color: white;">
                                <?= Yii::t('backend', 'Ads per day') ?>
                            </td>
                            <td style="background-color: #367CFF; color: white;">
                                <?= Yii::t('backend', 'Ads total') ?>
                            </td>
                            <?php
                            if (isset($meshs) && count($meshs) > 0) : foreach ($meshs as $mesh) :
                                    switch ($mesh->important) {
                                        case 1: $color = '#C6F3FF';
                                            $partday = 'Moning';
                                            break;
                                        case 2: $color = '#3FB2FF';
                                            $partday = 'Day';
                                            break;
                                        case 3: $color = '#C7C7C7';
                                            $partday = 'Evening';
                                            break;
                                        case 4: $color = '#B1B1B1';
                                            $partday = 'Night';
                                            break;
                                        default: $color = '#B1B1B1';
                                            $partday = 'Night';
                                            break;
                                    }
                                    ?>
                                    <td style="background-color: <?= $color ?>"><?= $partday ?><br/><?= Yii::$app->formatter->asTime($mesh->start_time) ?></td>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tr>
                    <thead>
                    <tbody>
                        <?php
                        if ($models && count($models) > 0) {
                            foreach ($models as $model) :
                                ?>
                                <tr class="text-center">
                                    <td style="background-color: #367CFF; color: white;">
                                        <?= $model['client'] ?>
                                    </td>
                                    <td style="background-color: #367CFF; color: white;">
                                        <?= $model['title'] ?>
                                    </td>
                                    <td style="background-color: #367CFF; color: white;">
                                        <?php
                                        $file = Yii::getAlias('@backend') . '/web/uploads/sound_reklam/' . $model['audio'];
                                        if (file_exists($file)) {
                                            echo Html::a($model['audio'], '/backend/web/uploads/sound_reklam/' . $model['audio'], ['download' => 'download', 'style' => 'color: white;']);
                                        }
                                        ?>
                                    </td>
                                    <td style="background-color: #367CFF; color: white;">
                                        <?= $model['how_seconds'] ?>
                                    </td>
                                    <td style="background-color: #367CFF; color: white;">
                                        <?= $model['count_days'] ?>
                                    </td>
                                    <td style="background-color: #367CFF; color: white;">
                                        <?= $model['count_onday'] ?>
                                    </td>
                                    <td style="background-color: #367CFF; color: white;">
                                        <?= $model['used_days'] ?>
                                    </td>
                                    <?php
                                    if (isset($meshs) && count($meshs) > 0) : foreach ($meshs as $mesh) :
                                            $usedsecond = 0;
                                            $usedproc = 0;
                                            $sellsecond = $mesh->long;
                                            if (isset($usedmeshs) && count($usedmeshs) > 0) {
                                                foreach ($usedmeshs as $usedmesh) {
                                                    if ($usedmesh->adv_mesh_id == $mesh->id && $usedmesh->used_date === $sdate) {
                                                        $usedsecond = $usedmesh->used;
                                                        $usedproc = $usedmesh->used_procent;
                                                        $sellsecond = $usedmesh->less_seconds;

                                                        break;
                                                    }
                                                }
                                            }

                                            if ($usedproc < 20) {
                                                $colort = '#00F700';
                                            } elseif (20 < $usedproc && $usedproc <= 50) {
                                                $colort = '#00C300';
                                            } elseif (50 < $usedproc && $usedproc <= 70) {
                                                $colort = '#fffc00';
                                            } elseif (70 < $usedproc && $usedproc <= 99) {
                                                $colort = '#FF3F14';
                                            } else {
                                                $colort = '#9b0000';
                                            }
                                            ?>
                                            <td style="background-color: <?= $colort ?>;" title="Used in seconds: <?= $usedsecond ?>sec;  Used procant: <?= $usedproc ?>%; Less seconds: <?= $sellsecond ?>sec.">
                                                <?php
                                                foreach ($model['mesh_id'] as $mdmesh) {
                                                    if ($mdmesh['id'] == $mesh->id) {
                                                        ?>
                                                        <a style="color: white; text-shadow: 1px 1px 2px #2f2f2c;" href="<?= Url::to(['/advertising/adv-calendar/update', 'id' => $model['id']]); ?>">
                                                            <?php
                                                            echo /* $mdmesh['count_time'] . ' -- ' . */ $mdmesh['count_time'] * $model['how_seconds'];
                                                            ?>
                                                        </a>
                                                        <?php
                                                        break;
                                                    } else {
                                                        echo ' ';
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tr>
                                <?php
                            endforeach;
                        } else {
                            ?>
                            No data
                            <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style="background-color: #367CFF; color: white;">

                            </td>
                            <td style="background-color: #367CFF; color: white;">

                            </td>
                            <td style="background-color: #367CFF; color: white;">

                            </td>
                            <td style="background-color: #367CFF; color: white;">

                            </td>
                            <td style="background-color: #367CFF; color: white;">

                            </td>
                            <td style="background-color: #367CFF; color: white;">

                            </td>
                            <td style="background-color: #367CFF; color: white;">

                            </td>
                            <?php
                            if (isset($meshs) && count($meshs) > 0) : foreach ($meshs as $mesh) :
                                    $used = 0;
                                    $less = $mesh->long;
                                    ?>
                                    <td style="background-color: white">
                                        <?php
                                        foreach ($usedmeshs as $mdmesh) {
                                            if ($mdmesh->adv_mesh_id == $mesh->id) {
                                                $used = $mdmesh->used;
                                                $less = $mdmesh->less_seconds;
                                                break;
                                            }
                                        }
                                        echo 'used: ' . $used . 's<br/>free: ' . $less . 's';
                                        ?>

                                    </td>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

