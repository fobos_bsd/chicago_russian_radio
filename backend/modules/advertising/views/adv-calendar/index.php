<?php

use yii\bootstrap\Html,
    yii\helpers\Url,
    yii\widgets\LinkPager,
    yii\widgets\Pjax,
    yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AdvCalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Advertising schedule');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adv-calendar-index">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
    <div class="box">
        <div class="box-header">
            <div class="col-sm-4 no-padding text-left">
                <?= Html::a(Yii::t('backend', 'Add Order'), ['/advertising/adv-orders/create'], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('backend', 'Add Advertiser'), ['/advertising/clients/create'], ['class' => 'btn btn-success']) ?>

            </div>
            <!-- Export data -->
            <?=
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'status',
                        'label' => Yii::t('frontend', 'Status'),
                        'vAlign' => 'middle',
                    ],
                    [
                        'label' => Yii::t('frontend', 'Company Name'),
                        'value' => function($model, $key, $index, $widget) {
                            return $model->clients->name;
                        },
                        'vAlign' => 'middle',
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'title',
                        'label' => Yii::t('frontend', 'Ads` title'),
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'how_seconds',
                        'label' => Yii::t('frontend', 'Length'),
                        'vAlign' => 'middle',
                    ],
                    [
                        'label' => Yii::t('frontend', 'Start'),
                        'value' => function($model, $key, $index, $widget) {
                            return Yii::$app->formatter->asDate($model->start_at);
                        },
                        'vAlign' => 'middle',
                    ],
                    [
                        'label' => Yii::t('frontend', 'End'),
                        'value' => function($model, $key, $index, $widget) {
                            return Yii::$app->formatter->asDate($model->finish_at);
                        },
                        'vAlign' => 'middle',
                    ],
                    [
                        'label' => Yii::t('frontend', 'Days'),
                        'value' => function($model, $key, $index, $widget) {
                            switch ($model->count_days) {
                                case 7: $result = 'EveryDay';
                                    break;
                                case 5: $result = 'WorkDay';
                                    break;
                                case 2: $result = 'WeekEnd';
                                    break;
                                case 3: $result = 'Odd days';
                                    break;
                                case 4: $result = 'Even days';
                                    break;
                                default: $result = 'EveryDay';
                                    break;
                            }
                            return $result;
                        },
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'content',
                        'label' => Yii::t('frontend', 'Note/Preferences'),
                        'vAlign' => 'middle',
                        'format' => 'raw'
                    ],
                    [
                        'label' => Yii::t('frontend', 'Advertising blocks'),
                        'vAlign' => 'middle',
                        'value' => function($model, $key, $index, $widget) use ($meshs) {
                            $arrtimes = array();
                            if (isset($model->advCalendars) && count($model->advCalendars) > 0) {
                                foreach ($model->advCalendars as $calendar) {
                                    foreach ($meshs as $onemesh) {
                                        if ($onemesh->id == $calendar['adv_mesh_id']) {
                                            $arrtimes[] = Yii::$app->formatter->asTime($onemesh->start_time);
                                            break;
                                        }
                                    }
                                }
                            }
                            $times = array_unique($arrtimes);
                            $result = implode(', ', $times);

                            return $result;
                        }
                    ]
                ],
                'dropdownOptions' => [
                    'class' => 'btn btn-default btn-xs'
                ],
                'columnSelectorOptions' => [
                    'class' => 'btn btn-default btn-xs'
                ],
                'container' => [
                    'class' => 'col-sm-4 text-left'
                ]
            ]);
            ?>
            <!-- End Export data -->
            <div class="col-sm-4 no-padding text-right">
                <?= Html::a('<i class="fa fa-sort-amount-asc"></i>', Url::toRoute(['/advertising/adv-calendar/index', 'sort' => 1]), ['class' => 'btn btn-default']) ?>
                <?= Html::a('<i class="fa fa-sort-amount-desc"></i>', Url::toRoute(['/advertising/adv-calendar/index', 'sort' => 0]), ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        <div class="box-body boxcalendar" id="chat-box">
            <?php Pjax::begin(); ?>
            <div class="col-sm-6 no-padding table-responsive">
                <table class="table table-fixed">
                    <thead class="fixed">
                        <tr>
                            <th><?= Yii::t('frontend', 'Status') ?></th>
                            <th><?= Yii::t('frontend', 'Company Name') ?></th>
                            <th style="min-width:200px; max-width: 200px;"><?= Yii::t('frontend', 'Ads` title') ?></th>
                            <th><?= Yii::t('frontend', 'Length') ?></th>
                            <th><?= Yii::t('frontend', 'Start') ?></th>
                            <th><?= Yii::t('frontend', 'End') ?></th>
                            <th><?= Yii::t('frontend', 'Days') ?></th>
                            <th><?= Yii::t('frontend', 'Note/Preferences') ?></th>
                            <!--
                            <th><?php // echo Yii::t('frontend', 'Times per day')                                  ?></th>
                            <th><?php // echo Yii::t('frontend', 'Used times')                                  ?></th>
                            -->
                            <th><?= Yii::t('frontend', 'Sound') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($orders) && count($orders) > 0) : foreach ($orders as $order) : ?>
                                <tr>
                                    <td><?= $order->status ?></td>
                                    <td><div class="tooltipbox"><a href="/admin/advertising/clients/update/<?= $order->clients->id ?>" target="_blank"><?= $order->clients->name ?><span class="tooltiptext tooltip-top">Email: <?= $order->clients->email ?>,  Phone: <?= $order->clients->phone ?></span></a></div></td>
                                    <td style="min-width: 200px; max-width: 200px;"><a href="/admin/advertising/adv-orders/update/<?= $order->id ?>"><?= $order->title ?></a></td>
                                    <td><?= $order->how_seconds ?></td>
                                    <td><?= Yii::$app->formatter->asDate($order->start_at) ?></td>
                                    <td><?= Yii::$app->formatter->asDate($order->finish_at) ?></td>
                                    <td>
                                        <?php
                                        switch ($order->count_days) {
                                            case 7: echo 'EveryDay';
                                                break;
                                            case 5: echo 'WorkDay';
                                                break;
                                            case 2: echo 'WeekEnd';
                                                break;
                                            case 3: echo 'Odd days';
                                                break;
                                            case 4: echo 'Even days';
                                                break;
                                            default: echo 'EveryDay';
                                                break;
                                        }
                                        ?>
                                    </td>
                                    <td><?= $order->content ?></td>
                                    <!--
                                    <td><?php //echo $order->count_onday                                  ?></td>
                                    <td><?php // echo $order->used_days                                  ?></td>
                                    -->
                                    <td><audio src="/admin/uploads/sound_reklam/<?= $order->fileAudio ?>" controls></audio></td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-6 no-padding table-responsive">
                <table class="table table-fixed">
                    <thead class="fixed">
                        <tr>
                            <?php if (isset($meshs) && count($meshs) > 0) : foreach ($meshs as $mesh) : ?>
                                    <th>
                                        <?= Yii::$app->formatter->asTime($mesh->start_time) ?><br/>
                                        <?php
                                        if (isset($arrmeshsum[$mesh->id])) {
                                            echo $arrmeshsum[$mesh->id] . 's';
                                        } else {
                                            echo 0 . 's';
                                        }
                                        ?>
                                    </th>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($orders) && count($orders) > 0 && isset($meshs) && count($meshs) > 0) : foreach ($orders as $order) : ?>
                                <tr>
                                    <?php foreach ($meshs as $mesh) : ?>
                                        <td <?php if ($mesh->spacialy == 1) { ?> style="background-color: yellow;" <?php } ?>>
                                            <?php
                                            $checked = 0;
                                            if ((isset($calendar) && count($calendar) > 0)) {
                                                $i = 0;
                                                foreach ($calendar as $tbl) {
                                                    if (isset($tbl['adv_orders_id']) && isset($tbl['adv_mesh_id']) && $tbl['adv_orders_id'] == $order->id && $tbl['adv_mesh_id'] == $mesh->id) {
                                                        $checked = 1;
                                                        unset($calendar[$i]);
                                                        break;
                                                    }
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            <div class="checkbox">
                                                <label>
                                                    <input name="calendar_<?= $mesh->id ?>" type="checkbox" <?php if (isset($checked) && $checked == 1) { ?> checked="checked"<?php } ?>
                                                           onclick="checkCalendar('<?= $order->start_at ?>', '<?= $order->finish_at ?>',<?= $order->id ?>,<?= $mesh->id ?>,<?= $order->count_days ?>, <?php
                                                           if (isset($checked) && $checked == 1) {
                                                               echo 0;
                                                           } else {
                                                               echo 1;
                                                           }
                                                           ?>)">
                                                </label>
                                            </div>
                                        </td>
                                        <?php
                                    endforeach;
                                    ?>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-12 no-padding">
                <?=
                LinkPager::widget([
                    'pagination' => $pages,
                ])
                ?>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<?php
$script = <<< JS
        function checkCalendar(startat,finishat,orderID,meshID,countDay,status) {
            $.post('/admin/advertising/adv-calendar/ajax-calendar', {start: startat, finish: finishat, order_id: orderID, mesh_id: meshID, count_days: countDay, status: status});
        }
JS;

$this->registerJs($script, yii\web\View::POS_END);
?>
