<?php

use yii\bootstrap\Html,
    yii\helpers\Url,
    yii\bootstrap\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\AdvCalendar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adv-calendar-form">
    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'adv_orders_id')->widget(Select2::className(), [
        'data' => $orders,
        'options' => ['placeholder' => 'Select a order ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])
    ?>

    <div class="col-sm-4">
        <?=
        $form->field($model, 'adv_mesh_id')->widget(Select2::className(), [
            'data' => $meshs,
            'options' => ['placeholder' => 'Select a period ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ?>
    </div>
    <div class="col-sm-4">
        <?=
        $form->field($model, 'online_day')->widget(DatePicker::className(), [
            'options' => ['placeholder' => 'Select start date ...'],
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy/dd/mm',
                'todayHighlight' => true,
                'startDate' => date('Y/d/m')
            ]
        ])
        ?>
    </div>
    <div class="col-sm-4">
        <?=
        $form->field($model, 'gravity')->widget(Select2::className(), [
            'data' => [0 => 'Romove allow', 1 => 'Remove deny'],
            'pluginOptions' => [
                'allowClear' => false
            ],
        ])
        ?>
        <?= $form->field($model, 'count_time')->input('number', ['min' => 1, 'max' => '100', 'step' => 1]) ?>
    </div>
    <div class="form-group">
        <?php if (!$model->isNewRecord) echo Html::a(Yii::t('backend', 'Delete'), Url::to(['/advertising/adv-calendar/delete', 'id' => $model->id]), ['class' => 'btn btn-danger']); ?>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
