<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AdvCalendarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adv-calendar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'adv_mesh_id') ?>

    <?= $form->field($model, 'adv_orders_id') ?>

    <?= $form->field($model, 'online_day') ?>

    <?= $form->field($model, 'full_time') ?>

    <?php // echo $form->field($model, 'less_seconds') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
