<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AdvSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Advertising Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adv-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Advertising Settings'), ['create'], ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    ['attribute' => 'site_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->site->name;
                        }
                    ],
                    'name',
                    'setvalue',
                    'description:ntext',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('backend', 'Actions'),
                        'headerOptions' => ['width' => '140'],
                        'template' => '<div class="btn-group">{update}{delete}</div>',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit"></i>', $url, ['class' => 'btn btn-success']);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times-circle-o"></i>', $url, ['data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ],
                                            'class' => 'btn btn-danger'
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>