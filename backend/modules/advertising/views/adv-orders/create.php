<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AdvOrders */

$this->title = Yii::t('backend', 'Create Advertising Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Advertising Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advorders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'clients' => $clients,
        'isused' => $isused,
        'meshes' => $meshes,
        'usedmesh' => $usedmesh
    ]);
    ?>

</div>
