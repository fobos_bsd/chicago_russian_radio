<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AdvOrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Advertising Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adv-orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'clients' => $clients]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Advertising Orders'), ['create'], ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    'id',
                    [
                        'attribute' => 'clients_id',
                        'label' => 'Client name',
                        'content' => function($data) {
                            return $data->clients->name . '  tel: ' . $data->clients->phone;
                        }
                    ],
                    ['attribute' => 'site_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->site->name;
                        }
                    ],
                    'status',
                    'title',
                    'fileAudio',
                    'file_link',
                    [
                        'attribute' => 'count_days',
                        'content' => function($data) {
                            switch ($data->count_days) {
                                case 7: $result = 'EveryDay';
                                    break;
                                case 5: $result = 'WorkDay';
                                    break;
                                case 2: $result = 'WeekEnd';
                                    break;
                                case 4: $result = 'Even days';
                                    break;
                                case 3: $result = 'Odd days';
                                    break;
                                default: $result = 'EveryDay';
                                    break;
                            }
                            return $result;
                        }
                    ],
                    'count_onday',
                    'used_days',
                    // 'content:ntext',
                    'how_seconds',
                    [
                        'attribute' => 'start_at',
                        'label' => 'Start date',
                        'content' => function($data) {
                            return date('m/d/Y', strtotime($data->start_at));
                        }
                    ],
                    [
                        'attribute' => 'finish_at',
                        'label' => 'Finish date',
                        'content' => function($data) {
                            return date('m/d/Y', strtotime($data->finish_at));
                        }
                    ],
                    // 'created_at',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('backend', 'Actions'),
                        'headerOptions' => ['width' => '140'],
                        'template' => '<div class="btn-group">{view}{update}{delete}</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-primary']);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit"></i>', $url, ['class' => 'btn btn-success']);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times-circle-o"></i>', $url, ['data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ],
                                            'class' => 'btn btn-danger'
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
