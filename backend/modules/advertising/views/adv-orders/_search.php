<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\search\AdvOrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">

        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-3 col-lg-3">
            <?= $form->field($model, 'count_days')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-3 col-lg-4 no-padding">
            <?= $form->field($model, 'count_onday')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-4 col-lg-4">
            <?= $form->field($model, 'used_days')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>
        <div class="col-sm-5 col-lg-4 no-padding">
            <?=
            $form->field($model, 'clients_id')->widget(Select2::className(), [
                'data' => $clients,
                'options' => ['placeholder' => 'Select a client ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-2 col-sm-3 no-padding-right">
            <?=
            $form->field($model, 'status')->widget(Select2::className(), [
                'data' => [
                    'On Air' => 'On Air',
                    'New' => 'New'
                ],
                'options' => ['placeholder' => 'Select status ...'],
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ])
            ?>
        </div>
        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>
        <div class="clearfix"></div>
        <?php // echo $form->field($model, 'content') ?>

        <?php // echo $form->field($model, 'how_seconds') ?>

        <?php // echo $form->field($model, 'start_at') ?>

        <?php // echo $form->field($model, 'finish_at') ?>

        <?php // echo $form->field($model, 'created_at')   ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/advertising/adv-orders/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
