<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AdvOrders */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
            'modelClass' => 'Advertising Order',
        ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Advertising Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="advorders-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    $this->render('_form', [
        'model' => $model,
        'clients' => $clients,
        'isused' => $isused,
        'meshes' => $meshes,
        'usedmesh' => $usedmesh
    ])
    ?>
</div>
