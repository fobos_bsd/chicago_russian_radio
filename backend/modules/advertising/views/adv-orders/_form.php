<?php

use yii\bootstrap\Html,
    yii\bootstrap\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\DatePicker,
    kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\AdvOrders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Advertising Order') : Yii::t('backend', 'Update Advertising Order') ?></h3>
        <?php
        if ($isused !== 0) {
            $disabled = true;
            ?>
            <br/><br/>
            <div class="alert alert-danger alert-dismissible">
                <button class="close" type="button" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><?= Yii::t('backend', 'Atantion!') ?></h4>
                <?= Yii::t('backend', 'The order olready used!') ?>
            </div>
            <?php
        } else {
            $disabled = false;
        }
        ?>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(['id' => 'ordersForm', 'options' => ['enctype' => 'multipart/form-data']]); ?>

        <?=
        $form->field($model, 'clients_id')->widget(Select2::className(), [
            'data' => $clients,
            'disabled' => $disabled,
            'options' => ['placeholder' => 'Select a client ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ?>

        <div class="col-sm-4">
            <?=
            $form->field($model, 'count_days')->widget(Select2::className(), [
                'data' => [
                    7 => 'EveryDay',
                    5 => 'WorkDay',
                    2 => 'WeekEnd',
                    4 => 'Even days',
                    3 => 'Odd days'
                ],
                'disabled' => $disabled,
                'options' => ['placeholder' => 'Select a count days ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'count_onday')->input('number', ['min' => 1, 'max' => 10000, 'step' => 1, 'disabled' => $disabled]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'used_days')->input('number', ['min' => 0, 'max' => 365, 'step' => 1]) ?>
        </div>
        <?= $form->field($model, 'title')->textInput(['disabled' => $disabled]) ?>
        <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

        <div class="col-sm-4">
            <?= $form->field($model, 'how_seconds')->input('number', ['disabled' => $disabled]) ?>
        </div>
        <div class="col-sm-4">
            <?=
            $form->field($model, 'start_at')->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Select start date ...'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'disabled' => $disabled,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy/dd/mm',
                    'todayHighlight' => true,
                    'startDate' => date('Y/d/m')
                ]
            ])
            ?>
        </div>
        <div class="col-sm-4">
            <?=
            $form->field($model, 'finish_at')->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Select finish date ...'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy/dd/mm',
                    'todayHighlight' => true,
                    'startDate' => date('Y/d/m')
                ]
            ])
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'file_link')->textInput(['disabled' => $disabled]) ?>
        </div>
        <div class="col-sm-3">
            <?=
            $form->field($model, 'status')->widget(Select2::className(), [
                'data' => [
                    'On Air' => 'On Air',
                    'New' => 'New'
                ],
                'disabled' => $disabled,
                'options' => ['placeholder' => 'Select status ...'],
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <?php if (isset($model->fileAudio)) { ?>
            <br/>
            <p class="bg-info" style="padding: 10px 10px; color: black;">
                <audio src="/admin/uploads/sound_reklam/<?= $model->fileAudio ?>" controls></audio>&nbsp;&nbsp;<?= $model->fileAudio ?>
            </p>
        <?php } ?>
        <?=
        $form->field($model, 'fileAudio')->widget(FileInput::className(), [
            'options' => ['accept' => 'audio/*'],
            'disabled' => $disabled,
            'pluginOptions' => [
                'showPreview' => false,
                'showCaption' => true,
                'showRemove' => false,
                'showUpload' => false,
                'maxFileSize' => 10240,
            ],
        ])
        ?>

        <div class="clearfix"></div>
        <div class="col-sm-12 no-padding">
            <div class="form-group field-advorders-time_period">
                <label class="control-label" for="advorders-time_period"><?= Yii::t('backend', 'Advertising blocks') ?></label>
                <?= Html::checkboxList('AdvOrders[time_period]', $usedmesh, $meshes) ?>
            </div>

        </div>

        <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
