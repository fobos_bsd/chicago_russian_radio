<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AdvOrders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Advertising Order'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adv-orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'clients_id',
            'count_days',
            'count_onday',
            'used_days',
            'title',
            'content:ntext',
            'fileAudio',
            'file_link',
            'how_seconds',
            [
                'label' => 'Start',
                'value' => date('Y/d/m', strtotime($model->start_at)),
            ],
            [
                'label' => 'Finish',
                'value' => date('Y/d/m', strtotime($model->finish_at)),
            ],
            [
                'label' => 'Created',
                'value' => date('Y/d/m', $model->created_at),
            ],
        ],
    ])
    ?>

</div>
