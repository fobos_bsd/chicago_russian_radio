<?php

use yii\bootstrap\Html,
    kartik\grid\GridView,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DifferentDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Different Datas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="different-data-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Different Data'), ['create'], ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php
            $gridColumns = [
                ['class' => 'kartik\grid\SerialColumn'],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'name',
                    'format' => 'text',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'filter' => true
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'action_date',
                    'format' => ['date', 'php:Y-m-d H:i'],
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'filter' => true
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'description',
                    'format' => 'text',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'filter' => true
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'note',
                    'format' => 'html',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'filter' => true
                ],
                [
                    'class' => \kartik\grid\ActionColumn::className(),
                    'noWrap' => false,
                    'template' => '{edit}{delete}',
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return "<a href='/admin/different/index/delete/{$model->id}' title='Delete' class='glyphicon glyphicon-trash btn-sm' data-confirm='Are you sure to delete this item?' data-method='post' data-pjax='1'></a>";
                        },
                        'edit' => function ($url, $model) {
                            return "<a href='/admin/different/index/update/{$model->id}' title='Edit' class='glyphicon glyphicon-edit btn-sm' data-method='get' data-pjax='1'></a>";
                        }
                    ],
                ]
            ];

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'toolbar' => [
                    ['content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-primary']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('backend', 'Reset Grid')])
                    ],
                    '{toggleData}'
                ],
                'pjax' => true,
                'bordered' => true,
                'striped' => false,
                'condensed' => false,
                'responsive' => true,
                'hover' => true,
                'floatHeader' => true,
                'showPageSummary' => false,
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
