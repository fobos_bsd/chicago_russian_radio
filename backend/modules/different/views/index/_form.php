<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\DifferentData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Page') : Yii::t('backend', 'Update Page') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="col-sm-8 col-xs-12 no-paddingleft">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4 col-xs-12 no-paddingright">
            <?=
            $form->field($model, 'action_date')->widget(DateTimePicker::className(), [
                'options' => ['placeholder' => 'Select start date time ...'],
                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd HH:ii',
                    'todayHighlight' => true,
                    'startDate' => date('Y-m-d H:i')
                ]
            ])
            ?>
        </div>      

        <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

        <?=
        $form->field($model, 'note')->widget(Redactor::className(), [
            'clientOptions' => [
                'options' => [
                    'style' => 'height: 300px;',
                ],
                'convertDivs' => false,
                'replaceDivs' => false,
                'removeEmptyTags' => false,
                'paragraphize' => false,
                'pastePlainText' => true,
                'autoresize' => true,
                'convertVideoLinks' => true,
                //'buttons' => ['format', 'bold', 'italic', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule', 'underline', 'ol', 'ul', 'indent', 'outdent'],
                'plugins' => ['clips', 'fontcolor', 'imagemanager', 'advanced']
            ]
        ])
        ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
