<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DifferentData */

$this->title = Yii::t('backend', 'Create Different Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Different Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="different-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
