<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\OpinionsAndWishs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Opinion') : Yii::t('backend', 'Update Opinion') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-6 col-lg-6 no-padding">
            <?= $form->field($model, 'user_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6 col-lg-6 no-padding-right">
            <?= $form->field($model, 'email')->input('email') ?>
        </div>

        <div class="col-sm-4 col-lg-2 no-padding">
            <?= $form->field($model, 'raiting')->input('number', ['min' => 0, 'max' => 5, 'step' => 1]) ?>
        </div>
        <div class="col-sm-4 col-lg-2 no-padding-right">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <?= $form->field($model, 'opinion')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>