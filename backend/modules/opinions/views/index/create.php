<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OpinionsAndWishs */

$this->title = Yii::t('backend', 'Create Opinions And Wishs');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Opinions And Wishs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opinions-and-wishs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
