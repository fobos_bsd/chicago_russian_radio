<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OpinionsAndWishSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Opinions And Wishs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opinions-and-wishs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Opinions And Wishs'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Opinions And Wishs'), ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    'id',
                    ['attribute' => 'site_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->site->name;
                        }
                    ],
                    'user_name',
                    'email',
                    [
                        'attribute' => 'created_at',
                        'content' => function($data) {
                            return Yii::$app->formatter->asDate($data->created_at);
                        }
                    ],
                    [
                        'attribute' => 'opinion',
                        'format' => 'raw'
                    ],
                    'raiting',
                    [
                        'attribute' => 'status',
                        'content' => function($data) {
                            $tmplstatus = ($data->status == 1) ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-close" style="color:red"></i>';
                            return $tmplstatus;
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{update} &nbsp; {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>