<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-4 no-padding">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'code') ?>
        </div>
        <div class="col-sm-4 no-padding">
            <?= $form->field($model, 'locale') ?>
        </div>

        <?= $form->field($model, 'name') ?>

        <?php // echo $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-info']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/lang/index', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
