<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Lang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Language') : Yii::t('backend', 'Update Language') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-2 no-padding">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'locale')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-7 no-padding">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-danger' : 'btn btn-warning']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
