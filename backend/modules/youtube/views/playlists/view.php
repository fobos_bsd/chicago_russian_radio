<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\YoutubePlaylists */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Youtube Playlists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="youtube-playlists-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Youtube Playlist'), ['create'], ['class' => 'btn btn-info']) ?>
            <?= Html::a(Yii::t('backend', 'Update Playlist`s archive'), ['update-archive', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        </div>
        <div class="box-body">
            <p>
                <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
                <?=
                Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])
                ?>
            </p>

            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'youtube_channels_id',
                    'etag',
                    'playlist_id',
                    'publish_at',
                    'title',
                    'description:ntext',
                    'thumbnails_medium:ntext',
                    'thumbnails_maxres:ntext',
                    'status',
                ],
            ])
            ?>
        </div>
    </div>
</div>