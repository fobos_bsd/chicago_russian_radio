<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

$this->title = Yii::t('backend', 'Youtube Playlists');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="youtube-playlists-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'channels' => $channels]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Youtube Playlist'), ['create'], ['class' => 'btn btn-info']) ?>
            <?= Html::a(Yii::t('backend', 'Update Playlist`s archive'), ['allupdate-archive'], ['class' => 'btn btn-danger']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'label' => 'ID',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'youtube_channels_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->youtubeChannels->name;
                        }
                    ],
                    ['attribute' => 'title',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'playlist_id',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    [
                        'attribute' => 'thumbnails_medium',
                        'format' => 'raw',
                        'content' => function($data) {
                            return Html::img($data->thumbnails_medium, [
                                        'alt' => 'icon',
                                        'style' => 'width:35px;',
                                        'class' => 'img-responsive'
                            ]);
                        }
                    ],
                    ['attribute' => 'priority',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'status',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            if ($data->status == 1) {
                                return '<i class="fa fa-toggle-on" style="color: #00a65a;" aria-hidden="true"></i>';
                            } else {
                                return '<i class="fa fa-toggle-off" style="color: #dd4b39;" aria-hidden="true"></i>';
                            }
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{view} &nbsp; {update} &nbsp; {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye" style="color: #0073b7;" aria-hidden="true"></i>', $url);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ])
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
