<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\YoutubePlaylists */

$this->title = Yii::t('backend', 'Create Youtube Playlists');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Youtube Playlists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="youtube-playlists-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'channels' => $channels,
        'folders' => $folders
])
    ?>

</div>
