<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    pendalf89\filemanager\widgets\FileInput;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Yuotube Playlist') : Yii::t('backend', 'Update Yuotube Playlist') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?=
        $form->field($model, 'youtube_channels_id')->widget(Select2::className(), [
            'data' => $channels,
            'options' => ['multiple' => false, 'placeholder' => 'Select youtube channel ...']
        ])
        ?>

        <?= $form->field($model, 'etag')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'playlist_id')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'publish_at')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'title')->textInput() ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?=
        $form->field($model, 'thumbnails_medium')->widget(FileInput::className(), [
            'buttonTag' => 'button',
            'buttonName' => 'Browse',
            'buttonOptions' => ['class' => 'btn btn-default'],
            'options' => ['class' => 'form-control'],
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'thumb' => 'original',
            'imageContainer' => '.img',
            'pasteData' => FileInput::DATA_URL,
            'callbackBeforeInsert' => 'function(e, data) {
                console.log( data );
            }',
        ])
        ?>

        <?=
        $form->field($model, 'thumbnails_maxres')->widget(FileInput::className(), [
            'buttonTag' => 'button',
            'buttonName' => 'Browse',
            'buttonOptions' => ['class' => 'btn btn-default'],
            'options' => ['class' => 'form-control'],
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'thumb' => 'original',
            'imageContainer' => '.img',
            'pasteData' => FileInput::DATA_URL,
            'callbackBeforeInsert' => 'function(e, data) {
                console.log( data );
            }',
        ])
        ?>

        <div class="col-sm-3 col-lg-2 no-paddingl">
            <?= $form->field($model, 'priority')->input('number', ['min' => 0, 'max' => 1000000, 'step' => 1]) ?>
        </div>

        <div class="col-sm-2 col-lg-1 no-paddingright">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <?= $form->field($model, 'meta_key')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_description')->textarea(['rows' => 3]) ?>

        <?=
        $form->field($model, 'archive_slug')->widget(Select2::className(), [
            'data' => $folders,
            'options' => ['multiple' => false, 'placeholder' => 'Select folder ...']
        ])
        ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-info']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
