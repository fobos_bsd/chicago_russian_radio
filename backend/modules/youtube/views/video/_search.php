<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-6 col-lg-6 no-paddingleft">
            <?=
            $form->field($model, 'youtube_playlists_id')->widget(Select2::className(), [
                'data' => $playlists,
                'options' => ['multiple' => false, 'placeholder' => 'Select youtube playlist ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-6 col-lg-6 no-paddingright">
            <?= $form->field($model, 'video_id') ?>
        </div>

        <?= $form->field($model, 'title') ?>

        <div class="col-sm-3 col-lg-1 no-paddingleft">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-sm-3 col-lg-1 no-paddingright">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <?php // echo $form->field($model, 'description') ?>

        <?php // echo $form->field($model, 'thumbnails_medium') ?>

        <?php // echo $form->field($model, 'thumbnails_maxres')  ?>

        <?php // echo $form->field($model, 'publish_at') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-info']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/youtube/video', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
