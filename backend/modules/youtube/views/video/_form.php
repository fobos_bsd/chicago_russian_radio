<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Yuotube Video') : Yii::t('backend', 'Update Yuotube Video') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
        $form->field($model, 'youtube_playlists_id')->widget(Select2::className(), [
            'data' => $playlists,
            'options' => ['multiple' => false, 'placeholder' => 'Select youtube playlist ...']
        ])
        ?>

        <?= $form->field($model, 'video_id')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'publish_at')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'thumbnails_medium')->textInput() ?>

        <?= $form->field($model, 'thumbnails_maxres')->textInput() ?>

        <div class="col-sm-2 col-lg-1 no-paddingleft">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="col-sm-4 col-lg-3 no-paddingright">
            <?=
            $form->field($model, 'socialcode')->widget(Select2::className(), [
                'data' => [
                    'fb' => 'Facebook',
                    'vk' => 'В контакте',
                    'tw' => 'Twitter',
                    'youtube' => 'Youtube'
                ],
                'options' => ['multiple' => false, 'placeholder' => 'Select type of cocial link']
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-info']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
