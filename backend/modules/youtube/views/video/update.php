<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\YoutubeVideo */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
            'modelClass' => 'Youtube Video',
        ]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Youtube Videos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="youtube-video-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'playlists' => $playlists
    ])
    ?>

</div>
