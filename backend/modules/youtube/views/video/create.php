<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\YoutubeVideo */

$this->title = Yii::t('backend', 'Create Youtube Video');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Youtube Videos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="youtube-video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'playlists' => $playlists
    ])
    ?>

</div>
