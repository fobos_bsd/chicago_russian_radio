<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\YoutubeChannels */

$this->title = Yii::t('backend', 'Create Youtube Channels');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Youtube Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="youtube-channels-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'sites' => $sites
    ])
    ?>

</div>
