<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-6 col-lg-6 no-padding">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-sm-6 col-lg-6 no-paddingright">
            <?= $form->field($model, 'channel_id') ?>
        </div>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-sm-3 col-lg-1 no-paddingright">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <?php // echo $form->field($model, 'slug') ?>

        <?php // echo $form->field($model, 'description') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-info']) ?>
            <?= Html::a(Yii::t('backend', 'Reset'), '/admin/youtube/channels', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
