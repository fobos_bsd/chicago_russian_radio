<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Channel') : Yii::t('backend', 'Update Channel') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'slug')->hiddenInput(['maxlength' => true, 'disabled' => 'disabled'])->label(false) ?>

        <?= $form->field($model, 'channel_id')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <div class="col-sm-4 col-lg-2 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>
        <div class="clearfix"></div>

        <?= $form->field($model, 'meta_key')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_description')->textarea(['rows' => 3]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-info']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
