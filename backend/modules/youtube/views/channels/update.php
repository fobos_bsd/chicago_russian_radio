<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\YoutubeChannels */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
            'modelClass' => 'Youtube Channels',
        ]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Youtube Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="youtube-channels-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'sites' => $sites
    ])
    ?>

</div>
