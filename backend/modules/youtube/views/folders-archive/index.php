<?php

use yii\bootstrap\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PlaylistsFolderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Playlists Folders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="youtube-playlists-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Playlists Folder'), ['create'], ['class' => 'btn btn-info']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'label' => 'ID',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    'link:ntext',
                    ['attribute' => 'status',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            if ($data->status == 1) {
                                return '<i class="fa fa-toggle-on" style="color: #00a65a;" aria-hidden="true"></i>';
                            } else {
                                return '<i class="fa fa-toggle-off" style="color: #dd4b39;" aria-hidden="true"></i>';
                            }
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{view} &nbsp; {update} &nbsp; {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye" style="color: #0073b7;" aria-hidden="true"></i>', $url);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ])
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
