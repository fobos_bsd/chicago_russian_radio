<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PlaylistsFolder */

$this->title = Yii::t('backend', 'Create Playlists Folder');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Playlists Folders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="youtube-channels-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
