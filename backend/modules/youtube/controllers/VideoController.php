<?php

namespace backend\modules\youtube\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper;
use common\models\YoutubeVideo,
    common\models\search\YoutubeVideoSearch,
    common\models\YoutubePlaylists,
    common\models\GoogleYoutube;

/**
 * VideoController implements the CRUD actions for YoutubeVideo model.
 */
class VideoController extends Controller {

    public $playlists, $youtube;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // Get all active playlists ID
        $this->playlists = ArrayHelper::map(YoutubePlaylists::find()->where(['status' => 1])->asArray()->all(), 'id', 'title');

        // Iniialisation youtube model
        $this->youtube = new GoogleYoutube();
    }

    /**
     * Lists all YoutubeVideo models.
     * @return mixed
     */
    public function actionIndex() {
        // Auto apgrade playlists
        $playlists = YoutubePlaylists::find()->where(['status' => 1])->asArray()->all();

        foreach ($playlists as $playlist) {
            $video_data = $this->youtube->getPlaylistsVideo($playlist['playlist_id'], 50);

            /*
              if (isset($video_data['modelData']['pageInfo']['totalResults']) && isset($video_data['modelData']['pageInfo']['resultsPerPage']) && ($video_data['modelData']['pageInfo']['totalResults'] <= $video_data['modelData']['pageInfo']['resultsPerPage'])) {
              $this->youtube->setVideosID($playlist_data['modelData']['items'], 50, $chanel['id']);
              }
             * 
             */

            if (isset($video_data['modelData']['pageInfo']['totalResults']) && isset($video_data['modelData']['pageInfo']['resultsPerPage'])) {
                $this->youtube->setVideosID($video_data['modelData']['items'], 50, $playlist['id']);
            }
        }

        $searchModel = new YoutubeVideoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'playlists' => $this->playlists
        ]);
    }

    /**
     * Displays a single YoutubeVideo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new YoutubeVideo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new YoutubeVideo();
        $model->status = 1;
        $model->publish_at = time();
        $model->socialcode = 'youtube';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'playlists' => $this->playlists
            ]);
        }
    }

    /**
     * Updates an existing YoutubeVideo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'playlists' => $this->playlists
            ]);
        }
    }

    /**
     * Deletes an existing YoutubeVideo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the YoutubeVideo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return YoutubeVideo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = YoutubeVideo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
