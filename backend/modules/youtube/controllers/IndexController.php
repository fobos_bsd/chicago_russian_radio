<?php

namespace backend\modules\youtube\controllers;

use Yii,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\web\Controller;
use common\models\GoogleYoutube,
    common\models\Settings;

class IndexController extends Controller {

    private $youtube;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['Администраторы', 'Radioadmins'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->youtube = new GoogleYoutube();
    }

    public function actionIndex() {
        $chanel = $this->getChannelID();

        $chanels_data = $this->youtube->getChanelPlaylists($chanel, 5);
        var_dump($chanels_data);

        return $this->render('index');
    }

    protected function getChannelID() {
        if (($objkey = Settings::find()->where(['site_id' => Yii::$app->Siteselector->getSitesCookie(), 'name' => 'radio_youtube_channel'])->one()) !== null) {
            return $objkey->value;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
