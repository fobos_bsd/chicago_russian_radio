<?php

namespace backend\modules\youtube\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper,
    linslin\yii2\curl;
use common\models\YoutubePlaylists,
    common\models\search\YoutubePlaylistSearch,
    common\models\YoutubeChannels,
    common\models\GoogleYoutube,
    common\models\PlaylistsFolder,
    common\models\PlaylistFiles;

/**
 * PlaylistsController implements the CRUD actions for YoutubePlaylists model.
 */
class PlaylistsController extends Controller {

    public $channels, $youtube, $folders;

    const UPLOADPATH = 'http://archive.chicago1330.com/archive/программы/';

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'update-archive', 'allupdate-archive'],
                        'roles' => ['Администраторы', 'Radioadmins'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // Get all active channels ID
        $this->channels = ArrayHelper::map(YoutubeChannels::find()->where(['status' => 1])->asArray()->all(), 'id', 'name');

        // Get all playlist`s folders ID
        $this->folders = ArrayHelper::map(PlaylistsFolder::find()->where(['status' => 1])->asArray()->all(), 'link', 'link');

        // Iniialisation youtube model
        $this->youtube = new GoogleYoutube();
    }

    /**
     * Lists all YoutubePlaylists models.
     * @return mixed
     */
    public function actionIndex() {
        // Auto apgrade playlists
        $channels = YoutubeChannels::find()->where(['status' => 1])->asArray()->all();
        foreach ($channels as $chanel) {
            $playlist_data = $this->youtube->getChanelPlaylists($chanel['channel_id'], 50);

            /*
              if (isset($playlist_data['modelData']['pageInfo']['totalResults']) && isset($playlist_data['modelData']['pageInfo']['resultsPerPage']) && ($playlist_data['modelData']['pageInfo']['totalResults'] <= $playlist_data['modelData']['pageInfo']['resultsPerPage'])) {
              $this->youtube->setPlaylistsID($playlist_data['modelData']['items'], 50, $chanel['id']);
              }
             * 
             */

            if (isset($playlist_data['modelData']['pageInfo']['totalResults']) && isset($playlist_data['modelData']['pageInfo']['resultsPerPage'])) {
                $this->youtube->setPlaylistsID($playlist_data['modelData']['items'], 50, $chanel['id']);
            }
        }

        $searchModel = new YoutubePlaylistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'channels' => $this->channels
        ]);
    }

    /**
     * Displays a single YoutubePlaylists model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new YoutubePlaylists model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new YoutubePlaylists();
        $model->status = 1;
        $model->publish_at = time();
        $model->priority = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'channels' => $this->channels,
                        'folders' => $this->folders
            ]);
        }
    }

    /**
     * Updates an existing YoutubePlaylists model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'channels' => $this->channels,
                        'folders' => $this->folders
            ]);
        }
    }

    /**
     * Deletes an existing YoutubePlaylists model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the YoutubePlaylists model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return YoutubePlaylists the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = YoutubePlaylists::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // Get list of program folder
    private function getListFiles($folder_prog) {
        $curl = new curl\Curl();

        $user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0';

        $list_files = $curl->setOptions([CURLOPT_USERAGENT => $user_agent, CURLOPT_HEADER => 0, CURLOPT_FOLLOWLOCATION => 1, CURLOPT_RETURNTRANSFER => 1])->get(self::UPLOADPATH . $folder_prog);
        if ($curl->errorCode === null) {
            return $list_files;
        } else {
            return '';
        }
    }

    // Parse html list files in folder
    private function parserHtml($html_body) {
        $arr_files = [];
        //$this->stdout($html_body . PHP_EOL, Console::FG_GREEN);
        preg_match_all('/\"live.*\.mp3\"/i', $html_body, $matches, PREG_OFFSET_CAPTURE);
        if (isset($matches[0]) && count($matches[0]) > 0) {
            foreach ($matches[0] as $match) {
                array_push($arr_files, str_replace('"', '', $match[0]));
            }
        }
        return $arr_files;
    }

    // Reread program archive
    public function actionUpdateArchive($id) {
        $arr_titles = ArrayHelper::map(YoutubePlaylists::find()->where(['status' => 1, 'id' => $id])->asArray()->all(), 'id', 'archive_slug');
        $begin = new \DateTime("2017-06-09");
        $end = new \DateTime(date('Y-m-d'));

        if (isset($arr_titles) && is_array($arr_titles) && count($arr_titles) > 0) {
            for ($i = $begin; $i <= $end; $i->modify('+1 month')) {
                foreach ($arr_titles as $key => $title) {
                    $folder_prog = mb_strtolower(strip_tags(trim($title)));
                    $curdate = $i->format("Y.m");
                    $arrfiles = $this->parserHtml($this->getListFiles($folder_prog . '/' . $curdate));
                    if (isset($arrfiles) && is_array($arrfiles) && count($arrfiles) > 0) {
                        foreach ($arrfiles as $filename) {
                            $file = self::UPLOADPATH . $folder_prog . '/' . $curdate . '/' . $filename;
                            $arrfile = explode('-', str_replace('.mp3', '', $filename));
                            $afir_time = $arrfile[4] . ':' . $arrfile[5] . ':00';
                            $afir_date = $arrfile[1] . '-' . $arrfile[2] . '-' . $arrfile[3];

                            if (!PlaylistFiles::find()->where(['fileName' => $file])->one()) {

                                $model = new PlaylistFiles();
                                $model->scenario = 'console';
                                $model->name = 'Программа от ';
                                $model->youtube_playlists_id = $key;
                                $model->radio_brodcast_day = $afir_date;
                                $model->radio_brodcast_time = $afir_time;
                                $model->status = 1;
                                $model->long_time = null;
                                $model->fileName = $file;
                                if (!$model->insert()) {
                                    var_dump($model->getErrors());
                                }
                            }
                        }
                    }
                }
            }
        }

        return $this->redirect('/admin/youtube/playlists/view/' . $id);
    }

    // Re read all program`s archives
    public function actionAllupdateArchive() {
        $arr_titles = ArrayHelper::map(YoutubePlaylists::find()->where(['status' => 1])->asArray()->all(), 'id', 'archive_slug');
        $begin = new \DateTime("2017-06-09");
        $end = new \DateTime(date('Y-m-d'));

        if (isset($arr_titles) && is_array($arr_titles) && count($arr_titles) > 0) {
            for ($i = $begin; $i <= $end; $i->modify('+1 month')) {
                foreach ($arr_titles as $key => $title) {
                    $folder_prog = mb_strtolower(strip_tags(trim($title)));
                    $curdate = $i->format("Y.m");
                    $arrfiles = $this->parserHtml($this->getListFiles($folder_prog . '/' . $curdate));
                    if (isset($arrfiles) && is_array($arrfiles) && count($arrfiles) > 0) {
                        foreach ($arrfiles as $filename) {
                            $file = self::UPLOADPATH . $folder_prog . '/' . $curdate . '/' . $filename;
                            $arrfile = explode('-', str_replace('.mp3', '', $filename));
                            $afir_time = $arrfile[4] . ':' . $arrfile[5] . ':00';
                            $afir_date = $arrfile[1] . '-' . $arrfile[2] . '-' . $arrfile[3];

                            if (!PlaylistFiles::find()->where(['fileName' => $file])->one()) {

                                $model = new PlaylistFiles();
                                $model->scenario = 'console';
                                $model->name = 'Программа от ';
                                $model->youtube_playlists_id = $key;
                                $model->radio_brodcast_day = $afir_date;
                                $model->radio_brodcast_time = $afir_time;
                                $model->status = 1;
                                $model->long_time = null;
                                $model->fileName = $file;
                                if (!$model->insert()) {
                                    var_dump($model->getErrors());
                                }
                            }
                        }
                    }
                }
            }
        }

        return $this->redirect('/admin/youtube/playlists');
    }

    // Get number of week day
    protected function getDayOfWeek($number) {
        switch ($number) {
            case 7: $result = 'Su';
                break;
            case 1: $result = 'Mo';
                break;
            case 2: $result = 'Tu';
                break;
            case 3: $result = 'We';
                break;
            case 4: $result = 'Th';
                break;
            case 5: $result = 'Fr';
                break;
            case 6: $result = 'Sa';
                break;
            default: $result = 'Su';
                break;
        }

        return $result;
    }

}
