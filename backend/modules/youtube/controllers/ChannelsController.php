<?php

namespace backend\modules\youtube\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    yii\helpers\ArrayHelper;
use common\models\YoutubeChannels,
    common\models\search\YoutubeChannelSearch,
    common\models\Site;

/**
 * ChannelsController implements the CRUD actions for YoutubeChannels model.
 */
class ChannelsController extends Controller {

    public $sites;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['Администраторы'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->sites = ArrayHelper::map(Site::find()->where(['status' => 1])->asArray()->all(), 'id', 'name');
    }

    /**
     * Lists all YoutubeChannels models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new YoutubeChannelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'sites' => $this->sites
        ]);
    }

    /**
     * Displays a single YoutubeChannels model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new YoutubeChannels model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new YoutubeChannels();
        $model->site_id = Yii::$app->Siteselector->getSitesCookie();
        $model->status = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'sites' => $this->sites
            ]);
        }
    }

    /**
     * Updates an existing YoutubeChannels model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'sites' => $this->sites
            ]);
        }
    }

    /**
     * Deletes an existing YoutubeChannels model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the YoutubeChannels model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return YoutubeChannels the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = YoutubeChannels::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
