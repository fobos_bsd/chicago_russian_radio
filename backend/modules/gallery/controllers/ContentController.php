<?php

namespace backend\modules\gallery\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\helpers\ArrayHelper,
    yii\web\UploadedFile;
use common\models\GalleryData,
    common\models\search\GalleryDataSearch,
    common\models\Gallery,
    backend\modules\gallery\forms\MultiUploads;

/**
 * IndexController implements the CRUD actions for Gallery model.
 */
class ContentController extends Controller {

    public $gallery, $arrplaylist;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->gallery = ArrayHelper::map(Gallery::find()->where(['status' => 1])->asArray()->all(), 'id', 'name');
        $this->arrplaylist = ArrayHelper::map(Gallery::find()->where(['status' => 1])->asArray()->all(), 'id', 'youtube_playlists_id');
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new GalleryDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'gallery' => $this->gallery
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new GalleryData();
        $model->status = 1;
        $model->gravity = 0;

        if ($model->load(Yii::$app->request->post())) {
            $model->fileImage = UploadedFile::getInstance($model, 'fileImage');
            if ($model->save()) {
                return $this->redirect(['index']);
            } else {
                var_dump($model->getErrors());
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'gallery' => $this->gallery,
                        'arrplaylist' => $this->arrplaylist
            ]);
        }
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->fileImage = UploadedFile::getInstance($model, 'fileImage');

            if ($model->save()) {
                return $this->redirect(['index']);
            } else {
                var_dump($model->getErrors());
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'gallery' => $this->gallery,
                        'arrplaylist' => $this->arrplaylist
            ]);
        }
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Creates many images in Gallery.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionMultiUploads() {
        $model = new MultiUploads();

        if ($model->load(Yii::$app->request->post())) {
            $model->fileImages = UploadedFile::getInstances($model, 'fileImages');
            if ($model->save()) {
                return $this->redirect(['index']);
            } else {
                var_dump($model);
            }
        } else {      
            return $this->render('multi-uploads', [
                        'model' => $model,
                        'gallery' => $this->gallery,
                        'arrplaylist' => $this->arrplaylist
            ]);
        }
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = GalleryData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
