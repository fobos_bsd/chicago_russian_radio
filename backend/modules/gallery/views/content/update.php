<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Gallery */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
            'modelClass' => 'Gallery Image',
        ]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Galleries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= Html::encode($this->title) ?></h4>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <?=
                    $this->render('_form', [
                        'model' => $model,
                        'gallery' => $gallery,
                        'arrplaylist' => $arrplaylist
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
