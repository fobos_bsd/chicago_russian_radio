<?php

use yii\bootstrap\Html,
    yii\widgets\ListView,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Gallery Data');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel, 'title' => $this->title, 'gallery' => $gallery]); ?><br />

    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Add one image'), ['create'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Add many images'), ['multi-uploads'], ['class' => 'btn btn-warning']) ?>
        </div>
        <div class="box-body">
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <!-- div.table-responsive -->
            <!-- div.dataTables_borderWrap -->
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_list',
                'options' => [
                    'tag' => 'ul',
                    'class' => 'ace-thumbnails clearfix',
                ],
                'itemOptions' => [
                    'tag' => 'li',
                    'class' => 'gallery-item',
                ],
                'layout' => "{items}\n<div class='summary'>{summary}</div>\n<nav class='nav'>{pager}</nav>",
                'summary' => 'Показано {count} из {totalCount}',
                'summaryOptions' => [
                    'tag' => 'span',
                    'class' => 'summary'
                ],
                'emptyText' => 'Список пуст',
            ]);
            ?>
            <div id="dialog-confirm" class="hide">
                <div class="alert alert-info bigger-110">
                    These items will be permanently deleted and cannot be recovered.
                </div>

                <div class="space-6"></div>

                <p class="bigger-110 bolder center grey">
                    <i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>
                    Are you sure?
                </p>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
<?php
$script = <<< JS
    var overflow = '';
        var colorbox_params = {
            rel: 'colorbox',
            reposition: true,
            scalePhotos: true,
            scrolling: false,
            previous: '<i class="ace-icon fa fa-arrow-left"></i>',
            next: '<i class="ace-icon fa fa-arrow-right"></i>',
            close: '&times;',
            current: '{current} of {total}',
            maxWidth: '100%',
            maxHeight: '100%',
            onOpen: function () {
                overflow = document.body.style.overflow;
                document.body.style.overflow = 'hidden';
            },
            onClosed: function () {
                document.body.style.overflow = overflow;
            },
            onComplete: function () {
                $.colorbox.resize();
            }
        };

        $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
        $("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");

        $(document).one('ajaxloadstart.page', function (e) {
            $('#colorbox, #cboxOverlay').remove();
        });
JS;

$this->registerJs($script, yii\web\View::POS_END);
?>

