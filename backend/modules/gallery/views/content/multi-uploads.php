<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Gallery */

$this->title = Yii::t('backend', 'Add many images');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Galleries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="box box-primary">
                        <div class="box-body">
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                            <div class="col-sm-4 col-lg-3 no-paddingleft">
                                <?=
                                $form->field($model, 'gallery_id')->widget(Select2::className(), [
                                    'data' => $gallery,
                                    'language' => 'ru',
                                    'options' => ['placeholder' => 'Select a language ...'],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ])
                                ?>
                                <div id="playlists_id" class="hidden">
                                    <?php if (isset($arrplaylist) && count($arrplaylist) > 0) { ?>
                                        <ul>
                                            <?php foreach ($arrplaylist as $key => $val) { ?>
                                                <li id="item-<?= $key ?>"><?= $val ?></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-sm-8 col-lg-9 no-paddingright">
                                <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="clearfix"></div>

                            <?=
                            $form->field($model, 'fileImages[]')->widget(FileInput::classname(), [
                                'language' => 'ru',
                                'options' => [
                                    'multiple' => true,
                                    'accept' => 'image/*'
                                ],
                                'pluginOptions' => [
                                    'showPreview' => true,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                    'previewFileType' => 'image',
                                    'browseClass' => 'btn btn-info btn-sm',
                                    'removeClass' => 'btn btn-danger btn-sm',
                                    'maxFileSize' => 5000,
                                    'maxFileCount' => 10,
                                    'overwriteInitial' => true
                                ],
                            ])
                            ?>

                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('backend', 'Add'), ['class' => 'btn btn-primary']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS
    $("#multiuploads-gallery_id").on('change', function(){
        var pID = $("#playlists_id").find("ul li#item-" + $(this).val()).text();
        $("#multiuploads-link").val('/programmy/' + pID);
    });
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_END);
?>