<?php

use yii\bootstrap\Html;
use kartik\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\search\GallerySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-padding">
            <?= $form->field($model, 'id')->input('number', ['min' => 1, 'max' => 16777215]) ?>
        </div>
        <div class="col-sm-3 col-lg-3">
            <?=
            $form->field($model, 'gallery_id')->widget(Select2::className(), [
                'data' => $gallery,
                'language' => 'ru',
                'options' => ['placeholder' => 'Select a gallery ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-3 col-lg-6 no-padding-left">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-sm-2 col-lg-1 no-padding-left">
            <?= $form->field($model, 'gravity')->input('number', ['max' => 255, 'min' => 0, 'step' => 1]) ?>
        </div>
        <div class="col-sm-2 col-lg-1 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'pluginOptions' => [
                    'onColor' => 'success',
                    'offColor' => 'danger',
                    'onText' => 'Вкл',
                    'offText' => 'Выкл',
                ]
            ])
            ?>
        </div>

        <hr />

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
