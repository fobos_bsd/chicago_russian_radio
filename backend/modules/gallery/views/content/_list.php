<?php

use yii\bootstrap\Html;
use yii\helpers\HtmlPurifier;
?>

<?= Html::a(Html::img('/backend/web/uploads/gallery/' . $model->fileName, ['width' => "150", 'height' => "150", 'alt' => $model->name]) . '<div class="text"><div class="inner">' . $model->title . '</div></div>', '/backend/web/uploads/gallery/' . $model->fileName, ['data-rel' => "colorbox"]) ?>
<div class="tools tools-bottom">
    <?= Html::a('<i class="ace-icon fa fa-pencil"></i>', ['update', 'id' => $model->id]) ?>
    <?=
    Html::a('<i class="ace-icon fa fa-times red"></i>', ['delete', 'id' => $model->id], [
        'data' => [
            'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ])
    ?>
</div>   

