<?php

use yii\bootstrap\Html,
    yii\helpers\Url,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput,
    kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Gallery') : Yii::t('backend', 'Update Gallery') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="col-sm-4 col-lg-3 no-padding">
            <?=
            $form->field($model, 'gallery_id')->widget(Select2::className(), [
                'data' => $gallery,
                'language' => 'ru',
                'options' => ['placeholder' => 'Select a language ...'],
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ])
            ?>
            <div id="playlists_id" class="hidden">
                <?php if (isset($arrplaylist) && count($arrplaylist) > 0) { ?>
                    <ul>
                        <?php foreach ($arrplaylist as $key => $val) { ?>
                            <li id="item-<?= $key ?>"><?= $val ?></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
        <div class="col-sm-offset-1 col-lg-offset-5 col-sm-4 col-lg-1 no-paddingleft">
            <?= $form->field($model, 'gravity')->input('number', ['max' => 255, 'min' => 0, 'step' => 1]) ?>
        </div>
        <div class="col-sm-4 col-lg-2 no-padding">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'pluginOptions' => [
                    'onColor' => 'success',
                    'offColor' => 'danger',
                    'onText' => 'Вкл',
                    'offText' => 'Выкл',
                ]
            ])
            ?>
        </div>
        <div class="clearfix"></div>

        <div class="col-sm-6 no-paddingleft">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6 no-paddingright">
            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
        </div>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?=
        $form->field($model, 'fileImage')->widget(FileInput::classname(), [
            'language' => 'ru',
            'options' => [
                'multiple' => false
            ],
            'pluginOptions' => [
                'showPreview' => true,
                'showCaption' => true,
                'showRemove' => true,
                'showUpload' => false,
                'previewFileType' => 'image',
                'browseClass' => 'btn btn-info btn-sm',
                'removeClass' => 'btn btn-danger btn-sm',
                'maxFileSize' => 5000,
                'initialPreview' => [
                    Html::img('/backend/web/uploads/gallery/' . $model->fileName, ['class' => 'img-responsive'])
                ],
                'overwriteInitial' => true
            ],
        ])
        ?>

        <?= $form->field($model, 'fileName')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$script = <<< JS
    $("#gallerydata-gallery_id").on('change', function(){
        var pID = $("#playlists_id").find("ul li#item-" + $(this).val()).text();
        $("#gallerydata-link").val('/programmy/' + pID);
    });
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_END);
?>