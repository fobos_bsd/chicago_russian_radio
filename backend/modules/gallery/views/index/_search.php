<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Filter') ?></h3>
    </div>
    <div class="box-body">

        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                    'options' => [
                        'data-pjax' => 1
                    ],
        ]);
        ?>

        <div class="col-sm-2 col-lg-1 no-paddingleft">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-sm-7 col-lg-6 no-padding">
            <?=
            $form->field($model, 'parent_id')->widget(Select2::className(), [
                'data' => $galleries,
                'language' => 'ru',
                'options' => ['placeholder' => 'Select a parent gallery ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-sm-3 col-lg-2 col-lg-offset-3 no-paddingright">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>
        <div class="clearfix"></div>
        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="col-sm-6 col-lg-6 no-paddingleft">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-sm-6 col-lg-6 no-paddingright">
            <?= $form->field($model, 'slug') ?>
        </div>

        <?php // echo $form->field($model, 'title')  ?>

        <?php // echo $form->field($model, 'keywords')  ?>

        <?php // echo $form->field($model, 'meta_description')  ?>

        <?php // echo $form->field($model, 'description')  ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
