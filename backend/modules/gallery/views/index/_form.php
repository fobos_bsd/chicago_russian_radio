<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\Select2,
    kartik\widgets\SwitchInput;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Gallery') : Yii::t('backend', 'Update Gallery') ?></h3>
    </div>
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <?=
        $form->field($model, 'parent_id')->widget(Select2::className(), [
            'data' => $galleries,
            'language' => 'ru',
            'options' => ['placeholder' => 'Select a parent gallery ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ?>

        <?=
        $form->field($model, 'youtube_playlists_id')->widget(Select2::className(), [
            'data' => $playlists,
            'language' => 'ru',
            'options' => ['placeholder' => 'Select a program ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ?>

        <div class="col-sm-6 col-lg-6 no-paddingleft">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6 col-lg-6 no-paddingright">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>

        <?= $form->field($model, 'keywords')->textarea(['rows' => 2]) ?>

        <?= $form->field($model, 'meta_description')->textarea(['rows' => 4]) ?>

        <?=
        $form->field($model, 'description')->widget(Redactor::className(), [
            'clientOptions' => [
                'options' => [
                    'style' => 'height: 300px;',
                ],
                'convertDivs' => false,
                'replaceDivs' => false,
                'removeEmptyTags' => false,
                'paragraphize' => false,
                'pastePlainText' => true,
                'autoresize' => true,
                'convertVideoLinks' => true,
                //'buttons' => ['format', 'bold', 'italic', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule', 'underline', 'ol', 'ul', 'indent', 'outdent'],
                'plugins' => ['clips', 'fontcolor', 'imagemanager', 'advanced']
            ]
        ])
        ?>
        <div class="col-sm-3 col-lg-1 col-lg-offset-11 col-sm-offset-9 no-paddingright">
            <?=
            $form->field($model, 'status')->widget(SwitchInput::className(), [
                'type' => SwitchInput::CHECKBOX
            ])
            ?>
        </div>


        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'slug')->hiddenInput(['maxlength' => true, 'disabled' => 'disabled'])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

