<?php

namespace backend\modules\gallery\forms;

use Yii,
    yii\base\Model;
use common\models\GalleryData,
    common\models\Gallery;

class MultiUploads extends Model {

    public $gallery_id, $fileImages, $link;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['gallery_id', 'fileImages'], 'required'],
            [['gallery_id'], 'integer'],
            [['link'], 'string'],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
            [['fileImages'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif', 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'gallery_id' => Yii::t('common', 'Gallery ID'),
            'fileNames' => Yii::t('common', 'Files'),
            'link' => Yii::t('common', 'Link'),
        ];
    }

    // Save images
    public function save() {
        if ($this->validate()) {
            foreach ($this->fileImages as $file) {
                $model = new GalleryData();
                $model->gallery_id = $this->gallery_id;
                $model->name = $file->baseName;
                $model->status = 1;
                $model->gravity = 0;
                $model->title = $file->baseName;
                $model->link = $this->link;
                $model->fileName = $this->upload($file, $this->gallery_id);
                $model->save();
            }
            return true;
        } else {
            return $this->getErrors();
        }
    }

    // Save image to folder
    public function upload($image, $gallery_id) {
        $filename = $image->baseName . '.' . $image->extension;
        $folder =  Yii::getAlias('@backend') . '/web/uploads/gallery';

        // Is folder not exist, than create folder
        if (!file_exists($folder))
            mkdir($folder, 0777, true);
        $path = $folder . '/' . $filename;
        $image->saveAs($path);
        return $filename;
    }

}
