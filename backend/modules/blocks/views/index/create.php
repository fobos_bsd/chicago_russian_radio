<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustomBlocks */

$this->title = Yii::t('backend', 'Create Custom Blocks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Custom Blocks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-blocks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'langs' => $langs,
    ])
    ?>

</div>
