<?php

use yii\bootstrap\Html,
    yii\widgets\ActiveForm,
    yii\redactor\widgets\Redactor;
use kartik\widgets\Select2;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $model->isNewRecord ? Yii::t('backend', 'Create Block') : Yii::t('backend', 'Update Block') ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-4 col-lg-3 no-paddingleft">
            <?=
            $form->field($model, 'lang_id')->widget(Select2::className(), [
                'data' => $langs,
                'options' => ['multiple' => false, 'placeholder' => 'Select language ...']
            ])
            ?>
        </div>
        <div class="col-sm-8 col-lg-9 no-paddingright">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-12 no-padding">
            <?=
            $form->field($model, 'content')->widget(Redactor::className(), [
                'clientOptions' => [
                    'options' => [
                        'style' => 'height: 300px;',
                    ],
                    'convertDivs' => false,
                    'replaceDivs' => false,
                    'removeEmptyTags' => false,
                    'paragraphize' => false,
                    'pastePlainText' => true,
                    'autoresize' => true,
                    'convertVideoLinks' => true,
                    //'buttons' => ['format', 'bold', 'italic', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule', 'underline', 'ol', 'ul', 'indent', 'outdent'],
                    'plugins' => ['clips', 'fontcolor', 'imagemanager', 'advanced']
                ]
            ])
            ?>
        </div>

        <?= $form->field($model, 'site_id')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-info']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
