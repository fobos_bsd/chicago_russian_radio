<?php

use yii\bootstrap\Html,
    yii\helpers\HtmlPurifier,
    yii\grid\GridView,
    yii\widgets\Pjax;

$this->title = Yii::t('backend', 'Custom Blocks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-blocks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'langs' => $langs,]); ?>
    <div class="box">
        <div class="box-header">
            <?= Html::a(Yii::t('backend', 'Create Custom Blocks'), ['create'], ['class' => 'btn btn-info']) ?>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable',
                    'role' => "grid"
                ],
                'columns' => [
                    ['attribute' => 'id',
                        'label' => 'ID',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'lang_id',
                        'label' => 'Language',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->lang->name;
                        }
                    ],
                    ['attribute' => 'name',
                        'headerOptions' => ['class' => 'sorting'],
                    ],
                    ['attribute' => 'site_id',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return $data->site->name;
                        }
                    ],
                    ['attribute' => 'content',
                        'headerOptions' => ['class' => 'sorting'],
                        'content' => function($data) {
                            return substr(htmlspecialchars($data->content), 0, 250) . '...';
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '40'],
                        'template' => '{view} &nbsp; {update} &nbsp; {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye" style="color: #0073b7;" aria-hidden="true"></i>', $url);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit" style="color: #f39c12;" aria-hidden="true"></i>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>', $url, [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method' => 'post',
                                            ]
                                ]);
                            },
                        ],
                    ],
                ],
            ])
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
