<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'homeUrl' => '/admin',
    'language' => 'ru-RU',
    'components' => [
        'request' => [
            'baseUrl' => '/admin',
            'enableCsrfValidation' => true,
            'csrfParam' => '_csrf-backend',
        ],
        /*
          'user' => [
          'identityClass' => 'common\models\User',
          'enableAutoLogin' => true,
          'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
          ],
          'session' => [
          // this is the name of the session cookie used for login on the backend
          'name' => 'advanced-backend',
          ],
         * 
         */
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'start/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'start/index',
                '<action>' => 'site/<action>',
                '<controller>' => '<controller>/home',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
                '<module:[\w-]+>/<folder:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<folder>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
            ]
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:m/d/Y',
            'datetimeFormat' => 'php:m/d/Y H:i:s',
            'timeFormat' => 'php:H:i',
            'defaultTimeZone' => 'America/Chicago',
            'timeZone' => 'America/Chicago',
        ],
        'Siteselector' => 'backend\components\Siteselector',
        'userAccess' => 'backend\components\access\UserAccess',
    ],
    'modules' => [
        'user' => [
            'as backend' => 'dektrium\user\filters\BackendFilter',
        ],
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@frontend/web/uploads/editor',
            'uploadUrl' => '/frontend/web/uploads/editor',
            'imageAllowExtensions' => ['jpg', 'png', 'gif'],
        ],
        'lang' => [
            'class' => 'backend\modules\lang\Module',
        ],
        'site' => [
            'class' => 'backend\modules\site\Module',
        ],
        'pages' => [
            'class' => 'backend\modules\pages\Module',
        ],
        'settings' => [
            'class' => 'backend\modules\settings\Module',
        ],
        'questions' => [
            'class' => 'backend\modules\questions\Module',
        ],
        'menu' => [
            'class' => 'backend\modules\menu\Module',
        ],
        'banner' => [
            'class' => 'backend\modules\banner\Module',
        ],
        'youtube' => [
            'class' => 'backend\modules\youtube\Module',
        ],
        'timetable' => [
            'class' => 'backend\modules\timetable\Module',
        ],
        'vote' => [
            'class' => 'backend\modules\vote\Module',
        ],
        'soundcloud' => [
            'class' => 'backend\modules\soundcloud\Module',
        ],
        'blocks' => [
            'class' => 'backend\modules\blocks\Module',
        ],
        'contacts' => [
            'class' => 'backend\modules\contacts\Module',
        ],
        'reclams' => [
            'class' => 'backend\modules\reclams\Module',
        ],
        'sitemap' => [
            'class' => 'backend\modules\sitemap\Module',
        ],
        'different' => [
            'class' => 'backend\modules\different\Module',
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module'
        ],
        'google' => [
            'class' => 'backend\modules\google\Module',
        ],
        'onlinevotes' => [
            'class' => 'backend\modules\onlinevotes\Module',
        ],
        'advertising' => [
            'class' => 'backend\modules\advertising\Module',
        ],
        'gallery' => [
            'class' => 'backend\modules\gallery\Module',
        ],
        'playlist_file' => [
            'class' => 'backend\modules\playlist_file\Module',
        ],
        'opinions' => [
            'class' => 'backend\modules\opinions\Module',
        ],
        'tags' => [
            'class' => 'backend\modules\tags\Module',
        ],
        'seo_content' => [
            'class' => 'backend\modules\seo_content\Module',
        ],
        'top_video' => [
            'class' => 'backend\modules\top_video\Module',
        ],
        'radioquiz' => [
            'class' => 'backend\modules\radioquiz\Module',
        ],
        'songs' => [
            'class' => 'backend\modules\songs\Module',
        ],
    ],
    'bootstrap' => [
        'Siteselector',
    ],
    'params' => $params,
];
